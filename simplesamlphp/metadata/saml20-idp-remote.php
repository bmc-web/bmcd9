<?php

/**
 * @file
 * SAML 2.0 remote IdP metadata for SimpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-remote
 */

$metadata['http://adfs-farm.brynmawr.edu/adfs/services/trust'] = [
  'entityid' => 'http://adfs-farm.brynmawr.edu/adfs/services/trust',
  'contacts' => [
    [
      'contactType' => 'support',
    ],
  ],
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => [
    [
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://adfs-farm.brynmawr.edu/adfs/ls/',
    ],
    [
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://adfs-farm.brynmawr.edu/adfs/ls/',
    ],
  ],
  'ArtifactResolutionService' => [],
  'NameIDFormats' => [
    'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
    'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
    'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
  ],
  'keys' => [
    [
      'encryption' => true,
      'signing' => false,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIC7jCCAdagAwIBAgIQGP4tVIm1zb9PEBeayOTrfTANBgkqhkiG9w0BAQsFADAzMTEwLwYDVQQDEyhBREZTIEVuY3J5cHRpb24gLSBhZGZzLWZhcm0uYnJ5bm1hd3IuZWR1MB4XDTI0MDYxNjE1NDE0OVoXDTI1MDYxNjE1NDE0OVowMzExMC8GA1UEAxMoQURGUyBFbmNyeXB0aW9uIC0gYWRmcy1mYXJtLmJyeW5tYXdyLmVkdTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKnknwEwAWLFqBfKHEQVuHBNUA+iqjZamQCoHhLVlMC8i25kXFl1ZKefuKSN2OzbCPKIuj2rQGR4KaFYY+Dnqs6km6iBftBnHuPKkt1dOvILRVFnfrTtNAKSH6dGcTzd+Wm2/1tiCJBOk/5VMeUD+M3C0tTjXpiIjd9FDYrBqc2uIyM0NIdR389UR8cHg5O+ifFpt/Y/v9HiRDCDBhocBuhNZztzSmUpP4dbJenm4atL4hFmFn272Aa7qxgYte600P01YYttepoawNBi+7vDMKprje5VTQD1I9/L19uizL4gizZxZEIji9cxWlBBtzRvnKfaqDiG8mOONEpESGIjt7kCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAPJQUn9ndZtvLjUjMqeCHOOubHC4grmnREd4aEP0zZfUMol6LfzsUzPXudHjHdXKiJfxkmAv+HD0WR2/hTXZPn+if77DW8YXlfYlPwpXAlXOYY0/yRkWEEuTte+6LWvCYo0ffXk/zMr0+WF6YyMOta+6AAXUmaVGgh5o6Puax0xJo+KWBcwOLA6ak7gJ5jLM0Yg3HCrMwYMQoT6+oJJVZatPqvP9U35o27N3nunzO2muVyzHy6l6l6IRQ5FCm9+DOp5JpTWay73zBtqLK+B5lytInaqqRuvRq1uDCV5AQRyJuCK9c1zmtfKx/iJA5z2Z/go/mNPUinY4SnG9ijDNDVw==',
    ],
    [
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIC6DCCAdCgAwIBAgIQN2hRHa/Q+7tEoeQe0/SMIDANBgkqhkiG9w0BAQsFADAwMS4wLAYDVQQDEyVBREZTIFNpZ25pbmcgLSBhZGZzLWZhcm0uYnJ5bm1hd3IuZWR1MB4XDTI0MDYxNjE1NDE1MVoXDTI1MDYxNjE1NDE1MVowMDEuMCwGA1UEAxMlQURGUyBTaWduaW5nIC0gYWRmcy1mYXJtLmJyeW5tYXdyLmVkdTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANQV1D0QgyducGnrZQLs9UOhG1SCiirdTQVm+q7DOhXNS6VoiNtY/ulloGKPscqy8NnyW7PxeNSKU1KY+VTKuRB3lF46hvxZLEeaXsZ66Yf/bcQKI1Q75inF9U8CG+vzDBQis09vMcn+wlGnOl/D7D5PnggqaWksN6+2xDEmnT4cwItCYiKp4jIdnTsW8+SiLF4j13aExJ7oh0Pwr4W2l79orkzzDN3f4cijXshvY23ZMTGmSk641TpTJjAotJhIj7XMdn8zesrlYRHYEs9sOLoS6bGb8oD5Vmdb3MhPK4n0+wOPuiLZ2rW5rXeb8Lkzq4e/prCRurXwsqIN9YB5ZMUCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEACBijBH4p3nBNf8l7T+QXH23LMDqe35GjBxxpFTO2z7oYPmP0X8WZRWLr23KsQwbzF5D1/26kwOpX8p5ePpMK160el9kSW/wid8cBT3QscRpn75nbiEQ84JCQINbBboRxv8jX2lrHy6RdWHOJeP4lnvxpgBuB5lSrSlxp1HePEtvvxxsDsoN7kQ/Pybko0KNpf0VqHa0gliAWmri1mzkz7/4AfXHs0YzuVfus9PQZgyun1VuMgmxrk0gklvtK7CP29p9vLZiKTqeY7NUAbz/xoXfvbdWAxEF5LaVq5S/9fuNrE385ctSVaQbMXx5dVTNF81ux8/TWu70n5vB8T+gm4g==',
   ],
  ],
];

