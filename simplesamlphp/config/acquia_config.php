<?php

/**
 * @file
 * SimpleSamlPhp Acquia Configuration.
 *
 * This file was last modified on in July 2018.
 *
 * All custom changes below. Modify as needed.
 */

/**
 * Defines Acquia account specific options in $config keys.
 *
 *   - 'store.sql.name': Defines the Acquia Cloud database name which
 *     will store SAML session information.
 *   - 'store.type: Define the session storage service to use in each
 *     Acquia environment ("defualts to sql").
 */

// Set some security and other configs that are set above, however we
// overwrite them here to keep all changes in one area.
$config['technicalcontact_name'] = "Chelsea Lobdell";
$config['technicalcontact_email'] = "clobdell@brynmawr.edu";

/**
 * Support SSL Redirects to SAML login pages.
 *
 * Uncomment the code following code block to set
 * server port to 443 on HTTPS environment.
 *
 * This is a requirement in SimpleSAML when providing a redirect path.
 *
 * @link https://github.com/simplesamlphp/simplesamlphp/issues/450
 */
/* $_SERVER['SERVER_PORT'] = 443;
$_SERVER['HTTPS'] = 'true';
$protocol = 'https://';
$port = ':' . $_SERVER['SERVER_PORT'];*/

/**
 * Cookies No Cache.
 *
 * Allow users to be automatically logged in if they signed in via the same
 * SAML provider on another site by uncommenting the setcookie line below.
 *
 * Warning: This has performance implications for anonymous users.
 *
 * @link https://docs.acquia.com/resource/using-simplesamlphp-acquia-cloud-site
 */
// setcookie('NO_CACHE', '1');.
/**
 * Generate Acquia session storage via hosting creds.json.
 *
 * Session sorage defaults using the database for the current request.
 *
 * @link https://docs.acquia.com/resource/using-simplesamlphp-acquia-cloud-site/#storing-session-information-using-the-acquia-cloud-sql-database
 */

/**
 * Support multi-site and single site installations at different base URLs.
 *
 * Overide $config['baseurlpath'] = "https://{yourdomain}/simplesaml/"
 * to customize the default Acquia configuration.
 */
// $config['baseurlpath'] = $protocol . $_SERVER['HTTP_HOST'] . $port . '/simplesaml/';
// Set ACE and ACSF sites based on hosting database and site name.
$config['certdir'] = "/var/www/html/{$_ENV['AH_SITE_GROUP']}.{$_ENV['AH_SITE_ENVIRONMENT']}/vendor/simplesamlphp/simplesamlphp/cert/";
$config['metadatadir'] = "/var/www/html/{$_ENV['AH_SITE_GROUP']}.{$_ENV['AH_SITE_ENVIRONMENT']}/vendor/simplesamlphp/simplesamlphp/metadata";
$config['baseurlpath'] = 'https://' . $_SERVER['HTTP_HOST'] . '/simplesaml/';
// Setup basic file based logging.
$config['logging.handler'] = 'file';
// on Acquia Cloud Next, the preferred location is /shared/logs
// on Acquia Cloud Classic, the preferred location is the same directory as ACQUIA_HOSTING_DRUPAL_LOG
$config['loggingdir'] = (file_exists('/shared/logs/'))?'/shared/logs/':dirname(getenv('ACQUIA_HOSTING_DRUPAL_LOG'));
$config['logging.logfile'] = 'simplesamlphp-' . date('Ymd') . '.log';
$creds_json = file_get_contents('/var/www/site-php/' . $_ENV['AH_SITE_GROUP'] . '.' . $_ENV['AH_SITE_ENVIRONMENT'] . '/creds.json');
$databases = json_decode($creds_json, TRUE);
$creds = $databases['databases'][$_ENV['AH_SITE_GROUP']];
$config['store.type'] = 'sql';
$config['store.sql.dsn'] = sprintf('mysql:host=%s;port=%s;dbname=%s', $creds['host'], $creds['port'], $creds['name']);
$config['store.sql.username'] = $creds['user'];
$config['store.sql.password'] = $creds['pass'];

if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
  $_SERVER['SERVER_PORT'] = 443;
  $_SERVER['HTTPS'] = 'true';
}
