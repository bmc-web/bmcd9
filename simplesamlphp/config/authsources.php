<?php

/**
 * @file
 */

$config = [
  // This is an authentication source which handles admin authentication.
  'admin' => [
    // The default is to use core:AdminPassword, but it can be replaced with
    // any authentication source.
    'core:AdminPassword',
  ],

  // An authentication source which can authenticate against both SAML 2.0
  // and Shibboleth 1.3 IdPs.
  'default-sp' => [
    'saml:SP',

    // The entity ID of this SP.
    // Can be NULL/unset, in which case an entity ID
    // is generated based on the metadata URL.
    'entityID' => 'https://www.brynmawr.edu',

    // The entity ID of the IdP this SP should contact.
    // Can be NULL/unset, in which case the user will be shown
    // a list of available IdPs.
    'idp' => 'http://adfs-farm.brynmawr.edu/adfs/services/trust',

    // The URL to the discovery service.
    // Can be NULL/unset, in which case a builtin
    // discovery service will be used.
    'discoURL' => NULL,
    'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
    'NameIDPolicy' => [],
    'simplesaml.nameidattribute' => 'eduPersonTargetedID',
  ],
];

if (!empty($_ENV['AH_SITE_ENVIRONMENT'])) {
  switch ($_ENV['AH_SITE_ENVIRONMENT']) {
    case 'dev':
      $config['default-sp']['entityID'] = 'https://www-dev.brynmawr.edu';
      break;

    case 'test':
      $config['default-sp']['entityID'] = 'https://www-test.brynmawr.edu';
      break;

    case 'prod':
      $config['default-sp']['entityID'] = 'https://www.brynmawr.edu';
      break;
  }
}
