<?php

namespace Drupal\bmc_peoplesoft\Commands;

use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class BmcPeoplesoftCommands extends DrushCommands {

  /**
   * @command bmc-peoplesoft:update-departments
   * @aliases update-departments
   */
  public function updateDepartments() {
    bmc_peoplesoft_update_departments_vocabulary();
  }

  /**
   * @command bmc-peoplesoft:update-directory-nodes
   * @aliases update-directory-nodes, update-directory
   */
  public function updateDirectoryNodes() {
    bmc_peoplesoft_update_directory_nodes();
  }

}
