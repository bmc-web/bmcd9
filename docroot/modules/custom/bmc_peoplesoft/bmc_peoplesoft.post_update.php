<?php

/**
 * Implements hook_post_update_NAME
 */
function bmc_peoplesoft_post_update_www_1269(&$sandbox) {
  // As part of the resolution to WWW-1269,
  // we need to make sure that all subdepartment codes have the prefix "SUB"
  $term_storage = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term');

  if (!isset($sandbox['progress'])) {
    $sandbox['progress'] = 0;
    $sandbox['max'] = count($term_storage->getQuery()->accessCheck()->condition('vid', 'dept')->execute());
  }

  $limit = 5;
  $tids = $term_storage->getQuery()
    ->accessCheck()
    ->condition('vid', 'dept')
    ->sort('tid', 'asc')
    ->range($sandbox['progress'], $limit)
    ->execute();

  foreach ($tids as $tid) {
    if (count($term_storage->loadParents($tid)) > 0) {
      $term = $term_storage->load($tid);
      $term->set('field_t_dept_code', 'SUB_' . $term->field_t_dept_code->getValue()[0]['value']);
      $term->save();
    }
    // Update our progress information.
    $sandbox['progress']++;
  }
  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($sandbox['progress'] != $sandbox['max']) {
    $sandbox['#finished'] = $sandbox['progress'] / $sandbox['max'];
  }
  return t('Department/Subdepartment update in progress');
}

/**
 * Implements hook_post_update_NAME
 *
 * Remove all directory revisions
 */
function bmc_peoplesoft_post_update_www_1293(&$sandbox) {
  $storage = $entity = \Drupal::entityTypeManager()->getStorage('node');
  $entities = $storage->loadByProperties(['type' => 'directory']);
  foreach ($entities as $entity) {
    $revisionIds = $storage->revisionIds($entity);
    foreach ($revisionIds as $id) {
      if ($id !== $entity->getLoadedRevisionId()) {
        $storage->deleteRevision($id);
      }
    }
  }
}

/**
 * Implements hook_post_update_NAME
 */
function bmc_peoplesoft_post_update_www_1410(&$sandbox) {
  $storage = $entity = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
  $tids = $storage->getQuery()->accessCheck()->condition('field_t_dept_code', 'SUB_', 'STARTS_WITH')->execute();
  $terms = $storage->loadMultiple($tids);
  foreach ($terms as $tid =>$term) {
    $parents = $storage->loadParents($tid);
    if (count($parents) > 0) {
      $parent = reset($parents);
      $parent_code = $parent->field_t_dept_code[0]->value;
      $subdepartment_code = $term->field_t_dept_code[0]->value;
      $subdepartment_code_pieces = explode('_', $subdepartment_code);
      $new_subdepartment_code = implode('_', [$parent_code, $subdepartment_code_pieces[1]]);
      $term->field_t_dept_code = $new_subdepartment_code;
      $term->save();
    }
  }
}


