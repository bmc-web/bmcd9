<?php

namespace Drupal\trico_course_guide\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Render\Markup;

/**
 * Class TriCoCourseGuideController.
 */
class TriCoCourseGuideController extends ControllerBase {

  /**
   * Utility service.
   */
  protected $utility;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('trico_course_guide.utility'));
  }

  /**
   * Class constructor.
   */
  public function __construct($utility) {
    $this->utility = $utility;
  }

  /**
   * Show Course Results
   */
  public function resultsDisplay() {

    $params = \Drupal::request()->query->all();
    $results = $this->utility->getCourseResults($params);

    // num_pages and page may be empty.
    //$results['num_pages'] = $data['num_pages'];
    //$results['num_results'] = $data['num_results'];
    //$results['page'] = $data['page'];

    //build our listing string
    if ($results['num_results'] == 0) {
      $start = 0;
      $end = 0;
    }
    else {
      //$results['page'] = ($results['page'] == 1) ? 1 : $results['page'];
      //$results['num_pages'] = isset($results['num_pages']) ? 1 : $results['num_pages'];
      $start = ($results['page'] == 1 || $results['num_results'] < 50) ? 1 : ($results['page'] - 1) * 50 + 1;
      $end = ($start + 49) <= $results['num_results'] ? $start + 49 : $results['num_results'];
    }

    $result_numbers['total'] = $results['num_results'];
    $result_numbers['start'] = $start;
    $result_numbers['end'] = $end;

    $next_page_url = '';
    if ($params['page'] < $results['num_pages']) {
      $next_params = $params;
      $next_params['page']++;
      $next_page_url = Url::fromRoute('trico_course_guide.course_search_results', [$next_params])->toString();
    }
    $prev_page_url = '';
    if ($params['page'] > 1) {
      $prev_params = $params;
      $prev_params['page']--;
      $prev_page_url = Url::fromRoute('trico_course_guide.course_search_results', [$prev_params])->toString();
    }

    $sorted_results = $this->sortCourses($results['data']);

    $course_results = [];
    foreach ($sorted_results as $key => $result) {

      $header = [
        'Reg' => t('Registration-ID'),
        'Cn' => t('Course Name'),
        'In' => t('Instructor'),
        'misc' => t('Misc'),
        'day' => t('Days and Times'),
        'loc' => t('Location'),
      ];

      $course_result = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => [],
        '#attributes' => ['class' => ['result-table']],
        '#prefix' => "<div class=\"table\"><div class=\"table__inner\">",
        '#suffix' => "</div></div>",
      ];

      foreach ($result['results'] as $course) {
        // Custom parsing.
        $course_link = $this->getCourseLink($course);
        $instructors = $this->getInstructors($course['instructors']);
        $times = $this->getDaysTimes($course['times']);
        $location_mode = '';
        $mode = '';
        if ($course['college'] == 'bryn_mawr') {
          $mode = $this->getMode($course);
        }
        if (isset($course['location']) && !empty(trim($course['location']))) {
          if (strlen($mode) > 0) {
            $location_mode = $course['location'] . '<br><strong>' . $mode . '</strong>';
          } else {
            $location_mode = $course['location'];
          }
        }
        else if (strlen($mode) > 0) {
          $location_mode = '<strong>' . $mode . '</strong>';
        }

        $course_result['#rows'][] = [
          Markup::create('<a href="' . $course_link . '">' . $course['reg_id'].'</a>'),
          $course['title'],
          isset($course['instructors']) ? Markup::create(implode('<br>', $instructors)) : '',
          Markup::create($course['misc']),
          Markup::create($times),
          Markup::create($location_mode)
        ];
      }

      $course_results[] = [
        [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => t($result['listing_details']),
        ],
        $course_result
      ];
    }

    return [
      '#theme' => 'trico_course_search_results',
      '#result_info' => $course_results,
      '#result_numbers' => $result_numbers,
      '#new_search_url' => Url::fromRoute('trico_course_guide.course_search')->toString(),
      '#next_page_url' => $next_page_url,
      '#prev_page_url' => $prev_page_url,
      '#cache' => [
        'contexts' => ['url.query_args'],
      ]
    ];

  }

  /**
   * Show a single course.
   */
  public function courseDisplay() {

    $params = \Drupal::request()->query->all();
    $orig_params = [];
    foreach ($params as $param => $value) {
      if (strpos($param, 'orig_') === 0) {
        $orig_params[substr($param, 5)] = $value;
      }
    }
    $course = $this->utility->getSingleCourse($params);

    if (empty($course)) {
      \Drupal::messenger()->addWarning('No matching course could be found.');
      $response = new RedirectResponse(Url::fromRoute('trico_course_guide.course_search')->toString());
      $response->send();
    }

    $college = ucwords(str_replace('_', ' ', $course[0]['college']));
    $instructors = $this->getInstructors($course[0]['instructors']);
    $times = $this->getDaysTimes($course[0]['times']);
    $location_mode = '';
    $mode = '';
    if ($course['college'] == 'bryn_mawr') {
      $mode = $this->getMode($course);
    }
    if (isset($course['location']) && !empty(trim($course['location']))) {
      if (strlen($mode) > 0) {
        $location_mode = $course['location'] . '<br><strong>' . $mode . '</strong>';
      } else {
        $location_mode = $course['location'];
      }
    }
    else if (strlen($mode) > 0) {
      $location_mode = '<strong>' . $mode . '</strong>';
    }

    $links = $this->getMiscLinks($course[0]['urls']);

    $location_header = strlen($mode) > 0 ? t('Location / Instruction Mode') : t('Location');

    $rows = [
      'campus' => [
        ['data' => Markup::create('<strong>' . t('Campus') . '</strong>'), 'width' => '25%'], isset($college) ? $college : '',
      ],
      'semester' => [
        Markup::create('<strong>' . t('Semester') . '</strong>'), isset($course[0]['semester']) ? ucfirst(str_replace('_', ' ', $course[0]['semester'])) : '',
      ],
      'reg_id' => [
        Markup::create('<strong>' . t('Registration ID') . '</strong>'), isset($course[0]['reg_id']) ? $course[0]['reg_id'] : '',
      ],
      'course_title' => [
        Markup::create('<strong>' . t('Course Title') . '</strong>'), isset($course[0]['title']) ? $course[0]['title'] : '',
      ],
      'credit' => [
        Markup::create('<strong>' . t('Credit') . '</strong>'), isset($course[0]['credits']) ? $course[0]['credits'] : '',
      ],
      'dept' => [
        Markup::create('<strong>' . t('Department') . '</strong>'), isset($course[0]['department']) ? $course[0]['department'] : '',
      ],
      'instructor' => [
        Markup::create('<strong>' . t('Instructor') . '</strong>'), isset($instructors) ? Markup::create(implode('<br>', $instructors)) : '',
      ],
      'date_time' => [
        Markup::create('<strong>' . t('Time and Days') . '</strong>'), isset($course[0]['times']) ? Markup::create($times) : '',
      ],
      'location' => [
        Markup::create('<strong>' . $location_header . '</strong>'), Markup::create($location_mode),
      ],
      'addtl' => [
        Markup::create('<strong>' . t('Additional Course Info') . '</strong>'), isset($course[0]['additional']) ? Markup::create($course[0]['additional']) : '',
      ],
      'misc_links' => [
        Markup::create('<strong>' . t('Miscellaneous Links') . '</strong>'), isset($course[0]['urls']) ? Markup::create($links) : '',
      ],
    ];

    $course_result = [
      '#type' => 'table',
      '#header' => [],
      '#rows' => $rows,
      '#attributes' => ['class' => ['course-table']],
    ];

    return [
      '#theme' => 'trico_course_single',
      '#course_result' => $course_result,
      '#new_search_url' => Url::fromRoute('trico_course_guide.course_search')->toString(),
      '#return_results_url' => Url::fromRoute('trico_course_guide.course_search_results', [$orig_params])->toString(),
      '#cache' => [
        'contexts' => ['url.query_args'],
      ]
    ];

  }

  /**
   * Sort courses by Semester/College
   */
  private function sortCourses($results) {
    $sorted_results = [];
    $college_weight_map = [
      'haverford' => 2,
      'swarthmore' => 3,
      'bryn_mawr' => 1,
    ];
    $semester_weight_map = array_keys($this->utility->getSemesterList());

    foreach ($results as $result) {
      $college = ucwords(str_replace('_', ' ', $result['college']));
      $semester = ucfirst(str_replace('_', ' ', $result['semester']));
      $college_weight = $college_weight_map[$result['college']];
      $semester_weight = array_search($result['semester'], $semester_weight_map);

      if (!isset($sorted_results[$semester_weight . $college_weight])) {
        $sorted_results[$semester_weight . $college_weight]['listing_details'] = $semester. ': ' . $college;
        $sorted_results[$semester_weight . $college_weight]['results'] = [];
      }
      $sorted_results[$semester_weight . $college_weight]['results'][] = $result;
    }

    ksort($sorted_results);
    return $sorted_results;
  }

  /**
   * Creates link to course passing params to maintain state.
   */
  private function getCourseLink($result) {
    $params = \Drupal::request()->query->all();
    $original_params = [];
    foreach ($params as $param => $value) {
      $original_params['orig_' . $param] = $value;
    }
    $query_params = [
      'id' => $result['course_id'],
      'semester' => $result['semester'],
      'college' => $result['college'],
    ];
    $query_params = array_merge($query_params, $original_params);
    $url = Url::fromRoute('trico_course_guide.course_single', [$query_params]);
    return $url->toString();
  }

  /**
   * Parse instructors, setup email link.
   */
  private function getInstructors($result) {
    $instructors = [];
    foreach ($result as $instructor) {
      if (isset($instructor['email']) && $instructor['email']) {
        $instructors[] = '<a href="mailto:' . $instructor['email'] . '">' . $instructor['name'] . '</a>';
      }
      else {
        $instructors = $instructor['name'];
      }
    }
    return $instructors;
  }

  /**
   * Parse Days/Times
   */
  private function getDaysTimes($result) {
    // Group course times by their start/end pairs.
    $time_list = [];
    foreach ($result as $time) {
      $combined_time = $time['start_time'] . '-' . $time['end_time'];
      if (!isset($time_list[$combined_time])) {
        $time_list[$combined_time] = $time['day'];
      }
      else {
        $time_list[$combined_time] .= $time['day'];
      }
    }

    $times = '';
    foreach ($time_list as $time => $days) {
      $times .= '<span>' . $days . ' ' . $time . '</span><br>';
    }

    return $times;
  }

  /**
   * Get mode of instruction
   */
  private function getMode($result) {

    $mode_of_instruction = '';
    $subjectareacode = substr($result['reg_id'], 0, -7);
    $catalognumber = substr($result['reg_id'], -7, -3);
    $section = substr($result['reg_id'], -3);
    $split_sem = explode("_", $result['semester']);
    $term = substr($split_sem[1], -2);

    if ($split_sem[0] == 'fall') {
      $term .= '10';
    }
    elseif ($split_sem[0] == 'spring') {
      $term .= '00';
    }

    $mode_of_instruction =
      \Drupal::database()
        ->query(
          "SELECT mode_of_instruction FROM {ps2_course} c
            JOIN {ps2_classcomponent} cc ON c.courseid = cc.courseid
            WHERE c.catalognumber = :catalognumber
            AND c.homedepartment = :subjectareacode
            AND cc.section = :section
            AND cc.term = :term", [
              ':catalognumber' => $catalognumber,
              ':subjectareacode' => $subjectareacode,
              ':section' => $section,
              ':term' => $term,
            ]
        )->fetchField();

    return trim($mode_of_instruction);

  }

  /**
   * Format urls in item list
   */
  private function getMiscLinks($links_raw) {
    $links = '';
    foreach($links_raw as $link) {
      $links .= '<a href="' . $link['url'] . '">' . $link['description'] . '</a><br>';
    }
    return $links;
  }

}
