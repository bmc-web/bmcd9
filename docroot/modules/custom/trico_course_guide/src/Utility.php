<?php

namespace Drupal\trico_course_guide;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\HandlerStack;
use Kevinrob\GuzzleCache\CacheMiddleware;
use Kevinrob\GuzzleCache\Strategy\PrivateCacheStrategy;
use Drupal\guzzle_cache\DrupalGuzzleCache;

/**
 * Utility Service.
 */
class Utility {

  public function getSemesterList() {
    $config = \Drupal::config('trico_course_guide.trico_course_guide_settings');
    $client = new Client();
    $api_url = 'https://apis.haverford.edu/courses/v1/semester/list';
    $user = $config->get('trico_course_guide_settings.trico_course_guide_api_username');
    $pw = $config->get('trico_course_guide_settings.trico_course_guide_api_password');
    $cache_id = 'trico_semesters';

    if ($cache = \Drupal::cache()->get($cache_id)) {
      $semesters = $cache->data;
      return $semesters;
    }

    try {

      $response = $client->request('GET', $api_url,
        [
          'auth' => [
            $user, $pw
          ],
          'headers' => [
            'Accept' => 'application/json',
          ],
        ]
      );

      $data = json_decode($response->getBody(), TRUE);

      $semesters = $data['response'];
      $semester_list = [];
      // Clean up semester format for readability.
      foreach ($semesters as $semester) {
        $semester_list[$semester] = ucfirst(str_replace('_', ' ', $semester));
      }

      $semester_list = array_reverse($semester_list,TRUE);
      // Only last 4 semesters.
      $semester_list = array_slice($semester_list,-4,4,TRUE);

      \Drupal::cache()->set($cache_id, $semester_list, time() + 900);
      return $semester_list;

    }
    catch (RequestException $e) {
      \Drupal::logger('Trico Course Guide')->error('Error fetching semesters. Message: ' . $e->getMessage());
      $semesters = [];
    }

    return $semesters;
  }

  public function getRegIdList() {
    return [
      '0\d\d' => t('000 Level'),
      '1\d\d' => t('100 Level'),
      '2\d\d' => t('200 Level'),
      '3\d\d' => t('300 Level'),
      '4\d\d' => t('400 Level'),
    ];
  }

  public function getCollegeList() {
    return [
      'bryn_mawr' => t('Bryn Mawr'),
      'haverford' => t('Haverford'),
      'swarthmore' => t('Swarthmore'),
    ];
  }

  public function getDepartmentList() {

    $config = \Drupal::config('trico_course_guide.trico_course_guide_settings');

    $client = new Client();
    $api_url = 'https://apis.haverford.edu/courses/v1/department/list';
    $user = $config->get('trico_course_guide_settings.trico_course_guide_api_username');
    $pw = $config->get('trico_course_guide_settings.trico_course_guide_api_password');
    $cache_id = 'trico_departments';

    if ($cache = \Drupal::cache()->get($cache_id)) {
      $departments = $cache->data;
      return $departments;
    }

    try {

      $response = $client->request('GET', $api_url,
        [
          'auth' => [
            $user, $pw
          ],
          'headers' => [
            'Accept' => 'application/json',
          ],
          'query' => [
            'per_page' => '200'
          ]
        ]
      );

      $data = json_decode($response->getBody(), TRUE);
      $departments = $data['response'];
      sort($departments);
      \Drupal::cache()->set($cache_id, $departments, time() + 900);

    }
      catch (RequestException $e) {
        \Drupal::logger('Trico Course Guide')->error('Error fetching departments. Message: ' . $e->getMessage());
        $departments = [];
    }

    return $departments;
  }

  /**
   * Prepares default search options if not all fields are filled out.
   */
  public function prepareInput($input) {
    $search_params = [];
    $basic_params = [
      'keyword',
      'reg_id',
      'instructor',
      'days',
      'page',
      'semester',
      'department',
      'college',
      'start_times',
    ];

    foreach ($basic_params as $param_id) {
      if ($param_id == 'semester' && !$input->getValue($param_id)) {
        $search_params[$param_id] = array_keys($this->getSemesterList());
      }
      if ($input->getValue($param_id)) {
        if (is_array($input->getValue($param_id))) {
          $temp = array_filter($input->getValue($param_id));
          $search_params[$param_id] = array_values($temp);
        }
        else {
          $search_params[$param_id] = $input->getValue($param_id);
        }
      }
    }

    if (!isset($search_params['page'])) {
      $search_params['page'] = 1;
    }
    return $search_params;
  }

  public function getCourseResults($input) {

    //The API doesn't support ?param[0]=foo&param[1]=bar.
    //So we have to strip out the brackets and attach to the URL
    //and can't use the query array in the Guzzle request.
    $request_params = http_build_query($input);
    $request_params = preg_replace('/%5B[0-20]+%5D/', '', $request_params);

    $config = \Drupal::config('trico_course_guide.trico_course_guide_settings');

    // Create default HandlerStack
    $stack = HandlerStack::create();

    // Create a Drupal Guzzle cache. Its' useful to have a separate cache bin to
    $cache = new DrupalGuzzleCache(\Drupal::service('cache.trico_cache_bin'));

    // Push the cache to the stack.
    $stack->push(
      new CacheMiddleware(
        new PrivateCacheStrategy($cache)
      ),
      'cache'
    );

    // Initialize the client with the handler option
    $client = new Client(['handler' => $stack]);

    $api_url = 'https://apis.haverford.edu/courses/v1/course/search?' . $request_params;
    $user = $config->get('trico_course_guide_settings.trico_course_guide_api_username');
    $pw = $config->get('trico_course_guide_settings.trico_course_guide_api_password');

    try {

      $response = $client->request('GET', $api_url,
        [
          'auth' => [
            $user, $pw
          ],
          'headers' => [
            'Accept' => 'application/json',
          ],
        ]
      );

      $data = json_decode($response->getBody(), TRUE);
      $results['data'] = $data['response'];
      $results['num_pages'] = 1;
      $results['page'] = 1;
      $results['num_results'] = $data['num_results'];
      if (isset($data['num_pages'])) {
        $results['num_pages'] = $data['num_pages'];
      }
      if (isset($data['page'])) {
        $results['page'] = $data['page'];
      }


    }
      catch (RequestException $e) {
        \Drupal::logger('Trico Course Guide')->error('Trico Course Guide content currently unavailable. Message: ' . $e->getMessage());
        $results = [];

    }

    return $results;

  }

  public function getSingleCourse($input) {

    $config = \Drupal::config('trico_course_guide.trico_course_guide_settings');

    $stack = HandlerStack::create();
    $cache = new DrupalGuzzleCache(\Drupal::service('cache.trico_cache_bin'));

    // Push the cache to the stack.
    $stack->push(
      new CacheMiddleware(
        new PrivateCacheStrategy($cache)
      ),
      'cache'
    );

    // Initialize the client with the handler option
    $client = new Client(['handler' => $stack]);

    $api_url = 'https://apis.haverford.edu/courses/v1/course/get';
    $user = $config->get('trico_course_guide_settings.trico_course_guide_api_username');
    $pw = $config->get('trico_course_guide_settings.trico_course_guide_api_password');

    try {

      $response = $client->request('GET', $api_url,
        [
          'auth' => [
            $user, $pw
          ],
          'headers' => [
            'Accept' => 'application/json',
          ],
          'query' => $input,
        ]
      );
      $data = json_decode($response->getBody(), TRUE);
      $course = $data['response'];
    }
      catch (RequestException $e) {
        \Drupal::logger('Trico Course Guide')->error('Trico Course Guide content currently unavailable. Message: ' . $e->getMessage());
        $results = [];

    }

    return $course;

  }


}
