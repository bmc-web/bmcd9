<?php

namespace Drupal\trico_course_guide\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 *
 */
class TriCoCourseGuideSettings extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'trico_course_guide.trico_course_guide_settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trico_course_guide_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(static::SETTINGS);

    if (NULL !== $config->get('trico_course_guide_settings.trico_course_guide_api_username')) {
      $username = $config->get('trico_course_guide_settings.trico_course_guide_api_username');
    }
    else {
      $username = NULL;
    }

    if (NULL !== $config->get('trico_course_guide_settings.trico_course_guide_api_password')) {
      $password = $config->get('trico_course_guide_settings.trico_course_guide_api_password');
    }
    else {
      $password = NULL;
    }

    if (NULL !== $config->get('trico_course_guide_settings.trico_course_guide_search_body')) {
      $search_text = $config->get('trico_course_guide_settings.trico_course_guide_search_body')['value'];
    }
    else {
      $search_text = NULL;
    }

    $form['trico_course_guide_api_username'] = [
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#default_value' => $username,
      '#required' => TRUE,
    ];

    $form['trico_course_guide_api_password'] = [
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#default_value' => $password,
      '#required' => TRUE,
    ];

    $form['trico_course_guide_search_body'] = [
      '#type' => 'text_format',
      '#title' => t('Search Text'),
      '#format' => 'intermediate',
      '#default_value' => $search_text,
      '#required' => FALSE,
      '#description' => t('This text will be added below the main course search form.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('trico_course_guide_settings.trico_course_guide_api_username',
        $form_state->getValue('trico_course_guide_api_username'))
      ->set('trico_course_guide_settings.trico_course_guide_api_password',
        $form_state->getValue('trico_course_guide_api_password'))
      ->set('trico_course_guide_settings.trico_course_guide_search_body',
        $form_state->getValue('trico_course_guide_search_body'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
