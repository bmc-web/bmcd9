<?php

namespace Drupal\trico_course_guide\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 *
 */
class TriCoCourseGuideSearchForm extends FormBase {

  /**
   * Utility service.
   */
  protected $utility;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('trico_course_guide.utility'));
  }

  /**
   * Class constructor.
   */
  public function __construct($utility) {
    $this->utility = $utility;
  }

  /**
   * @inerhitDoc
   */
  public function getFormId(): string {
    return 'trico_course_guide_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $semesters = $this->utility->getSemesterList();
    $colleges = $this->utility->getCollegeList();
    $departments = $this->utility->getDepartmentList();
    $reg_ids = $this->utility->getRegIdList();
    $config = \Drupal::config('trico_course_guide.trico_course_guide_settings');
    $search_text = $config->get('trico_course_guide_settings.trico_course_guide_search_body')['value'];

    $departments_formatted = [];
    foreach ($departments as $department) {
      $departments_formatted[$department] = $department;
    }

    $form['text']['#markup'] = t('<h1>Tri-Co Course Search</h1>');
    $form['#attributes'] = ['class' => ['row']];
    $form['general'] = [
      '#type' => 'container',
      //'#attributes' => ['class' => ['small-12 medium-9 ']],
      'keyword' => [
        '#title' => t('Keyword Search'),
        '#type' => 'textfield',
      ],
    ];
    $form['general']['semester_set'] = [
      '#type' => 'fieldset',
      '#title' => t('Semester'),
    ];
    $form['general']['semester_set']['semester'] = [
      '#type' => 'checkboxes',
      '#options' => $semesters,
    ];
    $form['general']['college_set'] = [
      '#type' => 'fieldset',
      '#title' => t('College'),
    ];
    $form['general']['college_set']['college'] = [
      '#type' => 'checkboxes',
      '#options' => $colleges,
      '#default_value' => [],
    ];
    $form['general'][] = [
      'department' => [
        '#title' => t('Subject Areas'),
        '#type' => 'select',
        '#options' => $departments_formatted,
        '#empty_option' => t('All'),
        '#multiple' => TRUE,
      ],
      'reg_id' => [
        '#title' => t('Course Number/Level'),
        '#type' => 'select',
        '#options' => $reg_ids,
        '#empty_option' => t('Any'),
      ],
      'instructor' => [
        '#title' => t('Instructor'),
        '#type' => 'textfield',
      ],
    ];

    $form['date_time'] = [
      '#type' => 'container',
      '#prefix' => '<p class="trico-help-text">Select as many as are needed for both Day and Start time by using the shift or the ctrl / command key as you click the options.</p>',
      //'#attributes' => ['class' => ['small-12 medium-3 columns']],
      'days' => [
        '#title' => t('Day'),
        '#type' => 'select',
        '#multiple' => TRUE,
        '#options' => [
          'M' => t('Monday'),
          'T' => t('Tuesday'),
          'W' => t('Wednesday'),
          'Th' => t('Thursday'),
          'F' => t('Friday'),
          'Sa' => t('Saturday'),
          'Su' => t('Sunday'),
        ],
      ],
      'start_times' => [
        '#title' => t('Start time'),
        '#type' => 'select',
        '#options' => [
          '07:00am' => '7:00 am',
          '08:00am' => '8:00 am',
          '09:00am' => '9:00 am',
          '10:00am' => '10:00 am',
          '11:00am' => '11:00 am',
          '12:00pm' => '12:00 pm',
          '01:00pm' => '1:00 pm',
          '02:00pm' => '2:00 pm',
          '03:00pm' => '3:00 pm',
          '04:00pm' => '4:00 pm',
          '05:00pm' => '5:00 pm',
          '06:00pm' => '6:00 pm',
          '07:00pm' => '7:00 pm',
          '08:00pm' => '8:00 pm',
          '09:00pm' => '9:00 pm',
        ],
        '#multiple' => TRUE,
        '#size' => 5,
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Search',
    ];
    $form['suffix']['#markup'] = t($search_text);
    return $form;
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $course_input = $this->utility->prepareInput($form_state);
    $course_input['per_page'] = 50;
    $results = $this->utility->getCourseResults($course_input);

    if (!empty($results)) {
      $url = Url::fromRoute('trico_course_guide.course_search_results', [$course_input]);
      $form_state->setRedirectUrl($url);
    } else {
      $this->messenger()->addMessage($this->t('No results found. Please try another search.'));
    }
  }


}
