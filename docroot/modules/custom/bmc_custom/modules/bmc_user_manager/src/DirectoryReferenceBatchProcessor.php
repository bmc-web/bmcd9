<?php declare(strict_types = 1);

namespace Drupal\bmc_user_manager;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * @todo Add class description.
 */
final class DirectoryReferenceBatchProcessor {
  public static function initiateBatchProcessing() {
    $uids = \Drupal::entityTypeManager()->getStorage('user')
      ->getQuery()
      ->accessCheck(FALSE)
      ->execute();
    $uids = array_values($uids);

    $operation_callback = [DirectoryReferenceBatchProcessor::class, 'operationCallback'];
    $finished_callback = [DirectoryReferenceBatchProcessor::class, 'finishedCallback'];

    $batch_builder = (new BatchBuilder())
      ->setTitle('Updating directory reference field for users')
      ->addOperation($operation_callback, [$uids])
      ->setFinishCallback($finished_callback);

    batch_set($batch_builder->toArray());
  }

  public static function operationCallback($uids, &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($uids);
      if (!$context['sandbox']['max']) {
        $context['finished'] = 1;
        return;
      }
    }
    $items_per_batch = 5;
    while ($items_per_batch > 0) {
      if ($context['sandbox']['progress'] < $context['sandbox']['max']) {
        $uid = $uids[$context['sandbox']['progress']];

        $context['message'] = t('Processing @item of @count', [
          '@item' => $context['sandbox']['progress'],
          '@count' => $context['sandbox']['max'],
        ]);
        $user = \Drupal::entityTypeManager()->getStorage('user')->load($uid);
        /** @var \Drupal\bmc_bundle_classes\Entity\User\BmcUser $user */
        $user->updateDirectoryReferenceField();
      }
      $context['sandbox']['progress']++;
      $items_per_batch--;
    }
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }

  public static function finishedCallback($success, $results, $operations) {
    if ($success) {
      $message = 'Batch processing completed successfully.';
    }
    else {
      $message = 'Batch processing did not complete successfully.';
    }
    \Drupal::messenger()->addMessage($message);
  }

}
