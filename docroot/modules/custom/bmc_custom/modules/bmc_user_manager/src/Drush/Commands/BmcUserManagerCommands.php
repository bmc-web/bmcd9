<?php

namespace Drupal\bmc_user_manager\Drush\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\bmc_user_manager\DirectoryReferenceBatchProcessor;
use Drupal\Core\Utility\Token;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
final class BmcUserManagerCommands extends DrushCommands {

  /**
   * Command description here.
   */
  #[CLI\Command(name: 'bmc_user_manager:update-references')]
  #[CLI\Usage(name: 'bmc_user_manager:update-references', description: 'Update user directory reference field.')]
  public function commandName() {
    DirectoryReferenceBatchProcessor::initiateBatchProcessing();
    drush_backend_batch_process();
    $this->logger()->success('User directory reference fields updated.');
  }

}
