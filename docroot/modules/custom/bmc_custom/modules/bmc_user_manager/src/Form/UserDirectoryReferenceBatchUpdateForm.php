<?php declare(strict_types = 1);

namespace Drupal\bmc_user_manager\Form;

use Drupal\bmc_user_manager\DirectoryReferenceBatchProcessor;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Bmc user manager form.
 */
final class UserDirectoryReferenceBatchUpdateForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'bmc_user_manager_user_directory_reference_batch_update';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Update directory reference field for all users'),
      ],
    ];

    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    DirectoryReferenceBatchProcessor::initiateBatchProcessing();
  }

}
