<?php declare(strict_types = 1);

namespace Drupal\bmc_bundle_classes\Entity\User;

use Drupal\user\Entity\User;

/**
 * A bundle class for user entities.
 */
final class BmcUser extends User {

  const DIRECTORY_REFERENCE_FIELD = 'field_directory_reference';

  public function updateDirectoryReferenceField(): void {
    $email = $this->getEmail();
    if (empty($email)) {
      return;
    }

    if (!$this->hasField(self::DIRECTORY_REFERENCE_FIELD)) {
      return;
    }

    $entityTypeManager = \Drupal::entityTypeManager();
    $nodeStorage = $entityTypeManager->getStorage('node');
    $query = $nodeStorage->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', 'directory')
      ->condition('field_dir_email', $email);
    $nids = $query->execute();
    if (empty($nids)) {
      $this->get(self::DIRECTORY_REFERENCE_FIELD)->setValue(null);
    }
    else {
      $this->get(self::DIRECTORY_REFERENCE_FIELD)->setValue(array_values($nids)[0]);
    }
    $this->save();
  }

}
