<?php

namespace Drupal\bmc_bundle_classes\Entity\Paragraph;

use Drupal\taxonomy\Entity\Term;

interface HasParentThemeInterface {

  /**
   * Assuming the parent entity is a node, get its main theme term.
   *
   * @return \Drupal\taxonomy\Entity\Term|null
   */
  public function getParentTheme(): ?Term;
}
