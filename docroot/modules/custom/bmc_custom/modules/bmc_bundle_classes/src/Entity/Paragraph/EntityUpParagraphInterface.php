<?php

namespace Drupal\bmc_bundle_classes\Entity\Paragraph;

use Drupal\Core\StringTranslation\TranslatableMarkup;

interface EntityUpParagraphInterface {

  public function getHeading(): string;

  public function hasIntro(): bool;

  public function getCtaText(): string|TranslatableMarkup;

  public function getCtaHref(): string;

  public function getItems(): array|false;

  public function getItemsSource(): string;
}
