<?php declare(strict_types = 1);

namespace Drupal\bmc_bundle_classes\Entity\BlockContent;

use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * A bundle class for block_content entities.
 */
final class InContextBlock extends BlockContent {

  //entity::save
  public function save() {
    if ($this->hasField('field_include_block_on_path') &&
      !$this->get('field_include_block_on_path')->isEmpty()
    ) {
      $trimmed_value = trim($this->get('field_include_block_on_path')->value);
      $this->set('field_include_block_on_path', $trimmed_value);
    }
    if ($this->hasField('field_exclude_block_on_path') &&
      !$this->get('field_exclude_block_on_path')->isEmpty()
    ) {
      $trimmed_value = trim($this->get('field_exclude_block_on_path')->value);
      $this->set('field_exclude_block_on_path', $trimmed_value);
    }
    parent::save();
  }

}
