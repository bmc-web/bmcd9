<?php

namespace Drupal\bmc_bundle_classes\Entity\Paragraph;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

class Event3Up extends EntityUpParagraphBase {

  protected const FIELD_HEADING = 'field_p_event3up_heading';

  protected const FIELD_INTRO = 'field_p_event3up_intro';

  protected const FIELD_CUSTOM_CTA = 'field_p_event3up_custom_cta';

  protected const FIELD_CUSTOM_CTA_DEFAULT_TAGGED_STRING = 'More Events';

  protected const FIELD_CUSTOM_CTA_DEFAULT_MANUAL_STRING = 'Events Home';

  protected const FIELD_MANUAL_ITEMS = 'field_p_event3up_manual_items';

  protected const FIELD_INCLUDE_TAG = 'field_p_event3up_tag';

  protected const FIELD_EXCLUDE_TAG = 'field_p_event3up_extag';

  protected const TARGET_BUNDLE = 'event';

  protected const TAGS_QUERY_RANGE = 3;

  public function getCtaHref(): string {
    // If there is a custom CTA, return the url.
    if ($customCtaHref = $this->doGetCustomCtaHref(static::FIELD_CUSTOM_CTA)) {
      return $customCtaHref;
    }

    $listingPageHelper = \Drupal::service('bmc_custom.listing_page_helper');
    $tag = null;
    if (!empty($this->get(static::FIELD_INCLUDE_TAG)->referencedEntities())) {
      $tag = $this->get(static::FIELD_INCLUDE_TAG)->getValue()[0]['target_id'];
    }
    $url = $listingPageHelper->getEventsListingPage($tag);
    return $url ? $url->toString() : '';

  }

  public function getItems(): array|false {
    if ($items = $this->doGetManualItems(static::FIELD_MANUAL_ITEMS)) {
      return $items;
    }

    $query = $this->doGetDefaultEntityUpTaggedQuery();
    if ($query === false) {
      return false;
    }

    $date = new DrupalDateTime('today');
    $date->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $formatted = $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    $query->condition('field_event_date.end_value', $formatted, '>')
      ->sort('field_event_date.end_value', 'ASC');
    $query->sort('changed', 'DESC');

    $nids = $query->execute();
    if (empty($nids)) {
      return false;
    }

    $build = $this->buildFromQueryResults(static::TARGET_BUNDLE, 'teaser_3_up', $nids);
    return $build;
  }

}
