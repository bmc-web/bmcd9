<?php

namespace Drupal\bmc_bundle_classes\Entity\Node;

/**
 * If a node provides its own metatag image, it should implement this interface.
 */
interface CustomMetatagImageInterface {

  /**
   * Return the path to the metatag image.
   */
  public function getMetatagImagePath(): string;

  /**
   * Return the mimetype of the metatag image.
   */
  public function getMetatagImageType(): string;

  /**
   * Return the width of the metatag image.
   */
  public function getMetatagImageWidth(): int;

  /**
   * Return the height of the metatag image.
   */
  public function getMetatagImageHeight(): int;

  /**
   * Return the alt text of the metatag image.
   */
  public function getMetatagImageAlt(): string;

}
