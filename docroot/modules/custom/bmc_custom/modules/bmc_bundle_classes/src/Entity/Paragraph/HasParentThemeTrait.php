<?php

namespace Drupal\bmc_bundle_classes\Entity\Paragraph;

use Drupal\taxonomy\Entity\Term;

trait HasParentThemeTrait {

  /**
   * {@inheritDoc}
   */
  public function getParentTheme(): ?Term {
    /** @var \Drupal\taxonomy\TermStorage $termStorage */
    $termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $parentTerm = null;
    $parent = $this->getParentEntity();
    if ($parent && $parent->hasField('field_theme_main') && !$parent->get('field_theme_main')->isEmpty()) {
      $parentTerm = $termStorage->load($parent->get('field_theme_main')->target_id);
    }
    return $parentTerm;
  }
}
