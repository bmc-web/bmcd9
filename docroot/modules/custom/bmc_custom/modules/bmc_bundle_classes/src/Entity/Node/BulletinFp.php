<?php declare(strict_types = 1);

namespace Drupal\bmc_bundle_classes\Entity\Node;

use Drupal\node\Entity\Node;

/**
 * A bundle class for node entities.
 */
final class BulletinFp extends Node implements CustomMetatagImageInterface {

  use HasCalculatedMetatagImageTrait;

  /**
   * @return array|false
   */
  protected function getNegotiatedData(): array|false {
    if ($this->get('field_bulletin_fp_carousel')->isEmpty()) {
      return false;
    }
    try {
      $data = $this->get('field_bulletin_fp_carousel')->entity->get('field_p_slide_image')->entity->get('field_media_image')->getValue()[0];
      return $this->getMetatagImageNegotiator()->negotiateFile(
        $data['target_id'],
        $data['alt'],
        $data['width'],
        $data['height'],
      );
    } catch (Exception $e) {
      return false;
    }
  }

}
