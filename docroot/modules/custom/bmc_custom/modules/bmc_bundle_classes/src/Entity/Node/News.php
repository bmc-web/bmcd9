<?php declare(strict_types = 1);

namespace Drupal\bmc_bundle_classes\Entity\Node;

use Drupal\node\Entity\Node;
use Exception;

/**
 * A bundle class for node entities.
 */
final class News extends Node implements CustomMetatagImageInterface {

  use HasCalculatedMetatagImageTrait;

  /**
   * Get negotiated metatag for the metatag image.
   *
   * If this function returns array, those values should populate the metatag
   * tokens. If this function returns false, use the default values provided by
   * the MetaTagImageNegotiator service to populate the images.
   *
   * Override this function in the entity class to provide custom logic for
   * negotiating the metatag image.
   *
   * @return array|false
   *   The negotiated data or false if the image is not available.
   */
  protected function getNegotiatedData(): array|false {
    if ($this->get('field_news_img')->isEmpty()) {
      return false;
    }
    try {
      $data = $this->get('field_news_img')->entity->get('field_media_image')->getValue()[0];
      return $this->getMetatagImageNegotiator()->negotiateFile(
        $data['target_id'],
        $data['alt'],
        $data['width'],
        $data['height'],
      );
    } catch (Exception $e) {
      return false;
    }
  }

  public function isBulletin(): bool {
    if ($this->get('field_theme_main')->isEmpty()) {
      return false;
    }
    // The tid for alumnae bulletin is 1.
    // @todo: Don't use a magic number here.
    return $this->get('field_theme_main')->value === '1';
  }
}
