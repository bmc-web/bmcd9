<?php declare(strict_types = 1);

namespace Drupal\bmc_bundle_classes\Entity\Paragraph;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * A bundle class for paragraph entities.
 */
final class Anncmnt4Up extends EntityUpParagraphBase {

  protected const FIELD_HEADING = 'field_p_anncmnt_head';

  protected const FIELD_INTRO = 'field_p_anncmnt_intro';

  protected const FIELD_CUSTOM_CTA = 'field_p_anncmnt_custom_cta';

  protected const FIELD_CUSTOM_CTA_DEFAULT_TAGGED_STRING = 'More Announcements';

  protected const FIELD_CUSTOM_CTA_DEFAULT_MANUAL_STRING = 'Announcements Home';

  protected const FIELD_MANUAL_ITEMS = 'field_p_anncmnt_manual_items';

  protected const FIELD_INCLUDE_TAG = 'field_p_anncmnt_tag';

  protected const FIELD_EXCLUDE_TAG = 'field_p_anncmnt_extag';

  protected const TARGET_BUNDLE = 'anncmnt';

  protected const TAGS_QUERY_RANGE = 4;

  public function getCtaHref(): string {
    // First, check if there is a CTA with an acceptable url. If so, use that.
    if ($customCtaHref = $this->doGetCustomCtaHref(static::FIELD_CUSTOM_CTA)) {
      return $customCtaHref;
    }

    $listingPageHelper = \Drupal::service('bmc_custom.listing_page_helper');
    $tag = null;
    if (!empty($this->get(static::FIELD_INCLUDE_TAG)->referencedEntities())) {
      $tag = $this->get(static::FIELD_INCLUDE_TAG)->getValue()[0]['target_id'];
    }
    $url = $listingPageHelper->getAnnouncementsListingPage($tag);
    return $url ? $url->toString() : '';

  }

  public function getItems(): array|false {
    if ($items = $this->doGetManualItems(static::FIELD_MANUAL_ITEMS)) {
      return $items;
    }

    $query = $this->doGetDefaultEntityUpTaggedQuery();
    if ($query === false) {
      return false;
    }

    $date = new DrupalDateTime('today');
    $date->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $formatted = $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    // Business logic unique to this paragraph type.
    $query->sort('field_anncmnt_date', 'DESC')
      ->condition('field_anncmnt_date', $formatted, '<=')
      ->condition('field_anncmnt_nolonger_timely', $formatted, '>');
    $query->sort('changed', 'DESC');

    $nids = $query->execute();
    if (empty($nids)) {
      return false;
    }

    $build = $this->buildFromQueryResults(static::TARGET_BUNDLE, 'teaser', $nids);
    return $build;
  }

}
