<?php

namespace Drupal\bmc_bundle_classes\Entity\Node;

use Drupal\bmc_custom\HasMetatagImageNegotiatorTrait;
use Exception;


trait HasCalculatedMetatagImageTrait {
  use HasMetatagImageNegotiatorTrait;

  public function getMetatagImagePath(): string {
    $data = $this->getNegotiatedData();
    if ($data === false) {
      return $this->getMetatagImageNegotiator()->getDefaultMetatagImagePath();
    }
    return $data['path'];
  }

  public function getMetatagImageType(): string {
    $data = $this->getNegotiatedData();
    if ($data === false) {
      return $this->getMetatagImageNegotiator()->getDefaultMetatagImageType();
    }
    return $data['type'];
  }

  public function getMetatagImageWidth(): int {
    $data = $this->getNegotiatedData();
    if ($data === false) {
      return $this->getMetatagImageNegotiator()->getDefaultMetatagImageWidth();
    }
    return $data['width'];
  }

  public function getMetatagImageHeight(): int {
    $data = $this->getNegotiatedData();
    if ($data === false) {
      return $this->getMetatagImageNegotiator()->getDefaultMetatagImageHeight();
    }
    return $data['height'];
  }

  public function getMetatagImageAlt(): string {
    $data = $this->getNegotiatedData();
    if ($data === false) {
      return $this->getMetatagImageNegotiator()->getDefaultMetatagImageAlt();
    }
    return $data['alt'];
  }

  /**
   * Get negotiated metatag for the metatag image.
   *
   * If this function returns array, those values should populate the metatag
   * tokens. If this function returns false, use the default values provided by
   * the MetaTagImageNegotiator service to populate the images.
   *
   * Override this function in the entity class to provide custom logic for
   * negotiating the metatag image.
   *
   * @return array|false
   *   The negotiated data or false if the image is not available.
   */
  protected function getNegotiatedData(): array|false {
    if ($this->get('field_page_hero_img')->isEmpty()) {
      return false;
    }
    try {
      $data = $this->get('field_page_hero_img')->entity->get('field_media_image')->getValue()[0];
      return $this->getMetatagImageNegotiator()->negotiateFile(
        $data['target_id'],
        $data['alt'],
        $data['width'],
        $data['height'],
      );
    } catch (Exception $e) {
      return false;
    }
  }
}
