<?php declare(strict_types = 1);

namespace Drupal\bmc_bundle_classes\Entity\Node;

use Drupal\node\Entity\Node;
use Exception;

/**
 * A bundle class for node entities.
 */
final class Story extends Node implements CustomMetatagImageInterface {
  use HasCalculatedMetatagImageTrait;

  /**
   * @return array|false
   */
  protected function getNegotiatedData(): array|false {
    if ($this->get('field_story_img')->isEmpty()) {
      return false;
    }
    try {
      $data = $this->get('field_story_img')->entity->get('field_media_image')->getValue()[0];
      return $this->getMetatagImageNegotiator()->negotiateFile(
        $data['target_id'],
        $data['alt'],
        $data['width'],
        $data['height'],
      );
    } catch (Exception $e) {
      return false;
    }
  }
}
