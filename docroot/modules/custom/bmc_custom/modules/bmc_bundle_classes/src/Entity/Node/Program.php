<?php declare(strict_types = 1);

namespace Drupal\bmc_bundle_classes\Entity\Node;

use Drupal\node\Entity\Node;
use Exception;

/**
 * A bundle class for node entities.
 *
 * Note: This is NOT the Program Description.
 */
final class Program extends Node implements CustomMetatagImageInterface {
  use HasCalculatedMetatagImageTrait;

  /**
   * @return array|false
   */
  protected function getNegotiatedData(): array|false {
    if ($this->get('field_prog_img_detail')->isEmpty()) {
      return false;
    }
    try {
      $data = $this->get('field_prog_img_detail')->entity->get('field_media_image')->getValue()[0];
      return $this->getMetatagImageNegotiator()->negotiateFile(
        $data['target_id'],
        $data['alt'],
        $data['width'],
        $data['height'],
      );
    } catch (Exception $e) {
      return false;
    }
  }
}
