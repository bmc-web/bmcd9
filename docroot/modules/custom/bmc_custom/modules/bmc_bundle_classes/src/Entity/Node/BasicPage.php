<?php declare(strict_types = 1);

namespace Drupal\bmc_bundle_classes\Entity\Node;

use Drupal\node\Entity\Node;
use Exception;

/**
 * A bundle class for node entities.
 */
final class BasicPage extends Node implements CustomMetatagImageInterface {
  use HasCalculatedMetatagImageTrait;

}
