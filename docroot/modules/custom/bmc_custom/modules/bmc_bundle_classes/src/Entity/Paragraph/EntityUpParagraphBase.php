<?php declare(strict_types = 1);

namespace Drupal\bmc_bundle_classes\Entity\Paragraph;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\taxonomy\Entity\Term;

/**
 * A base bundle class for paragraph entities.
 */
abstract class EntityUpParagraphBase extends Paragraph implements HasParentThemeInterface, EntityUpParagraphInterface {
  use HasParentThemeTrait;
  use stringTranslationTrait;

  protected const FIELD_HEADING = '';

  protected const FIELD_INTRO = '';

  protected const FIELD_CUSTOM_CTA = '';

  protected const FIELD_CUSTOM_CTA_DEFAULT_TAGGED_STRING = '';

  protected const FIELD_CUSTOM_CTA_DEFAULT_MANUAL_STRING = '';

  protected const FIELD_MANUAL_ITEMS = '';

  protected const NODE_FIELD_THEME_DISPLAY = 'field_theme_display';

  protected const CUSTOM_CTA_PARA_TITLE_FIELD = 'field_p_custom_cta';

  protected const FIELD_INCLUDE_TAG = '';

  protected const FIELD_EXCLUDE_TAG = '';

  protected const TARGET_BUNDLE = '';

  protected const TAGS_QUERY_RANGE = 3;

  public function getHeading(): string {
    return $this->doGetHeading(static::FIELD_HEADING);
  }

  protected function doGetHeading(string $fieldName): string {
    if (!$this->hasField($fieldName)) {
      return '';
    }
    return $this->get($fieldName)->isEmpty() ? '' : $this->get($fieldName)->value;
  }

  public function hasIntro(): bool {
    return $this->doHasIntro(static::FIELD_INTRO);
  }

  protected function doHasIntro(string $fieldName): bool {
    if (!$this->hasField($fieldName)) {
      return false;
    }
    return !$this->get($fieldName)->isEmpty();
  }

  public function getCtaText(): string|TranslatableMarkup {
    $tagField = $this->get(static::FIELD_INCLUDE_TAG);
    // There are different default CTA strings depending on whether there is a tag.
    $defaultString = $tagField->isEmpty() ? static::FIELD_CUSTOM_CTA_DEFAULT_MANUAL_STRING : static::FIELD_CUSTOM_CTA_DEFAULT_TAGGED_STRING;
    return $this->doGetCtaText(static::FIELD_CUSTOM_CTA, $defaultString);
  }

  protected function doGetCtaText(string $fieldName, string $defaultString): string|TranslatableMarkup {
    if (!$this->hasField($fieldName) || $this->get($fieldName)->isEmpty()) {
      return $defaultString;
    }
    /** @var \Drupal\paragraphs\Entity\Paragraph $cta_para */
    $cta_para = $this->get($fieldName)->referencedEntities()[0];
    /** @var string $title */
    $title = $cta_para->get(static::CUSTOM_CTA_PARA_TITLE_FIELD)->getValue()[0]['title'];
    return strlen(trim($title)) ? $title : $this->t($defaultString);
  }

  protected function doGetCustomCtaHref(string $fieldName): string|false {
    if (!$this->hasField($fieldName) || $this->get($fieldName)->isEmpty()) {
      return false;
    }
    /** @var \Drupal\paragraphs\Entity\Paragraph $cta_para */
    $cta_para = $this->get($fieldName)->referencedEntities()[0];
    /** @var \Drupal\link\Plugin\Field\FieldType\LinkItem $linkItem */
    $linkItem = $cta_para->get(static::CUSTOM_CTA_PARA_TITLE_FIELD)[0];
    /** @var \Drupal\Core\Url $url */
    $url = $linkItem->getUrl();
    if (strlen($url->toString()) > 0) {
      return $url->toString();
    }
    return false;
  }

  public function getItemsSource(): string {
    return $this->doGetItemsSource(static::FIELD_MANUAL_ITEMS);
  }

  protected function doGetItemsSource(string $fieldName): string {
    if ($this->hasField($fieldName) && !$this->get($fieldName)->isEmpty()) {
      return 'manual';
    }
    return 'tags';
  }

  protected function doGetManualItems(string $fieldName): array|false {
    if ($this->hasField($fieldName) && !$this->get($fieldName)->isEmpty()) {
      return $this->get($fieldName)->view('default');
    }
    return false;
  }

  protected function doGetDefaultEntityUpTaggedQuery() {
    return $this->doGetEntityUpTaggedQuery(
      static::TARGET_BUNDLE,
      static::FIELD_INCLUDE_TAG,
      static::FIELD_EXCLUDE_TAG,
      static::TAGS_QUERY_RANGE);
  }

  /**
   * Most queries can use this method as a starting point and customize from there.
   *
   * Be sure to add additional sorting and conditions in the bundle class.
   *
   * @param string $bundle
   * @param string $includeTermsFieldName
   * @param string $excludeTermsFieldName
   * @param int $range
   * @return \Drupal\Core\Entity\Query\QueryInterface
   */
  protected function doGetEntityUpTaggedQuery(
    string $bundle,
    string $includeTermsFieldName = '',
    string $excludeTermsFieldName = '',
    int $range = 3,
  ) {
    /** @var Term $parentTerm */
    $parentTerm = $this->getParentTheme();
    /** @var $nodeStorage $nodeStorage */
    $nodeStorage = \Drupal::entityTypeManager()->getStorage('node');
    $query = $nodeStorage->getQuery()
      ->accessCheck(TRUE)
      ->condition('status', 1)
      ->condition('type', $bundle)
      ->range(0, $range);
    if ($parentTerm) {
      $query->condition(static::NODE_FIELD_THEME_DISPLAY, $parentTerm->id());
    }

    if ($includeTermsFieldName && $this->hasField($includeTermsFieldName)) {
      $includeTermsField = $this->get($includeTermsFieldName);
      if (!$includeTermsField->isEmpty()) {
        $includedTerms = $includeTermsField->getValue();
        foreach ($includedTerms as $term) {
          $includeTermsConditionGroup = $query->andConditionGroup();
          $includeTermsConditionGroup->condition('field_tags', $term['target_id']);
          $query->condition($includeTermsConditionGroup);
        }
      }
    }

    if ($excludeTermsFieldName && $this->hasField($excludeTermsFieldName)) {
      $excludeTermsField = $this->get($excludeTermsFieldName);
      if (!$excludeTermsField->isEmpty()) {
        $excludeTerms = $excludeTermsField->getValue();
        $excludeNids = [];
        foreach ($excludeTerms as $term) {
          $excludeQuery = $nodeStorage->getQuery()
            ->accessCheck(FALSE)
            ->condition('type', $bundle)
            ->condition('field_tags', $term['target_id']);
          $excludes = $excludeQuery->execute();
          $excludeNids = array_merge($excludeNids, $excludes);
        }
        $excludeNids = array_unique($excludeNids);
        if (!empty($excludeNids)) {
          $query->condition('nid', $excludeNids, 'NOT IN');
        }
      }
    }

    return $query;
  }

  protected function buildFromQueryResults(string $bundle, string $viewMode, array $nids): array {
    /** @var $nodeStorage $nodeStorage */
    $nodeStorage = \Drupal::entityTypeManager()->getStorage('node');
    $nodes = $nodeStorage->loadMultiple($nids);
    /** @var \Drupal\node\NodeViewBuilder $viewBuilder */
    $viewBuilder = \Drupal::entityTypeManager()->getViewBuilder('node');
    $build = [];
    $build['#cache']['tags'] = ['node_list:' . $bundle];
    $build['items'] = $viewBuilder->viewMultiple($nodes, $viewMode);
    return $build;
  }
}
