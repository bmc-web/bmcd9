<?php declare(strict_types = 1);

namespace Drupal\bmc_bundle_classes\Entity\Paragraph;

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * A bundle class for paragraph entities.
 */
final class Story3Up extends EntityUpParagraphBase {
  protected const FIELD_HEADING = 'field_p_story3up_heading';

  protected const FIELD_INTRO = 'field_p_story3up_intro';

  protected const FIELD_CUSTOM_CTA = 'field_p_story3up_custom_cta';

  protected const FIELD_CUSTOM_CTA_DEFAULT_TAGGED_STRING = 'More Stories';

  protected const FIELD_CUSTOM_CTA_DEFAULT_MANUAL_STRING = 'Stories Home';

  protected const FIELD_MANUAL_ITEMS = 'field_p_story3up_manual';

  protected const FIELD_INCLUDE_TAG = 'field_p_story3up_tag';

  protected const FIELD_EXCLUDE_TAG = 'field_p_story3up_extag';

  protected const TARGET_BUNDLE = 'story';

  protected const TAGS_QUERY_RANGE = 3;

  public function getCtaHref(): string {
    // First, check if there is a CTA with an acceptable url. If so, use that.
    if ($customCtaHref = $this->doGetCustomCtaHref(static::FIELD_CUSTOM_CTA)) {
      return $customCtaHref;
    }

    $listingPageHelper = \Drupal::service('bmc_custom.listing_page_helper');
    $tag = null;
    if (!empty($this->get(static::FIELD_INCLUDE_TAG)->referencedEntities())) {
      $tag = $this->get(static::FIELD_INCLUDE_TAG)->getValue()[0]['target_id'];
    }
    $parentTerm = $this->getParentTheme();
    $url = $listingPageHelper->getStoriesListingPage($parentTerm->id(), $tag);

    return $url->toString();
  }

  public function getItems(): array|false {
    if ($items = $this->doGetManualItems(static::FIELD_MANUAL_ITEMS)) {
      return $items;
    }

    $query = $this->doGetDefaultEntityUpTaggedQuery();
    if ($query === false) {
      return false;
    }

    // Business logic unique to this paragraph type.
    $query->sort('field_story_date', 'DESC');
    $query->sort('changed', 'DESC');

    $nids = $query->execute();
    if (empty($nids)) {
      return false;
    }

    $build = $this->buildFromQueryResults(static::TARGET_BUNDLE, 'teaser_3_up', $nids);
    return $build;

    return [];
  }


}
