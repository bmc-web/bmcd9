<?php

use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

// deploy 9003-9009 for event form restructure 9/2023
/**
 * On Campus Event sets default values.
 *
 */
function bmc_custom_deploy_9003(&$sandbox) {
  $oncampus = [261, 266];
  // Initialize some variables during the first pass through.
  if (!isset($sandbox['total'])) {
    $nids = \Drupal::entityQuery('node')
      ->accessCheck(TRUE)
      ->condition('type', 'event')
      ->condition('field_event_location', $oncampus, 'NOT IN')
      ->execute();
    $sandbox['total'] = count($nids);
    $sandbox['current'] = 0;
  }
  $nodes_per_batch = 25;
  // Handle one pass through.
  $nids = \Drupal::entityQuery('node')
    ->accessCheck(TRUE)
    ->condition('type', 'event')
    ->condition('field_event_location', $oncampus, 'NOT IN')
    ->range($sandbox['current'], $sandbox['current'] + $nodes_per_batch)
    ->execute();
  foreach ($nids as $nid) {
    $node = Node::load($nid)
      ->set('field_event_location_choice', 'on_campus')
      ->set('field_event_reservation_number', '111111')
      ->set('field_event_food', '0')
      ->save();
    $sandbox['current']++;
  }
  \Drupal::entityTypeManager()
    ->getStorage('node')
    ->resetCache([$nodes_per_batch]);
  print "Mem usage: " . memory_get_usage(TRUE) . "\n<br>";
  \Drupal::messenger()->addMessage($sandbox['current'] . ' nodes processed.');
  if ($sandbox['total'] == 0) {
    $sandbox['#finished'] = 1;
  }
  else {
    $sandbox['#finished'] = ($sandbox['current'] / $sandbox['total']);
  }
}

/**
 * On Campus Event location detail field values set to room.
 *
 */
function bmc_custom_deploy_9004() {
  // there are only 39 records so this simple non-batch query should do
  $nidsr = \Drupal::entityQuery('node')
    ->accessCheck(TRUE)
    ->condition('type', 'event')
    ->condition('field_event_location_choice', 'on_campus')
    ->condition('field_event_location_details', NULL, 'IS NULL')
    ->execute();
  $nodesr = Node::loadMultiple($nidsr);
  foreach ($nodesr as $noder) {
    $noder->set('field_event_location_details', 'room')
      ->save();
  }
}

/**
 * Off Campus Event sets default values.
 *
 */
function bmc_custom_deploy_9005() {
  // there are only 163 records so this simple non-batch query should do
  $nidsoc = \Drupal::entityQuery('node')
    ->accessCheck(TRUE)
    ->condition('type', 'event')
    ->condition('field_event_location', '261', 'IN')
    ->execute();
  $nodesoc = Node::loadMultiple($nidsoc);
  foreach ($nodesoc as $nodeoc) {
    $nodeoc->set('field_event_location_choice', 'off_campus')
      ->set('field_event_food', '0')
      ->save();
  }
}

/**
 * Virtual Only Event sets default values.
 *
 */
function bmc_custom_deploy_9006(&$sandbox) {
  // Initialize some variables during the first pass through.
  if (!isset($sandbox['total'])) {
    $nidsv = \Drupal::entityQuery('node')
      ->accessCheck(TRUE)
      ->condition('type', 'event')
      ->condition('field_event_location', '266', 'IN')
      ->execute();
    $sandbox['total'] = count($nidsv);
    $sandbox['current'] = 0;
  }
  $nodes_per_batchv = 25;
  // Handle one pass through.
  $nidsv = \Drupal::entityQuery('node')
    ->accessCheck(TRUE)
    ->condition('type', 'event')
    ->condition('field_event_location', '266', 'IN')
    ->range($sandbox['current'], $sandbox['current'] + $nodes_per_batchv)
    ->execute();
  foreach ($nidsv as $nidv) {
    $nodev = Node::load($nidv)
      ->set('field_event_location_choice', 'virtual_only')
      ->set('field_virtual_event_access_link', 'https://brynmawr-edu.zoom.us/meeting/register/tJUtc-GtrzorHtG3PafivgKuh9CV5YEkrhky')
      ->set('field_event_food', '0')
      ->save();
    $sandbox['current']++;
  }
  \Drupal::entityTypeManager()
    ->getStorage('node')
    ->resetCache([$nodes_per_batchv]);
  print "Mem usage: " . memory_get_usage(TRUE) . "\n<br>";
  \Drupal::messenger()->addMessage($sandbox['current'] . ' nodes processed.');
  if ($sandbox['total'] == 0) {
    $sandbox['#finished'] = 1;
  }
  else {
    $sandbox['#finished'] = ($sandbox['current'] / $sandbox['total']);
  }
}

/**
 * Virtual Only Event location field set value to -none-.
 *
 */

function bmc_custom_deploy_9007() {
  // there are 351 records so this simple non-batch query should do
  $nidsvn = \Drupal::entityQuery('node')
    ->accessCheck(TRUE)
    ->condition('type', 'event')
    ->condition('field_event_location_choice', 'virtual_only')
    ->execute();
  $nodesvn = Node::loadMultiple($nidsvn);
  foreach ($nodesvn as $nodevn) {
    $nodevn->set('field_event_location', NULL)
      ->save();
  }
}

/**
 * Off Campus Event location field set value to -none-.
 *
 */
function bmc_custom_deploy_9008() {
  // there are only 163 records so this simple non-batch query should do
  $nidsocn = \Drupal::entityQuery('node')
    ->condition('type', 'event')
    ->condition('field_event_location_choice', 'off_campus')
    ->execute();
  $nodesocn = Node::loadMultiple($nidsocn);
  foreach ($nodesocn as $nodeocn) {
    $nodeocn->set('field_event_location', NULL)
      ->save();
  }
}

/**
 * EMS role added to iftest_events uid=211.
 *
 */
function bmc_custom_deploy_9009() {
  $user = User::load(211);
  $user->addRole('ems');
  $user->save();
}

/**
 * Announcements set field_anncmnt_nolonger_timely for existing nodes.
 *
 * @param array $sandbox
 */
function bmc_custom_deploy_www1722(array &$sandbox): void {
  // Initialize some variables during the first pass through.
  if (!isset($sandbox['total'])) {
    $nids = \Drupal::entityQuery('node')
      ->accessCheck(FALSE)
      ->condition('type', 'anncmnt')
      ->execute();
    $sandbox['total'] = count($nids);
    $sandbox['current'] = 0;
    \Drupal::messenger()->addMessage($sandbox['total'] . ' nodes total.');
  }
  $nodes_per_batch = 1000;

  $nids = \Drupal::entityQuery('node')
    ->accessCheck(FALSE)
    ->condition('type', 'anncmnt')
    ->sort('nid', 'ASC')
    ->range($sandbox['current'], $nodes_per_batch)
    ->execute();

  $nodes = Node::loadMultiple($nids);
  foreach ($nodes as $node) {
    /** @var string $anncmntDateValue */
    $anncmntDateValue = $node->get('field_anncmnt_date')->value;
    $anncmntDate = new DateTime($anncmntDateValue);
    $anncmntDate->modify('+30 days')->setTime(0, 0, 0);
    $node->set('field_anncmnt_nolonger_timely', $anncmntDate->format('Y-m-d'));
    $node->save();
    $sandbox['current']++;
  }
  \Drupal::messenger()->addMessage($sandbox['current'] . ' nodes processed.');
  if ($sandbox['total'] == 0) {
    $sandbox['#finished'] = 1;
  }
  else {
    $sandbox['#finished'] = ($sandbox['current'] / $sandbox['total']);
  }
}

/**
 * Truncates the search index tables.
 *
 * As part of WWW-1814, we also delete the Content search page configuration
 * enttity, and set the default to Google.
 */
function bmc_custom_deploy_www1814() {
  // If this is needed in the gui, see the searchindex_wipe module.
  \Drupal::database()->truncate('search_index')->execute();
  \Drupal::database()->truncate('search_dataset')->execute();
  \Drupal::database()->truncate('search_total')->execute();
}

/**
 * Delete paragraph_report.report_data.
 */
function bmc_custom_deploy_www1872() {
  $state = \Drupal::service('state')->delete('paragraph_report.report_data');
}

/**
 * Migrate the ode config page content to Production
 */
function bmc_custom_deploy_www1830() {
  // This is hacky but it works. We just export the config_pages entity as an
  // array, get only the keys we want, serialize it, and then add the serialized
  // data to the variable below. Note: we only do this for fields that are not
  // entity references, like media or files.
  $serialized_data = 'a:30:{s:29:"field_cp_wrapper_gsas_contact";a:1:{i:0;a:3:{s:3:"uri";s:24:"mailto:gsas@brynmawr.edu";s:5:"title";s:17:"gsas@brynmawr.edu";s:7:"options";a:0:{}}}s:31:"field_cp_wrapper_gsas_hlogolink";a:1:{i:0;a:3:{s:3:"uri";s:14:"internal:/gsas";s:5:"title";s:0:"";s:7:"options";a:0:{}}}s:27:"field_cp_wrapper_gsas_links";a:11:{i:0;a:3:{s:3:"uri";s:18:"internal:/node/216";s:5:"title";s:28:"Offices & Services Directory";s:7:"options";a:0:{}}i:1;a:3:{s:3:"uri";s:19:"internal:/node/4976";s:5:"title";s:25:"Faculty & Staff Directory";s:7:"options";a:0:{}}i:2;a:3:{s:3:"uri";s:20:"internal:/node/13111";s:5:"title";s:17:"Work at Bryn Mawr";s:7:"options";a:0:{}}i:3;a:3:{s:3:"uri";s:20:"internal:/node/18016";s:5:"title";s:23:"Libraries & Collections";s:7:"options";a:0:{}}i:4;a:3:{s:3:"uri";s:30:"https://bookshop.brynmawr.edu/";s:5:"title";s:8:"Bookshop";s:7:"options";a:0:{}}i:5;a:3:{s:3:"uri";s:23:"https://gobrynmawr.com/";s:5:"title";s:9:"Athletics";s:7:"options";a:0:{}}i:6;a:3:{s:3:"uri";s:18:"internal:/node/831";s:5:"title";s:15:"Events Calendar";s:7:"options";a:0:{}}i:7;a:3:{s:3:"uri";s:18:"internal:/node/396";s:5:"title";s:8:"Newsroom";s:7:"options";a:0:{}}i:8;a:3:{s:3:"uri";s:20:"internal:/node/50171";s:5:"title";s:12:"Plan a Visit";s:7:"options";a:0:{}}i:9;a:3:{s:3:"uri";s:20:"internal:/campusmap/";s:5:"title";s:10:"Campus Map";s:7:"options";a:0:{}}i:10;a:3:{s:3:"uri";s:137:"https://host.nxt.blackbaud.com/donor-form?svcid=renxt&formId=fd285a9c-3301-4472-94d0-1562f8517842&envid=p-AJWGzZxxCEaq7gUjd-FBDw&zone=usa";s:5:"title";s:11:"Make a Gift";s:7:"options";a:0:{}}}s:28:"field_cp_wrapper_gsas_pf_cta";a:4:{i:0;a:3:{s:3:"uri";s:19:"internal:/node/7416";s:5:"title";s:12:"How to Apply";s:7:"options";a:0:{}}i:1;a:3:{s:3:"uri";s:19:"internal:/node/7861";s:5:"title";s:8:"Visit Us";s:7:"options";a:0:{}}i:2;a:3:{s:3:"uri";s:69:"https://admissions.brynmawr.edu/register/grad-request-for-information";s:5:"title";s:19:"Request Information";s:7:"options";a:0:{}}i:3;a:3:{s:3:"uri";s:71:"internal:/inside/offices-services/alumnaei-relations-development/giving";s:5:"title";s:12:"Ways to Give";s:7:"options";a:0:{}}}s:30:"field_cp_wrapper_gsas_pf_title";a:1:{i:0;a:1:{s:5:"value";s:18:"Take the Next Step";}}s:27:"field_cp_wrapper_gsas_phone";a:1:{i:0;a:1:{s:5:"value";s:12:"610-526-5074";}}s:29:"field_cp_wrapper_ibmc_contact";a:1:{i:0;a:3:{s:3:"uri";s:21:"internal:/node/110626";s:5:"title";s:10:"Contact Us";s:7:"options";a:0:{}}}s:31:"field_cp_wrapper_ibmc_hlogolink";a:1:{i:0;a:3:{s:3:"uri";s:16:"internal:/inside";s:5:"title";s:0:"";s:7:"options";a:0:{}}}s:27:"field_cp_wrapper_ibmc_links";a:11:{i:0;a:3:{s:3:"uri";s:18:"internal:/node/216";s:5:"title";s:28:"Offices & Services Directory";s:7:"options";a:0:{}}i:1;a:3:{s:3:"uri";s:19:"internal:/node/4976";s:5:"title";s:25:"Faculty & Staff Directory";s:7:"options";a:0:{}}i:2;a:3:{s:3:"uri";s:20:"internal:/node/13111";s:5:"title";s:17:"Work at Bryn Mawr";s:7:"options";a:0:{}}i:3;a:3:{s:3:"uri";s:20:"internal:/node/18016";s:5:"title";s:23:"Libraries & Collections";s:7:"options";a:0:{}}i:4;a:3:{s:3:"uri";s:30:"https://bookshop.brynmawr.edu/";s:5:"title";s:8:"Bookshop";s:7:"options";a:0:{}}i:5;a:3:{s:3:"uri";s:23:"https://gobrynmawr.com/";s:5:"title";s:9:"Athletics";s:7:"options";a:0:{}}i:6;a:3:{s:3:"uri";s:18:"internal:/node/831";s:5:"title";s:15:"Events Calendar";s:7:"options";a:0:{}}i:7;a:3:{s:3:"uri";s:18:"internal:/node/396";s:5:"title";s:8:"Newsroom";s:7:"options";a:0:{}}i:8;a:3:{s:3:"uri";s:20:"internal:/node/50171";s:5:"title";s:12:"Plan a Visit";s:7:"options";a:0:{}}i:9;a:3:{s:3:"uri";s:20:"internal:/campusmap/";s:5:"title";s:10:"Campus Map";s:7:"options";a:0:{}}i:10;a:3:{s:3:"uri";s:137:"https://host.nxt.blackbaud.com/donor-form?svcid=renxt&formId=fd285a9c-3301-4472-94d0-1562f8517842&envid=p-AJWGzZxxCEaq7gUjd-FBDw&zone=usa";s:5:"title";s:11:"Make a Gift";s:7:"options";a:0:{}}}s:28:"field_cp_wrapper_ibmc_pf_cta";a:4:{i:0;a:3:{s:3:"uri";s:20:"internal:/node/49656";s:5:"title";s:12:"How to Apply";s:7:"options";a:0:{}}i:1;a:3:{s:3:"uri";s:67:"https://admissions.brynmawr.edu/register/ug-request-for-information";s:5:"title";s:19:"Request Information";s:7:"options";a:0:{}}i:2;a:3:{s:3:"uri";s:20:"internal:/node/49746";s:5:"title";s:15:"Schedule a Tour";s:7:"options";a:0:{}}i:3;a:3:{s:3:"uri";s:71:"internal:/inside/offices-services/alumnaei-relations-development/giving";s:5:"title";s:12:"Ways to Give";s:7:"options";a:0:{}}}s:30:"field_cp_wrapper_ibmc_pf_title";a:1:{i:0;a:1:{s:5:"value";s:17:"Inside Pre-Footer";}}s:27:"field_cp_wrapper_ibmc_phone";a:1:{i:0;a:1:{s:5:"value";s:14:"(610) 526-5000";}}s:28:"field_cp_wrapper_mkt_contact";a:1:{i:0;a:3:{s:3:"uri";s:21:"internal:/node/110626";s:5:"title";s:10:"Contact Us";s:7:"options";a:0:{}}}s:30:"field_cp_wrapper_mkt_hlogolink";a:1:{i:0;a:3:{s:3:"uri";s:10:"internal:/";s:5:"title";s:0:"";s:7:"options";a:0:{}}}s:26:"field_cp_wrapper_mkt_links";a:11:{i:0;a:3:{s:3:"uri";s:18:"internal:/node/216";s:5:"title";s:28:"Offices & Services Directory";s:7:"options";a:0:{}}i:1;a:3:{s:3:"uri";s:19:"internal:/node/4976";s:5:"title";s:25:"Faculty & Staff Directory";s:7:"options";a:0:{}}i:2;a:3:{s:3:"uri";s:20:"internal:/node/13111";s:5:"title";s:17:"Work at Bryn Mawr";s:7:"options";a:0:{}}i:3;a:3:{s:3:"uri";s:20:"internal:/node/18016";s:5:"title";s:23:"Libraries & Collections";s:7:"options";a:0:{}}i:4;a:3:{s:3:"uri";s:30:"https://bookshop.brynmawr.edu/";s:5:"title";s:8:"Bookshop";s:7:"options";a:0:{}}i:5;a:3:{s:3:"uri";s:23:"https://gobrynmawr.com/";s:5:"title";s:9:"Athletics";s:7:"options";a:0:{}}i:6;a:3:{s:3:"uri";s:18:"internal:/node/831";s:5:"title";s:15:"Events Calendar";s:7:"options";a:0:{}}i:7;a:3:{s:3:"uri";s:18:"internal:/node/396";s:5:"title";s:8:"Newsroom";s:7:"options";a:0:{}}i:8;a:3:{s:3:"uri";s:20:"internal:/node/50171";s:5:"title";s:12:"Plan a Visit";s:7:"options";a:0:{}}i:9;a:3:{s:3:"uri";s:20:"internal:/campusmap/";s:5:"title";s:10:"Campus Map";s:7:"options";a:0:{}}i:10;a:3:{s:3:"uri";s:137:"https://host.nxt.blackbaud.com/donor-form?svcid=renxt&formId=fd285a9c-3301-4472-94d0-1562f8517842&envid=p-AJWGzZxxCEaq7gUjd-FBDw&zone=usa";s:5:"title";s:11:"Make a Gift";s:7:"options";a:0:{}}}s:27:"field_cp_wrapper_mkt_pf_cta";a:4:{i:0;a:3:{s:3:"uri";s:20:"internal:/node/49656";s:5:"title";s:12:"How to Apply";s:7:"options";a:0:{}}i:1;a:3:{s:3:"uri";s:67:"https://admissions.brynmawr.edu/register/ug-request-for-information";s:5:"title";s:19:"Request Information";s:7:"options";a:0:{}}i:2;a:3:{s:3:"uri";s:20:"internal:/node/49746";s:5:"title";s:15:"Schedule a Tour";s:7:"options";a:0:{}}i:3;a:3:{s:3:"uri";s:71:"internal:/inside/offices-services/alumnaei-relations-development/giving";s:5:"title";s:12:"Ways to Give";s:7:"options";a:0:{}}}s:29:"field_cp_wrapper_mkt_pf_title";a:1:{i:0;a:1:{s:5:"value";s:18:"Take the Next Step";}}s:26:"field_cp_wrapper_mkt_phone";a:1:{i:0;a:1:{s:5:"value";s:14:"(610) 526-5000";}}s:30:"field_cp_wrapper_pstbc_contact";a:1:{i:0;a:3:{s:3:"uri";s:27:"mailto:postbac@brynmawr.edu";s:5:"title";s:20:"postbac@brynmawr.edu";s:7:"options";a:0:{}}}s:32:"field_cp_wrapper_pstbc_hlogolink";a:1:{i:0;a:3:{s:3:"uri";s:17:"internal:/postbac";s:5:"title";s:0:"";s:7:"options";a:0:{}}}s:28:"field_cp_wrapper_pstbc_links";a:11:{i:0;a:3:{s:3:"uri";s:18:"internal:/node/216";s:5:"title";s:28:"Offices & Services Directory";s:7:"options";a:0:{}}i:1;a:3:{s:3:"uri";s:19:"internal:/node/4976";s:5:"title";s:25:"Faculty & Staff Directory";s:7:"options";a:0:{}}i:2;a:3:{s:3:"uri";s:20:"internal:/node/13111";s:5:"title";s:17:"Work at Bryn Mawr";s:7:"options";a:0:{}}i:3;a:3:{s:3:"uri";s:20:"internal:/node/18016";s:5:"title";s:23:"Libraries & Collections";s:7:"options";a:0:{}}i:4;a:3:{s:3:"uri";s:30:"https://bookshop.brynmawr.edu/";s:5:"title";s:8:"Bookshop";s:7:"options";a:0:{}}i:5;a:3:{s:3:"uri";s:23:"https://gobrynmawr.com/";s:5:"title";s:9:"Athletics";s:7:"options";a:0:{}}i:6;a:3:{s:3:"uri";s:18:"internal:/node/831";s:5:"title";s:15:"Events Calendar";s:7:"options";a:0:{}}i:7;a:3:{s:3:"uri";s:18:"internal:/node/396";s:5:"title";s:8:"Newsroom";s:7:"options";a:0:{}}i:8;a:3:{s:3:"uri";s:20:"internal:/node/50171";s:5:"title";s:12:"Plan a Visit";s:7:"options";a:0:{}}i:9;a:3:{s:3:"uri";s:20:"internal:/campusmap/";s:5:"title";s:10:"Campus Map";s:7:"options";a:0:{}}i:10;a:3:{s:3:"uri";s:137:"https://host.nxt.blackbaud.com/donor-form?svcid=renxt&formId=fd285a9c-3301-4472-94d0-1562f8517842&envid=p-AJWGzZxxCEaq7gUjd-FBDw&zone=usa";s:5:"title";s:11:"Make a Gift";s:7:"options";a:0:{}}}s:29:"field_cp_wrapper_pstbc_pf_cta";a:3:{i:0;a:3:{s:3:"uri";s:20:"internal:/node/56426";s:5:"title";s:12:"How to Apply";s:7:"options";a:0:{}}i:1;a:3:{s:3:"uri";s:69:"https://admissions.brynmawr.edu/register/grad-request-for-information";s:5:"title";s:19:"Request Information";s:7:"options";a:0:{}}i:2;a:3:{s:3:"uri";s:71:"internal:/inside/offices-services/alumnaei-relations-development/giving";s:5:"title";s:12:"Ways to Give";s:7:"options";a:0:{}}}s:31:"field_cp_wrapper_pstbc_pf_title";a:1:{i:0;a:1:{s:5:"value";s:18:"Take the Next Step";}}s:28:"field_cp_wrapper_pstbc_phone";a:1:{i:0;a:1:{s:5:"value";s:12:"610-526-7350";}}s:27:"field_cp_wrapper_sw_contact";a:1:{i:0;a:3:{s:3:"uri";s:30:"mailto:socialwork@brynmawr.edu";s:5:"title";s:23:"socialwork@brynmawr.edu";s:7:"options";a:0:{}}}s:29:"field_cp_wrapper_sw_hlogolink";a:1:{i:0;a:3:{s:3:"uri";s:20:"internal:/socialwork";s:5:"title";s:0:"";s:7:"options";a:0:{}}}s:25:"field_cp_wrapper_sw_links";a:11:{i:0;a:3:{s:3:"uri";s:18:"internal:/node/216";s:5:"title";s:28:"Offices & Services Directory";s:7:"options";a:0:{}}i:1;a:3:{s:3:"uri";s:19:"internal:/node/4976";s:5:"title";s:25:"Faculty & Staff Directory";s:7:"options";a:0:{}}i:2;a:3:{s:3:"uri";s:20:"internal:/node/13111";s:5:"title";s:17:"Work at Bryn Mawr";s:7:"options";a:0:{}}i:3;a:3:{s:3:"uri";s:20:"internal:/node/18016";s:5:"title";s:23:"Libraries & Collections";s:7:"options";a:0:{}}i:4;a:3:{s:3:"uri";s:30:"https://bookshop.brynmawr.edu/";s:5:"title";s:8:"Bookshop";s:7:"options";a:0:{}}i:5;a:3:{s:3:"uri";s:23:"https://gobrynmawr.com/";s:5:"title";s:9:"Athletics";s:7:"options";a:0:{}}i:6;a:3:{s:3:"uri";s:18:"internal:/node/831";s:5:"title";s:15:"Events Calendar";s:7:"options";a:0:{}}i:7;a:3:{s:3:"uri";s:18:"internal:/node/396";s:5:"title";s:8:"Newsroom";s:7:"options";a:0:{}}i:8;a:3:{s:3:"uri";s:20:"internal:/node/50171";s:5:"title";s:12:"Plan a Visit";s:7:"options";a:0:{}}i:9;a:3:{s:3:"uri";s:20:"internal:/campusmap/";s:5:"title";s:10:"Campus Map";s:7:"options";a:0:{}}i:10;a:3:{s:3:"uri";s:137:"https://host.nxt.blackbaud.com/donor-form?svcid=renxt&formId=fd285a9c-3301-4472-94d0-1562f8517842&envid=p-AJWGzZxxCEaq7gUjd-FBDw&zone=usa";s:5:"title";s:11:"Make a Gift";s:7:"options";a:0:{}}}s:26:"field_cp_wrapper_sw_pf_cta";a:4:{i:0;a:3:{s:3:"uri";s:20:"internal:/node/10881";s:5:"title";s:12:"How to Apply";s:7:"options";a:0:{}}i:1;a:3:{s:3:"uri";s:20:"internal:/node/10821";s:5:"title";s:8:"Visit Us";s:7:"options";a:0:{}}i:2;a:3:{s:3:"uri";s:69:"https://admissions.brynmawr.edu/register/grad-request-for-information";s:5:"title";s:19:"Request Information";s:7:"options";a:0:{}}i:3;a:3:{s:3:"uri";s:71:"internal:/inside/offices-services/alumnaei-relations-development/giving";s:5:"title";s:12:"Ways to Give";s:7:"options";a:0:{}}}s:28:"field_cp_wrapper_sw_pf_title";a:1:{i:0;a:1:{s:5:"value";s:18:"Take the Next Step";}}s:25:"field_cp_wrapper_sw_phone";a:1:{i:0;a:1:{s:5:"value";s:12:"610-520-2600";}}}';
  $unserialized_data = unserialize($serialized_data);

  // Get the config_pages entity.
  $config_page_loader = \Drupal::service('config_pages.loader');
  $header_footer_wrapper = $config_page_loader->load('header_footer_wrapper');

  // Loop through the unserialized data and set the values on the config_pages entity.
  foreach ($unserialized_data as $field_name => $field_data) {
    $header_footer_wrapper->set($field_name, $field_data);
  }

  // Get the services we need to copy our files into the file system.
  /** @var \Drupal\Core\Extension\ExtensionList $extension_list */
  $extension_list = \Drupal::service('extension.list.module');
  $assetsDirectory = $extension_list->getPath('bmc_custom') . '/assets/2025_reskin_assets/';
  // Create the directory if it doesn't exist. We'll just use the current month.
  $directory = "public://2025-01";
  /** @var \Drupal\Core\File\FileSystemInterface $file_system */
  $file_system = \Drupal::service('file_system');
  $file_system->prepareDirectory($directory, FileSystemInterface:: CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);

  /** @var \Drupal\file\FileUsage\DatabaseFileUsageBackend $file_usage */
  $file_usage = \Drupal::service('file.usage');

  $marketing_alt = 'Bryn Mawr College';
  $sw_alt = 'Bryn Mawr College Graduate School of Social Work and Social Research';
  $pstbc_alt = 'Bryn Mawr College Postbaccalaureate Premedical Program';
  $gsas_alt = 'Bryn Mawr College Graduate School of Arts and Sciences';

  // An array of all of our logos.
  $assets = [
    'bmc_desktop' => [
      'file' => 'bmc_web_logo_with_seal_left_aligned.svg',
      'alt' => $marketing_alt,
    ],
    'bmc_header_mobile' => [
      'file' => 'bmc_web_logo_without_seal_left_aligned.svg',
      'alt' => $marketing_alt,
    ],
    'bmc_footer_mobile' => [
      'file' => 'bmc_web_logo_without_seal_centered.svg',
      'alt' => $marketing_alt,
    ],
    'gsas_desktop' => [
      'file' => 'gsas_web_logo_with_seal_left_aligned.svg',
      'alt' => $gsas_alt,
    ],
    'gsas_header_mobile' => [
      'file' => 'gsas_web_logo_without_seal_left_aligned.svg',
      'alt' => $gsas_alt,
    ],
    'gsas_footer_mobile' => [
      'file' => 'gsas_web_logo_without_seal_centered.svg',
      'alt' => $gsas_alt,
    ],
    'gsswsr_desktop' => [
      'file' => 'gsswsr_web_logo_with_seal_left_aligned.svg',
      'alt' => $sw_alt,
    ],
    'gsswsr_header_mobile' => [
      'file' => 'gsswsr_web_logo_without_seal_left_aligned.svg',
      'alt' => $sw_alt,
    ],
    'gsswsr_footer_mobile' => [
      'file' => 'gsswsr_web_logo_without_seal_centered.svg',
      'alt' => $sw_alt,
    ],
    'postbac_desktop' => [
      'file' => 'postbac_web_logo_with_seal_left_aligned.svg',
      'alt' => $pstbc_alt,
    ],
    'postbac_header_mobile' => [
      'file' => 'postbac_web_logo_without_seal_left_aligned.svg',
      'alt' => $pstbc_alt,
    ],
    'postbac_footer_mobile' => [
      'file' => 'postbac_web_logo_without_seal_centered.svg',
      'alt' => $pstbc_alt,
    ],
    // These aren't logos, but we need them.
    'footer_image_with_window' => [
      'file' => 'footer-image-with-window.jpg',
      'alt' => 'Footer Image with Window',
    ],
    'prefooter_01' => [
      'file' => 'prefooter_01.jpg',
      'alt' => '',
    ],
    'prefooter_02' => [
      'file' => 'prefooter_02.jpg',
      'alt' => '',
    ],
    'prefooter_03' => [
      'file' => 'prefooter_03.jpg',
      'alt' => '',
    ],
    'prefooter_04' => [
      'file' => 'DSC_0325_copy.jpg',
      'alt' => '',
    ],
  ];

  // Loop through the logos, copy them to the file system, and create the file
  // entities.
  foreach ($assets as $asset_key => $asset_name) {
    $filepath = $assetsDirectory . $asset_name['file'];
    $file_system->copy($filepath, $directory . '/' . basename($filepath), \Drupal\Core\File\FileExists::Replace);
    $file = File::create([
      'filename' => basename($filepath),
      'uri' => 'public://2025-01/' . basename($filepath),
      'status' => 1,
      'uid' => 1,
    ]);
    $file->save();
    $assets[$asset_key]['fid'] = $file->id();
    $assets[$asset_key]['entity'] = $file;
  }

  // Now assign the files to the appropriate fields in our config page.
  $image_fields = [
    "field_cp_wrapper_gsas_flogo",
    "field_cp_wrapper_gsas_fmlogo",
    "field_cp_wrapper_gsas_hlogo",
    "field_cp_wrapper_gsas_hmlogo",
    "field_cp_wrapper_ibmc_flogo",
    "field_cp_wrapper_ibmc_fmlogo",
    "field_cp_wrapper_ibmc_hlogo",
    "field_cp_wrapper_ibmc_hmlogo",
    "field_cp_wrapper_mkt_flogo",
    "field_cp_wrapper_mkt_fmlogo",
    "field_cp_wrapper_mkt_hlogo",
    "field_cp_wrapper_mkt_hmlogo",
    "field_cp_wrapper_pstbc_flogo",
    "field_cp_wrapper_pstbc_fmlogo",
    "field_cp_wrapper_pstbc_hlogo",
    "field_cp_wrapper_pstbc_hmlogo",
    "field_cp_wrapper_sw_flogo",
    "field_cp_wrapper_sw_fmlogo",
    "field_cp_wrapper_sw_hlogo",
    "field_cp_wrapper_sw_hmlogo",
  ];

  foreach ($image_fields as $image_field) {
    $pieces = explode('_', $image_field);
    switch ($pieces[3]) {
      case 'mkt':
      case 'ibmc':
        $key_1 = 'bmc';
        break;
      case 'gsas':
        $key_1 = 'gsas';
        break;
      case 'sw':
        $key_1 = 'gsswsr';
        break;
      case 'pstbc':
        $key_1 = 'postbac';
        break;
    }
    switch ($pieces[4]) {
      case 'flogo':
      case 'hlogo';
        $key_2 = 'desktop';
        break;
      case 'fmlogo':
        $key_2 = 'footer_mobile';
        break;
      case 'hmlogo':
        $key_2 = 'header_mobile';
        break;
    }
    $key = $key_1 . '_' . $key_2;
    if (isset($assets[$key])) {
      $image = [
        0 => [
          'target_id' => $assets[$key]['fid'],
          'alt' => $assets[$key]['alt'],
          'title' => '',
        ],
      ];
      $header_footer_wrapper->set($image_field, $image);
      // Do we need to add a file_usage counter here? Probably not.
      $file_usage->add($assets[$asset_key]['entity'], 'config_pages', 'config_pages', $header_footer_wrapper->id());
    }
  }

  // Create the footer image media entity.
  $footer_image_media = Media::create([
    'name' => 'footer-image-with-window',
    'bundle' => 'image',
    'uid' => 1,
    'langcode' => 'en',
    'status' => 1,
    'field_media_image' => [
      'target_id' => $assets['footer_image_with_window']['fid'],
      'alt' => '',
    ],
  ]);
  $footer_image_media->save();

  $footer_media_fields = [
    "field_cp_wrapper_gsas_footer_img",
    "field_cp_wrapper_ibmc_footer_img",
    "field_cp_wrapper_mkt_footer_img",
    "field_cp_wrapper_pstbc_footer_im",
    "field_cp_wrapper_sw_footer_img",
  ];

  // All the footer images use the same asset. So, we can just set them all
  // to the same media entity.
  foreach ($footer_media_fields as $media_field) {
    $header_footer_wrapper->set($media_field, [
      0 => [
        'target_id' => $footer_image_media->id(),
      ],
    ]);
  }

  // Create the prefooter options.
  // The file names and the media names are confusing, but they are correct.
  $marketing_prefooter_media = Media::create([
    'name' => 'Marketing Prefooter Image',
    'bundle' => 'image',
    'uid' => 1,
    'langcode' => 'en',
    'status' => 1,
    'field_media_image' => [
      'target_id' => $assets['prefooter_04']['fid'],
      'alt' => '',
    ],
  ]);
  $marketing_prefooter_media->save();

  $prefooter_2_media = Media::create([
    'name' => 'Prefooter Option 2',
    'bundle' => 'image',
    'uid' => 1,
    'langcode' => 'en',
    'status' => 1,
    'field_media_image' => [
      'target_id' => $assets['prefooter_02']['fid'],
      'alt' => '',
    ],
  ]);
  $prefooter_2_media->save();

  $prefooter_3_media = Media::create([
    'name' => 'Prefooter Option 3',
    'bundle' => 'image',
    'uid' => 1,
    'langcode' => 'en',
    'status' => 1,
    'field_media_image' => [
      'target_id' => $assets['prefooter_01']['fid'],
      'alt' => '',
    ],
  ]);
  $prefooter_3_media->save();

  $prefooter_4_media = Media::create([
    'name' => 'Prefooter Option 4',
    'bundle' => 'image',
    'uid' => 1,
    'langcode' => 'en',
    'status' => 1,
    'field_media_image' => [
      'target_id' => $assets['prefooter_03']['fid'],
      'alt' => '',
    ],
  ]);
  $prefooter_4_media->save();

  // Assign the prefooter media. Some of them are preexisting.
  // Not all of our new prefooter media are used.
  $header_footer_wrapper->set('field_cp_wrapper_mkt_pf_img', [
    0 => [
      'target_id' => $marketing_prefooter_media->id(),
    ],
  ]);

  $header_footer_wrapper->set('field_cp_wrapper_sw_pf_img', [
    0 => [
      'target_id' => 55366,
    ],
  ]);

  $header_footer_wrapper->set('field_cp_wrapper_gsas_pf_img', [
    0 => [
      'target_id' => 47151,
    ],
  ]);

  $header_footer_wrapper->set('field_cp_wrapper_pstbc_pf_img', [
    0 => [
      'target_id' => $prefooter_4_media->id(),
    ],
  ]);

  $header_footer_wrapper->set('field_cp_wrapper_ibmc_pf_img', [
    0 => [
      'target_id' => $prefooter_3_media->id(),
    ],
  ]);

  $header_footer_wrapper->save();
}
