<?php declare(strict_types = 1);

namespace Drupal\bmc_custom\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber.
 */
final class AggregatorRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    /**
     * We remove access to the feed canonical route here. This change
     * should not change who has access to the feed when rendered elsewhere
     * (e.g. when referenced from a paragraph).
     *
     * We also change the permissions in the views (/aggregator/sources)
     * in the view configuration, and we commit that configuration.
     */
    if ($route = $collection->get('entity.aggregator_feed.canonical')) {
      $route->setRequirement('_permission', 'administer news feeds');
    }
  }

}
