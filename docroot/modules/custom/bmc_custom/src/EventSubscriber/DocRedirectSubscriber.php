<?php

namespace Drupal\bmc_custom\EventSubscriber;

use Symfony\Component\HttpKernel\Event\RequestEvent;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\media\Entity\Media;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\file\Entity\File;

/**
 * Class DocRedirectSubscriber.
 *
 * @package Drupal\bmc_custom\EventSubscriber
 */
class DocRedirectSubscriber implements EventSubscriberInterface {

  /**
   * Cache kill switch.
   *
   * @var KillSwitch
   */
  protected $cacheKiller;

  /**
   * DocRedirectSubscriber constructor.
   *
   * @param KillSwitch $kill_switch
   *   Inject KillSwitch service.
   */
  public function __construct(KillSwitch $kill_switch) {
    $this->cacheKiller = $kill_switch;
  }

  /**
   * Redirect a media doc entity to its document file.
   *
   * @param GetResponseEvent $event
   *   Response event.
   */
  public function docFileRedirect(RequestEvent $event) {
    $media = $event->getRequest()->attributes->get('media');
    $route_name = $event->getRequest()->attributes->get('_route');

    if ($route_name === 'entity.media.canonical' && $media instanceof Media && $media->bundle() === 'file') {
      if (strpos($_REQUEST["destination"], '/admin/content/media') !== 0 ) {
        $this->cacheKiller->trigger();

        $file = $media->get('field_media_file')->getValue();
        $file_id = $file[0]['target_id'];
        $doc = File::load($file_id);
        $doc_url = \Drupal::service('file_url_generator')->generateAbsoluteString($doc->get('uri')->value);

        $event->setResponse(new RedirectResponse($doc_url));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['docFileRedirect'];

    return $events;
  }

}
