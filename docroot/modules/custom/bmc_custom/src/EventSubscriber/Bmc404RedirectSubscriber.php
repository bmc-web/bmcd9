<?php

namespace Drupal\bmc_custom\EventSubscriber;

use Drupal\config_pages\ConfigPagesLoaderServiceInterface;
use Drupal\Core\EventSubscriber\HttpExceptionSubscriberBase;
use Drupal\Core\Path\CurrentPathStack;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * BMC Custom event subscriber.
 */
class Bmc404RedirectSubscriber extends HttpExceptionSubscriberBase {
  /**
   * The config loader service.
   */
  protected $configLoader;

  /**
   * The current path stack service.
   */
  protected $currentPath;

  /**
   * Constructs event subscriber.
   */
  public function __construct(ConfigPagesLoaderServiceInterface $configLoader, CurrentPathStack $currentPath) {
    $this->configLoader = $configLoader;
    $this->currentPath = $currentPath;
  }

  /**
   * On a 404, check if a 440 redirect is registered. If so, use it.
   */
  public function on404(ExceptionEvent $event) {
    // Tge config page.
    // If there are no values, this will be null.
    $configPage = $this->configLoader->load('404_regex_redirects');
    // The path from the request.
    $path = $this->currentPath->getPath();

    if ($configPage) {
      $paragraphs = $this->configLoader->load('404_regex_redirects')->field_cp_regex_redirect->referencedEntities();
      foreach ($paragraphs as $paragraph) {
        // Stop the foreeach loop as soon as a regex is found.
        $loopBreak = FALSE;
        $regex = $paragraph->field_p_404_redirect_regex->value;
        $redirectPath = $paragraph->field_p_404_redirect_path->value;
        if (preg_match($regex, $path)) {
          $event->setResponse(new RedirectResponse($event->getRequest()->getSchemeAndHttpHost() . $redirectPath));
          $loopBreak = TRUE;
        }
        if ($loopBreak) {
          break;
        }
      }
    }
  }

  /**
   *
   */
  protected function getHandledFormats() {
    return ['html'];
  }

}
