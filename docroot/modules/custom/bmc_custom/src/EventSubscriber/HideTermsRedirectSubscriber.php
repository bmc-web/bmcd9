<?php

namespace Drupal\bmc_custom\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class DocRedirectSubscriber.
 *
 * @package Drupal\bmc_custom\EventSubscriber
 */
class HideTermsRedirectSubscriber implements EventSubscriberInterface {

  /**
   * The current user.
   *
   * @var AccountInterface
   */
  protected $current_user;

  /**
   * The entity type manager.
   *
   * @var EntitytypeManagerInterface
   */
  protected $entity_type_manager;

  /**
   * HideTermsRedirectSubscriber constructor.
   *
   * @param AccountInterface $current_user
   * @param EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(AccountInterface $current_user, EntityTypeManagerInterface $entity_type_manager) {
    $this->current_user = $current_user;
    $this->entity_type_manager = $entity_type_manager;
  }

  /**
   * Redirect a term if its vocabulary .
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   Response event.
   */
  public function hideVocabularyRedirect(RequestEvent $event) {
    $route_name = $event->getRequest()->attributes->get('_route');

    if ($route_name === 'entity.taxonomy_term.canonical'  && !$this->current_user->hasPermission('bypass taxonomy term redirect')) {
      // todo: add some validation to this;
      $vid = $event->getRequest()->attributes->get('taxonomy_term')->bundle();
      $vocabulary = $this->entity_type_manager->getStorage('taxonomy_vocabulary')->load($vid);
      if ($vocabulary->getThirdPartySetting('bmc_custom', 'hide_terms')) {
        $redirect_path = $vocabulary->getThirdPartySetting('bmc_custom', 'redirect_path');
        if ($redirect_path) {
          $event->setResponse(new RedirectResponse($event->getRequest()->getSchemeAndHttpHost() . $redirect_path));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['hideVocabularyRedirect'];
    return $events;
  }

}
