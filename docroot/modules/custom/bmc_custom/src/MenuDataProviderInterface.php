<?php

namespace Drupal\bmc_custom;

interface MenuDataProviderInterface {

  public function getMenuData(string $menu_name = 'main'): array;

}
