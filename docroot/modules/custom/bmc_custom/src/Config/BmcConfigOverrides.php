<?php

namespace Drupal\bmc_custom\Config;

use Drupal\config_pages\ConfigPagesLoaderServiceInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;

/**
 * Example configuration override.
 */
class BmcConfigOverrides implements ConfigFactoryOverrideInterface {

  /**
   * The config pages loader service.
   *
   * @var \Drupal\config_pages\ConfigPagesLoaderServiceInterface
   */
  protected ConfigPagesLoaderServiceInterface $configPagesLoader;

  /**
   * The cache tags invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected CacheTagsInvalidatorInterface $cacheTagsInvalidator;

  public function __construct(
    ConfigPagesLoaderServiceInterface $configPagesLoader,
    CacheTagsInvalidatorInterface $cacheTagsInvalidator
) {
    $this->configPagesLoader = $configPagesLoader;
    $this->cacheTagsInvalidator = $cacheTagsInvalidator;
  }

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    /**
     * Note: This only overrides the system.site config if the relevant
     * config page field is not empty. This, it is always important to
     * have an acceptable value in the system.site config.
     */
    $overrides = [];
    if (in_array('system.site', $names)) {
      // Check if the site_homepage config page exists/
      $siteHomepageConfig = $this->configPagesLoader->load('site_homepage');
      if (
        $siteHomepageConfig &&
        $siteHomepageConfig->hasField('field_cp_site_homepage') &&
        !$siteHomepageConfig->get('field_cp_site_homepage')->isEmpty()
      ) {
        $this->cacheTagsInvalidator->invalidateTags(['route_match']);
        $overrides['system.site']['page']['front'] = $siteHomepageConfig->get('field_cp_site_homepage')->referencedEntities()[0]->toUrl()->toString();
      }
    }
    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return 'bmc_custom_overrides';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    $meta = new CacheableMetadata();
    $meta->addCacheTags(['config_pages_list:site_homepage']);
    return $meta;
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

}
