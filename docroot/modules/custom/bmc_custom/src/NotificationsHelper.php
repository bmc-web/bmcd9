<?php

declare(strict_types = 1);

namespace Drupal\bmc_custom;

use DateTime;
use Drupal\Component\Utility\Html;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\config_pages\ConfigPagesLoaderServiceInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * @todo Add class description.
 */
final class NotificationsHelper implements NotificationsHelperInterface {

  protected const DEFAULT_FROM = 'communications@brynmawr.edu';
  protected const REPLY_REPLYTO = 'communications@brynmawr.edu';
  protected const EVENT_BCC = 'communications@brynmawr.edu';
  protected const ANNCMNT_BCC = 'communications@brynmawr.edu';

  /**
   * {@inheritdoc}
   */
  public function __construct(
    private readonly ConfigPagesLoaderServiceInterface $configPagesLoader,
    private readonly AccountProxyInterface $currentUser,
    private readonly MailManagerInterface $pluginManagerMail,
    private readonly MessengerInterface $messenger,
    private readonly LoggerChannelFactoryInterface $loggerFactory,
  ) {}

  /**
   * Implements hook_mail.
   */
  public function hookMail($key, &$message, $params) {
    switch ($key) {
      case 'event':
        $message['id'] = 'event';
        $message['from'] = $params['event_from'];
        $message['headers']['Reply-To'] = $params['event_reply_to'];
        $message['headers']['Content-Type'] = 'text/html; charset=UTF-8;';
        $message['headers']['From'] = 'Bryn Mawr College' . '<' . $params['event_from'] . '>';
        $message['headers']['Sender'] = $params['event_from'];
        $message['headers']['Return-Path'] = $params['event_from'];
        $message['headers']['Bcc'] = $params['event_bcc'];
        $message['subject'] = $params['subject'];
        $message['format'] = 'text/html';
        $message['body'][] = Html::escape($params['message']);
        break;
      case 'anncmnt':
        $message['id'] = 'anncmnt';
        $message['from'] = $params['anncmnt_from'];
        $message['headers']['Reply-To'] = $params['anncmnt_reply_to'];
        $message['headers']['Content-Type'] = 'text/html; charset=UTF-8;';
        $message['headers']['From'] = 'Bryn Mawr College' . '<' . $params['anncmnt_from'] . '>';
        $message['headers']['Sender'] = $params['anncmnt_from'];
        $message['headers']['Return-Path'] = $params['anncmnt_from'];
        $message['headers']['Bcc'] = $params['anncmnt_bcc'];
        $message['subject'] = $params['subject'];
        $message['format'] = 'text/html';
        $message['body'][] = Html::escape($params['message']);
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sendNotificationEmail(NodeInterface $entity) {
    $params = [];

    // Set Defaults in case Config Page doesn't have a value.
    $params['anncmnt_from'] = $this::DEFAULT_FROM;
    $params['anncmnt_reply_to'] = $this::REPLY_REPLYTO;
    $params['event_from'] = $this::DEFAULT_FROM;
    $params['event_reply_to'] = $this::REPLY_REPLYTO;
    $params['event_bcc'] = $this::EVENT_BCC;
    $params['anncmnt_bcc'] = $this::ANNCMNT_BCC;

    $notifications_config = $this->configPagesLoader->load('email_notifications');
    if (isset($notifications_config->field_cp_notifications_ann_from->value)) {
      $params['anncmnt_from'] = $notifications_config->field_cp_notifications_ann_from->value;
    }
    if (isset($notifications_config->field_cp_notifications_ann_reply->value)) {
      $params['anncmnt_reply_to'] = $notifications_config->field_cp_notifications_ann_reply->value;
    }
    if (isset($notifications_config->field_cp_notifications_ev_from->value)) {
      $params['event_from'] = $notifications_config->field_cp_notifications_ev_from->value;
    }
    if (isset($notifications_config->field_cp_notifications_ev_reply->value)) {
      $params['event_reply_to'] = $notifications_config->field_cp_notifications_ev_reply->value;
    }
    if (!empty($notifications_config->field_cp_notifications_event)) {
      $params['event_bcc'] = '';
      foreach ($notifications_config->field_cp_notifications_event as $event_bcc) {
        $params['event_bcc'] .= $event_bcc->value . ',';
      }
      $params['event_bcc'] = rtrim($params['event_bcc'], ", ");
    }
    if (!empty($notifications_config->field_cp_notifications_anncmnt)) {
      $params['anncmnt_bcc'] = '';
      foreach ($notifications_config->field_cp_notifications_anncmnt as $anncmnt_bcc) {
        $params['anncmnt_bcc'] .= $anncmnt_bcc->value . ',';
      }
      $params['anncmnt_bcc'] = rtrim($params['anncmnt_bcc'], ", ");
    }
    $key = '';
    if ($entity->bundle() === 'event') {
      $key = 'event';
      $params['subject'] = 'Event Submission Confirmation: ' . $entity->getTitle();
      $params['message'] = "<p>Thank you for completing the Event/Daily Digest submission form!</p>" .
        "<p>Before publication, all submissions undergo a review and approval process by Conferences and Events and College Communications. You may be contacted by email as part of that process.</p>" .
        "<p>Once submitted, event details cannot be changed using the form. If you need to alter any details of your submission, please contact <a href='mailto:roomres@brynmawr.edu'>roomres@brynmawr.edu</a>.</p>";
      $params['message'] .= '<p><strong>Event Title</strong>: ' . '"' . $entity->getTitle() . '"' . '<br />';
      if (isset($entity->field_event_type[0])) {
        $event_types = '<strong>Event Types</strong>: ';
        foreach ($entity->field_event_type as $event_type_ref) {
          if (isset($event_type_ref->target_id)) {
            $event_types .= (Term::load($event_type_ref->target_id))->getName() . ',';
          }
        }
        $event_types = rtrim($event_types, ", ");
        $params['message'] .= $event_types . '<br />';
      }
      if (isset($entity->field_event_audience[0]->target_id)) {
        $event_audience = (Term::load($entity->field_event_audience[0]->target_id))->getName();
        $params['message'] .= '<strong>Event Audience</strong>: ' . $event_audience . '<br />';
      }
      // Code for Event Form Restructure 9/2023.
      if (isset($entity->field_event_location_choice->value)) {
        $allowed_values = $entity->field_event_location_choice->getFieldDefinition()->getSetting('allowed_values');
        $choice_value = $entity->get('field_event_location_choice')->value;
        $params['message'] .= '<strong>Where is the Event</strong>: ' . $allowed_values[$choice_value] . '<br />';
      }
      // Code for Event Form Restructure 9/2023.
      if (isset($entity->field_event_reservation_number->value)) {
        $params['message'] .= '<strong>Event Reservation Number</strong>: ' . $entity->field_event_reservation_number->value . '<br />';
      }

      if (isset($entity->field_event_location[0]->target_id)) {
        $event_location = (Term::load($entity->field_event_location[0]->target_id))->getName();
        $params['message'] .= '<strong>Event Campus Location</strong>: ' . $event_location . '<br />';
      }
      // Code for Event Form Restructure 9/2023.
      if (isset($entity->field_event_food->value)) {
        $location_value = $entity->get('field_event_location_choice')->value;
        if (($location_value === 'on_campus') || (($location_value === 'hybrid_on_campus'))) {
          $food_choice = $entity->get('field_event_food')->value;
          if ($food_choice != '0') {
            $food_choice_value = 'Yes, by selecting this I certify that a catering contract or waiver has been completed for this event.';
          }
          else {
            $food_choice_value = 'No';
          }
          $params['message'] .= '<strong>Will there be Food at the Event</strong>: ' . $food_choice_value . '<br />';
        }
      }


      if (isset($entity->field_event_location_details->value)) {
        $params['message'] .= '<strong>Event Location Details</strong>: ' . $entity->field_event_location_details->value . '<br />';
      }
      // code for Event Form Restructure 9/2023
      if (isset($entity->field_virtual_event_access_link->uri)) {
        $params['message'] .= '<strong>Virtual Event Access Link</strong>: ' . $entity->field_virtual_event_access_link->uri . '<br />';
      }
      if (isset($entity->field_event_date->value)) {
        $event_date = bmc_custom_preprocess_smart_date_field($entity);
        $params['message'] .= '<strong>Event Start Date</strong>: ' . $event_date['start_date'] . ' ' . $event_date['start_month'] . ', ' . $event_date['start_year'] . '<br />';
        if (isset($event_date['end_date'])) {
          $params['message'] .= '<strong>Event End Date</strong>: ' . $event_date['end_date'] . ' ' . $event_date['end_month'] . ', ' . $event_date['end_year'] . '<br />';
        }
        else {
          $params['message'] .= '<strong>Event End Date</strong>: ' . $event_date['start_date'] . ' ' . $event_date['start_month'] . ', ' . $event_date['start_year'] . '<br />';
        }

        if ($event_date['all_day'] == TRUE) {
          $params['message'] .= '<strong>Event Time</strong>: All Day <br />';
        }
        else {
          $params['message'] .= '<strong>Event Start Time</strong>: ' . $event_date['start_time'] . '<br />';
          $params['message'] .= '<strong>Event End Time</strong>: ' . $event_date['end_time'] . '<br />';
        }

      }
      if (isset($entity->field_event_submitter_email->value)) {
        $params['to'] = $entity->field_event_submitter_email->value;
        $params['message'] .= '<strong>Event Submitter</strong>: ' . $entity->field_event_submitter_email->value . '<br />';
      }
      if (isset($entity->field_event_contact_name->value)) {
        $params['message'] .= '<strong>Event Contact</strong>: ' . $entity->field_event_contact_name->value . '<br />';
      }
      if (isset($entity->field_event_contact_email->value)) {
        $params['message'] .= '<strong>Event Contact Email</strong>: ' . $entity->field_event_contact_email->value . '<br />';
      }
      if (isset($entity->field_event_blurb->value)) {
        $params['message'] .= '<strong>Event Blurb</strong>: ' . $entity->field_event_blurb->value . '<br />';
      }
      if (isset($entity->field_event_body->value)) {
        $params['message'] .= '<strong>Event Description</strong>: ' . $entity->field_event_body->value . '<br />';
      }
      $params['message'] .= '</p>';
      if ($entity->field_ddigest_nopub->value === 1) {
        $params['message'] .= '<strong><strong>DO NOT PUBLISH IN THE DAILY DIGEST</strong></p>';
      }
      else {
        if (!empty($entity->field_ddigest_date)) {
          $params['message'] .= '<strong>Daily Digest Date(s)</strong>: ';
          $ddates = '';
          foreach ($entity->field_ddigest_date as $ddate) {
            $ddate_value = DateTime::createFromFormat('Y-m-d', $ddate->value);
            $ddate_formatted = date_format($ddate_value, 'M j, Y');
            $ddates .= $ddate_formatted . '; ';
          }
          $params['message'] .= rtrim($ddates, "; ") . '</p>';
        }
        $params['message'] .= "<p> If Daily Digest dates were selected, this event will appear on those dates, unless the submission does not meet the <a href='https://www.brynmawr.edu/sites/default/files/media/documents/2023-08/Events%20and%20Announcements%20Submission%20and%20Management%20Policy.pdf'>Events and Announcements Submission and Management Policy</a>.</p>";
      }
    }
    if ($entity->bundle() === 'anncmnt') {
      $key = 'anncmnt';
      $params['subject'] = 'Announcement Submission Confirmation: ' . $entity->getTitle();
      $params['message'] = "<p>Thank you for completing the Announcement/Daily Digest submission form!</p>" .
        "<p>Before publication, all submissions undergo a review and approval process by College Communications. You may be contacted by email as part of that process.</p>" .
        "<p>Once submitted, event details cannot be changed using the form. If you need to alter any details of your submission, please contact <a href='mailto:communications@brynmawr.edu'>communications@brynmawr.edu</a>.</p>";
      $params['message'] .= '<p><strong>Announcement Title</strong>: ' . '"' . $entity->getTitle() . '"' . '<br />';

      if (isset($entity->field_anncmnt_blurb->value)) {
        $params['message'] .= '<strong>Announcement Blurb</strong>:</p>' . $entity->field_anncmnt_blurb->value . '<br />';
      }
      if (isset($entity->field_anncmnt_body->value)) {
        $params['message'] .= '<strong>Announcement Text</strong>:</p>' . $entity->field_anncmnt_body->value . '<br />';
      }
      if (isset($entity->field_anncmnt_submitter->value)) {
        $params['to'] = $entity->field_anncmnt_submitter->value;
        $params['message'] .= '<strong>Announcement Submitter</strong>: ' . $entity->field_anncmnt_submitter->value . '<br />';
      }
      if (isset($entity->field_anncmnt_url->uri)) {
        $url = Url::fromUri($entity->field_anncmnt_url->uri)->toString();
        $params['message'] .= '<strong>Announcement External Link</strong>: '
          . '<a href="'
          . $url
          . '">'
          . $url
          . '</a></p>';
      }

      $params['message'] .= '</p>';
      if ($entity->field_ddigest_nopub->value === 1) {
        $params['message'] .= '<strong><strong>DO NOT PUBLISH IN THE DAILY DIGEST</strong></p>';
      }
      else {
        if ($entity->field_ddigest_sec->value) {
          $params['message'] .= '<strong>Daily Digest Section: </strong>' . $entity->field_ddigest_sec->value . '<br />';
        }
        if (!empty($entity->field_ddigest_date)) {
          $params['message'] .= '<strong>Daily Digest Date(s)</strong>: ';
          $ddates = '';
          foreach ($entity->field_ddigest_date as $ddate) {
            $ddate_value = DateTime::createFromFormat('Y-m-d', $ddate->value);
            $ddate_formatted = date_format($ddate_value, 'M j, Y');
            $ddates .= $ddate_formatted . '; ';
          }
          $params['message'] .= rtrim($ddates, "; ") . '</p>';
        }
        $params['message'] .= "<p> If Daily Digest dates were selected, this event will appear on those dates, unless the submission does not meet the <a href='https://www.brynmawr.edu/sites/default/files/media/documents/2023-08/Events%20and%20Announcements%20Submission%20and%20Management%20Policy.pdf'>Events and Announcements Submission and Management Policy</a>.</p>";
      }
    }
    $langcode = $this->currentUser->getPreferredLangcode();
    $module = 'bmc_custom';
    $send = TRUE;
    $to = $params['to'];
    $result = $this->pluginManagerMail->mail($module, $key, $to, $langcode, $params, NULL, $send);
    if ($result['result'] != true) {
      $message = t('There was a problem sending your email notification to @email.', array('@email' => $to));
      $this->messenger->addError($message);
      $this->loggerFactory->get('mail-log')->error($message);
      return;
    }
    $message = t('An email notification has been sent to @email ', array('@email' => $to));
    $this->messenger->addMessage($message);
    $this->loggerFactory->get('mail-log')->notice($message);
  }

}
