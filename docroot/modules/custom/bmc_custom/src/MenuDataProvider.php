<?php

declare(strict_types=1);

namespace Drupal\bmc_custom;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Url;
use Drupal\path_alias\AliasManagerInterface;

/**
 * Returns the menu tree as an array. Useful for attaching in javascript
 * via drupal settings.
 *
 * The rest_menu_items module does almost everything we want, but it
 * only returns the data as a rest endpoint. If that service could return the
 * data as an array, we wouldn't need our own service.
 */

final class MenuDataProvider implements MenuDataProviderInterface {

  /**
   * A instance of modulehandler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    ModuleHandlerInterface $moduleHandler,
    ConfigFactoryInterface $configFactory,
    AliasManagerInterface $aliasManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->moduleHandler = $moduleHandler;
    $this->configFactory = $configFactory;
    $this->aliasManager = $aliasManager;

  }

  /**
   * Get the menu data.
   *
   * @param string $menu_name
   *  The name of the menu to get.
   *
   * @return array
   *  A render array of the menu data.
   */
  public function getMenuData(string $menu_name = 'main'): array {
    if (empty($menu_name)) {
      return [];
    }

    $parameters = new MenuTreeParameters();
    $parameters->onlyEnabledLinks();

    // Load the tree based on this set of parameters.
    $menu_tree = \Drupal::menuTree();
    $tree = $menu_tree->load($menu_name, $parameters);

    // Transform the tree using the manipulators you want.
    $manipulators = [
      // Only show links that are accessible for the current user.
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      // Use the default sorting of menu links.
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $this->moduleHandler->alter('rest_menu_items_resource_manipulators', $manipulators, $menu_name);
    $tree = $menu_tree->transform($tree, $manipulators);

    // Finally, build a renderable array from the transformed tree.
    $menu = $menu_tree->build($tree);

    // Return if the menu has no entries.
    if (empty($menu['#items'])) {
      return [];
    }

    $menu_items = [];
    $this->getMenuItems($menu['#items'], $menu_items);

    // Other modules can alter the output before rendering.
    $this->moduleHandler->alter('rest_menu_items_output', $menu_items);

    return $menu_items;

  }

  protected function getMenuItems(array $tree, array &$items = []) {
    $config = $this->configFactory->get('rest_menu_items.config');
    $output_values = $config->get('output_values');

    // Loop through the menu items.
    foreach ($tree as $item_value) {
      /** @var \Drupal\Core\Menu\MenuLinkInterface $org_link */
      $org_link = $item_value['original_link'];

      /** @var \Drupal\Core\Url $url */
      $url = $item_value['url'];

      $newValue = [];

      foreach ($output_values as $valueKey) {
        if (!empty($valueKey)) {
          $this->getElementValue($newValue, $valueKey, $org_link, $url);
        }
      }

      if (!empty($item_value['below'])) {
        $newValue['below'] = [];
        $this->getMenuItems($item_value['below'], $newValue['below']);
      }

      $items[] = $newValue;
    }
  }

  /**
   * Generate the menu element value.
   *
   * @param array $returnArray
   *   The return array we want to add this item to.
   * @param string $key
   *   The key to use in the output.
   * @param \Drupal\Core\Menu\MenuLinkInterface $link
   *   The link from the menu.
   * @param \Drupal\Core\Url $url
   *   The URL object of the menu item.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getElementValue(array &$returnArray, $key, MenuLinkInterface $link, Url $url) {
    $config = $this->configFactory->get('rest_menu_items.config');
    $external = $url->isExternal();
    $routed = $url->isRouted();
    $existing = TRUE;
    $value = NULL;

    // Check if the url is a <nolink> and do not do anything for some keys.
    $itemsToRemoveWhenNoLink = [
      'uri',
      'alias',
      'absolute',
      'relative',
    ];
    if (!$external && $routed && $url->getRouteName() === '<nolink>' && in_array($key, $itemsToRemoveWhenNoLink)) {
      return;
    }

    if ($external || !$routed) {
      $uri = $url->getUri();
    }
    else {
      try {
        $uri = $url->getInternalPath();
      }
      catch (\UnexpectedValueException $e) {
        $uri = $relative = Url::fromUri($url->getUri())
          ->toString();
        $existing = FALSE;
      }
    }

    switch ($key) {
      case 'key':
        $value = $link->getDerivativeId();
        if (empty($value)) {
          $value = $link->getBaseId();
        }
        break;

      case 'title':
        $value = $link->getTitle();
        break;

      case 'description':
        $value = $link->getDescription();
        break;

      case 'uri':
        $value = $uri;
        break;

      case 'alias':
        if ($routed) {
          $value = ltrim($this->aliasManager->getAliasByPath("/$uri"), '/');
        }
        break;

      case 'external':
        $value = $external;
        break;

      case 'absolute':
        $base_url = $config->get('base_url');

        if ($external) {
          $value = $uri;
        }
        elseif (!$routed) {
          if (empty($base_url)) {
            $url->setAbsolute();
          }

          $value = $url->toString(TRUE)
            ->getGeneratedUrl();

          if (!empty($base_url)) {
            $value = $base_url . $value;
          }
        }
        else {
          $options = [];
          if (empty($base_url)) {
            $options = ['absolute' => TRUE];
          }

          $value = Url::fromUri('internal:/' . $uri, $options)
            ->toString(TRUE)
            ->getGeneratedUrl();

          if (!empty($base_url)) {
            $value = $base_url . $value;
          }
        }
        break;

      case 'relative':
        if (!$external) {
          $value = Url::fromUri('internal:/' . $uri, ['absolute' => FALSE])
            ->toString(TRUE)
            ->getGeneratedUrl();
        }

        if (!$routed) {
          $url->setAbsolute(FALSE);
          $value = $url->toString(TRUE)
            ->getGeneratedUrl();
        }

        if (!$existing) {
          $value = Url::fromUri($url->getUri())
            ->toString();
        }
        break;

      case 'existing':
        $value = $existing;
        break;

      case 'weight':
        $value = $link->getWeight();
        break;

      case 'expanded':
        $value = $link->isExpanded();
        break;

      case 'enabled':
        $value = $link->isEnabled();
        break;

      case 'uuid':
        if (!$external && $routed) {
          $params = Url::fromUri('internal:/' . $uri)
            ->getRouteParameters();
          $entity_type = key($params);

          if (!empty($entity_type) && $this->entityTypeManager->hasDefinition($entity_type)) {
            $entity = $this->entityTypeManager->getStorage($entity_type)
              ->load($params[$entity_type]);
            $value = $entity->uuid();
          }
        }
        break;

      case 'options':
        $value = $link->getOptions();
        break;
    }

    $addFragmentElements = [
      'alias',
      'absolute',
      'relative',
    ];
    if (!empty($config->get('add_fragment')) && in_array($key, $addFragmentElements)) {
      $this->addFragment($value, $link);
    }

    $returnArray[$key] = $value;
  }

  /**
   * Add the fragment to the value if neccesary.
   *
   * @param string $value
   *   The value to add the fragment to. Passed by reference.
   * @param \Drupal\Core\Menu\MenuLinkInterface $link
   *   The link from the menu.
   */
  private function addFragment(&$value, MenuLinkInterface $link) {
    $options = $link->getOptions();
    if (!empty($options) && isset($options['fragment'])) {
      $value .= '#' . $options['fragment'];
    }
  }

}
