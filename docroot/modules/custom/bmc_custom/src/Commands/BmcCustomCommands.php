<?php

namespace Drupal\bmc_custom\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile for bmc_custom.
 */
class BmcCustomCommands extends DrushCommands {
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inehritDoc}
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * @usage bmc_custom-delete-temp-files
   *
   * @command bmc_custom:delete-temp-files
   * @aliases delete-temp-files
   */
  public function deleteTempFiles() {
    $storage = $this->entityTypeManager->getStorage('file');
    $tmp_fids = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('status', '0')->execute();
    if (count($tmp_fids) > 0) {
      $tmp_files = $storage->loadMultiple($tmp_fids);
      $this->output->writeln("\n" . "Temporary Files to Delete" . "\n");
      foreach ($tmp_files as $file) {
        $this->output->writeln($file->getFileName());
      }
      $confirm = $this->io()->confirm('Delete all of these files');
      if ($confirm) {
        foreach ($tmp_files as $file) {
          $file->delete();
        }
        $this->logger()->success('Temporary files deleted.');
      }
      else {
        $this->logger()->alert('Aborting. No files deleted');
      }

    }
    else {
      $this->logger()->success('No temporary files found');
    }
  }

}
