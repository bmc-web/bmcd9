<?php declare(strict_types = 1);

namespace Drupal\bmc_custom;

use Drupal\node\NodeInterface;

/**
 * @todo Add interface description.
 */
interface NotificationsHelperInterface {

  /**
   * Implements hook_mail().
   */
  public function hookMail($key, &$message, $params);

  /**
   * Sends notification emails for unpublished Announcement & Event nodes.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @throws \Exception
   */
  public function sendNotificationEmail(NodeInterface $entity);

}
