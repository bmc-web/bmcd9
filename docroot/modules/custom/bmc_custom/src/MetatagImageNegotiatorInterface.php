<?php declare(strict_types = 1);

namespace Drupal\bmc_custom;

use Drupal\node\NodeInterface;

/**
 * Defines functionality for the Metatag Image Negotiator service.
 */
interface MetatagImageNegotiatorInterface {

  /**
   * Return the path to the default metatag placeholder image.
   */
  public function getDefaultMetatagImagePath(): string;

  /**
   * Return the path to the default metatag placeholder image.
   */
  public function getDefaultMetatagImageType(): string;

  /**
   * Return the height of the default metatag placeholder image.
   */
  public function getDefaultMetatagImageWidth(): int;

  /**
   * Return the wioth of the default metatag placeholder image.
   */
  public function getDefaultMetatagImageHeight(): int;

  /**
   * Return the alt text for the default metatag placeholder image.
   */
  public function getDefaultMetatagImageAlt(): string;

  /**
   * Return the path to the metatag image for a node.
   */
  public function getMetatagImagePathForNode(NodeInterface $node): string;

  /**
   * Return the mimetype of the metatag image for a node.
   */
  public function getMetatagImageTypeForNode(NodeInterface $node): string;

  /**
   * Return the width of the metatag image for a node.
   */
  public function getMetatagImageWidthForNode(NodeInterface $node): int;

  /**
   * Return the height of the metatag image for a node.
   */
  public function getMetatagImageHeightForNode(NodeInterface $node): int;

  /**
   * Return the alt text of the metatag image for a node.
   */
  public function getMetatagImageAltForNode(NodeInterface $node): string;


  /**
   * Negotiate the metatag image for a file.
   */
  public function negotiateFile(
    int|string $fid,
    string $alt,
    int|string $width,
    int|string $height,
  ): array|false;

}
