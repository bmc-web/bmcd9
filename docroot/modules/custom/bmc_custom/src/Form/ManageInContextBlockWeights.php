<?php declare(strict_types = 1);

namespace Drupal\bmc_custom\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Reorder the weight of in context blocks.
 *
 * This is mostly taken from the examples module.
 */
final class ManageInContextBlockWeights extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * Construct a form.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'bmc_custom_manage_in_context_block_weights';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['table-row'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Block Name'),
        $this->t('Is Active'),
        $this->t('Paths'),
        $this->t('Weight'),
      ],
      '#empty' => $this->t('Sorry, There are no items!'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
    ];

    $results = $this->entityTypeManager->getStorage('block_content')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', 'in_context_block')
      ->sort('field_weight')
      ->execute();
    $blocks = $this->entityTypeManager->getStorage('block_content')
      ->loadMultiple($results);
    /** @var \Drupal\block_content\Entity\BlockContent $block */
    foreach ($blocks as $block) {
      $form['table-row'][$block->id()]['#attributes']['class'][] = 'draggable';
      $form['table-row'][$block->id()]['block_description'] = [
        '#markup' => $block->toLink($block->label(), 'edit-form')->toString(),
      ];
      $form['table-row'][$block->id()]['block_is_active'] = [
        '#markup' => $block->get('field_active')->value ? 'Yes' : 'No',
      ];
      $form['table-row'][$block->id()]['block_paths'] = $this->calculatePathsColumnValue($block);
      $form['table-row'][$block->id()]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $block->label()]),
        '#title_display' => 'invisible',
        '#default_value' => $block->get('field_weight')->value,
        '#attributes' => ['class' => ['table-sort-weight']],
        '#delta' => count($blocks)
      ];
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save All Changes'),
    ];
    $form['actions']['cancel'] = [
      '#type' => 'submit',
      '#value' => 'Cancel',
      '#attributes' => [
        'title' => $this->t('Return to TableDrag Overview'),
      ],
      '#submit' => ['::cancel'],
      '#limit_validation_errors' => [],
    ];

    return $form;
  }

  /**
   * Form submission handler for the 'Return to' action.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function cancel(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.block_content.collection');
  }

  /**
   * Form submission handler for the simple form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $rows = $form_state->getValue('table-row');
    foreach ($rows as $id => $value) {
      $block = $this->entityTypeManager->getStorage('block_content')->load($id);
      if (!$block) {
        continue;
      }
      /** @var \Drupal\block_content\Entity\BlockContent $block */
      $block->set('field_weight', $value['weight']);
      $block->save();
    }
  }

  private function calculatePathsColumnValue($block) {
    $value = '';
    if (!$block->get('field_include_block_on_path')->isEmpty()) {
      $value .= '<p><strong>Include:</strong></p>';
      $included_paths = $block->get('field_include_block_on_path')->value;
      $included_paths = explode("\n", $included_paths);
      $value .= "<ul>";
      foreach ($included_paths as $path) {
        $value .= "<li>$path</li>";
      }
      $value .= "</ul>";
    }

    if (!$block->get('field_exclude_block_on_path')->isEmpty()) {
      $value .= '<p><strong>Exclude:</strong></p>';
      $excluded_paths = $block->get('field_exclude_block_on_path')->value;
      $excluded_paths = explode("\n", $excluded_paths);
      $value .= "<ul>";
      foreach ($excluded_paths as $path) {
        $value .= "<li>$path</li>";
      }
      $value .= "</ul>";
    }
    return ['#markup' => $value];
  }

}
