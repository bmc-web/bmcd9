<?php declare(strict_types = 1);

namespace Drupal\bmc_custom;

use Drupal\bmc_bundle_classes\Entity\Node\CustomMetatagImageInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;

/**
 * Functionality for providing values related to metatag imagges.
 *
 * @todo: Abstract all of the consts in this class.
 */
final class MetatagImageNegotiator implements MetatagImageNegotiatorInterface {
  public const SOURCE_MODULE = 'bmc_custom';
  public const SOURCE_CONFIG = 'bmc_custom.settings';
  public const CONFIG_IMAGE_KEY = 'default_og_image';

  public const OG_IMAGE_LARGE_WIDTH = 1200;

  public const OG_IMAGE_LARGE_HEIGHT = 630;

  public const OG_IMAGE_SMALL_WIDTH = 600;

  public const OG_IMAGE_SMALL_HEIGHT = 315;

  public const OG_IMAGE_ALT = 'Bryn Mawr College Lantern Logo';

  public const OG_IMAGE_STYLE_LARGE = 'og_image_large';
  public const OG_IMAGE_STYLE_SMALL = 'og_image_small';

  public ImmutableConfig $config;

  /**
   * Constructs a MetatagImageNegotiator object.
   */
  public function __construct(
    private readonly ConfigFactoryInterface $configFactory,
    private readonly ExtensionPathResolver $extensionPathResolver,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly FileUrlGeneratorInterface $fileUrlGenerator,
    private readonly ImageFactory $imageFactory,
  ) {
    $this->config = $this->configFactory->get(self::SOURCE_CONFIG);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultMetatagImagePath(): string {
    $imagePath = $this->config->get(self::CONFIG_IMAGE_KEY);
    if (empty($imagePath)) {
      return '';
    }
    if (strpos($imagePath, '/') !== 0) {
      $imagePath = '/' . $imagePath;
    }
    return $this->extensionPathResolver->getPath('module', self::SOURCE_MODULE) . $imagePath;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultMetatagImageType(): string {
    // TODO: Don't hardcode this.
    return 'image/jpeg';
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultMetatagImageWidth(): int {
    return self::OG_IMAGE_LARGE_WIDTH;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultMetatagImageHeight(): int {
    return self::OG_IMAGE_LARGE_HEIGHT;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultMetatagImageAlt(): string {
    // TODO: Don't hardcode this.
    return self::OG_IMAGE_ALT;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetatagImagePathForNode(NodeInterface $node): string {
    if ($node instanceof CustomMetatagImageInterface) {
      return $node->getMetatagImagePath();
    }
    return $this->getDefaultMetatagImagePath();
  }

  /**
   * {@inheritdoc}
   */
  public function getMetatagImageTypeForNode(NodeInterface $node): string {
    if ($node instanceof CustomMetatagImageInterface) {
      return $node->getMetatagImageType();
    }
    return $this->getDefaultMetatagImageType();
  }

  /**
   * {@inheritdoc}
   */
  public function getMetatagImageWidthForNode(NodeInterface $node): int {
    if ($node instanceof CustomMetatagImageInterface) {
      return $node->getMetatagImageWidth();
    }
    return self::OG_IMAGE_LARGE_WIDTH;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetatagImageHeightForNode(NodeInterface $node): int {
    if ($node instanceof CustomMetatagImageInterface) {
      return $node->getMetatagImageHeight();
    }
    return self::OG_IMAGE_LARGE_HEIGHT;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetatagImageAltForNode(NodeInterface $node): string {
    if ($node instanceof CustomMetatagImageInterface) {
      return $node->getMetatagImageAlt();
    }
    return $this->getDefaultMetatagImageAlt();
  }

  /**
   * {@inheritdoc}
   */
  public function negotiateFile(
    int|string $fid,
    string $alt,
    int|string  $width,
    int|string $height,
  ): array|false {
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    if (!$file) {
      return false;
    }

    if ($width >= self::OG_IMAGE_LARGE_WIDTH && $height >= self::OG_IMAGE_LARGE_HEIGHT) {
      $image_style = self::OG_IMAGE_STYLE_LARGE;
    }
    elseif ($width >= self::OG_IMAGE_SMALL_WIDTH && $height >= self::OG_IMAGE_SMALL_HEIGHT) {
      $image_style = self::OG_IMAGE_STYLE_SMALL;
    }
    else {
      return false;
    }

    $style = $this->entityTypeManager->getStorage('image_style')->load($image_style);
    if (!$style) {
      return false;
    }

    $destination = $style->buildUri($file->getFileUri());
    $url = $this->fileUrlGenerator->generate($destination);
    $image_path = parse_url($url->toString(), PHP_URL_PATH);
    $image_path = ltrim($image_path, '/');
    $image_path = implode('/', array_map('rawurlencode', explode('/', $image_path)));

    if (!file_exists($destination)) {
      $success = $style->createDerivative($file->getFileUri(), $destination);
      if ($success === false) {
        return false;
      }
    }
    $image = $this->imageFactory->get($style->buildUri($file->getFileUri()));

    return [
      'path' => $image_path,
      'type' => $file->getMimeType(),
      'width' => $image->getWidth(),
      'height' => $image->getHeight(),
      'alt' => $alt,
    ];
  }

}
