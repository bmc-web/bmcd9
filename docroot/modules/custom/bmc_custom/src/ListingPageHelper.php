<?php declare(strict_types=1);

namespace Drupal\bmc_custom;

use Drupal\config_pages\ConfigPagesLoaderServiceInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;

/**
 * @todo Add class description.
 */
final class ListingPageHelper {

  private ?\Drupal\config_pages\Entity\ConfigPages $listingPageConfig;

  /**
   * Constructs a ListingPageHelper object.
   */
  public function __construct(
    private readonly ConfigPagesLoaderServiceInterface $configPagesLoader,
    private readonly EntityTypeManagerInterface $entityTypeManager,
  ) {
    $this->listingPageConfig = $this->configPagesLoader->load('listing_page_settings');
  }

  public function getAnnouncementsListingPage($tag = NULL): ?Url {
    if ($this->listingPageConfig === NULL) {
      return NULL;
    }
    $listingPageField = $this->listingPageConfig->field_cp_anncmnt_list_node;
    if ($listingPageField->isEmpty()) {
      return NULL;
    }
    $landingPageUrl = $listingPageField->entity->toUrl();
    if ($tag !== NULL) {
      $landingPageUrl->setOption('query', ['tags' => $tag]);
    }
    return $landingPageUrl;
  }

  public function getEventsListingPage($tag = NULL): ?Url {
    if ($this->listingPageConfig === NULL) {
      return NULL;
    }
    if (!isset($tag) || is_null($tag)) {
      $listingPageField = $this->listingPageConfig->field_cp_event_list_node;
      if ($listingPageField->isEmpty()) {
        return NULL;
      }
      return $listingPageField->entity->toUrl();
    }
    else {
      $listingPageField = $this->listingPageConfig->field_cp_event_list_tag_node;
      if ($listingPageField->isEmpty()) {
        return NULL;
      }
      $landingPageUrl = $listingPageField->entity->toUrl();
      $landingPageUrl->setOption('query', ['tags' => $tag]);
      return $landingPageUrl;
    }
  }

  public function getNewsListingPage($theme = NULL, $tag = NULL): ?Url {
    if ($this->listingPageConfig === NULL) {
      return NULL;
    }

    if (is_numeric($theme)) {
      $theme = $this->handleThemeTerm($theme);
    }

    switch ($theme) {
      case 'gsswsr':
      case 'Graduate School of Social Work and Social Research':
        $listingPageField = $this->listingPageConfig->field_cp_news_node_gsswsr;
        break;
      case 'gsas':
      case 'Graduate School of Arts and Sciences':
        $listingPageField = $this->listingPageConfig->field_cp_news_node_gsas;
        break;
      case 'postbac':
      case 'Postbaccalaureate Premedical Program':
        $listingPageField = $this->listingPageConfig->field_cp_news_node_postbac;
        break;
      case 'inside':
      case 'Inside Bryn Mawr':
        $listingPageField = $this->listingPageConfig->field_cp_news_node_inside;
        break;
      case 'marketing':
      case 'Marketing':
      default:
        $listingPageField = $this->listingPageConfig->field_cp_news_node_mkt;
        break;
    }
    if (!isset($listingPageField) || $listingPageField->isEmpty()) {
      $listingPageField = $this->listingPageConfig->field_cp_news_node_mkt;
    }
    if ($listingPageField->isEmpty()) {
      return NULL;
    }

    $landingPageUrl = $listingPageField->entity->toUrl();
    if ($tag !== NULL) {
      $landingPageUrl->setOption('query', ['tagged' => $tag]);
    }
    return $landingPageUrl;
  }

  public function getStoriesListingPage($theme = NULL, $tag = NULL): ?Url {
    if ($this->listingPageConfig === NULL) {
      return NULL;
    }

    if (is_numeric($theme)) {
      $theme = $this->handleThemeTerm($theme);
    }

    switch ($theme) {
      case 'gsswsr':
      case 'Graduate School of Social Work and Social Research':
        $listingPageField = $this->listingPageConfig->field_cp_story_node_gsswsr;
        break;
      case 'gsas':
      case 'Graduate School of Arts and Sciences':
        $listingPageField = $this->listingPageConfig->field_cp_story_node_gsas;
        break;
      case 'postbac':
      case 'Postbaccalaureate Premedical Program':
        $listingPageField = $this->listingPageConfig->field_cp_story_node_postbac;
        break;
      // There is no Inside stories listing.
      case 'inside':
      case 'Inside Bryn Mawr':
      case 'marketing':
      case 'Marketing':
      default:
        $listingPageField = $this->listingPageConfig->field_cp_story_node_mkt;
        break;
    }
    if (!isset($listingPageField) || $listingPageField->isEmpty()) {
      $listingPageField = $this->listingPageConfig->field_cp_story_node_mkt;
    }
    if ($listingPageField->isEmpty()) {
      return NULL;
    }

    $landingPageUrl = $listingPageField->entity->toUrl();
    if ($tag !== NULL) {
      $landingPageUrl->setOption('query', ['tagged' => $tag]);
    }
    return $landingPageUrl;
  }

  private function handleThemeTerm($theme) {
    $tid = (int) $theme;
    $termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $term = $termStorage->load($tid);
    if ($term) {
      $theme = $term->label();
    }
    return $theme;
  }

}
