<?php declare(strict_types = 1);

namespace Drupal\bmc_custom\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a Value Is Pathlike constraint.
 *
 * @Constraint(
 *   id = "ValueIsPathlike",
 *   label = @Translation("Value Is Pathlike", context = "Validation"),
 * )
 *
 * @DCG
 * To apply this constraint on third party field types. Implement
 * hook_field_info_alter() as follows.
 * @code
 * function bmc_custom_field_info_alter(array &$info): void {
 *   $info['FIELD_TYPE']['constraints']['ValueIsPathlike'] = [];
 * }
 * @endcode
 *
 * @see https://www.drupal.org/node/2015723
 */
final class ValueIsPathlikeConstraint extends Constraint {

  public string $message = 'Each path must start with a forward slash (\'/\').';

}
