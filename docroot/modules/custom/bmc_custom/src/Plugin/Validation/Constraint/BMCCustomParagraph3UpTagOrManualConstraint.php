<?php declare(strict_types = 1);

namespace Drupal\bmc_custom\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a Paragraph3UpTagOrManual constraint.
 *
 * @Constraint(
 *   id = "BMCCustomParagraph3UpTagOrManual",
 *   label = @Translation("Paragraph 3-Up Tag Or Manual", context = "Validation"),
 * )
 *
 * @see https://www.drupal.org/node/2015723.
 */
final class BMCCustomParagraph3UpTagOrManualConstraint extends Constraint {

  public string $message = 'You must select either manual items or a tag.';

}
