<?php declare(strict_types = 1);

namespace Drupal\bmc_custom\Plugin\Validation\Constraint;

use Drupal\Core\Field\FieldItemInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the Value Is Pathlike constraint.
 */
final class ValueIsPathlikeConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate(mixed $item, Constraint $constraint): void {
    if ($item->isEmpty()) {
      return;
    }
    $value = $item->value;
    $trimmed_value = trim($value);
    if (empty($trimmed_value)) {
      return;
    }
    $pieces = explode(PHP_EOL, $trimmed_value);
    $addViolation = false;
    if (count($pieces) > 0) {
      foreach ($pieces as $piece) {
        if (!str_starts_with($piece, '/')) {
          $addViolation = true;
          break;
        }
      }
    }

    if ($addViolation) {
      $this->context->addViolation($constraint->message);
    }
  }

}
