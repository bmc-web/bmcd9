<?php declare(strict_types = 1);

namespace Drupal\bmc_custom\Plugin\Validation\Constraint;

use Drupal\paragraphs\ParagraphInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\Core\Entity\EntityInterface;


/**
 * Validates the Paragraph3UpTagOrManual constraint.
 */
final class BMCCustomParagraph3UpTagOrManualConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate(mixed $entity, Constraint $constraint): void {
    if (!$entity instanceof ParagraphInterface) {
      throw new \InvalidArgumentException(
        sprintf('The validated value must be instance of Drupal\paragraphs\ParagraphInterface, %s was given.', get_debug_type($entity))
      );
    }

    /** @var \Drupal\paragraphs\ParagraphInterface $entity */
    $type = $entity->bundle();
    switch ($type) {
      case 'anncmnt_4_up':
        if ($entity->field_p_anncmnt_manual_items->isEmpty() && $entity->field_p_anncmnt_tag->isEmpty()) {
          $this->context->addViolation($constraint->message);
        }
        break;
      case 'event_3_up':
        if ($entity->field_p_event3up_manual_items->isEmpty() && $entity->field_p_event3up_tag->isEmpty()) {
          $this->context->addViolation($constraint->message);
        }
        break;
      case 'news_3_up':
        if ($entity->field_p_news3up_manual_items->isEmpty() && $entity->field_p_news3up_tag->isEmpty()) {
          $this->context->addViolation($constraint->message);
        }
        break;
      case 'story_3_up':
        if ($entity->field_p_story3up_manual->isEmpty() && $entity->field_p_story3up_tag->isEmpty()) {
          $this->context->addViolation($constraint->message);
        }
        break;
      case 'news_contact':
        if ($entity->field_p_newscontact_manual->isEmpty() && $entity->field_p_newscontact_tag->isEmpty()) {
          $this->context->addViolation($constraint->message);
        }
        break;
      case 'story_1_up':
        if ($entity->field_p_story_manual_item->isEmpty() && $entity->field_p_story_tag->isEmpty()) {
          $this->context->addViolation($constraint->message);
        }
        break;
      default:
        break;
    }

  }

}
