<?php

namespace Drupal\bmc_custom\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\embed\DomHelperTrait;

/**
 * Provides a filter to display embedded entities based on data attributes.
 *
 * @Filter(
 *   id = "fs_table_wrapper",
 *   title = @Translation("Wrap Tables"),
 *   description = @Translation("Wraps tables with a Div tag for responsive styling."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class TableWrapperFilter extends FilterBase implements ContainerFactoryPluginInterface {
  use DomHelperTrait;

  /**
   * The renderer service.
   *
   * @var RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a TableWrapperFilter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager Interface.
   * @param ModuleHandlerInterface $module_handler
   *   The Module Handler.
   * @param RendererInterface $renderer
   *   The Render Interface.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);

    if (strpos($text, '<table') !== FALSE) {
      $dom = Html::load($text);
      $xpath = new \DOMXPath($dom);

      foreach ($xpath->query('//table') as $node) {
        $class_list = 'table table--wysiwyg' . $this->getClassListFromNodeWithLeadingSpace($node);
        $this->replaceNodeContent($node, '<div class="' . $class_list . '"><div class="table__inner">' . $dom->saveHTML($node) . '</div></div>');
      }

      $result->setProcessedText(Html::serialize($dom));
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    if ($long) {
      return $this->t('
        <p>Wrap tables with [div class="table-wrapper table-wrapper--wysiwyg]", as well as any additional classes that the table itself may have.</p>');
    }
    else {
      return $this->t('Wrap tables with a div.');
    }
  }

  /**
   * Gets the class list from a DOM node and returns it with a leading space.
   *
   * @param \DOMElement $node
   *   A DOM Element.
   *
   * @return string
   *   Class list (if any) with a leading space.
   */
  public function getClassListFromNodeWithLeadingSpace(\DOMElement $node): string {
    $class_list = $node->getAttribute('class');
    return $class_list ? ' ' . $class_list : '';
  }

}
