<?php

namespace Drupal\bmc_custom\Plugin\Block;

use Drupal\config_pages\Entity\ConfigPages;
use Drupal\Core\Block\BlockBase;
use Drupal\bmc_custom\Plugin\Block\Drupal;

/**
 * Provides a Story Listing container block.
 *
 * @Block(
 *  id = "story_listing_postbac",
 *  admin_label = @Translation("Story Listing: Postbac"),
 * )
 */
class StoryListingPostbac extends BlockBase {

  /**
   * Renders the Postbac story variables.
   */
  public function build(): array {
    $featured_story_postbac_nid = '';
    $first_or_only_page = TRUE;
    $tagged = '';

    // Load the Listing Page Settings config page.
    $listing_page_config = ConfigPages::config('listing_page_settings');

    if (!empty($listing_page_config->field_cp_listpage_story_postbac->target_id)) {
      $featured_story_postbac_nid = $listing_page_config->field_cp_listpage_story_postbac->target_id;
    }

    if (
      \Drupal::request()->query->has('page')
      && (\Drupal::request()->query->get('page') !== '0')
    ) {
      $first_or_only_page = FALSE;
    }

    if (\Drupal::request()->query->has('tagged')) {
      $tagged = \Drupal::request()->query->get('tagged');
    }

    return [
      '#theme' => 'story_listing_postbac',
      '#featured_story_postbac_nid' => $featured_story_postbac_nid,
      '#first_or_only_page' => $first_or_only_page,
      '#tagged' => $tagged,
    ];
  }

}
