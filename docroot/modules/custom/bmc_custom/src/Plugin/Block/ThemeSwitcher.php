<?php

namespace Drupal\bmc_custom\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides a header container block.
 *
 * @Block(
 *  id = "bmc_custom_theme_switcher",
 *  admin_label = @Translation("Theme Switcher Menu"),
 * )
 */
class ThemeSwitcher extends BlockBase {

  /**
   * Renders the header variables.
   */
  public function build(): array {
    $theme_machine_names = [
      '1' => 'bulletin',
      '2' => 'gsas',
      '3' => 'socialwork',
      '4' => 'inside',
      '5' => 'marketing',
      '6' => 'postbac',
    ];
    // Set default theme to marketing.
    $theme_tid = '5';
    $node = \Drupal::request()->attributes->get('node');
    if (isset($node) && is_object($node)) {
      if (isset($node->field_theme_main->target_id)) {
        $theme_tid = $node->field_theme_main->target_id;
      }
    } else {
      $current_path = \Drupal::service('path.current')->getPath();
      $current_alias_arr = explode('/', $current_path);
      if(isset($current_alias_arr[1])) {
        if($current_alias_arr[1] == 'inside') {
          $theme_tid= 4;
        } elseif($current_alias_arr[1] == 'socialwork'){
          $theme_tid= 3;
        } elseif($current_alias_arr[1] == 'gsas'){
          $theme_tid= 2;
        } elseif($current_alias_arr[1] == 'bulletin'){
          $theme_tid= 1;
        } elseif($current_alias_arr[1] == 'postbac'){
          $theme_tid= 6;
        }
      }
    }

    $links = [];
    // Add Marketing Term as first item.
    $marketing_link['machine_name'] = 'marketing';
    $marketing_link['title'] = 'Bryn Mawr Home';
    $marketing_theme_term = Term::load('5');
    if (isset($marketing_theme_term->field_t_theme_home->target_id)) {
      $marketing_link['url'] =
        \Drupal::service('path_alias.manager')
          ->getAliasByPath(
            '/node/'
            . $marketing_theme_term->field_t_theme_home->target_id
          );
      $marketing_link['path'] = 'node/' . $marketing_theme_term->field_t_theme_home->target_id;
    }
    else {
      $marketing_link['url'] = 'https://www.brynmawr.edu';
    }
    if ($theme_tid === '5') {
      $marketing_link['active'] = TRUE;
    }

    $links[] = $marketing_link;

    $graduate_top_level['url'] = '#';
    $graduate_top_level['machine_name'] = 'grad_top_level';
    $graduate_top_level['title'] = 'Graduate & Postbac';
    $graduate_top_level['child_links'] = [];

    $vid = 'themes';
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term_ref) {
      $tid = $term_ref->tid;
      $term = Term::load($tid);
      if ($term->field_t_theme_is_grad->value == '1') {
        $grad_link = [];
        $grad_link['machine_name'] = $theme_machine_names[$tid];
        $grad_link['title'] = $term->getName();
        $grad_link['url'] = 'https://www.brynmawr.edu';
        if (isset($term->field_t_theme_home->target_id)) {
          $grad_link['url'] =
            \Drupal::service('path_alias.manager')
              ->getAliasByPath(
                '/node/'
                . $term->field_t_theme_home->target_id
              );
          $grad_link['path'] = 'node/' . $term->field_t_theme_home->target_id;
        }

        if ($tid == $theme_tid) {
          $grad_link['active'] = TRUE;
          $graduate_top_level['active'] = TRUE;
        }
        $graduate_top_level['child_links'][] = $grad_link;
      }
    }
    $links[] = $graduate_top_level;
    $inside_link['machine_name'] = 'inside';
    $inside_link['title'] = 'Inside Bryn Mawr';
    $inside_theme_term = Term::load('4');
    if (isset($inside_theme_term->field_t_theme_home->target_id)) {
      $inside_link['url'] =
        \Drupal::service('path_alias.manager')
          ->getAliasByPath(
            '/node/'
            . $inside_theme_term->field_t_theme_home->target_id
          );
      $inside_link['path'] = $inside_theme_term->field_t_theme_home->target_id;
    }
    else {
      $inside_link['url'] = 'https://www.brynmawr.edu/inside';
    }
    if ($theme_tid === '4') {
      $inside_link['active'] = TRUE;
    }
    $links[] = $inside_link;
    // Create the build array.
    return [
      '#theme' => 'theme_switcher',
      '#links' => $links,
    ];
  }

  /**
   * Ensures block does not cache.
   */
  public function getCacheMaxAge(): int {
    return 0;
  }

}
