<?php

namespace Drupal\bmc_custom\Plugin\Block;

use Drupal\config_pages\Entity\ConfigPages;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Provides a footer container block.
 *
 * @Block(
 *  id = "bmc_custom_footer_container_gsswsr",
 *  admin_label = @Translation("Footer Container: GSSWSR"),
 * )
 */
class FooterContainerGsswsr extends BlockBase {

  /**
   * Renders the footer variables.
   */
  public function build() {

    $build = [];
    $prefooter = [];
    $footer_logo = [];
    $footer_logo_mobile = [];
    $address = '';
    $contact_link = [];
    $phone = [];
    $footer_links = [];
    $footer_image = [];
    $social_media_links = [];
    $copyright_text = '';

    // Load the Header & Footer Wrapper config page.
    $wrapper_config = ConfigPages::config('header_footer_wrapper');

    if (
      !$wrapper_config->field_cp_wrapper_sw_pf_img->isEmpty()
    ) {
      $prefooter = [
        '#theme' => 'prefooter',
        '#prefooter_title' => !$wrapper_config->field_cp_wrapper_sw_pf_title->isEmpty() ? $wrapper_config->field_cp_wrapper_sw_pf_title->value : '',
        '#prefooter_image' => $wrapper_config->field_cp_wrapper_sw_pf_img->view('default'),
        '#prefooter_cta' => !$wrapper_config->field_cp_wrapper_sw_pf_cta->isEmpty() ? $wrapper_config->field_cp_wrapper_sw_pf_cta->view('default') : [],
      ];
    }

    if (!empty($wrapper_config->field_cp_wrapper_sw_flogo->target_id)) {
      $image_file = File::load($wrapper_config->field_cp_wrapper_sw_flogo->target_id);

      if ($image_file) {
        $image_alt = $wrapper_config->field_cp_wrapper_sw_flogo->alt;
        $image_src = $image_file->getFileUri();
        $footer_logo = [
          '#theme' => 'image',
          '#uri' => $image_src,
          '#alt' => $image_alt,
        ];
      }
    }

    if (!empty($wrapper_config->field_cp_wrapper_sw_fmlogo->target_id)) {
      $image_file = File::load($wrapper_config->field_cp_wrapper_sw_fmlogo->target_id);

      if ($image_file) {
        $image_alt = $wrapper_config->field_cp_wrapper_sw_fmlogo->alt;
        $image_src = $image_file->getFileUri();
        $footer_logo_mobile = [
          '#theme' => 'image',
          '#uri' => $image_src,
          '#alt' => $image_alt,
        ];
      }
    }



    if (!empty($wrapper_config->field_cp_wrapper_sw_address)) {
      $address = $wrapper_config->field_cp_wrapper_sw_address->value;
    }

    if (!empty($wrapper_config->field_cp_wrapper_sw_phone->value)) {
      $phone = $wrapper_config->field_cp_wrapper_sw_phone->value;
    }

    if (!$wrapper_config->field_cp_wrapper_sw_contact->isEmpty()) {
      $contact_link = $wrapper_config->field_cp_wrapper_sw_contact[0]->view();
    }

    if (!$wrapper_config->field_cp_wrapper_sw_links->isEmpty()) {
      foreach ($wrapper_config->field_cp_wrapper_sw_links as $link) {
        $footer_links[] = [
          'title' => $link->getTitle(),
          'url' => $link->getUrl()->toString(),
        ];
      }
    }

    if (!empty($wrapper_config->field_cp_wrapper_sw_socialm)) {
      $sm_paras = $wrapper_config->field_cp_wrapper_sw_socialm->getValue();
      foreach ($sm_paras as $element) {
        $sm_link = [];
        $sm_para = Paragraph::load($element['target_id']);
        $sm_link['service'] = $sm_para->field_p_smcalloutlink_service->value;
        $sm_link['url'] = Url::fromUri($sm_para->field_p_smcalloutlink_url->uri);
        $social_media_links[] = $sm_link;
      }
    }

    if (!empty($wrapper_config->field_cp_wrapper_copyright_txt->value)) {
      $copyright_text = $wrapper_config->field_cp_wrapper_copyright_txt->value;
    }

    if (!$wrapper_config->field_cp_wrapper_sw_footer_img->isEmpty()) {
      $footer_image = $wrapper_config->field_cp_wrapper_sw_footer_img->view('default');
    }

    $build = [
      '#theme' => 'footer_container_gsswsr',
      '#prefooter' => $prefooter,
      '#footer_logo' => $footer_logo,
      '#footer_logo_mobile' => $footer_logo_mobile,
      '#footer_address' => $address,
      '#footer_phone' => $phone,
      '#footer_contact_link' => $contact_link,
      '#footer_links' => $footer_links,
      '#footer_image' => $footer_image,
      '#footer_social_media_links' => $social_media_links,
      '#footer_copyright_text' => $copyright_text,
    ];
    return $build;
  }

  /**
   * @inheritdoc
   */
  public function getCacheTags() {
    // Load the Header & Footer Wrapper config page.
    $wrapper_config = ConfigPages::config('header_footer_wrapper');
    return Cache::mergeTags(parent::getCacheTags(), $wrapper_config->getCacheTags());
  }
}
