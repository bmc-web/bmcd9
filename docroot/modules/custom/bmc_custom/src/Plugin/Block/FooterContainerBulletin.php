<?php

namespace Drupal\bmc_custom\Plugin\Block;

use Drupal\config_pages\Entity\ConfigPages;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * Provides a footer container block.
 *
 * @Block(
 *  id = "bmc_custom_footer_container_bulletin",
 *  admin_label = @Translation("Footer Container: Bulletin"),
 * )
 */
class FooterContainerBulletin extends BlockBase {

  /**
   * Renders the footer variables.
   */
  public function build() {

    $build = [];
    $footer_logo = [];
    $social_media_links = [];

    // Load the Header & Footer Wrapper config page.
    $wrapper_config = ConfigPages::config('bulletin_wrapper');

    if (!empty($wrapper_config->field_cp_bwrapper_flogo->target_id)) {
      $image_file = File::load($wrapper_config->field_cp_bwrapper_flogo->target_id);

      if ($image_file) {
        $image_alt = $wrapper_config->field_cp_bwrapper_flogo->alt;
        $image_src = $image_file->getFileUri();
        $footer_logo = [
          '#theme' => 'image',
          '#uri' => $image_src,
          '#alt' => $image_alt,
        ];
      }
    }

    if (!empty($wrapper_config->field_cp_bwrapper_fb_link)) {
      $social_media_links['facebook']
        = Url::fromUri($wrapper_config->field_cp_bwrapper_fb_link->uri);
    }

    if (!empty($wrapper_config->field_cp_bwrapper_tw_link)) {
      $social_media_links['twitter']
        = Url::fromUri($wrapper_config->field_cp_bwrapper_tw_link->uri);
    }

    $build = [
      '#theme' => 'footer_container_bulletin',
      '#footer_logo' => $footer_logo,
      '#social_media_links' => $social_media_links,
      '#cache' => [
        'tags' => [
          $wrapper_config->getCacheTags()[0],
        ],
      ],
    ];
    return $build;
  }

}
