<?php

namespace Drupal\bmc_custom\Plugin\Block;

use Drupal\config_pages\Entity\ConfigPages;
use Drupal\Core\Block\BlockBase;
use Drupal\bmc_custom\Plugin\Block\Drupal;

/**
 * Provides a Story Listing container block.
 *
 * @Block(
 *  id = "story_listing_gsswsr",
 *  admin_label = @Translation("Story Listing: GSSWSR"),
 * )
 */
class StoryListingGsswsr extends BlockBase {

  /**
   * Renders the GSSWSR Story Listing Variables.
   */
  public function build(): array {
    $featured_story_gsswsr_nid = '';
    $first_or_only_page = TRUE;
    $tagged = '';

    // Load the Listing Page Settings config page.
    $listing_page_config = ConfigPages::config('listing_page_settings');

    if (!empty($listing_page_config->field_cp_listpage_story_gsswsr->target_id)) {
      $featured_story_gsswsr_nid = $listing_page_config->field_cp_listpage_story_gsswsr->target_id;
    }

    if (
      \Drupal::request()->query->has('page')
      && (\Drupal::request()->query->get('page') !== '0')
    ) {
      $first_or_only_page = FALSE;
    }

    if (\Drupal::request()->query->has('tagged')) {
      $tagged = \Drupal::request()->query->get('tagged');
    }

    return [
      '#theme' => 'story_listing_gsswsr',
      '#featured_story_gsswsr_nid' => $featured_story_gsswsr_nid,
      '#first_or_only_page' => $first_or_only_page,
      '#tagged' => $tagged,
    ];
  }

}
