<?php

namespace Drupal\bmc_custom\Plugin\Block;

use Drupal\config_pages\Entity\ConfigPages;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\file\Entity\File;
use Drupal\Core\Url;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Provides a header container block.
 *
 * @Block(
 *  id = "bmc_custom_header_container_gsswsr",
 *  admin_label = @Translation("Header Container: GSSWSR"),
 * )
 */
class HeaderContainerGsswsr extends BlockBase {
  /**
   * Renders the header variables.
   */
  public function build(): array {

    $build = [];
    $image_src = '';
    $image_alt = '';
    $header_logo_link = '';
    $social_media_links = [];

    // Load the Header & Footer Wrapper config page.
    $wrapper_config = ConfigPages::config('header_footer_wrapper');

    // Get relevant variables from it.
    if (!empty($wrapper_config->field_cp_wrapper_sw_hlogo->target_id)) {
      $image_file = File::load($wrapper_config->field_cp_wrapper_sw_hlogo->target_id);

      if ($image_file) {
        $image_alt = $wrapper_config->field_cp_wrapper_sw_hlogo->alt;
        $image_src = $image_file->getFileUri();
      }
    }

    if (!empty($wrapper_config->field_cp_wrapper_sw_hlogolink)) {
      $header_logo_link = Url::fromUri($wrapper_config->field_cp_wrapper_sw_hlogolink->uri);
    }

    if (!empty($wrapper_config->field_cp_wrapper_sw_socialm)) {
      $sm_paras = $wrapper_config->field_cp_wrapper_sw_socialm->getValue();
      foreach ($sm_paras as $element) {
        $sm_link = [];
        $sm_para = Paragraph::load($element['target_id']);
        $sm_link['service'] = $sm_para->field_p_smcalloutlink_service->value;
        $sm_link['url'] = Url::fromUri($sm_para->field_p_smcalloutlink_url->uri);
        $social_media_links[] = $sm_link;
      }
    }

    // Create the build array.
    $build = [
      '#theme' => 'header_container_gsswsr',
      '#header_logo' => [
        '#theme' => 'image',
        '#uri' => $image_src,
        '#alt' => $image_alt,
      ],
      '#header_logo_mobile' => [
        '#theme' => 'image',
        '#uri' => $image_src,
        '#alt' => $image_alt,
      ],
      '#header_logo_link' => $header_logo_link,
      '#social_media_links' => $social_media_links,
    ];

    if (!$wrapper_config->field_cp_wrapper_sw_hmlogo->isEmpty()) {
      $image_file = File::load($wrapper_config->field_cp_wrapper_sw_hmlogo->target_id);
      if ($image_file) {
        $build['#header_logo_mobile'] = [
          '#theme' => 'image',
          '#uri' => $wrapper_config->field_cp_wrapper_sw_hmlogo->entity->getFileUri(),
          '#alt' => $wrapper_config->field_cp_wrapper_sw_hmlogo->alt,
        ];
      }
    }

    $menu_data_provider = \Drupal::getContainer()->get('bmc_custom.menu_data_provider');
    $menu_data = $menu_data_provider->getMenuData('socialwork');

    $build['#attached']['library'][] = 'bmc_d9/marketing-grad-menu';
    $build['#attached']['drupalSettings']['marketing-grad-menu'] = $menu_data;

    return $build;
  }

  /**
   * @inheritdoc
   */
  public function getCacheTags() {
    // Load the Header & Footer Wrapper config page.
    $wrapper_config = ConfigPages::config('header_footer_wrapper');
    return Cache::mergeTags(parent::getCacheTags(), $wrapper_config->getCacheTags(), ['config:system.menu.socialwork']);
  }

}
