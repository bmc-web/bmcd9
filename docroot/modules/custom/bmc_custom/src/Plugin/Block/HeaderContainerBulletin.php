<?php

namespace Drupal\bmc_custom\Plugin\Block;

use Drupal\node\Entity\Node;
use Drupal\config_pages\Entity\ConfigPages;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides a header container block.
 *
 * @Block(
 *  id = "bmc_custom_header_container_bulletin",
 *  admin_label = @Translation("Header Container: Bulletin"),
 * )
 */
class HeaderContainerBulletin extends BlockBase {

  /**
   * Renders the header variables.
   */
  public function build(): array {
    $build = [];
    $image_src = '';
    $image_alt = '';
    $header_logo_link = '';
    $issue_identifier = [];

    // Load the Header & Footer Wrapper config page.
    $wrapper_config = ConfigPages::config('bulletin_wrapper');

    // Get relevant variables from it.
    if (!empty($wrapper_config->field_cp_bwrapper_hlogo->target_id)) {
      $image_file = File::load($wrapper_config->field_cp_bwrapper_hlogo->target_id);

      if ($image_file) {
        $image_alt = $wrapper_config->field_cp_bwrapper_hlogo->alt;
        $image_src = $image_file->getFileUri();
      }
    }

    if (!empty($wrapper_config->field_cp_bwrapper_hlogolink)) {
      $header_logo_link = Url::fromUri($wrapper_config->field_cp_bwrapper_hlogolink->uri)->toString();
    }

    $bulletin_cts = [
      'bulletin_fp',
      'bulletin_spotlight',
      'bookshelf_item',
      'news',
    ];

    // Get the issue title if this block is placed on a Bulletin node.
    $block_config = $this->getConfiguration();
    $node = \Drupal::routeMatch()->getParameter('node');
    if (isset($node)) {
      $node_type = $node->getType();
      if (in_array($node_type, $bulletin_cts)) {
        if (isset($node->field_bulletin_issue->target_id)) {
          $issue_tid = $node->field_bulletin_issue->target_id;
        }
        if (isset($node->field_news_bulletin_issue->target_id)) {
          $issue_tid = $node->field_news_bulletin_issue->target_id;
        }

        if (isset($node->field_bulletin_fp_issue->target_id)) {
          $issue_tid = $node->field_bulletin_fp_issue->target_id;
        }

        if (isset($node->field_s_bulletin_issue->target_id)) {
          $issue_tid = $node->field_s_bulletin_issue->target_id;
        }

        if (isset($issue_tid)) {
          $issue_term = Term::load($issue_tid);
          $issue_identifier['name'] = $issue_term->getName();
          $issue_identifier['url'] =
            '/bulletin/' .
            strtolower(
              str_replace(' ', '-', $issue_identifier['name'])
            );
        }
      }
    }

    // Create the build array.
    return [
      '#theme' => 'header_container_bulletin',
      '#header_logo' => [
        '#theme' => 'image',
        '#uri' => $image_src,
        '#alt' => $image_alt,
      ],
      '#header_logo_link' => $header_logo_link,
      '#issue_identifier' => $issue_identifier,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
