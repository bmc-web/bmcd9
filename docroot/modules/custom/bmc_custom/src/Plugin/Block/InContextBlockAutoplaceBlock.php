<?php declare(strict_types = 1);

namespace Drupal\bmc_custom\Plugin\Block;

use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an in context block autoplace block.
 *
 * @Block(
 *   id = "bmc_custom_in_context_block_autoplace",
 *   admin_label = @Translation("In Context Block Autoplace"),
 *   category = @Translation("BMC Custom"),
 * )
 */
final class InContextBlockAutoplaceBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var array|null $shown_entities
   * Cache the shown entities, since they're expensive to calculate.
   */
  private ?array $shown_entities = NULL;

  /**
   * Constructs the plugin instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly ConditionManager $conditionManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.condition')
    );
  }

  /**
   * {@inheritdoc}
   */
  function getCacheTags() {
    $tags = Cache::mergeTags(parent::getCacheTags(), ['block_content_list:in_context_block']);
    $shown_entities = $this->getShownInContextBlockContentEntities();
    foreach ($shown_entities as $entity) {
      $tags = Cache::mergeTags($tags, $entity->getCacheTags());
    }
    return $tags;
  }

  /**
   * {@inheritdoc}
   */
  function getCacheContexts() {
    // @todo: We can probably create a custom cache context for this.
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.path']);
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    // @todo: Should this be a lazy builder?
    $build = [];
    /** @var \Drupal\block_content\Entity\BlockContent $block */
    foreach ($this->getShownInContextBlockContentEntities() as $block) {
      $build[] = $this->viewInContextBlockContentEntity($block);
    }
    return $build;
  }

  /**
   * Get the in context block content entities that should be shown.
   *
   * @return \Drupal\block_content\Entity\BlockContent[]
   */
  private function getShownInContextBlockContentEntities(): array {
   if ($this->shown_entities === NULL) {
     $this->setShownInContextBlockContentEntities();
   }
    return $this->shown_entities;
  }

  /**
   * Set the in context block content entities that should be shown.
   */
  private function setShownInContextBlockContentEntities(): void {
    $shown_entities = [];
    $in_context_block_entities = $this->getAllInContextBlockContentEntities();
    foreach ($in_context_block_entities as $entity) {
      if ($this->doShowInContextBlockEntity($entity)) {
        $shown_entities[] = $entity;
      }
    }
    $this->shown_entities = $shown_entities;
  }

  /**
   * Load all in context block content entities.
   *
   * @return BlockContent[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getAllInContextBlockContentEntities(): array {
    $query = $this->entityTypeManager->getStorage('block_content')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', 'in_context_block')
      ->condition('field_active', TRUE)
      ->sort('field_weight')
      ->execute();
    return $this->entityTypeManager->getStorage('block_content')
      ->loadMultiple($query);
  }

  /**
   * Determine whether a given block entity should be shown on the route.
   *
   * @param \Drupal\block_content\Entity\BlockContent $entity
   *
   * @return bool
   */
  private function doShowInContextBlockEntity(BlockContent $entity): bool {
    $includePaths = $entity->get('field_include_block_on_path')->value;
    $excludePaths = $entity->get('field_exclude_block_on_path')->value;
    if ($this->matches($includePaths) && !$this->matches($excludePaths)) {
      return TRUE;
    }
    return false;
  }

  /**
   * Check if the current path matches any of the given paths.
   *
   * Note: The request_path condition::evaluate() returns true if the 'pages'
   * parameter is empty. For our case, we need it to return false.
   *
   * @param string|null $paths
   *  A string of paths separated by newlines.
   */
  private function matches(string|null $paths) {
    if (empty($paths)) {
      return FALSE;
    }
    $requestPathCheck = $this->conditionManager->createInstance('request_path');
    $requestPathCheck->setConfiguration(['pages' => $paths]);
    return $requestPathCheck->evaluate();
  }

  /**
   * View an in context block content entity.
   *
   * @param \Drupal\block_content\Entity\BlockContent $block
   *
   * @return array
   */
  private function viewInContextBlockContentEntity(BlockContent $block): array {
    return $this->entityTypeManager->getViewBuilder('block_content')
      ->view($block);
  }

}
