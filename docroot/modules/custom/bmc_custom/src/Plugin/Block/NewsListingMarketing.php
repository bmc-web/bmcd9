<?php

namespace Drupal\bmc_custom\Plugin\Block;

use Drupal\config_pages\Entity\ConfigPages;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a News Listing container block.
 *
 * @Block(
 *  id = "news_listing_marketing",
 *  admin_label = @Translation("News Listing: Marketing"),
 * )
 */
class NewsListingMarketing extends BlockBase {

  /**
   * Renders the Marketing News listing block variables.
   */
  public function build(): array {
    $featured_news_mkt_nid = '';
    $first_or_only_page = TRUE;
    $tagged = '';
    $about_this_content = '';

    // Load the Listing Page Settings config page.
    $listing_page_config = ConfigPages::config('listing_page_settings');

    if (!empty($listing_page_config->field_cp_news_feat_mkt->target_id)) {
      $featured_news_mkt_nid = $listing_page_config->field_cp_news_feat_mkt->target_id;
    }

    if ($listing_page_config->field_field_cp_news_mkt_about) {
      $about_this_content = $listing_page_config->field_field_cp_news_mkt_about->view(['label' => 'hidden']);
    }

    if (
      \Drupal::request()->query->has('page')
      && (\Drupal::request()->query->get('page') !== '0')
    ) {
      $first_or_only_page = FALSE;
    }

    if (\Drupal::request()->query->has('tagged')) {
      $tagged = \Drupal::request()->query->get('tagged');
    }

    return [
      '#theme' => 'news_listing_marketing',
      '#featured_news_mkt_nid' => $featured_news_mkt_nid,
      '#first_or_only_page' => $first_or_only_page,
      '#tagged' => $tagged,
      '#about_this_content' => $about_this_content,
    ];
  }

}
