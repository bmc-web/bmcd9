<?php

namespace Drupal\bmc_custom\Plugin\Action;

use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\group\Entity\Group;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Action description.
 *
 * @Action(
 *   id = "bmc_custom_assign_nodes_to_groups",
 *   label = @Translation("Assign Nodes to Group"),
 *   type = "node",
 *   confirm = TRUE,
 *   requirements = {
 *     "_permission" = "bulk assign nodes to groups",
 *   },
 * )
 */
class BMCAssignNodesToGroups extends ViewsBulkOperationsActionBase implements PluginFormInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $group = Group::load($this->configuration['groups']);
    $handle_existing = $this->configuration['handle_existing'];
    $storage = \Drupal::entityTypeManager()->getStorage('group_content');
    $owners = $storage->loadByEntity($entity);


    // Check if group type has content plugin installed for node.
    $plugin_id = 'group_node:' . $entity->bundle();
    if (!in_array($plugin_id, $group->getGroupType()->getInstalledContentPlugins()->getInstanceIds())) {
      return $this->t('Node type @type not allowed in group', ['@type' => $entity->bundle()]);
    }

    if (count($owners) == 0) {
      $group->addContent($entity, $plugin_id);
      return $this->t('Node added to group');
    }
    else {
      if ($handle_existing == 'preserve') {
        return $this->t('This node was not reassigned because it is already in a group');
      }
      elseif ($handle_existing == 'override') {
        foreach ($owners as $owner) {
          $owner->delete();
          $group->addContent($entity, $plugin_id);
          return $this->t('This node has been reassigned');
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($account->hasPermission('bulk assign nodes to groups')) {
      return $return_as_object ? new AccessResultAllowed() : TRUE;
    }
    else {
      return $return_as_object ? new AccessResultForbidden() : FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $groups = Group::loadMultiple();
    usort($groups, fn($a, $b) => strcmp($a->label(), $b->label()));

    $form['groups'] = [
      '#title' => t('Assign content to group'),
      '#type' => 'select',
      '#options' => [],
    ];

    foreach ($groups as $group) {
      $form['groups']['#options'][$group->id()] = $group->label();
    }

    $form['handle_existing'] = [
      '#title' => t('If the node is currently in a group, what should be done with current existing group memberships?'),
      '#type' => 'radios',
      '#options' => [
        'override' => $this->t('Remove node\'s current group assignment and assign to a new group.'),
        'preserve' => $this->t('Keep the node\'s current group assignment and do not reassign to a new group.'),
      ],
      '#default_value' => 'preserve',
    ];
    return $form;
  }

}
