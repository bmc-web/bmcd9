<?php

namespace Drupal\bmc_custom;

trait HasMetatagImageNegotiatorTrait {

  public function getMetatagImageNegotiator(): MetatagImageNegotiatorInterface {
    /** @var MetatagImageNegotiatorInterface $negotiator */
    $negotiator = \Drupal::service('bmc_custom.metatag_image_negotiator');
    return $negotiator;
  }

}
