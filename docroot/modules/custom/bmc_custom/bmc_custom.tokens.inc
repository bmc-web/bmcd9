<?php

use Drupal\bmc_bundle_classes\Entity\Node\CustomMetatagImageInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\node\Entity\Node;

/**
 * Implements hook_token_info().
 */
function bmc_custom_token_info() {
  $info = [];
  $info['types']['bmc_custom'] = [
    'name' => t('Bryn Mawr College Custom Tokens'),
    'description' => t('Tokens for custom site functionality.'),
  ];
  $info['tokens']['bmc_custom']['placeholder_image_url'] = [
    'name' => 'Placeholder Image',
    'description' => t('Path to the default placeholder image. Typically used for metatags.'),
  ];
  $info['tokens']['bmc_custom']['placeholder_image_mimetype'] = [
    'name' => 'Placeholder Image Type',
    'description' => t('The default placeholder image mimetype. Typically used for metatags.'),
  ];
  $info['tokens']['bmc_custom']['placeholder_image_alt'] = [
    'name' => 'Placeholder Image Alt',
    'description' => t('The default placeholder image alt text. Typically used for metatags.'),
  ];
  $info['tokens']['bmc_custom']['front_image_url'] = [
    'name' => 'Front Image',
    'description' => t('Path to the default front image. Typically used for metatags.'),
  ];
  $info['tokens']['bmc_custom']['front_image_mimetype'] = [
    'name' => 'Front Image Type',
    'description' => t('The default front image mimetype. Typically used for metatags.'),
  ];
  $info['tokens']['bmc_custom']['front_image_alt'] = [
    'name' => 'Front Image Alt',
    'description' => t('The default front image alt text. Typically used for metatags.'),
  ];
  $info['tokens']['node']['placeholder_image_url'] = [
    'name' => 'Node Placeholder Image',
    'description' => t('The node\s placeholder image. Typically used for metatags.'),
    ];
  $info['tokens']['node']['placeholder_image_mimetype'] = [
    'name' => 'Node Placeholder Image mimetype',
    'description' => t('The node\s placeholder image mimetype. Typically used for metatags.'),
  ];
  $info['tokens']['node']['placeholder_image_width'] = [
    'name' => 'Node Placeholder Image Width',
    'description' => t('The node\s placeholder image width. Typically used for metatags.'),
  ];
  $info['tokens']['node']['placeholder_image_height'] = [
    'name' => 'Node Placeholder Image Height',
    'description' => t('The node\s placeholder image height. Typically used for metatags.'),
  ];
  $info['tokens']['node']['placeholder_image_alt'] = [
    'name' => 'Node Placeholder Image Alt text',
    'description' => t('The node\s placeholder alt text. Typically used for metatags.'),
  ];
  return $info;
}

/**
 * Implements hook_tokens().
 */
function bmc_custom_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  /** @var \Drupal\bmc_custom\MetatagImageNegotiatorInterface $imageNegotiator */
  $imageNegotiator = \Drupal::service('bmc_custom.metatag_image_negotiator');

  $replacements = [];
  if ($type == 'bmc_custom') {

    // Get the front page, if it is a node
    $node = null;
    $frontPage = \Drupal::config('system.site')->get('page.front');
    $frontPage = ltrim($frontPage, '/');
    if (strpos($frontPage, 'node/') === 0) {
      $nid = substr($frontPage, 5);
      $node = Node::load($nid);
    }

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'placeholder_image_url':
          $replacements[$original] = $imageNegotiator->getDefaultMetatagImagePath();
          break;
        case 'placeholder_image_mimetype':
          $replacements[$original] = $imageNegotiator->getDefaultMetatagImageType();
          break;
        case 'placeholder_image_alt':
          $replacements[$original] = $imageNegotiator->getDefaultMetatagImageAlt();
          break;
        case 'front_image_url':
          if ($node && $node instanceof CustomMetatagImageInterface) {
            $replacements[$original] = $imageNegotiator->getMetatagImagePathForNode($node);
          }
          else {
            $replacements[$original] = $imageNegotiator->getDefaultMetatagImagePath();
          }
          break;
        case 'front_image_mimetype':
          if ($node && $node instanceof CustomMetatagImageInterface) {
            $replacements[$original] = $imageNegotiator->getMetatagImageTypeForNode($node);
          }
          else {
            $replacements[$original] = $imageNegotiator->getDefaultMetatagImageType();
          }
          break;
        case 'front_image_alt':
          if ($node && $node instanceof CustomMetatagImageInterface) {
            $replacements[$original] = $imageNegotiator->getMetatagImageAltForNode($node);
          }
          else {
            $replacements[$original] = $imageNegotiator->getDefaultMetatagImageAlt();
          }
          break;
      }
    }
  }
  if ($type == 'node') {
    $node = $data['node'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'placeholder_image_url':
          $replacements[$original] = $imageNegotiator->getMetatagImagePathForNode($node);
          break;
        case 'placeholder_image_mimetype':
          $replacements[$original] = $imageNegotiator->getMetatagImageTypeForNode($node);
          break;
        case 'placeholder_image_width':
          $replacements[$original] = $imageNegotiator->getMetatagImageWidthForNode($node);
          break;
        case 'placeholder_image_height':
          $replacements[$original] = $imageNegotiator->getMetatagImageHeightForNode($node);
          break;
        case 'placeholder_image_alt':
          $replacements[$original] = $imageNegotiator->getMetatagImageAltForNode($node);
          break;
      }
    }
  }

  return $replacements;
}
