<?php

use Drupal\Core\Config\FileStorage;

/**
 * Implements hook_post_update_NAME
 */
function bmc_custom_post_update_www_1344(&$sandbox) {
  // As part of WWW-1344,
  // we need to update all group techdoc links to knowledgeowl
  $groups = \Drupal::entityTypeManager()
    ->getStorage('group')
    ->loadByProperties(['type' => 'group']);
  foreach ($groups as $group) {
    $desc = $group->field_gp_desc->value;
    $new_desc = preg_replace('/https:\/\/techdocs\.blogs\.brynmawr\.edu\/[^ "]*/', 'https://askathena.brynmawr.edu/help/drupal', $desc);
    $group->field_gp_desc->value = $new_desc;
    $group->field_gp_desc->format = 'intermediate_plus';
    $group->save();
  }
}

/**
 * Update in context block content bundle.
 *
 * See \Drupal\bmc_custom\Plugin\Block\InContextBlock for more information.
 *
 * It has long been a pain point that site editors are creating in context
 * blocks and placeing them in the block layout page. As of July 2024, we have
 * over 200 such blocks. There are a few issues with this approach:
 * - the block layout page is unwieldy and cumbersome to use
 * - configuration synchronization is difficult, and that can make local
 * development and deployment to acquia difficult
 *
 * To solve this problem, we create a custom block that pulls in all relevant
 * in context blocks and displays them in a single config block. That way, site
 * editors can manage blocks without updating configuration, and the block
 * layout page can be kept tidier.
 *
 * In order to make this work, we need to move certain fields from the block
 * configuration to the block content entity. This post update function does
 * that.
 *
 * First, add the following fields to the in context block bundle:
 * - field_block_visibility
 * - field_active
 * - field_weight
 *
 * Second, update all in context block content entities.
 */
function bmc_custom_post_update_www_1707(&$sandbox) {
  if (!isset($sandbox['is_in_context_block_content_fields_added'])) {
    _bmc_custom_www_1707_add_fields_to_block_content();
    $sandbox['is_in_context_block_content_fields_added'] = TRUE;
  }
  // Initialize the sandbox
  if (empty($sandbox['max'])) {
    $blocks = \Drupal::entityTypeManager()
      ->getStorage('block')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('theme', 'bmc_d9')
      ->execute();
    $block_ids = array_values($blocks);
    $sandbox['block_ids'] = $block_ids;
    $sandbox['progress'] = 0;
    $sandbox['max'] = count($block_ids);
  }
  $current_item = $sandbox['block_ids'][$sandbox['progress']];
  _bmc_custom_www_1707_process_block($current_item);
  $sandbox['progress'] += 1;
  $sandbox['#finished'] = $sandbox['progress'] / $sandbox['max'];
}

/**
 * Add fields to the in context block content bundle.
 */
function _bmc_custom_www_1707_add_fields_to_block_content() {
  $entity_type = 'block_content';
  $bundle = 'in_context_block';

  // Add field_include_block_on_path
  $field_storage = \Drupal\field\Entity\FieldStorageConfig::create([
    'field_name' => 'field_include_block_on_path',
    'entity_type' => $entity_type,
    'type' => 'string_long',
    'cardinality' => 1,
    'uuid' => '9a6ae147-3b03-46d1-a814-576e0089ce2f',
  ]);
  $field_storage->save();
  $field = \Drupal\field\Entity\FieldConfig::create([
    'field_name' => 'field_include_block_on_path',
    'entity_type' => $entity_type,
    'bundle' => $bundle,
    'label' => 'Show this block on the following paths',
    'uuid' => '3df7ea2f-e41e-4a79-96cd-aceb63ebcd89',
    'description' => 'Enter one path per line. Use * as a wildcard. The paths must start with a slash. Example: /node/*',
  ]);
  $field->save();

  // Add field_exclude_block_on_path
  $field_storage = \Drupal\field\Entity\FieldStorageConfig::create([
    'field_name' => 'field_exclude_block_on_path',
    'entity_type' => $entity_type,
    'type' => 'string_long',
    'cardinality' => 1,
    'uuid' => '4a6c1c45-76ab-417a-8c4b-c638cca689d4',
  ]);
  $field_storage->save();
  $field = \Drupal\field\Entity\FieldConfig::create([
    'field_name' => 'field_exclude_block_on_path',
    'entity_type' => $entity_type,
    'bundle' => $bundle,
    'label' => 'Do NOT show this block on the following paths',
    'uuid' => '2b771f67-83a7-4a55-900d-2a1246800c83',
    'description' => 'Enter one path per line. Use * as a wildcard. The paths must start with a slash. This block will not show on these paths, even if it is included in the "Show this block on the following paths" field.',
  ]);
  $field->save();

  // Add field_active
  $field_storage = \Drupal\field\Entity\FieldStorageConfig::create([
    'field_name' => 'field_active',
    'entity_type' => $entity_type,
    'type' => 'boolean',
    'cardinality' => 1,
    'uuid' => '8f61beaa-bf45-4196-a8f7-f66010cb18dd',
  ]);
  $field_storage->save();
  $field = \Drupal\field\Entity\FieldConfig::create([
    'field_name' => 'field_active',
    'entity_type' => $entity_type,
    'bundle' => $bundle,
    'label' => 'Active',
    'default_value' => [
      ['value' => 1],
    ],
    'uuid' => 'd6df02d1-c463-40f0-90ff-fe4ad94c1fbc',
  ]);
  $field->save();

  // Add field_weight
  $field_storage = \Drupal\field\Entity\FieldStorageConfig::create([
    'field_name' => 'field_weight',
    'entity_type' => $entity_type,
    'type' => 'integer',
    'cardinality' => 1,
    'uuid' => '2457e867-b0d5-4302-aef1-52297899aa7c',
  ]);
  $field_storage->save();
  $field = \Drupal\field\Entity\FieldConfig::create([
    'field_name' => 'field_weight',
    'entity_type' => $entity_type,
    'bundle' => $bundle,
    'label' => 'Weight',
    'uuid' => '9d9c3ac1-cd89-405f-8a94-e09dfd57859d',
  ]);
  $field->save();
}

/**
 * @param string $block_id
 *
 * @return void
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function _bmc_custom_www_1707_process_block(string $block_id): void {
  $block = \Drupal::entityTypeManager()->getStorage('block')->load($block_id);
  $block_content = _bmc_custom_www_1707_get_block_content_from_block($block);
  if ($block_content && $block_content->bundle() === 'in_context_block') {
    $visibility_rules = _bmc_custom_www_1707_get_block_visibility($block->getVisibility()['request_path']['pages']);
    $block_content->set('field_weight', $block->getWeight());
    $block_content->set('field_include_block_on_path', $visibility_rules['include']);
    $block_content->set('field_exclude_block_on_path', $visibility_rules['exclude']);
    $block_content->set('field_active', $block->status());
    $block_content->save();
    $block->delete();
  }
}

/**
 * @param \Drupal\block\Entity\Block|null $block
 *  A placed block.
 *
 * @return \Drupal\block_content\Entity\BlockContent|null
 *  The block content that corresponds to the placed block, or null if it does
 *   not exist.
 */
function _bmc_custom_www_1707_get_block_content_from_block(\Drupal\block\Entity\Block|null $block) {
  if ($block) {
    $block_plugin = $block->getPlugin();
    $uuid = $block_plugin->getDerivativeId() ? $block_plugin->getDerivativeId() : '';
    return \Drupal::service('entity.repository')
      ->loadEntityByUuid('block_content', $uuid);
  }
  return NULL;
}

/**
 * Thanks to the block_exclude_pages module, some of our paths begin with
 * '!' to indicate that the block should not be shown on that path. In our
 * new system, we need to separate these paths into two fields: include and
 * exclude. This function does that.
 *
 * @param $pages
 *   A string of paths separated by newlines. Some paths may begin with '!'
 *   to indicate that the block should not be shown on that path.
 *
 * @return array
 *   An array with two keys: 'include' and 'exclude'. Each key contains a
 *   string of paths separated by newlines.
 */
function _bmc_custom_www_1707_get_block_visibility($pages) {
  $return_array = [];
  $visibility_array = explode("\n", $pages);
  $include_paths = [];
  $exclude_paths = [];
  foreach ($visibility_array as $path) {
    if (strpos($path, '!') === 0) {
      // If the path begins with '!', remove the '!' and add it to the exclude'
      // array.
      $exclude_paths[] = substr($path, 1);
    }
    else {
      $include_paths[] = $path;
    }
  }
  $return_array['include'] = implode("\n", $include_paths);
  $return_array['exclude'] = implode("\n", $exclude_paths);
  return $return_array;
}

/**
 * Delete all news contact paragraphs.
 */
function bmc_custom_post_update_www_1821() {
  $paragraphs = \Drupal::entityTypeManager()
    ->getStorage('paragraph')
    ->loadByProperties(['type' => 'news_contact']);
  foreach ($paragraphs as $paragraph) {
    $paragraph->delete();
  }
}

/**
 * Delete all feature grid and feature grid items paragraphs.
 */
function bmc_custom_post_update_www_1852() {
  $paragraphs_fg = \Drupal::entityTypeManager()
    ->getStorage('paragraph')
    ->loadByProperties(['type' => 'feat_grid']);
  foreach ($paragraphs_fg as $paragraph) {
    $paragraph->delete();
  }

  $paragraphs_fg_items = \Drupal::entityTypeManager()
    ->getStorage('paragraph')
    ->loadByProperties(['type' => 'feat_grid_item']);
  foreach ($paragraphs_fg_items as $paragraph) {
    $paragraph->delete();
  }
}

/**
 * Delete all social media connect and social media connect items paragraphs.
 */
function bmc_custom_post_update_www_1853() {
  $paragraphs_smc = \Drupal::entityTypeManager()
    ->getStorage('paragraph')
    ->loadByProperties(['type' => 'social_media_connect']);
  foreach ($paragraphs_smc as $paragraph) {
    $paragraph->delete();
  }

  $paragraphs_smc_items = \Drupal::entityTypeManager()
    ->getStorage('paragraph')
    ->loadByProperties(['type' => 'social_media_connect_item']);
  foreach ($paragraphs_smc_items as $paragraph) {
    $paragraph->delete();
  }
}

/**
 * Delete all giving footer block content entities.
 */
function bmc_custom_post_update_www_1878() {
  $giving_footer_blocks = \Drupal::entityTypeManager()
    ->getStorage('block_content')
    ->loadByProperties(['type' => 'giving_footer_block']);
  foreach ($giving_footer_blocks as $block) {
    $block->delete();
  }
}

/**
 * Delete all fab block content and paragraph entities.
 */
function bmc_custom_post_update_www_1875() {
  $fab_blocks = \Drupal::entityTypeManager()
    ->getStorage('block_content')
    ->loadByProperties(['type' => 'fab']);
  foreach ($fab_blocks as $block) {
    $block->delete();
  }

  $fab_paragraphs = \Drupal::entityTypeManager()
    ->getStorage('paragraph')
    ->loadByProperties(['type' => 'fab']);
  foreach ($fab_paragraphs as $paragraph) {
    $paragraph->delete();
  }
}

/**
 * Migrate content from pre_footer blocks to header & footer config page.
 */
function bmc_custom_post_update_www_1879() {
  _bmc_custom_post_update_www_1879_import_config_page_fields();
  _bmc_custom_post_update_www_1879_migrate_prefooter_blocks_to_config_page();
  _bmc_custom_post_update_www_1879_delete_prefooter_blocks();
}

/**
 * Unfortunately, we need to import some of our configuration before we can
 * migrate the pre_footer blocks to the header & footer config page. This is not
 * ideal.
 *
 * In a perfect world, we would create new fields during the config import step
 * of drush deploy, and then populate the fields during a post_update hook.
 *
 * However, the config import step deletes the fields we're trying to migrate
 * from. And, we need to delete the corresponding entities before we can import
 * the configuration deleting those fields.
 *
 * So, we create the fields to migrate into during hook_post_update.
 *
 * See
 * https://www.metaltoad.com/blog/programmatically-importing-drupal-8-field-configurations
 * See
 * https://blokspeed.net/2019/creating-fields-and-other-configuration-during-updates-drupal
 */
function _bmc_custom_post_update_www_1879_import_config_page_fields() {
  // Obtain configuration from yaml files
  $config_path = \Drupal\Core\Site\Settings::get('config_sync_directory');
  $fileStorage = new FileStorage($config_path);

  $fieldStorage = \Drupal::entityTypeManager()
    ->getStorage('field_storage_config');
  $fieldConfigStorage = \Drupal::entityTypeManager()
    ->getStorage('field_config');

  $entity_type = 'config_pages';
  $bundle = 'header_footer_wrapper';
  $fields = [
    'field_cp_wrapper_gsas_footer_img',
    'field_cp_wrapper_gsas_links',
    'field_cp_wrapper_gsas_pf_cta',
    'field_cp_wrapper_gsas_pf_img',
    'field_cp_wrapper_gsas_pf_title',
    'field_cp_wrapper_ibmc_footer_img',
    'field_cp_wrapper_ibmc_links',
    'field_cp_wrapper_ibmc_pf_cta',
    'field_cp_wrapper_ibmc_pf_img',
    'field_cp_wrapper_ibmc_pf_title',
    'field_cp_wrapper_mkt_footer_img',
    'field_cp_wrapper_mkt_links',
    'field_cp_wrapper_mkt_pf_cta',
    'field_cp_wrapper_mkt_pf_img',
    'field_cp_wrapper_mkt_pf_title',
    'field_cp_wrapper_pstbc_footer_im',
    'field_cp_wrapper_pstbc_links',
    'field_cp_wrapper_pstbc_pf_cta',
    'field_cp_wrapper_pstbc_pf_img',
    'field_cp_wrapper_pstbc_pf_title',
    'field_cp_wrapper_sw_footer_img',
    'field_cp_wrapper_sw_links',
    'field_cp_wrapper_sw_pf_cta',
    'field_cp_wrapper_sw_pf_img',
    'field_cp_wrapper_sw_pf_title',
  ];

  foreach ($fields as $field) {
    if (empty($fieldStorage->load("$entity_type.$field"))) {
      $fieldStorage
        ->createFromStorageRecord($fileStorage->read("field.storage.$entity_type.$field"))
        ->save();
    }
    if (empty($fieldConfigStorage->load("$entity_type.$bundle.$field"))) {
      $fieldConfigStorage
        ->createFromStorageRecord($fileStorage->read("field.field.$entity_type.$bundle.$field"))
        ->save();
    }
  }
}

/**
 * Copy field values from pre_footer blocks to header & footer config page.
 */
function _bmc_custom_post_update_www_1879_migrate_prefooter_blocks_to_config_page() {
  /** @var \Drupal\config_pages\ConfigPagesLoaderServiceInterface $configPageLoader */
  $configPageLoader = \Drupal::service('config_pages.loader');
  /** @var \Drupal\config_pages\Entity\ConfigPages $headerFooterConfigPage */
  $headerFooterConfigPage = $configPageLoader->load('header_footer_wrapper');

  $prefixes = [
    'mkt',
    'gsas',
    'sw',
    'pstbc',
  ];

  foreach ($prefixes as $prefix) {
    $blockField = $headerFooterConfigPage->get('field_cp_wrapper_' . $prefix . '_prefooter');
    /** @var \Drupal\block_content\Entity\BlockContent $block */
    $block = $blockField->entity;
    $headerFooterConfigPage->set('field_cp_wrapper_' . $prefix . '_pf_cta', $block->field_b_prefooter_cta->getValue());
    $headerFooterConfigPage->set('field_cp_wrapper_' . $prefix . '_pf_img', $block->field_b_prefooter_img->getValue());
    $headerFooterConfigPage->set('field_cp_wrapper_' . $prefix . '_pf_title', $block->field_b_prefooter_title->getValue());
  }

  $headerFooterConfigPage->save();
}

/**
 * Delete the pre_footer blocks.
 */
function _bmc_custom_post_update_www_1879_delete_prefooter_blocks() {
  $blocks = \Drupal::entityTypeManager()
    ->getStorage('block_content')
    ->loadByProperties(['type' => 'prefooter']);
  foreach ($blocks as $block) {
    $block->delete();
  }
}
