(function($, Drupal, once) {
  Drupal.behaviors.kwoodcountdown = {
    attach: function(context, settings) {
      once('kwoodcountdown', '.kwoodcountdown', context).forEach(function (element) {
        const el = $(element);
        const timer = el.find('.kwoodcountdown__timer');
        const timer_id = el.attr('data-kwoodcountdown-timer-id');
        const timerSettings = settings.kwoodcountdown_timer[timer_id];
        const targetDate = new Date(timerSettings.datetime);
        timer.countdown({
          [timerSettings.mode]: targetDate,
          expiryText: timerSettings.finished_text,
          alwaysExpire: true,
          format: timerSettings.format,
        });
      });
    }
  }
})(jQuery, Drupal, once);
