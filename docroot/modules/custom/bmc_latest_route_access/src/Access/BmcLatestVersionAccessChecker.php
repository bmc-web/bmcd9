<?php

namespace Drupal\bmc_latest_route_access\Access;

use \Drupal\content_moderation\Access\LatestRevisionCheck;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Checks if passed parameter matches the route configuration.
 *
 * @DCG
 * To make use of this access checker add '_bmc_latest_route_access: Some value' entry to route
 * definition under requirements section.
 */
class BmcLatestVersionAccessChecker extends LatestRevisionCheck {

  protected $entityTypeManager;

  /**
   * Constructs a new LatestRevisionCheck.
   *
   * @param \Drupal\content_moderation\ModerationInformationInterface $moderation_information
   *   The moderation information service.
   */
  public function __construct(
    ModerationInformationInterface $moderationInfo,
    EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($moderationInfo);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritDoc}
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $entity = $this->loadEntity($route, $route_match);
    if ($this->moderationInfo->hasPendingRevision($entity)) {
      $groupContentStorage = $this->entityTypeManager->getStorage('group_content');
      $group_content_entities = $groupContentStorage->loadByEntity($entity);
      if (count($group_content_entities) > 0) {
        foreach ($group_content_entities as $group_content_entity) {
          $group = $group_content_entity->getGroup();
          if ($group->hasPermission('view group_node:' . $entity->bundle() . ' entity', $account)) {
            return AccessResult::allowed();
          }
          else {
            return AccessResult::forbidden();
          }
        }
      }
      else {
        return parent::access($route, $route_match, $account);
      }
    }
    else {
      return parent::access($route, $route_match, $account);
    }
  }

}
