<?php

namespace Drupal\bmc_latest_route_access\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 *
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * @inheritDoc
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.node.latest_version')) {
      $requirements = $route->getRequirements();
      unset($requirements['_content_moderation_latest_version']);
      $route->setRequirements($requirements);
      $route->setRequirement('_bmc_latest_route_access', 'TRUE');
    }
  }

}
