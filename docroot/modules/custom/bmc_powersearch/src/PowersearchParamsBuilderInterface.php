<?php

namespace Drupal\bmc_powersearch;

interface PowersearchParamsBuilderInterface {

  public function getEntityTypesToSearch(): array;

  public function setEntityTypesToSearch(array $types): PowersearchParamsBuilderInterface;

  public function getFieldTypesToSearch(): array;

  public function setFieldTypesToSearch(array $types): PowersearchParamsBuilderInterface;

  public function getFieldsToIgnore(): array;

  public function setFieldsToIgnore($fields_to_ignore): PowersearchParamsInterface;

  public function getEntityQueryRevisionContstraint(): string;

  public function setEntityQueryRevisionContstraint(string $constraint): PowersearchParamsBuilderInterface;

  public function getSearchOperator(): string;

  public function setSearchOperator(string $constraint): PowersearchParamsBuilderInterface;

  public function build(): PowersearchParamsInterface;
}
