<?php

namespace Drupal\bmc_powersearch\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\bmc_powersearch\PowersearchInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\FileInterface;
use Drupal\paragraphs\ParagraphInterface;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class BmcPowersearchCommands extends DrushCommands {
  use StringTranslationTrait;

  private PowersearchInterface $powersearch;
  private EntityTypeManagerInterface $entityTypeManager;
  private MemoryCacheInterface $memoryCache;

  /**
   * {@inehritDoc}
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(
    PowersearchInterface $powersearch,
    EntityTypeManagerInterface $entityTypeManager,
    MemoryCacheInterface $memoryCache) {
    $this->powersearch = $powersearch;
    $this->entityTypeManager = $entityTypeManager;
    $this->memoryCache = $memoryCache;
  }

  /**
   * Command description here.
   *
   * @usage bmc_powersearch-powersearch foo
   * @param $string
   *   the search string
   *
   * @field-labels
   *   label: Label
   *   entity_type: Entity Type
   *   bundle: Bundle
   *   id: ID
   *   url: URL
   *   field_name: Field Machine Name
   *   info: Additional Info
   * @default-fields label,id,url,entity_type,bundle,field_name,info
   * @command bmc_powersearch:powersearch
   * @aliases powersearch
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   */
  public function powersearch(string $string, $options = ['format' => 'table']) {
    $ids = $this->powersearch->powersearch($string);
    $data = $this->preparePowersearchData($ids);
    return new RowsOfFields($data);
  }

  /**
   * Command description here.
   *
   * @usage bmc_powersearch-filesearch foo
   * @param $fid
   *   the file id
   *
   * @field-labels
   *   label: Label
   *   entity_type: Entity Type
   *   bundle: Bundle
   *   id: ID
   *   url: URL
   *   field_name: Field Machine Name
   *   info: Additional Info
   * @default-fields label,id,url,entity_type,bundle,field_name,info
   * @command bmc_powersearch:filesearch
   * @aliases filesearch
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   */
  public function filesearch(int $fid, $options = ['format' => 'table']) {
    $ids = $this->powersearch->fileSearchByFid($fid);
    $data = $this->preparePowersearchData($ids);
    return new RowsOfFields($data);
  }

  /**
   * Build the RowOfFields array for each entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string $entity_type
   * @param string $field_name
   *
   * @return array
   */
  private function handleEntity(EntityInterface $entity, string $entity_type, string $field_name) {
    $returnArray = [
      'label' => $entity->label(),
      'entity_type' => $entity_type,
      'bundle' => $entity->bundle(),
      'id' => $entity->id(),
      'field_name' => $field_name,
      'info' => '',
      'url' => $this->getPowersearchEntityURL($entity),
    ];
    // Provide special handling for entity types in this switch statement.
    switch ($entity_type) {
      case 'paragraph':
        $this->handleParagraph($entity, $returnArray);
        break;
      case 'node':
        if (!$entity->isPublished()) {
          $returnArray['info'] .= "Unpublished page.";
        }
    }
    return $returnArray;
  }

  /**
   * Get the entity url.
   * @param EntityInterface $entity
   * @return \Drupal\Core\GeneratedUrl|string
   */
  private function getPowersearchEntityURL(EntityInterface $entity) {
    try {
//      $url = $entity->toUrl('canonical', ['absolute' => TRUE])->toString();
      $path = $entity->toUrl('canonical', ['absolute' => FALSE])->toString();
      $url = 'https://www.brynmawr.edu' . $path;
    }
    catch (\Exception $e) {
      $url = '';
    }
    return $url;
  }

  /**
   * For paragraphs, we want to return info about the closest non-paragraph ancestor.
   * If the paragraph is an orphan, say so.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   * @param array $returnArray
   */
  private function handleParagraph(ParagraphInterface $paragraph, array &$returnArray) {
    if (!(($parent = $paragraph->getParentEntity()) && $parent->hasField($paragraph->get('parent_field_name')->value))) {
      $returnArray['label'] = $this->t('An orhpaned paragraph');
    }
    else {
      if (strpos($returnArray['label'], '(previous revision)') !== FALSE) {
        $returnArray['info'] = $this->t('Paragraph match is likely from a previous revision of this node. The paragraph id is @pid. ',
          ['@pid' => $returnArray['id']]);
      }
      $ancestor = $this->getNonParaAncestor($paragraph);
      $returnArray['label'] = $ancestor->label();
      $returnArray['entity_type'] = $ancestor->getEntityTypeId();
      $returnArray['bundle'] = $ancestor->bundle();
      $returnArray['id'] = $ancestor->id();
      $returnArray['url'] = $this->getPowersearchEntityURL($ancestor);
      if ($ancestor->getEntityTypeId() == 'node') {
        if (!$ancestor->isPublished()) {
          $returnArray['info'] .= "Unpublished page.";
        }
      }
    }
  }

  /**
   * Get the nearest non-paragraph ancestor entity of the bundle.
   *
   * @param $entity
   *
   * @return mixed
   */
  private function getNonParaAncestor($entity): EntityInterface {
    if ($entity->getParentEntity() instanceof ParagraphInterface) {
      $returnEntity = $this->getNonParaAncestor($entity->getParentEntity());
    }
    else {
      $returnEntity = $entity->getParentEntity();
    }
    return $returnEntity;
  }

  /**
   * Usort the results of the powersearch by entity type, then bundle, then entity id.
   *
   * @param $a
   * @param $b
   *
   * @return int
   */
  private static function powerSearchSort($a, $b): int {
    if ($a['entity_type'] != $b['entity_type']) {
      return $a['entity_type'] > $b['entity_type'] ? 1 : -1;
    }
    else {
      if ($a['bundle'] != $b['bundle']) {
        return $a['bundle'] > $b['bundle'] ? 1 : -1;
      }
      else {
        return $a['id'] > $b['id'] ? 1 : -1;
      }
    }
  }

  private function preparePowersearchData($ids): array {
    $data = [];
    foreach ($ids as $entity_type => $field) {
      $storage = $this->entityTypeManager->getStorage($entity_type);
      foreach ($field as $field_name => $eids) {
        foreach ($eids as $id) {
          // If we don't do this we run out of memory on some searches.
          $this->memoryCache->deleteAll();
          $entity = $storage->load($id);
          $infoArray = $this->handleEntity($entity, $entity_type, $field_name);
          $data[] = $infoArray;
        }
      }
    }
    usort($data, [$this, 'powerSearchSort']);
    return $data;
  }

  /**
   * Command description here.
   *
   * @usage bmc_powersearch:filesearch-by-uri foo
   * @param $uri
   *   the search string
   * @field-labels
   *   label: Label
   *   entity_type: Entity Type
   *   bundle: Bundle
   *   id: ID
   *   url: URL
   *   field_name: Field Machine Name
   *   info: Additional Info
   * @default-fields label,id,url,entity_type,bundle,field_name,info
   *
   * @command bmc_powersearch:filesearch-by-uri
   * @aliases fsu
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   */
  public function filesearchByUri(string $uri, $options = ['format' => 'table']) {
    $file = $this->getFileFromUri($uri);
    if (is_null($file)) {
      return new RowsOfFields([]);
    }
    $fid = $file->id();
    if (is_null($fid)) {
      return new RowsOfFields([]);
    }
    $data = $this->powersearch->fileSearchByFid($fid);
    $results = $this->preparePowersearchData($data);
    return new RowsOfFields($results);
  }

  private function getFileFromUri(string $uri): ?FileInterface {
    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = \Drupal::service('file_system');
    $basename = $file_system->basename($uri);
    /** @var \Drupal\file\FileStorageInterface $fileStorage */
    $fileStorage = \Drupal::entityTypeManager()
      ->getStorage('file');
    $files = $fileStorage->loadByProperties(['filename' => urldecode($basename)]);
    if (empty($files)) {
      return null;
    }
    /** @var \Drupal\file\FileInterface $file */
    foreach ($files as $file) {
      $file_uri = $file->getFileUri();
      $file_info = parse_url($file_uri);
      $file_path = '/' . $file_info['host'] . $file_info['path'];
      if (str_contains($uri, $file_path)) {
        return $file;
      }
    }
    return null;
  }

}
