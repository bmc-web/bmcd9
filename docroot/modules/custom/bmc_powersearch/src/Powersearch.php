<?php

namespace Drupal\bmc_powersearch;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Powersearch service.
 */
class Powersearch implements PowersearchInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The Drupal config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a Powersearch object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    EntityFieldManagerInterface $entityFieldManager,
    ConfigFactoryInterface $configFactory
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->configFactory = $configFactory;
  }

  /**
   *
   */
  public function fileSearchByFid(int $fid, ?PowersearchParamsInterface $params = NULL): array {
    $stringSearchParams = $this->ensureParams($params);
    $fileStorage = $this->entityTypeManager->getStorage('file');
    $return_array = [];

    // Get the file.
    $file = $fileStorage->load($fid);
    if (!$file) {
      return [];
    }

    // Get the file path.
    $file_url = $file->createFileUrl();
    // Trim the leading forward slash from the url
    $file_url = ltrim($file_url, '/');
    // Powersearch on the file path.
    // This is a string search.
    $file_path_string_search_results = $this->powersearch($file_url, $stringSearchParams); // this is a string search

    // Powersearch on the file id.
    // Since this is an exact integer search, set search operator to '='.
    $fileSearchParams = $this->ensureParams();
    $fileSearchParams->setFieldTypesToSearch(['file', 'image']);
    $fileSearchParams->setSearchOperator('=');
    $file_fid_search_results = $this->powersearch($fid, $fileSearchParams);

    // Get the media object.
    $mediaStorage = $this->entityTypeManager->getStorage('media');
    $media_query_results = $mediaStorage->getQuery()->accessCheck(FALSE)->condition('field_media_file', $fid)->execute();
    $media_ids = count($media_query_results) > 0 ? array_values($media_query_results) : NULL;
    if ($media_ids == NULL) {
      return $this->filesearchCombineResults($file_fid_search_results, $file_path_string_search_results);
    }
    else {
      // Powersearch on the media path.
      // This is a string search.
      $media_search_results = [];
      foreach ($media_ids as $media_id) {
        $media_search_results[$media_id] = $this->powersearch('/media/' . $media_id);
      }

      return $this->filesearchCombineResults($file_fid_search_results, $file_path_string_search_results, $media_search_results);

    }
  }

  /**
   * @inheritdoc
   */
  public function powersearch(mixed $search_string, ?PowersearchParamsInterface $params = NULL): array {
    $params = $this->ensureParams($params);
    $reduced_field_map = $this->getReducedFieldMap($params->getFieldTypesToSearch(), $params->getEntityTypesToSearch(), $params->getFieldsToIgnore());
    $results_map = $this->doQueries($search_string, $reduced_field_map, $params->getEntityQueryRevisionContstraint(), $params->getSearchOperator());
    return $results_map;
  }

  /**
   *
   */
  public function getParamsBuilder(): PowersearchParamsBuilderInterface {
    return new PowersearchParamsBuilder($this->configFactory->get('bmc_powersearch.settings'));
  }

  /**
   * Get a field map of only the fields we care about (string-like fields)
   *
   * @return array
   */
  private function getReducedFieldMap(array $field_types_to_search, array $entity_types_to_search, array $fields_to_ignore = []): array {
    $reduced_field_map = [];
    foreach ($field_types_to_search as $field_type) {
      $field_map = $this->entityFieldManager->getFieldMapByFieldType($field_type);
      foreach ($field_map as $entity_type => $fields) {
        if (in_array($entity_type, $entity_types_to_search)) {
          foreach (array_keys($fields) as $field_name) {
            if (!in_array($field_name, $fields_to_ignore)) {
              $reduced_field_map[$entity_type][] = $field_name;
            }
          }
        }
      }
    }
    return $reduced_field_map;
  }

  /**
   * Loop through the reduced field map and perform queries.
   *
   * @param string $search_string
   * @param array $reduced_field_map
   *
   * @return array
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function doQueries(string $search_string, array $reduced_field_map, string $entity_query_revision_constraint, string $search_operator = 'CONTAINS'): array {
    $results_map = [];
    foreach ($reduced_field_map as $entity_type => $fields) {
      $storage = $this->entityTypeManager->getStorage($entity_type);
      foreach ($fields as $field) {
        // Set access check to FALSE because groups.
        $query = $storage->getQuery()->accessCheck(FALSE)->condition($field, $search_string, $search_operator);
        switch ($entity_query_revision_constraint) {
          case EntityQueryRevisionConstraints::ENTITY_QUERY_REVISIONS_ALL:
            break;

          case EntityQueryRevisionConstraints::ENTITY_QUERY_REVISIONS_CURRENT:
            $query->currentRevision();
            break;

          case EntityQueryRevisionConstraints::ENTITY_QUERY_REVISIONS_LATEST:
            $query->latestRevision();
            break;
        }
        $ids = $query->execute();
        if (count($ids) > 0) {
          $results_map[$entity_type][$field] = array_values($ids);
        }
      }
    }
    return $results_map;
  }

  /**
   *
   */
  private function ensureParams(?PowersearchParamsInterface $params = NULL): PowersearchParamsInterface {
    $params = is_null($params) ? $this->getParamsBuilder()->build() : $params;
    return $params;
  }

  /**
   *
   */
  protected function filesearchCombineResults($array1, $array2, $array3 = NULL): array {
    $combined_results = $this->doCombine([], $array1);
    $combined_results = $this->doCombine($combined_results, $array2);
    if (!is_null($array3)) {
      foreach ($array3 as $value) {
        $combined_results = $this->doCombine($combined_results, $value);
      }
    }
    return $combined_results;
  }

  protected function doCombine(array $combined_results, array $array_to_add_to_results): array {
    foreach ($array_to_add_to_results as $entity_machine_name => $array_of_fields_names) {
      foreach ($array_of_fields_names as $field_machine_name => $array_of_entity_ids) {
        if (!array_key_exists($entity_machine_name, $combined_results)) {
          $combined_results[$entity_machine_name] = [];
        }
        if (!array_key_exists($field_machine_name, $combined_results[$entity_machine_name])) {
          $combined_results[$entity_machine_name][$field_machine_name] = [];
        }
        foreach ($array_of_entity_ids as $id) {
          if (!in_array($id, $combined_results[$entity_machine_name][$field_machine_name])) {
            $combined_results[$entity_machine_name][$field_machine_name][] = $id;
          }
        }
      }
    }
    return $combined_results;
  }

}
