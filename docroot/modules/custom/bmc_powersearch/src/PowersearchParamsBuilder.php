<?php

namespace Drupal\bmc_powersearch;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;

/**
 * PowersearchParamsBuilder service.
 */
class PowersearchParamsBuilder implements PowersearchParamsBuilderInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var PowersearchParamsInterface
   */
  protected $params;

  /**
   * Constructs a PowersearchParamsBuilder object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ImmutableConfig $config) {
    $this->config = $config;
    $this->params = new PowersearchParams();
    $this->params->setFieldTypesToSearch($this->config->get('default_params.field_types'));
    $this->params->setEntityTypesToSearch($this->config->get('default_params.entity_types'));
    $this->params->setFieldsToIgnore($this->config->get('default_params.fields_to_ignore'));
    $this->params->setEntityQueryRevisionContstraint(constant($this->config->get('default_params.query_revision')));
    $this->params->setSearchOperator($this->config->get('default_params.search_operator'));
  }

  public function build(): PowersearchParamsInterface {
    return $this->params;
  }

  public function getEntityTypesToSearch(): array {
    return $this->params->getEntityTypesToSearch();
  }

  public function setEntityTypesToSearch(array $types): PowersearchParamsBuilderInterface {
    $this->params->setEntityTypesToSearch($types);
    return $this;
  }

  public function getFieldTypesToSearch(): array {
    return $this->params->getFieldTypesToSearch();
  }

  public function setFieldTypesToSearch(array $types): PowersearchParamsBuilderInterface {
    $this->params->setFieldTypesToSearch($types);
    return $this;
  }

  public function getFieldsToIgnore(): array {
    return $this->params->getFieldsToIgnore();
  }

  public function setFieldsToIgnore($fields_to_ignore): PowersearchParamsInterface {
    $this->params->setFieldsToIgnore($fields_to_ignore);
    return $this;
  }

  public function getEntityQueryRevisionContstraint(): string {
    return $this->params->getEntityQueryRevisionContstraint();
  }

  public function setEntityQueryRevisionContstraint(string $constraint): PowersearchParamsBuilderInterface {
    $this->params->setEntityQueryRevisionContstraint($constraint);
    return $this;
  }

  public function getSearchOperator(): string {
    return $this->params->getSearchOperator();
  }

  public function setSearchOperator(string $search_operator): PowersearchParamsBuilderInterface {
    $this->params->setSearchOperator($search_operator);
    return $this;
  }
}
