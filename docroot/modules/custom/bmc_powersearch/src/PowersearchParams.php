<?php

namespace Drupal\bmc_powersearch;

class PowersearchParams implements PowersearchParamsInterface {

  private $entity_types;

  private $field_types;

  private $fields_to_ignore;

  private $entity_query_revision_constraint;

  private $search_operator;

  public function __construct() {}

  public function getEntityTypesToSearch(): array {
    return $this->entity_types;
  }

  public function setEntityTypesToSearch(array $types): PowersearchParamsInterface {
    $this->entity_types = $types;
    return $this;
  }

  public function getFieldTypesToSearch(): array {
    return $this->field_types;
  }

  public function setFieldTypesToSearch(array $types): PowersearchParamsInterface {
    $this->field_types = $types;
    return $this;
  }

  public function getFieldsToIgnore(): array {
    return $this->fields_to_ignore;
  }

  public function setFieldsToIgnore($fields_to_ignore): PowersearchParamsInterface {
    $this->fields_to_ignore = $fields_to_ignore;
    return $this;
  }

  public function getEntityQueryRevisionContstraint(): string {
    return $this->entity_query_revision_constraint;
  }

  public function setEntityQueryRevisionContstraint(string $constraint): PowersearchParamsInterface {
    $this->entity_query_revision_constraint = $constraint;
    return $this;
  }

  public function getSearchOperator(): string {
    return $this->search_operator;
  }

  public function setSearchOperator(string $search_operator): PowersearchParamsInterface {
    $this->search_operator = $search_operator;
    return $this;
  }

}
