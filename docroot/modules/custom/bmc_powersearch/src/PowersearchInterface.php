<?php

namespace Drupal\bmc_powersearch;

/**
 * An interface for the Powersearch service.
 */
interface PowersearchInterface {

  /**
   * Iteratively query string-like fields across entity types and return a map of hits.
   *
   * @param string $search_string
   *   The string to search for.
   * @param \Drupal\bmc_powersearch\PowersearchParamsInterface $params
   *   Parameters limiting the scope of the PowerSearch query.
   *
   * @return array
   *   A nested array of entity types and ids matching the results.
   */
  public function powersearch(mixed $search_string, ?PowersearchParamsInterface $params = NULL): array;

  public function fileSearchByFid(int $fid, ?PowersearchParamsInterface $params = NULL): array;

  /**
   * Returns a PowersearchParamsBuilderInterface.
   */
  public function getParamsBuilder(): PowersearchParamsBuilderInterface;

}
