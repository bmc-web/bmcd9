<?php

namespace Drupal\bmc_powersearch;

interface PowersearchParamsInterface {

  public function getEntityTypesToSearch(): array;

  public function setEntityTypesToSearch(array $types): PowersearchParamsInterface;

  public function getFieldTypesToSearch(): array;

  public function getFieldsToIgnore(): array;
  public function setFieldsToIgnore(array $fields_to_ignore): PowersearchParamsInterface;

  public function setFieldTypesToSearch(array $types): PowersearchParamsInterface;

  public function getEntityQueryRevisionContstraint(): string;

  public function setEntityQueryRevisionContstraint(string $constraint): PowersearchParamsInterface;

  public function getSearchOperator(): string;

  public function setSearchOperator(string $constraint): PowersearchParamsInterface;

}
