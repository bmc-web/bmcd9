<?php

namespace Drupal\bmc_powersearch;

final class EntityQueryRevisionConstraints {

  public const ENTITY_QUERY_REVISIONS_ALL = 'all';

  public const ENTITY_QUERY_REVISIONS_CURRENT = 'current';

  public const ENTITY_QUERY_REVISIONS_LATEST = 'latest';

}
