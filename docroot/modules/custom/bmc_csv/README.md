# BMC_CSV

This module provides two services to assist with importing text delimited files into custom Drupal tables.

Michael Harris - 2023-01-30

## Requirements
No Special Requirements

## Use

### bmc_csv.importer (Drupal\bmc_csv\CSVImporter)

This class extracts data from a text delimited file and imports it into a specified table. The table must already exist
on the Drupal site; this module will not create the table.

If you can use a migration, you should. The Migrate module requires that each row of the source have a unique key.
If your CSV does not have a unique key, this service (along with custom tables) provides a fallback.

To run the import, first create a CsvImportDefinition object using CSVImporter::definition(). After building the
definition, execute the import using CSVImporter::import($definition).

### bmc_csv.psfeeds_fixes (Drupal\bmc_csv\PsfeedsFixes)

The feeds that to Bryn Mawr's Drupal from AIS have character formatting issues. Formerly, we fixed these formatting
issues with our perl scripts. We've migrated those fixes into this service. Use
$string = PsfeedsFixes::doAllFiexes($string) to fix the issues.

### Events
Rather than implement the Psfeed Fixes in the CSVImporter service, we split them out so that CSVImporter could be used
for future potential feeds that do not necessarily have the encoding issues that Bryn Mawr's psfeeds have. But, having
split the functionality into muiltiple services, we have to ensure that the fixes can be applied when needed.

We emit an event as part of the CSVImporter::import, before the fields are assembled into an array for inserting into
the database (\Drupal\bmc_csv\Event\BmcCsvEvents::PRE_ASSEMBLE_FIELDS). As part of your CsvImportDefinition, provide a name
that can be used to identify the import. Then, create an EventSubscriber to listen for the event, and modify the data
or the column names. See \Drupal\bmc_course\EventSubscriber\PsfeedPreAssembleFieldSubscriber for an example.
