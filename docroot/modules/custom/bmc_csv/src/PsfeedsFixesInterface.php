<?php

namespace Drupal\bmc_csv;

/**
 * PsfeedsFixes service.
 *
 * As of 2023-02-02, the psfeeds that come from AIS have always had weird
 * encoding issues. The perl scripts that handled these feeds previously had
 * code that fixed encoding issues prior to importing. This service attempts
 * to recreate those fixes.
 */
interface PsfeedsFixesInterface {

  /**
   * Runs a string through all fixes.
   */
  public function doAllFiexes(string $string): string;

  /**
   * Tries to set the string into utf-8.
   */
  public function windowsFilter(string $string): string;

  /**
   * Tries to fix windows character encoding issues.
   */
  public function asciiToUtf(string $string): string;

  /**
   * Tries to remove the scroll-to fragments from the string.
   */
  public function removeScrollToFragments(string $markup): string;

}
