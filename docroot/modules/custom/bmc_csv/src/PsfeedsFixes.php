<?php

namespace Drupal\bmc_csv;

/**
 * PsfeedsFixes service.
 *
 * As of 2023-02-02, the psfeeds that come from AIS have always had weird
 * encoding issues. The perl scripts that handled these feeds previously had
 * code that fixed encoding issues prior to importing. This service attempts
 * to recreate those fixes.
 */
class PsfeedsFixes implements PsfeedsFixesInterface {

  /**
   * Runs a string through all fixes.
   */
  public function doAllFiexes($string): string {
    $string = $this->windowsFilter($string);
    $string = $this->asciiToUtf($string);
    $string = $this->removeScrollToFragments($string);
    return trim($string);
  }

  /**
   * Tries to set the string into utf-8.
   */
  public function asciiToUtf(string $string): string {
    return mb_convert_encoding($string, 'UTF-8', 'CP1252');
  }

  /**
   * Tries to fix windows character encoding issues.
   */
  public function windowsFilter(string $string): string {
    $search = [
      chr(145),
      chr(146),
      chr(147),
      chr(148),
      chr(150),
      chr(151),
    ];

    $replace = [
      "'",
      "'",
      '"',
      '"',
      '-',
      '-',
    ];

    return str_replace($search, $replace, $string);
  }

  /**
   * Tries to remove the scroll-to fragments from the string.
   */
  public function removeScrollToFragments(string $markup): string {
    $pattern = '/<a\s+(.*?)href=["\'](.*?):~:text(.*?)["\'](.*?)>/i';
    $replacement = '<a $1href="$2"$4>';
    $newMarkup = preg_replace($pattern, $replacement, $markup);
    return $newMarkup;
  }

}
