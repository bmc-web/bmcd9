<?php

namespace Drupal\bmc_csv\Event;

/**
 * Events fired by the CSVImporter service.
 */
final class BmcCsvEvents {


  /**
   * The event fired immediately before fields are assembled.
   *
   * Use this event to modify data before it is assembled as part of the
   * bmc_csv.importer service.
   *
   * @Event
   *
   * @see \Drupal\bmc_csv\Event\PreAssembleFieldsEvent
   *
   * @var string
   */
  const PRE_ASSEMBLE_FIELDS = 'bmc_csv.pre_assemble_fields';

}
