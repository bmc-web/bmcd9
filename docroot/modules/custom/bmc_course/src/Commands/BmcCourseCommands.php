<?php

namespace Drupal\bmc_course\Commands;

use Drush\Commands\DrushCommands;
use Drupal\bmc_course\PsfeedsImporter;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class BmcCourseCommands extends DrushCommands {

  protected PsfeedsImporter $psfeedsImporter;

  /**
   *
   */
  public function __construct(PsfeedsImporter $feedsImporter) {
    $this->feedsImporter = $feedsImporter;
  }

  /**
   * Import the feeds
   *
   * @command bmc_course:import
   * @aliases bmc_course_import
   */
  public function import() {
    $this->feedsImporter->importFeeds();
  }

}
