<?php

namespace Drupal\bmc_course;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\bmc_course\CourseInfoInterface;

/**
 * Base class for bmc_course_renderer plugins.
 */
abstract class BmcCourseRendererPluginBase extends PluginBase implements BmcCourseRendererInterface {
  use StringTranslationTrait;

  protected string $codeType;
  protected string|array $code;
  protected string $label_override = '';
  protected $summer;
  protected Connection $database;
  protected EntityTypeManagerInterface $entityTypeManager;
  protected EntityStorageInterface $paragraphStorage;
  protected CourseInfoInterface $courseInfo;

  /**
   * {@inheritDoc}
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->paragraphStorage = $this->entityTypeManager->getStorage('paragraph');
  }

  /**
   * {@inheritDoc}
   */
  public function setDatabase($database) {
    $this->database = $database;
  }

  /**
   * {@inheritDoc}
   */
  public function setCode($codeType, $code) {
    $this->codeType = $codeType;
    $this->code = $code;
    if (in_array($code, $this->summerAllowed())) {
      $this->summer = TRUE;
    }
  }

  public function setCourseInfo(CourseInfoInterface $courseInfo) {
    $this->courseInfo = $courseInfo;
  }

  /**
   * {@inheritDoc}
   */
  public function currentTerm(int $listing = 6): string {
    $term_year = date('y');
    $term_month = date('n');

    if ($term_month < $listing) {
      $term_id = '00';
    }
    elseif ($term_month < 8 && $this->summer) {
      $term_id = '05';
    }
    else {
      $term_id = '10';
    }

    return $term_year . $term_id;
  }

  /**
   * {@inheritDoc}
   */
  public function termToText($term): string {
    // "0695" is a unique exception to the format
    if ($term == '0695') {
      return $this->t('Fall 2006');
    }
    else {
      $year_code = substr($term, 0, 2);
      $semester_code = substr($term, 2, 2);
      $year = '20' . $year_code;

      if ($semester_code == '00') {
        $semester = 'Spring';
      }
      elseif ($semester_code == '05') {
        $semester = 'Summer';
      }
      else {
        $semester = 'Fall';
      }

      return $this->t(':sem :year', [':sem' => $semester, ':year' => $year]);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function assertValidTerm($term) {
    $tests = 0;

    if (strlen($term) == 4) {
      $tests++;
    }
    if ((substr($term, 2, 2) == "00") || (substr($term, 2, 2) == "05") || (substr($term, 2, 2) == "10")) {
      $tests++;
    }
    if (preg_match("/[0-9][0-9]/", substr($term, 0, 2))) {
      $tests++;
    }

    if (($tests < 3) && ($term != "0695")) {
      print("Error: Invalid term $term\n");
      exit(0);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function summerAllowed(): array {
    return [
      'SOWK',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function genericProgramDescription() {
    $boilerplate_config = $this->getCoursesBoilerlpate();
    $output = '';
    try {
      if ($boilerplate_config && $boilerplate_config->field_cp_prog_desc_boilerplate && $boilerplate_config->field_cp_prog_desc_boilerplate->value) {
        $output .= "<div class=\"generic-description\">";
        $output .= $boilerplate_config->field_cp_prog_desc_boilerplate->value;
        $output .= "</div>";
      }
    }

    // @todo Inject drupal logger service
    catch (Exception $e) {
      \Drupal::logger('BMC Peoplesoft')->error($e->getMessage());
    }

    return [
      '#markup' => $output,
    ];
  }

  /**
   * @inheritDoc}
   */
  public function getCoursesIntro($is_sowk, $is_conc): array {
    $boilerplate_config = $this->getCoursesBoilerlpate();
    $container = ['#type' => 'container'];
    $container['top'] = ['#markup' => "<a id=\"top\"></a>"];
    $container[] = ['#markup' => $boilerplate_config->field_cp_courses_intro->value];
    if ($is_sowk) {
      $container[] = ['#markup' => $boilerplate_config->field_cp_courses_sowk_info->value];
    }
    else {
      $container[] = ['#markup' => $boilerplate_config->field_cp_courses_info->value];
    }
    if ($is_conc) {
      $container[] = ['#markup' => $boilerplate_config->field_cp_courses_conc_info->value];
    }

    return $container;
  }

  public function setLabelOverride(string $label) {
    $this->label_override = $label;
  }

  public function getLabel() {
    if ($this->label_override) {
      return $this->label_override;
    }

    elseif ($this->codeType == 'subject') {
      return implode(', ', $this->code);
    }
    else {
      return $this->code;
    }
  }

  /**
   * Return the courses boilerplate config page entity
   */
  protected function getCoursesBoilerlpate() {
    $configStorage = $this->entityTypeManager->getStorage('config_pages');
    return $configStorage->load('bmc_courses_boilerplate');
  }

}
