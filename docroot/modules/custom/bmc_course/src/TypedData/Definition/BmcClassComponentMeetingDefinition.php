<?php

namespace Drupal\bmc_course\TypedData\Definition;

use Drupal\Core\TypedData\ComplexDataDefinitionBase;
use Drupal\Core\TypedData\DataDefinition;

/**
 *
 */
class BmcClassComponentMeetingDefinition extends ComplexDataDefinitionBase {


  /**
   *
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {
      $info = &$this->propertyDefinitions;

      $info['comp_start'] = DataDefinition::create('string')
        ->setLabel('comp_start');
      $info['comp_end'] = DataDefinition::create('string')
        ->setLabel('comp_end');
      $info['comp_days'] = DataDefinition::create('string')
        ->setLabel('comp_days');
      $info['comp_facility'] = DataDefinition::create('string')
        ->setLabel('comp_facility');
      $info['comp_desc'] = DataDefinition::create('string')
        ->setLabel('comp_desc');
    }
    return $this->propertyDefinitions;
  }

}
