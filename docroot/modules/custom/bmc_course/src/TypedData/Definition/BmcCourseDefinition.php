<?php

namespace Drupal\bmc_course\TypedData\Definition;

use Drupal\Core\TypedData\ComplexDataDefinitionBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\ListDataDefinition;

/**
 *
 */
class BmcCourseDefinition extends ComplexDataDefinitionBase {


  /**
   *
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {
      $info = &$this->propertyDefinitions;

      $info['catalognumber'] = DataDefinition::create('string')
        ->setLabel('catalognumber');
      $info['longtitle'] = DataDefinition::create('string')
        ->setLabel('longtitle');
      $info['description'] = DataDefinition::create('string')
        ->setLabel('description');
      $info['divrequirement'] = DataDefinition::create('string')
        ->setLabel('divrequirement');
      $info['majorattr'] = DataDefinition::create('string')
        ->setLabel('majoprattr');
      $info['units'] = DataDefinition::create('string')
        ->setLabel('units');
      $info['homedepartment'] = DataDefinition::create('string')
        ->setLabel('homedepartment');
      $info['components'] = ListDataDefinition::create('bmc_classcomponent')
        ->setLabel('components');
      $info['approaches'] = ListDataDefinition::create('string')
        ->setLabel('approaches');
      $info['concdescs'] = ListDataDefinition::create('string')
        ->setLabel('concdescs');
      $info['crosslists'] = ListDataDefinition::create('string')
        ->setLabel('crosslists');
    }
    return $this->propertyDefinitions;
  }

}
