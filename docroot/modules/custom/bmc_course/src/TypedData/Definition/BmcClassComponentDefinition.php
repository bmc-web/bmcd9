<?php

namespace Drupal\bmc_course\TypedData\Definition;

use Drupal\Core\TypedData\ComplexDataDefinitionBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\ListDataDefinition;

/**
 *
 */
class BmcClassComponentDefinition extends ComplexDataDefinitionBase {


  /**
   *
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {
      $info = &$this->propertyDefinitions;

      $info['courseid'] = DataDefinition::create('string')
        ->setLabel('courseid');
      $info['term'] = DataDefinition::create('string')
        ->setLabel('term');
      $info['classnumber'] = DataDefinition::create('string')
        ->setLabel('classnumber');
      $info['section'] = DataDefinition::create('string')
        ->setLabel('section');
      $info['topic'] = DataDefinition::create('string')
        ->setLabel('topic');
      $info['instructor'] = DataDefinition::create('string')
        ->setLabel('instructor');
      $info['quarters'] = DataDefinition::create('string')
        ->setLabel('quarters');
      $info['mode_of_instruction'] = DataDefinition::create('string')
        ->setLabel('mode_of_instruction');
      $info['meetings'] = ListDataDefinition::create('bmc_classcomponent_meeting')
        ->setLabel('meetings');

    }
    return $this->propertyDefinitions;
  }

}
