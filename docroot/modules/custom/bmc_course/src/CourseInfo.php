<?php

namespace Drupal\bmc_course;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\bmc_course\TypedData\Definition\BmcCourseDefinition;

/**
 * CourseInfo service.
 */
class CourseInfo implements CourseInfoInterface {

  /**
   * A cache for the BmcCourse typed data objects.
   *
   * This probably doesn't help performance, since the pages will be served by
   * Varnish, anyway.
   *
   * @var array
   */
  protected $cache = [];

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a CourseInfo object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }


  /**
   *
   */
  public function fetchCourses(string|array $code, string $type): array {
    $query_results = [];
    switch ($type) {
      case 'conc':
        $query_results = $this->fetchCoursesByConcCode($code);
        break;

      case 'subject':
        $query_results = $this->fetchCoursesBySubjectCode($code);
        break;
    }
    $course_results = [];
    $definition = BmcCourseDefinition::create('bmc_course');
    foreach ($query_results as $query_result) {
      $uid = $query_result['homedepartment'] . '_' . $query_result['catalognumber'];
      if (!in_array($uid, array_keys($this->cache))) {
        $course = \Drupal::typedDataManager()->create($definition);
        $course->setValue($query_result);
        $this->cache[$uid] = $course;
      }
      $course_results[] = $this->cache[$uid];
    }
    return $course_results;
  }

  /**
   *
   */
  protected function fetchCoursesByConcCode(string $conccode): array {
    $query = $this->getCoursesQuery();
    $query->join('ps2_course_conccodeassignments', 'conccode_assignments', 'course.courseid = conccode_assignments.courseid');
    $query->condition('conccode_assignments.conccode', $conccode, '=')
      ->orderby('course.homedepartment')
      ->orderBy('course.catalognumber');
    $result = $query->execute();
    return $result->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * Method description.
   */
  protected function fetchCoursesBySubjectCode(array $subject): array {
    $homecourses = $this->fetchCoursesByHomeDepartment($subject);
    return array_merge($homecourses);
  }

  /**
   *
   */
  protected function getCoursesQuery(): SelectInterface {
    return $this->connection->select('ps2_course', 'course')
      ->fields('course', [
        'catalognumber',
        'longtitle',
        'description',
        'divrequirement',
        'majorattr',
        'units',
        'homedepartment',
      ])
      ->distinct();
  }

  /**
   *
   */
  protected function fetchCoursesByHomeDepartment(array $subject): array {
    $query = $this->getCoursesQuery();
    $query->leftJoin('ps2_course_crosslists', 'crosslists', 'course.courseid = crosslists.courseid');
    $implodedSubjects = sprintf("'%s'", implode("','", $subject ) );
    // Calculate the "in_list" column on the fly.
    // If the homedepartment is in the list, give it a 0. If not, give it a 1.
    $query->addExpression("if(FIELD(course.homedepartment," . $implodedSubjects . ")=0,1,0)", 'in_list');
    // Calculate the "list_index" column on the fly.
    // If the homedepartment is in the list, give it the index. If not, it gets a 0.
    $query->addExpression( 'FIELD(course.homedepartment,' . $implodedSubjects . ')', 'list_index');

    $orConditionGroup = $query->orConditionGroup()
      ->condition('course.homedepartment', $subject, 'in')
      ->condition('crosslists.subjectareacode', $subject, 'in');

    $query->condition($orConditionGroup);
    $query
      // Courses in the provided list (in_list = 0) come before those not in the list (in_list = 1)
      ->orderBy('in_list')
      // Sort courses by their order in the list.
      ->orderBy('list_index')
      // Sort courses by their homedepartment (
      ->orderby('course.homedepartment')
      ->orderBy('catalognumber');


    $result = $query->execute();
    return $result->fetchAll(\PDO::FETCH_ASSOC);
  }

}
