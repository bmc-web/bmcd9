<?php

namespace Drupal\bmc_course;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * BmcCourseRenderer plugin manager.
 */
class BmcCourseRendererPluginManager extends DefaultPluginManager {
  protected Connection $database;
  protected EntityTypeManagerInterface $entityTypeManager;
  protected CourseInfoInterface $courseInfo;

  /**
   * Constructs BmcCourseRendererPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
    Connection $database,
    EntityTypeManagerInterface $entityTypeManager,
    CourseInfoInterface $courseInfo
  ) {
    parent::__construct(
      'Plugin/BmcCourseRenderer',
      $namespaces,
      $module_handler,
      'Drupal\bmc_course\BmcCourseRendererInterface',
      'Drupal\bmc_course\Annotation\BmcCourseRenderer'
    );
    $this->alterInfo('bmc_course_renderer_info');
    $this->setCacheBackend($cache_backend, 'bmc_course_renderer_plugins');
    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
    $this->courseInfo = $courseInfo;
  }

  /**
   * {@inheritDoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    $instance = parent::createInstance($plugin_id, $configuration);
    $instance->setDatabase($this->database);
    $instance->setEntityTypeManager($this->entityTypeManager);
    $instance->setCourseInfo($this->courseInfo);
    return $instance;
  }

}
