<?php

namespace Drupal\bmc_course\EventSubscriber;

use Drupal\bmc_csv\PsfeedsFixesInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\bmc_csv\Event\PreAssembleFieldsEvent;
use Drupal\bmc_csv\Event\BmcCsvEvents;

/**
 *
 */
class PsfeedPreAssembleFieldSubscriber implements EventSubscriberInterface {

  /**
   * The PsfeedsFixes service.
   *
   * @var \Drupal\bmc_csv\PsfeedsFixesInterface
   */
  protected PsfeedsFixesInterface $psfeedsFixes;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The config.
   *
   * @var \Drupal\bmc_csv\PsfeedsFixesInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\bmc_csv\PsfeedsFixesInterface $psfeedsFixes
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(PsfeedsFixesInterface $psfeedsFixes, ConfigFactoryInterface $configFactory) {
    $this->psfeedsFixes = $psfeedsFixes;
    $this->config = $configFactory->get('bmc_course.settings');
  }

  /**
   * Apply the Psfeeds Fixes to all data coming from the feed
   */
  public function onPreAssembleFields(PreAssembleFieldsEvent $event) {
    $feeds = $this->config->get('psfeeds');
    foreach ($feeds as $feed_name => $feed_info) {
      if ($event->getDefinitionName() === 'bmc_course.' . $feed_name) {
        $data = $event->getData();
        foreach ($data as $key => $value) {
          $data[$key] = $this->psfeedsFixes->doAllFiexes($value);
        }
        $event->setData($data);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      BmcCsvEvents::PRE_ASSEMBLE_FIELDS => ['onPreAssembleFields'],
    ];
  }

}
