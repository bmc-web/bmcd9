<?php

namespace Drupal\bmc_course\Plugin\BmcCourseRenderer;

use Drupal\bmc_course\BmcCourseRendererPluginBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\node\NodeInterface;

/**
 * Plugin implementation of the bmc_course_renderer.
 *
 * @BmcCourseRenderer(
 *   id = "course_schedule"
 * )
 */
class BmcCourseScheduleRenderer extends BmcCourseRendererPluginBase {
  const INDEPENDENT_STUDY_CATALOGNUMBERS = ['B401', 'B403', 'B425'];

  /**
   * Assemble the course schedule tables.
   */
  private function output() {
    // Current term.
    $output = ['#type' => 'container'];
    $output[] = $this->termSchedule(0);
    // One term ahead.
    $output[] = $this->termSchedule(1);
    // Two terms ahead.
    $output[] = $this->termSchedule(2);

    return $output;
  }

  /**
   * Create the table output for a given term.
   */
  private function termSchedule($which_term) {
    $this_term = $this->getThisTerm($which_term);
    $results = $this->courseInfo->fetchCourses($this->code, $this->codeType);
    if (count($results) == 0) {
      return [
        '#markup' => '<h2 class="term">' . $this->termToText($this_term) . ' ' . $this->getLabel() . '</h2>' . "<p class='h3'>(Class schedules for this semester will be posted at a later date.)</p>",
      ];
    }

    $header = $this->prepareHeader($results, $this_term);

    $container = ['#type' => 'container'];
    $container['heading'] = [
      '#markup' => '<h2 class="term">' . $this->termToText($this_term) . ' ' . $this->getLabel() . '</h2>',
    ];
    $container['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#prefix' => "<div class=\"table table--wysiwyg\"><div class=\"table__inner\">",
      '#suffix' => "</div></div>",
    ];

    $rows = $this->prepareRows($results, $this_term);
    if (count($rows) == 0) {
      return [
        '#markup' => '<h2 class="term">' . $this->termToText($this_term) . ' ' . $this->getLabel() . '</h2>' . "<p class='h3'>(Class schedules for this semester will be posted at a later date.)</p>",
      ];
    }
    $container['table']['#rows'] = $rows;
    return $container;
  }

  /**
   * Assemble the rows for a single term schedule
   */
  private function prepareRows($courses, $term) {
    $rows = [];
    foreach ($courses as $course) {
      $components = $this->getQualifyingComponents($course, $term);
      foreach ($components as $component) {
        if (in_array($component->get('term')->getValue(), $this::INDEPENDENT_STUDY_CATALOGNUMBERS)) {
          $rows[] = $this->buildIndependeStudyRow($course, $component);
        }
        elseif ($component->get('meetings')->count() == 0) {
          $rows[] = $this->buildNoMeetingsRow($course, $component);
        }
        else {
          $rows = array_merge($rows, $this->buildSectionRows($course, $component));
        }
      }

    }
    return $rows;
  }

  /**
   * Assemble the header for a single term schedule
   */
  private function prepareHeader($courses, $term) {
    $hasMode = false;
    foreach ($courses as $course) {
      $components = $this->getQualifyingComponents($course, $term);
      foreach ($components as $component) {
        if ($component->get('mode_of_instruction')->getValue() != '') {
          $hasMode = TRUE;
        }
      }

    }
    if ($hasMode) {
      return [
        'course' => [
          'data' => $this->t('Course'),
          'scope' => 'col',
        ],
        'title' => [
          'data' => $this->t('Title'),
          'scope' => 'col',
        ],
        'schedule' => [
          'data' => $this->t('Schedule/Units'),
          'scope' => 'col',
        ],
        'type' => [
          'data' => $this->t('Meeting Type Times/Days'),
          'scope' => 'col',
        ],
        'location' => [
          'data' => $this->t('Location / Instruction Mode'),
          'scope' => 'col',
        ],
        'instr' => [
          'data' => $this->t('Instr(s)'),
          'scope' => 'col',
        ],
      ];
    } else {
      return [
        'course' => [
          'data' => $this->t('Course'),
          'scope' => 'col',
        ],
        'title' => [
          'data' => $this->t('Title'),
          'scope' => 'col',
        ],
        'schedule' => [
          'data' => $this->t('Schedule/Units'),
          'scope' => 'col',
        ],
        'type' => [
          'data' => $this->t('Meeting Type Times/Days'),
          'scope' => 'col',
        ],
        'location' => [
          'data' => $this->t('Location'),
          'scope' => 'col',
        ],
        'instr' => [
          'data' => $this->t('Instr(s)'),
          'scope' => 'col',
        ],
      ];
    }
  }

  /**
   * Courses with a certain catalognumber get special treatment
   */
  private function buildIndependeStudyRow($course, $component) {
    $course_string = $this->buildCourseString($course, $component);
    $title_link = $this->buildTitleLink($course, $component);
    $units_string = $this->buildUnitsString($course, $component);
    $class_instructor_string = 'Dept. staff, TBA';
    return [
      [
        'data' => $course_string,
      ],
      [
        'data' => $title_link,
      ],
      [
        'data' => $units_string,
      ],
      $row['data'][] = [],
      $row['data'][] = [],
      $row['data'][] = [
        'data' => $class_instructor_string,
      ],
    ];
  }

  /**
   * Course sections without course meetings get special treatment
   */
  private function buildNoMeetingsRow($course, $component) {
    $course_string = $this->buildCourseString($course, $component);
    $title_link = $this->buildTitleLink($course, $component);
    $units_string = $this->buildUnitsString($course, $component);
    $class_instructor_string = $component->get('instructor')->getValue();
    $mode_of_instruction = $component->get('mode_of_instruction')->getValue();
    return [
      [
        'data' => $course_string,
      ],
      [
        'data' => $title_link,
      ],
      [
        'data' => $units_string,
      ],
      $row['data'][] = [],
      $row['data'][] = [
        'data' => ['#markup' => "<strong>$mode_of_instruction</strong>"],
      ],
      $row['data'] = [
        'data' => $class_instructor_string,
      ],
    ];
  }

  /**
   *
   */
  private function buildSectionRows($course, $component) {
    $rows = [];
    $course_string = $this->buildCourseString($course, $component);
    $title_link = $this->buildTitleLink($course, $component);
    $units_string = $this->buildUnitsString($course, $component);
    $class_instructor_string = $component->get('instructor')->getValue();
    $meetings = $component->get('meetings');
    $mode = $component->get('mode_of_instruction')->getValue();
    // we need to do fancy rowspan calculations for some of the rows in the
    // table. see, for example, foreign langauge departments or lab-heavy
    // science departments
    foreach ($meetings as $i => $meeting) {
      $comp_start = $meeting->get('comp_start')->getValue();
      $comp_end = $meeting->get('comp_end')->getValue();
      $comp_days = $meeting->get('comp_days')->getValue();
      $comp_desc = $meeting->get('comp_desc')->getValue();
      $comp_facility = $meeting->get('comp_facility')->getValue();

      if ($comp_start == ' 0:00 AM') {
        $timestr = 'Date/Time TBA';
      }
      else {
        $timestr = $comp_start . '-' . $comp_end . ' ' . $comp_days;
      }

      $facility = $comp_facility;
      // $comp_facility is sometimes empty, so check before outputting <br/>
      if (!empty($comp_facility)) {
        $facility .= "<br>";
      }
      // Also output the mode of instruction in the "Location" table cell.
      if (!empty($mode)) {
        $facility .= "<strong>$mode</strong>";
      }

      if ($i == 0) {
        $rowspan = $meetings->count();
        $row = [
          'data' => [
            [
              // Column 1: course.
              'data' => $course_string,
              'rowspan' => $rowspan > 1 ? $rowspan : 1,
            ],
            [
              // Column 2: title.
              'data' => $title_link,
              'rowspan' => $rowspan > 1 ? $rowspan : 1,
            ],
            [
              // Column 3: Schedule / units.
              'data' => $units_string,
              'rowspan' => $rowspan > 1 ? $rowspan : 1,
            ],
            [
              // Column 4: Meeting Type / Days.
              'data' => "$comp_desc: $timestr",
            ],
            [
              // Column 5: Location / Mode.
              'data' => ['#markup' => $facility],
            ],
            [
              'data' => $class_instructor_string,
              'rowspan' => $rowspan > 1 ? $rowspan : 1,
            ],
          ],
        ];
      }

      else {
        $row = [
          [
            // Column 4: Meeting Type / Days.
            'data' => "$comp_desc: $timestr",
          ],
          [
            // Column 5: Location / Mode.
            'data' => ['#markup' => $facility],
          ],
        ];
      }

      $rows[] = $row;
    }
    return $rows;
  }

  /**
   *
   */
  private function buildCourseString($course, $component) {
    $homedepartment = $course->get('homedepartment')->getValue();
    $catalognumber = $course->get('catalognumber')->getValue();
    $section = $component->get('section')->getValue();
    return "$homedepartment $catalognumber-$section";

  }

  /**
   *
   */
  private function buildTitleLink($course, $component) {
    $longtitle = $course->get('longtitle')->getValue();
    $homedepartment = $course->get('homedepartment')->getValue();
    $catalognumber = $course->get('catalognumber')->getValue();
    $topic = $component->get('topic')->getValue();
    $title = $topic == '' ? $longtitle : "$longtitle: $topic";
    $title_url = Url::fromUserInput("#$homedepartment$catalognumber");
    return Link::fromTextAndUrl($title, $title_url);
  }

  /**
   *
   */
  private function buildUnitsString($course, $component) {
    $units = $course->get('units')->getValue();
    $quarters = $component->get('quarters')->getValue();
    $quarters = (trim($quarters) != '') ? $quarters : 'Semester';

    // No quarters/units if the primary section of the class is a lab.
    if ($component->get('meetings')->count() > 0) {
      if ($component->get('meetings')
        ->first()
        ->get('comp_desc')
        ->getValue() != 'LAB') {
        $units = "$quarters / $units";
      }
    }

    return $units;
  }

  /**
   *
   */
  private function getQualifyingComponents($course, $term) {
    $components = $course->get('components');
    $qualifying_components = [];
    foreach ($components as $component) {
      if ($component->get('term')->getValue() == $term) {
        $qualifying_components[] = $component;
      }
    }
    return $qualifying_components;
  }

  /**
   * Return the code of the next term.
   *
   * There is no upper limit on term codes (unlike the lower limit of "0695").
   */
  private function nextTerm($term): string {
    $year = '20' . substr($term, 0, 2);
    $code = substr($term, 2, 2);
    $next_year = '';
    $next_code = '';

    if ($code == '00' && $this->summer) {
      $next_year = $year;
      $next_code = '05';
    }
    elseif ($code == '00' || $code == '05') {
      $next_year = $year;
      $next_code = '10';
    }
    elseif ($code == '10') {
      $next_year = $year + 1;
      $next_code = '00';
    }

    // Get back to the 2-digit year.
    $next_year = substr($next_year, 2, 2);
    return $next_year . $next_code;
  }

  /**
   * {inheritDoc}
   */
  public function outputtoContainer(NodeInterface $node, $container): array {
    if (!empty($node->field_courses_code) && is_object($node)) {
      foreach ($node->field_courses_code as $course_para_ref) {
        $course_para = $this->paragraphStorage->load($course_para_ref->target_id);

        $code_type = $course_para->field_p_ccode_type->value;
        if ($code_type == 'subject') {
          $code = [];
          $values = $course_para->get('field_p_ccode_scode')->getValue();
          foreach ($values as $value) {
            $code[] = $value['value'];
          }
        }
        else {
          $code = $course_para->field_p_ccode_conccode->value;
        }

        if (!empty($course_para->field_p_ccode_desc)) {
          $description = $course_para->field_p_ccode_desc;
          $build[] = ['#markup' => $description->value, $description->format];
        }

        $this->setCode($code_type, $code);
        if (!$course_para->field_p_ccode_label_override->isEmpty()) {
          $this->setLabelOverride($course_para->field_p_ccode_label_override->value);
        }
        $container[] = $this->output();

        if (!empty($course_para->field_p_ccode_addl_text)) {
          $additional = $course_para->field_p_ccode_addl_text;
          $container[] = ['#markup' => $additional->value];
        }
      }
    }

    return $container;
  }

  /**
   *
   */
  private function getThisTerm($which_term) {
    $this_term = $this->currentTerm();

    if ($which_term > 0) {
      $this_term = $this->nextTerm($this_term);
    }
    if ($which_term > 1) {
      $this_term = $this->nextTerm($this_term);
    }

    return $this_term;
  }

}
