<?php

namespace Drupal\bmc_course\Plugin\BmcCourseRenderer;

use Drupal\bmc_course\BmcCourseRendererPluginBase;

/**
 * Plugin implementation of the bmc_course_renderer.
 *
 * @BmcCourseRenderer(
 *   id = "prog_desc"
 * )
 */
class BmcProgDescRenderer extends BmcCourseRendererPluginBase {

  /**
   * {@inheritDoc}
   */
  private function output() {
    $result = $this->database
      ->query("SELECT * FROM {ps2_academicplan} WHERE dcode = :dcode", [':dcode' => $this->code])->fetch();
    // TODO: throw an error if the query does not return a row
    if ($result) {
      return [
        '#prefix' => "<div class=\"program-description\">",
        '#markup' => $result->description,
        '#suffix' => "</div>",
      ];
    }
  }

  /**
   * {inheritDoc}
   */
  public function outputtoContainer($node, $container): array {
    if (!empty($node->field_progdesc_code) && is_object($node)) {
      foreach ($node->field_progdesc_code as $progdesc_para_ref) {
        $progdesc_para = $this->paragraphStorage->load($progdesc_para_ref->target_id);
        $dcode = $progdesc_para->field_p_progdesc_code->value;
        $this->setCode('', $dcode);
        $container[] = $this->output();
      }
    }
    return $container;
  }

}
