<?php

namespace Drupal\bmc_course\Plugin\BmcCourseRenderer;

use Drupal\bmc_course\BmcCourseRendererPluginBase;
use Drupal\node\NodeInterface;

/**
 * Plugin implementation of the bmc_course_renderer.
 *
 * @BmcCourseRenderer(
 *   id = "course_listing"
 * )
 */
class BmcCourseListingRenderer extends BmcCourseRendererPluginBase {

  /**
   * Assemble the output for the course listing.
   */
  private function output(): array {
    $courses = $this->courseInfo->fetchCourses($this->code, $this->codeType);

    $items = [];

    foreach ($courses as $course) {
      $components = $course->get('components');
      $ids = $course->getIds();
      $item = [
        '#theme' => 'course_listing_item',
        '#homedepartment' => check_markup($course->get('homedepartment')->getValue(), 'advanced'),
        '#catalognumber' => check_markup($course->get('catalognumber')->getValue(), 'advanced'),
        '#longtitle' => check_markup($course->get('longtitle')->getValue(), 'advanced'),
      ];

      if (!empty($course->get('majorattr')->getValue())) {
        $item['#majorattr'] = check_markup($course->get('majorattr')->getValue(), 'advanced');
      }

      foreach ($course->getApproaches() as $approach) {
        $item['#approaches'][] = check_markup($approach, 'advanced');
      }

      foreach ($course->getConcdescs() as $concdesc) {
        $item['#concdescs'][] = check_markup($concdesc, 'advanced');
      }

      foreach ($course->getCrosslists() as $crosslist) {
        $item['#crosslists'][] = check_markup($crosslist, 'advanced');
      }


      $topics = [];
      foreach ($course->get('components')->getValue() as $component) {
        if (!empty($component['topic'])) {
          $topicsection = $component['section'];
          $topicterm = $this->termToText($component['term']);
          $topicstr = "Section " . $topicsection . " (" . $topicterm . "): " . $component['topic'];
          $topics[] = $topicstr;
        }
      }
      if (!is_null($topics)) {
        $item['#topics'] = $topics;
      }

      $description = $course->get('description')->getValue();
      if (!empty($description)) {
        $description = preg_replace('/Current topic description:/', "<br><br><u>Current topic description:</u>", $description);
        $item['#description'] = check_markup($description, 'advanced');
      }

//       TODO: move this into its own method
      $term_results = $course->getUniqueTerms();
      $terms = NULL;
      if (count($term_results) > 0) {
        if ($course->get('catalognumber')->getValue() == 'B213') {

        }

        foreach ($term_results as $term_result) {
          // Only show terms in the current academic year,
          // otherwise "Not Offered".
          if ($this->academicYear($term_result) == $this->academicYear($this->currentTerm(4))) {
            $thisterm = $this->termToText($term_result);
            if (!empty($terms)) {
              $terms = $terms . ', ';
            }
            $terms = $terms . $thisterm;
          }
        }
      }
      if (empty($terms) && $course->get('homedepartment')->getValue() != 'SOWK') {
        $terms = "Not offered " . $this->academicYear($this->currentTerm(4));
      }
      // Extract the numerical part of the catalog code.
      $courseint = (int) substr($course->get('catalognumber')->getValue(), 1, 3);
      // No term line for "on demand" courses.
      if ($courseint < 398 || $courseint > 499) {
        $item['#terms'] = $terms;
      }
      $items[] = $item;
    }
    return [
      '#theme' => 'course_listing_block',
      '#items' => $items,
      '#heading' => check_markup($this->academicYear($this->currentTerm(4)) . " Catalog Data: " . $this->getLabel()),
    ];;
  }

  /**
   * Return the academic year from a term (ex. '2009-10').
   */
  private function academicYear($term): string {
    $this->assertValidTerm($term);

    // Exception.
    if ($term == '0695') {
      return '2006-07';
    }

    $year = '20' . substr($term, 0, 2);
    $year1 = $year;
    $year2 = $year;
    $code = substr($term, 2, 2);

    if ($code == '00') {
      $year1 = $year - 1;
      $year2 = substr($year, 2, 2);
    }
    elseif ($code == '10' || $code == '05') {
      $year1 = $year;
      $year2 = substr($year + 1, 2, 2);
    }

    return "$year1-$year2";
  }

  /**
   * {inheritDoc}
   */
  public function outputtoContainer(NodeInterface $node, $container): array {
    if (!empty($node->field_courses_code) && is_object($node)) {
      foreach ($node->field_courses_code as $course_para_ref) {
        $course_para = $this->paragraphStorage->load($course_para_ref->target_id);

        $code_type = $course_para->field_p_ccode_type->value;
        if ($code_type == 'subject') {
          $code = [];
          $values = $course_para->get('field_p_ccode_scode')->getValue();
          foreach ($values as $value) {
            $code[] = $value['value'];
          }
        }
        else {
          $code = $course_para->field_p_ccode_conccode->value;
        }

        $this->setCode($code_type, $code);
        if (!$course_para->field_p_ccode_label_override->isEmpty()) {
          $this->setLabelOverride($course_para->field_p_ccode_label_override->value);
        }
        $container[] = $this->output();
      }
    }

    return $container;
  }

}
