<?php

namespace Drupal\bmc_course\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\Map;
use PDO;

/**
 * Bmc Classcomponent Meeting data type.
 *
 * @DataType(
 *   id = "bmc_classcomponent",
 *   label = @Translation("BMC Classcomponent"),
 *   definition_class = "\Drupal\bmc_course\TypedData\Definition\BmcClassComponentDefinition"
 * )
 */
class BmcClassComponent extends Map {
  public function setValue($values, $notify = TRUE) {
    parent::setValue($values, $notify);
    $query = \Drupal::database()->select('ps2_classcomponentmeetings', 'meetings')
      ->fields('meetings',
        [
          'comp_start1',
          'comp_end1',
          'comp_days1',
          'comp_facility1',
          'comp_desc1',
          'comp_start2',
          'comp_end2',
          'comp_days2',
          'comp_facility2',
          'comp_desc2',
          'comp_start3',
          'comp_end3',
          'comp_days3',
          'comp_facility3',
          'comp_desc3',
          'comp_start4',
          'comp_end4',
          'comp_days4',
          'comp_facility4',
          'comp_desc4',
          'comp_start5',
          'comp_end5',
          'comp_days5',
          'comp_facility5',
          'comp_desc5',
          'comp_start6',
          'comp_end6',
          'comp_days6',
          'comp_facility6',
          'comp_desc6',
        ])
      ->condition('courseid', $this->get('courseid')->getValue(), '=')
      ->condition('term', $this->get('term')->getValue(), '=')
      ->condition('section', $this->get('section')->getValue(), '=')
      ->orderBy('section')
      ->execute();

    $result = $query->fetchAssoc();
    $meetings = $this->buildMeetings($result);
    $this->set('meetings', $meetings);
  }

  private function buildMeetings($row) {
    $meetings = [];
    $course_components_index = 1;
    $couse_component_max = 6;
    while ($course_components_index <= $couse_component_max) {
      if (!empty($row['comp_start' . $course_components_index])) {
        $meetings[] = [
          'comp_start' => $row['comp_start' . $course_components_index],
          'comp_end' => $row['comp_end' . $course_components_index],
          'comp_days' => $row['comp_days' . $course_components_index],
          'comp_facility' => $row['comp_facility' . $course_components_index],
          'comp_desc' => $row['comp_desc' . $course_components_index],
        ];
      }
      $course_components_index++;
    }
    return $meetings;
  }
}
