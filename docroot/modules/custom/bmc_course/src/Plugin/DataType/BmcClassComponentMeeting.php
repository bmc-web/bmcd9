<?php

namespace Drupal\bmc_course\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\Map;

/**
 * Bmc Classcomponent Meeting data type.
 *
 * @DataType(
 *   id = "bmc_classcomponent_meeting",
 *   label = @Translation("BMC Classcomponent Meeting"),
 *   definition_class = "\Drupal\bmc_course\TypedData\Definition\BmcClassComponentMeetingDefinition"
 * )
 */
class BmcClassComponentMeeting extends Map {

}
