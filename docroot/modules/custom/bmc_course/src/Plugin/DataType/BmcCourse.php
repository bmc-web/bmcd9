<?php

namespace Drupal\bmc_course\Plugin\DataType;

use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\Plugin\DataType\Map;
use Drupal\Core\TypedData\TypedDataInterface;
use PDO;

/**
 * Bmc Course data type.
 *
 * Sorry for using the \Drupal gloabl, but we can't inject services into
 * typed data :( See https://www.drupal.org/project/drupal/issues/2053415
 *
 * @DataType(
 *   id = "bmc_course",
 *   label = @Translation("BMC Course"),
 *   definition_class = "\Drupal\bmc_course\TypedData\Definition\BmcCourseDefinition"
 * )
 */
class BmcCourse extends Map {

  public function setValue($values, $notify = TRUE) {
    parent::setValue($values, $notify);
    $query = \Drupal::database()->select('ps2_classcomponent', 'classcomponent');
    $query->join('ps2_course', 'course', 'course.courseid = classcomponent.courseid');

    $query->fields('classcomponent')
      ->condition('course.homedepartment', $this->get('homedepartment')
        ->getValue(), '=')
      ->condition('course.catalognumber', $this->get('catalognumber')
        ->getValue(), '=')
      ->orderBy('term', 'ASC')
      ->orderBy('section', 'ASC')
      ->distinct();
    $results = $query->execute()->fetchAll(PDO::FETCH_ASSOC);
    foreach ($results as $component_data) {
      $this->get('components')->appendItem($component_data);
    }

    $ids = $this->getIds();
    if (count($ids) > 0) {
      $query = \Drupal::database()
        ->select('ps2_course_approachassignments', 'approaches')
        ->fields('approaches', ['approach'])
        ->condition('courseid', $ids, 'IN')
        ->orderBy('approach')
        ->distinct();
      $approaches = $query->execute()->fetchCol(0);
      $this->set('approaches', $approaches);

      $query = \Drupal::database()->select('ps2_conccodes', 'conccodes');
      $query->join('ps2_course_conccodeassignments', 'assignments', 'conccodes.code = assignments.conccode');

      $query->fields('conccodes', ['longname'])
        ->distinct()
        ->orderBy('longname')
        ->condition('assignments.courseid', $ids, 'IN');
      $concdescs = $query->execute()->fetchCol(0);
      $this->set('concdescs', $concdescs);

      $query = \Drupal::database()->select('ps2_course_crosslists', 'crosslists');
      $query->join('ps2_subjectareacodes', 'subjectareacodes', 'crosslists.subjectareacode = subjectareacodes.code');
      $query->fields('subjectareacodes', ['longname'])
        ->distinct()
        ->orderBy('longname')
        ->condition('crosslists.courseid', $ids, 'IN');
      $crosslists = $query->execute()->fetchCol(0);
      $this->set('crosslists', $crosslists);
    }

  }

  /**
   *
   */
  public function getIds() {
    $components = $this->get('components')->getValue();
    return array_column($components, 'courseid');
  }

  /**
   *
   */
  public function getApproaches() {
    return $this->get('approaches')->getValue();
  }

  /**
   *
   */
  public function getConcdescs() {
    return $this->get('concdescs')->getValue();
  }

  public function getCrosslists() {
    return $this->get('crosslists')->getValue();
  }

  /**
   *
   */
  public function getUniqueTerms() {
    $components = $this->get('components')->getValue();
    $terms = array_unique(array_column($components, 'term'));
    sort($terms);
    return $terms;
  }

}
