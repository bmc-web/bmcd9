<?php

namespace Drupal\bmc_course\Plugin\Block;

use Drupal\bmc_course\BmcCourseBlockBase;

/**
 * Provides a Course Schedule block.
 *
 * @Block(
 *  id = "bmc_course_schedule_block",
 *  admin_label = @Translation("BMC Courses Schedule"),
 * )
 */
class CourseScheduleBlock extends BmcCourseBlockBase {

  /**
   * Return courses table for code from node.
   */
  public function build(): array {
    $build = ['#type' => 'container'];
    $renderer = $this->coursePluginManager->createInstance('course_schedule');

    $node = $this->requestStack->getCurrentRequest()->attributes->get('node');
    if (!empty($node->field_courses_code) && is_object($node)) {
      $is_sowk = FALSE;
      $is_conc = FALSE;

      foreach ($node->field_courses_code as $course_para_ref) {
        $course_para = $this->paragraphStorage->load($course_para_ref->target_id);

        $code_type = $course_para->field_p_ccode_type->value;
        if ($code_type != 'subject') {
          $is_conc = TRUE;
        }
        else {
          $code = $course_para->field_p_ccode_scode->value;
          if ($code == 'SOWK') {
            $is_sowk = TRUE;
          }
        }
      }
      $build['intro'] = $renderer->getCoursesIntro($is_sowk, $is_conc);
    }

    return $this->buildFromCourseRenderer($renderer, $build);

    return $build;
  }

}
