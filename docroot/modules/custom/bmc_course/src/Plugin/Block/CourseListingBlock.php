<?php

namespace Drupal\bmc_course\Plugin\Block;

use Drupal\bmc_course\BmcCourseBlockBase;
use Drupal\bmc_course\PsCoursesListing;

/**
 * Provides a Course Listing block.
 *
 * @Block(
 *  id = "bmc_course_listing_block",
 *  admin_label = @Translation("BMC Courses Listing"),
 * )
 */
class CourseListingBlock extends BmcCourseBlockBase {

  /**
   * Renders the course listing variables.
   */
  public function build(): array {
    $build = ['#type' => 'container'];
    $renderer = $this->coursePluginManager->createInstance('course_listing');
    return $this->buildFromCourseRenderer($renderer, $build);
  }

}
