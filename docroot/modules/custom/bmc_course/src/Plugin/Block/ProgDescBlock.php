<?php

namespace Drupal\bmc_course\Plugin\Block;

use Drupal\bmc_course\BmcCourseBlockBase;

/**
 * Provides a Program Description block.
 *
 * @Block(
 *  id = "bmc_progdesc_block",
 *  admin_label = @Translation("BMC Program Description"),
 * )
 */
class ProgDescBlock extends BmcCourseBlockBase {

  /**
   * Renders the program description variables.
   */
  public function build(): array {
    $build = ['#type' => 'container'];
    $renderer = $this->coursePluginManager->createInstance('prog_desc');
    $build['generic_description'] = $renderer->genericProgramDescription();
    $build = $this->buildFromCourseRenderer($renderer, $build);
    return $build;
  }

}
