<?php

namespace Drupal\bmc_course;

/**
 * The CourseInfo service is for returning BmcCourses Typed Data objects.
 *
 */
interface CourseInfoInterface {

  /**
   * Retuns an array of BmcCourse Typed Data objects.
   */
  public function fetchCourses(string $code, string $type): array;

}
