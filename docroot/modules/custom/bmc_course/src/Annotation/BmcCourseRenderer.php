<?php

namespace Drupal\bmc_course\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines bmc_course_renderer annotation object.
 *
 * @Annotation
 */
class BmcCourseRenderer extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;
}
