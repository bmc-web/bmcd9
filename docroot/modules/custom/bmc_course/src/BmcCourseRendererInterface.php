<?php

namespace Drupal\bmc_course;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;
use Drupal\bmc_course\CourseInfoInterface;

/**
 * Interface for bmc_course_renderer plugins.
 */
interface BmcCourseRendererInterface {

  /**
   * Set the entity type manager.
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entityTypeManager);

  /**
   * Set the database connection.
   */
  public function setDatabase(Connection $database);

  /**
   * Set the database connection.
   */
  public function setCourseInfo(CourseInfoInterface $courseInfo);

  /**
   * Set the course type and course code.
   */
  public function setCode($codeType, $code);

  /**
   * An optional override for the renderer label.
   */
  public function setLabelOverride(string $label);

  /**
   * Return the current term code based on current date.
   *
   * @param int $listing
   *   The month code when spring semester ends.
   */
  public function currentTerm(int $listing = 6): string;

  /**
   * Return a human-readable text string (ex. 'Spring 2010') from a term.
   */
  public function termToText($term): string;

  /**
   * Check if term is valid term code, or halt the script with an error.
   */
  public function assertValidTerm($term);

  /**
   * Returns exception list of codes that can have summer terms.
   */
  public function summerAllowed(): array;

  /**
   * Use this method to build the render array.
   *
   * @param NodeInterface $node
   * @param array $container
   *
   * @return array
   */
  public function outputtoContainer(NodeInterface $node, $container): array;

  /**
   * Returns a markup render array for the generic program description.
   */
  public function genericProgramDescription();

  /**
   * Return a render array for the boilerplate at the top of the courses page.
   *
   * @para bool $is_sowk
   * @param vool $is_conc
   *
   * @return array
   */
  public function getCoursesIntro($is_sowk, $is_conc): array;

}
