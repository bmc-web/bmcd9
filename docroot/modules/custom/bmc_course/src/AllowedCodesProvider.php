<?php

namespace Drupal\bmc_course;

use Drupal\Core\Database\Connection;

/**
 * @todo Add class description.
 */
class AllowedCodesProvider {

  /**
   * @var \Drupal\Core\Database\Connection
   *   The database service.
   */
  protected Connection $database;

  /**
   * Constructs an AllowedCodesProvider object.
   */
  public function __construct(
    Connection $connection,
  ) {
    $this->database = $connection;
  }

  /**
   * Return an array of subject codes
   */
  public function getSubjectCodes(): array {
    return $this->database->select('ps2_subjectareacodes', 'c')
      ->fields('c', ['code'])
      ->distinct()
      ->orderBy('c.code', 'ASC')
      ->execute()
      ->fetchCol();
  }

  public function getSubjectCodesAllowedFieldValues(): array {
    $subject_codes = $this->getSubjectCodes();
    $subject_code_options = [];
    foreach ($subject_codes as $code) {
      // Data sometimes has extra whitespace.
      if (strlen(trim($code)) != 0) {
        $clean = trim($code);
        $subject_code_options[$clean] = $clean;
      }
    }
    return $subject_code_options;
  }

  /**
   * Return an array of subject codes
   */
  public function getConcCodes(): array {
    return $this->database->select('ps2_course_conccodeassignments', 'c')
      ->fields('c', ['conccode'])
      ->distinct()
      ->orderBy('conccode')
      ->execute()
      ->fetchCol();
  }

  public function getConcCodesAllowedFieldValues(): array {
    $concentrations = $this->getConcCodes();
    $concentration_options = [];
    foreach ($concentrations as $code) {
      // Data sometimes has extra whitespace.
      if (strlen(trim($code)) != 0) {
        $clean = trim($code);
        $concentration_options[$clean] = $clean;
      }
    }
    return $concentration_options;
  }

  /**
   * Return an array of subject codes
   */
  public function getProgDescCodes(): array {
    return $this->database->select('ps2_academicplan', 'c')
      ->fields('c', ['dcode'])
      ->distinct()
      ->execute()
      ->fetchCol();
  }

  public function getProgDescCodesAllowedFieldValues(): array {
    $dcodes = $this->getProgDescCodes();
    $dcodes_options = [];
    foreach ($dcodes as $dcode) {
      // Data sometimes has extra whitespace.
      if (strlen(trim($dcode)) != 0) {
        $clean = trim($dcode);
        $dcodes_options[$clean] = $clean;
      }
    }
    return $dcodes_options;
  }

}
