<?php

namespace Drupal\bmc_course;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Psr\Container\ContainerInterface;

/**
 *
 */
abstract class BmcCourseBlockBase extends BlockBase implements ContainerFactoryPluginInterface {
  protected PluginManagerInterface $coursePluginManager;
  protected Requeststack $requestStack;
  protected EntityTypeManagerInterface $entityTypeManager;
  protected EntityStorageInterface $paragraphStorage;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PluginManagerInterface $manager, RequestStack $requestStack, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->coursePluginManager = $manager;
    $this->requestStack = $requestStack;
    $this->entityTypeManager = $entityTypeManager;
    $this->paragraphStorage = $this->entityTypeManager->getStorage('paragraph');
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.bmc_course_renderer'),
      $container->get('request_stack'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Use this in the plugin's build() method. Returns a render array.
   *
   * @param BmcCourseRendererInterface $renderer
   * @param $container
   * @return mixed
   */
  protected function buildFromCourseRenderer(BmcCourseRendererInterface $renderer, $container): array
  {
    $node = $this->requestStack->getCurrentRequest()->attributes->get('node');
    return $renderer->outputtoContainer($node, $container);
  }

}
