<?php

namespace Drupal\bmc_course;

use Drupal\bmc_csv\CSVImporterInterface;
use Drupal\bmc_csv\PsfeedsFixesInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Connection;
use Drupal\Core\File\FileSystemInterface;

/**
 * PsfeedsImporter service.
 */
class PsfeedsImporter {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * @var \Drupal\bmc_csv\CSVImporterInterface
   */
  protected CSVImporterInterface $csvImporter;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * @var \Drupal\bmc_csv\PsfeedsFixesInterface
   */
  protected PsfeedsFixesInterface $psfeedsFixes;

  /**
   * Constructs a PsfeedsImporter object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct(
    FileSystemInterface $file_system,
    ConfigFactoryInterface $configFactory,
    CSVImporterInterface $csvImporter,
    Connection $database,
    PsfeedsFixesInterface $psfeedsFixes
  ) {
    $this->fileSystem = $file_system;
    $this->config = $configFactory->get('bmc_course.settings');
    $this->csvImporter = $csvImporter;
    $this->connection = $database;
    $this->psfeedsFixes = $psfeedsFixes;
  }

  /**
   * Import all of the feeds. Conventionally, we put all of the info needed in
   * bmc_course.settings.yml
   */
  public function importFeeds() {
    $feeds = $this->config->get('psfeeds');
    foreach ($feeds as $feed_name => $feed) {
      $this->importFeed($feed_name, $feed);
    }

    // Academicplan is a special snowflake.
    $academicPlan = $this->config->get('academicplan');
    $this->importAcademicPlan($academicPlan);
  }

  /**
   *
   */
  private function importFeed(string $feed_name, array $feed) {
    $definition = $this->csvImporter->definition()
      ->name('bmc_course.' . $feed_name)
      ->path($this->fileSystem->realpath($feed['path_to_feed']))
      ->tableName($feed['table_name'])
      ->columnNames($feed['column_names'])
      ->truncate($feed['truncate'])
      ->skipFirstLine($feed['skip_first_line'])
      ->separator($feed['separator'])
      ->build();

    $this->csvImporter->import($definition);
  }

  /**
   *
   */
  private function importAcademicPlan(array $feed) {
    // The academic plan feed is a special snowflake.
    // Instead of reading the file line by line, we need to read the whole file
    // into memory and then split it into rows.
    // We use the tilde (~) character to separate rows.
    $path = $this->fileSystem->realpath($feed['path_to_feed']);
    $table_name = $feed['table_name'];
    $column_names = $feed['column_names'];
    $truncate = $feed['truncate'];
    $skip_first_line = $feed['skip_first_line'];
    $lines = implode(file($path));
    // Splitting on the tilde worked fine, until we started getting feeds with
    // links that had scroll-to fragments in the hrefs. This caused the feed to
    // be split inappropriately, and MySQL to error out. So we need to remove
    // the scroll-to fragments from the feed before we split it into rows.
    $lines = $this->psfeedsFixes->removeScrollToFragments($lines);
    $rows = explode('~', $lines);
    $line_is_first_line = true;
    if ($truncate == true) {
      $this->connection->truncate($table_name)->execute();
    }

    foreach ($rows as $row) {
      if ($line_is_first_line && $skip_first_line === true) {
        $line_is_first_line = false;
        continue;
      }
      $line_is_first_line = false;

      $data = explode('%%%%', $row);
      $fields = [];
      foreach ($data as $index => $value) {
        $string = trim(preg_replace('/\s\s+/', ' ', $value));
        $string = $this->psfeedsFixes->doAllFiexes($string);
        $fields[$column_names[$index]] = $string;
      }

      $this->connection->insert($table_name)->fields($fields)->execute();
    }
  }

}
