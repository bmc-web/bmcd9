<?php

/**
 * Implements hook_post_update_NAME
 */
function bmc_course_post_update_www_1274(&$sandbox) {
  // As part of the resolution to WWW-1274,
  // we need to delete the "Program Description Boilerplate" config page
  $config_storage = \Drupal::entityTypeManager()
    ->getStorage('config_pages');
  $boilerplate_config = $config_storage->load('program_description_boilerplate');
  if (isset($boilerplate_config)) {
    $boilerplate_config->delete();
    return t('Program description boilerplate config deleted.');
  }
   else {
     return t('Program description boilerplate config page not found.');
   }

}
