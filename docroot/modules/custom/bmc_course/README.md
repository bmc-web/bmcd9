# BMC Course

## Background

We ported a lot of this functionality from D7, so there's some documentation debt. We'll work on that intermittently.

To display information, we have two content types: Courses and Program Description. The purpose of these content types
is to display data from custom Drupal tables. Those tables in turn are fed by feeds (psfeeds) from AIS.

In the past, we poulated the tables from the feeds based on perl scripts. However, we have refactored that work to rely
on more drupal-y approahces. We had envisioned using the migrate api for this work, but that doesn't make sense for a
few reasons. First: we haven't set up custom entities to import the data into. Second: we generally cannot guarantee
that the psfeeds themselves have a unique key for each row, which seems required for the psfeed.

So, we wrote a custom service (bmc_csv.importer) to handle extracting data from the psfeeds and import them into the
tables.

## Use

### PSfeedsImporter

The psfeedsImporter service handles ingesting the psfeeds. By convention, we keep all configuration required for
\Drupal\bmc_csv\CSVImporter::definition() in configuration files. That way, it is easy to, say, move the psfeeds to a
different part of the filesystem, or to add feeds to the process. See bmc_course.settings.yml for more info. All (almost)
feeds should be in bmc_course.settings.psfeeds, and defined in a similar pattern as those. The exception is the
academicplan psfeed, which has a very unorthodox structure and just requires hard coding the instructions for importing
it into the PsfeedsImporter service.

Override the settings for the imports in settings.php. For example, to import the psfeeds from the private filesystem
(the moduel expects the feeds from the public filesystem), add the following to settings.php:

```
$config['bmc_course.settings']['academicplan']['path_to_feed'] = 'private://psfeeds/ps2_academicplan.txt';
$config['bmc_course.settings']['psfeeds']['approachassignments']['path_to_feed'] = 'private://psfeeds/ps2_approachassignments.txt';
$config['bmc_course.settings']['psfeeds']['classcomponent']['path_to_feed'] = 'private://psfeeds/ps2_classcomponent.txt';
$config['bmc_course.settings']['psfeeds']['classcomponentmeetings']['path_to_feed'] = 'private://psfeeds/ps2_classcomponentmeetings.txt';
$config['bmc_course.settings']['psfeeds']['conccodes']['path_to_feed'] = 'private://psfeeds/ps2_conccodes.txt';
$config['bmc_course.settings']['psfeeds']['conc_code_assignments']['path_to_feed'] = 'private://psfeeds/ps2_conccodeassignments.txt';
$config['bmc_course.settings']['psfeeds']['course']['path_to_feed'] = 'private://psfeeds/ps2_course2.txt';
$config['bmc_course.settings']['psfeeds']['course_crosslists']['path_to_feed'] = 'private://psfeeds/ps2_course_crosslists.txt';
$config['bmc_course.settings']['psfeeds']['subjectareacodes']['path_to_feed'] = 'private://psfeeds/ps2_subjectareacodes.txt';
```

#### Running the PSfeeds Import

##### Via Drush

See the command bmc_course_import. This command does not update any of the state data that is set when bmc_course_cron
is run during Drupal Cron.

##### Via Drupal Cron

By default, running the course import via cron is turned off. To turn it on,
add `$config['bmc_courese.settings']['cron_is_active'] = true` to your settings.php file.

The bmc_course.settings.cron_run_time setting is the number of seconds after midnight cron should be run.
The bmc_course_cron() function checks whether cron should be run, and if so whether it has been run today, and if not
whether enough time has lapsed for the cron to run. These variables are all stored with \Drupal::state().

### Courseinfo

Every time a Courses or Program Description node is viewed, we have to query the custom tables for the data.
Historically, this has been done with queries directly to the tables from within the block plugins, and then dealing
with the nested arrays. The CoruseInfo service is an incremental improvement over that approach.

Instead of nested arrays, we used custom Typed Data objects to handle the content from the custom tables. This makes
it easier to manipulate the data for presentation.
