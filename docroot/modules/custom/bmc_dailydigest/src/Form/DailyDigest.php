<?php

namespace Drupal\bmc_dailydigest\Form;

use Drupal\Core\Form\FormBase as parentAlias;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

/**
 * Configure example settings for this site.
 */
class DailyDigest extends parentAlias
{

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'daily_digest';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $vars = $this->prepareBuildFormVars($form_state);

    $form['start_date'] = [
      '#type' => 'date',
      '#title' => 'From Date',
      '#default_value' => $vars['start_date'],
      '#date_format' => 'm-d-Y',
    ];
    $form['end_date'] = [
      '#type' => 'date',
      '#title' => 'To Date',
      '#default_value' => $vars['end_date'],
      '#date_format' => 'm-d-Y',
      '#description' => t("For Friday's digest, set this to Sunday's date"),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Apply dates',
    ];

    $ddigest_sections = ['campus', 'students', 'grad_schools', 'faculty_staff'];

    $form['output'] = $this->dailyDigestOutput($vars['start_date'], $vars['end_date'], $vars['sections']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
  }

  /**
   * Prepare a vars array to help build the form
   * Array keys:
   * 'start_date': the earliest date the daily digest should cover
   * 'end_date': the latest date the daily digest should cover
   * 'sections': info about which sections to build in the daily digest
   */
  private function prepareBuildFormVars($form_state): array {
    $vars = [];

    if (isset($form_state->getValues()['start_date'])) {
      $vars['start_date'] = $form_state->getValues()['start_date'];
      $vars['end_date'] = $form_state->getValues()['end_date'];
    }
    else {
      // Tomorrow.
      $vars['start_date'] = $vars['end_date'] = date('Y-m-d', time() + 60 * 60 * 24);
    }

    $vars['sections'] = [
      [
        'machine_name' => 'campus',
        'color' => '#03335f',
        'heading' => 'All Campus',
      ],
      [
        'machine_name' => 'students',
        'color' => '#77808d',
        'heading' => 'Students',
      ],
      [
        'machine_name' => 'grad_schools',
        'color' => '#7ba9a8',
        'heading' => 'Graduate Schools',
      ],
      [
        'machine_name' => 'faculty_staff',
        'color' => '#017db0',
        'heading' => 'Faculty and Staff',
      ],
    ];

    return $vars;
  }

  /**
   * Prepares an HTML string to be used for the Daily Digest email.
   */
  private function dailyDigestOutput($start_date, $end_date, array $sections = []): array {
    $contents = '';
    $preview = [
      '#prefix' => '<h3>Preview</h3>',
      '#type' => 'container',
    ];

    // Get announcements.
    $announcements = $this->fetchAnnouncementsForDailyDigest($start_date, $end_date);

    // Get events.
    $events = $this->fetchEventsForDailyDigest($start_date, $end_date);

    // Sort events and announcements for assembling into form elements.
    $sorted_ddigest_items = $this->sortItemsForDailyDigestIntoSections($announcements, $events, $start_date);
    foreach ($sections as $section) {
      if (isset($sorted_ddigest_items[$section['machine_name']]) &&
        count($sorted_ddigest_items[$section['machine_name']])) {
        $contents .= $this->dailyDigestFormatSection(
          $sorted_ddigest_items[$section['machine_name']],
          $section['color'],
          $section['heading']);
        $preview[$section['machine_name']] = $this->dailyDigestFormatSectionPreview(
          $sorted_ddigest_items[$section['machine_name']],
          $section['color'],
          $section['heading']);
      }
    }

    return [
      'raw' => [
        '#type' => 'textarea',
        '#value' => $contents,
        '#rows' => 10,
        '#title' => 'HTML for Mailchimp',
      ],
      'new_preview' => $preview,
    ];
  }

  /**
   * @param string $start_date_time
   * @param string $end_date_time
   * @return array
   */
  private function fetchAnnouncementsForDailyDigest(string $start_date_time, string $end_date_time): array {
    $announcements = [];

    $drupal_anncmnt_query = \Drupal::entityQuery('node')
      ->accessCheck()
      ->condition('type', 'anncmnt')
      ->condition('status', 1)
      ->condition('field_ddigest_date', $start_date_time, '>=')
      ->condition('field_ddigest_date', $end_date_time, '<=')
      ->condition('field_ddigest_nopub', 1, '<')
      ->exists('field_ddigest_sec')
      ->execute();
    $drupal_anncmnt_nids = array_values($drupal_anncmnt_query);
    foreach ($drupal_anncmnt_nids as $nid) {
      $announcements[$nid] = Node::load($nid);
    }

    return $announcements;
  }

  /**
   * @param string $start_date_time
   * @param string $end_date_time
   * @return array
   */
  private function fetchEventsForDailyDigest(string $start_date_time, string $end_date_time): array {
    $events = [];

    $calculated_start_date_time = $this->getUTCDateTime($start_date_time);
    $calculated_end_date_time = $this->getUTCDateTime($end_date_time, 'P1D');

    // Get events occurring in the range.
    $drupal_event_occurring_query = \Drupal::entityQuery('node')
      ->accessCheck()
      ->condition('type', 'event')
      ->condition('status', 1)
      ->condition('field_event_date', $calculated_end_date_time->format('Y-m-d\TH:i:s'), '<')
      ->condition('field_event_date.end_value', $calculated_start_date_time->format('Y-m-d\TH:i:s'), '>=')
      ->condition('field_ddigest_nopub', 1, '<')
      ->execute();
    $drupal_event_occurring_query_nids = array_values($drupal_event_occurring_query);

    // Get advertised events.
    $drupal_event_advertised_query = \Drupal::entityQuery('node')
      ->accessCheck()
      ->condition('type', 'event')
      ->condition('status', 1)
      ->condition('field_ddigest_date', $start_date_time, '>=')
      ->condition('field_ddigest_date', $end_date_time, '<=')
      ->condition('field_ddigest_nopub', 1, '<')
      ->execute();
    $drupal_event_advertised_query_nids = array_values($drupal_event_advertised_query);

    foreach ($drupal_event_occurring_query_nids as $nid) {
      $events[$nid] = Node::load($nid);
    }
    foreach ($drupal_event_advertised_query_nids as $nid) {
      $events[$nid] = Node::load($nid);
    }

    return $events;
  }

  /**
   * @param $announcements
   * @param $events
   * @param $start_date
   * @return array
   */
  private function sortItemsForDailyDigestIntoSections($announcements, $events, $start_date) {
    $sorted_items = [];

    foreach ($announcements as $announcement) {
      $section = $announcement->field_ddigest_sec->getValue()[0]['value'];
      $sorted_items[$section]['announcements'][] = $announcement;
    }

    foreach ($events as $event) {
      $audience_tid = $event->field_event_audience->getValue()[0]['target_id'];
      if (!empty($audience_tid)) {

        switch ($audience_tid) {
          // Students only.
          case 16:
            $section = 'students';
            break;

          // Faculty and staff only.
          case 21:
            $section = 'faculty_staff';
            break;

          // Graduate Schools only.
          case 5411:
          case 4696:
          case 36:
            $section = 'grad_schools';
            break;

          // Alumnae and Prospectives only.
          case 26:
          case 31:
            $section = 'no_print';
            break;

          // All Campus.
          default:
            $section = 'campus';
            break;
        }
      }

      $eventIsOnStartDate = $this->eventIsOnStartDate($event, $start_date);
      if (isset($section)) {
        if ($eventIsOnStartDate) {
          $sorted_items[$section]['today'][] = $event;
        }
        else {
          $sorted_items[$section]['upcoming'][] = $event;
        }
      }
    }
    foreach ($sorted_items as $audience => &$headings) {
      foreach ($headings as $heading => &$events) {
        if (in_array($heading, ['today', 'upcoming'])) {
          usort($events, fn($a, $b) => strcmp($a->field_event_date->getValue()[0]['value'], $b->field_event_date->getValue()[0]['value']));
        }
      }
    }

    return $sorted_items;
  }

  /**
   * Outputs a formatted HTML string for a Daily Diagest section.
   *
   * @param array $nodes
   *   Array of Announcement or Event nodes variables.
   * @param string $color
   *   Header color.
   * @param string $header
   *   Section header text.
   *
   * @return string
   *   Formatted HTML string of content.
   */
  private function dailyDigestFormatSection(array $nodes, string $color, string $header): string {
    $contents = '<h1 style="font-size: 22px;text-align: center;background-color: ' . $color . ';color: #ffffff;padding: .4em .3em .4em .6em;display: block;margin: 0;font-weight: bold;line-height: 125%;">' . $header . '</h1>';
    if (isset($nodes['announcements']) && count($nodes['announcements'])) {
      $contents .= '<h2 style="font-size: 20px;text-align: center;padding: .4em .3em .4em .6em;color: ' . $color . ';display: block;margin: 0;font-weight: bold;line-height: 125%;">ANNOUNCEMENTS</h2>';
      foreach ($nodes['announcements'] as $node) {
        $contents .= $this->dailyDigestFormatAnncmnt($node, $color);
      }
    }

    if (isset($nodes['today']) && count($nodes['today'])) {
      $contents .= '<h2 style="font-size: 20px;text-align: center;padding: .4em .3em .4em .6em;color: ' . $color . ';display: block;margin: 0;font-weight: bold;line-height: 125%;">TODAY\'S EVENTS</h2>';
      foreach ($nodes['today'] as $node) {
        $contents .= $this->dailyDigestFormatEvent($node, $color);
      }
    }
    if (isset($nodes['upcoming']) && count($nodes['upcoming'])) {
      $contents .= '<h2 style="font-size: 20px;text-align: center;padding: .4em .3em .4em .6em;color: ' . $color . ';display: block;margin: 0;font-weight: bold;line-height: 125%;">UPCOMING EVENTS</h2>';
      foreach ($nodes['upcoming'] as $node) {
        $contents .= $this->dailyDigestFormatEvent($node, $color);
      }
    }
    return $contents;
  }

  /**
   * @param array $nodes
   * @param string $color
   * @param string $header
   * @return array
   */
  private function dailyDigestFormatSectionPreview(array $nodes, string $color, string $header): array {
    $section = [
      '#theme' => 'ddigest_preview',
      '#heading' => $header,
      '#color' => $color,
    ];

    if (isset($nodes['announcements']) && count($nodes['announcements'])) {
      $section['#announcements'] = [];
      foreach ($nodes['announcements'] as $node) {
        $section['#announcements'][] = $this->dailyDigestFormatAnncmntPreview($node);
      }
    }

    if (isset($nodes['today']) && count($nodes['today'])) {
      $section['#today'] = [];
      foreach ($nodes['today'] as $node) {
        $section['#today'][] = $this->dailyDigestFormatEventPreview($node);
      }
    }

    if (isset($nodes['upcoming']) && count($nodes['upcoming'])) {
      $section['#upcoming'] = [];
      foreach ($nodes['upcoming'] as $node) {
        $section['#upcoming'][] = $this->dailyDigestFormatEventPreview($node);
      }
    }

    return $section;
  }

  /**
   * Outputs the details of an Announcement, formatted for the Daily Digest.
   *
   * @param object $node
   *   The Announcement node object.
   * @param string $color
   *   The color to be used for the Announcement title.
   *
   * @return string
   *   The formatted HTML with the Announcement listing.
   */
  private function dailyDigestFormatAnncmnt(object $node, string $color = '#000'): string {
    $output = '';
    $title = '<strong><span style="font-size:18px">';
    if ($node->field_anncmnt_body->value) {
      $href = $node->toUrl('canonical', ['absolute' => TRUE])->toString();
      $title .= '<a href="' . $href . '" style = "color:' . $color . '">' . $node->title->getValue()[0]['value'] . '</a>';
    }
    else if ($node->field_anncmnt_url->uri) {
      $title .= '<a href="' . $node->field_anncmnt_url->getValue()[0]['uri'] . '" style = "color:' . $color . '">' . $node->title->getValue()[0]['value'] . '</a>';
    }
    else {
      $title .= '<span style = "color:' . $color . '">' . $node->title->getValue()[0]['value'] . '</span>';
    }
    $title .= '</span></strong><br>';
    $output .= '<p>' . $title;
    if ($node->field_anncmnt_blurb->getValue()[0]) {
      $output .= nl2br($node->field_anncmnt_blurb->getValue()[0]['value']);
    }
    $output .= '</p>' . "\n" . '<hr>' . "\n" . "\n";
    return $output;
  }

  /**
   * Prepares the announcement for the Daily Digest Preview.
   *
   * @param object $node
   *
   * @return array
   */
  private function dailyDigestFormatAnncmntPreview(object $node): array {
    $announcement = [];
    if (isset($node->title->getValue()[0]['value'])) {
      $announcement['#title'] = $node->title->getValue()[0]['value'];
    }

    if ($node->field_anncmnt_body->value) {
      $announcement['#uri'] = $href = $node->toUrl('canonical', ['absolute' => FALSE])->toString();
    }
    else if ($node->field_anncmnt_url->uri) {
      $announcement['#uri'] = $node->field_anncmnt_url->getValue()[0]['uri'];
    }


    if (isset($node->field_anncmnt_blurb->getValue()[0]['value'])) {
      $announcement['#blurb'] = $node->field_anncmnt_blurb->getValue()[0]['value'];
    }

    return $announcement;
  }

  /**
   * Outputs the Event details in the format needed for the Daily Digest.
   */
  private function dailyDigestFormatEvent($node, $color = '#000'): string {
    $title = $node->getTitle();
    $href = $node->toUrl('canonical', ['absolute' => TRUE])->toString();
    $blurb = nl2br($node->field_event_blurb->getValue()[0]['value']);
    // code for Event Form Restructure 9/2023
    if (isset($node->field_event_location->getValue()[0]['target_id'])) {
      $location = Term::load($node->field_event_location->getValue()[0]['target_id'])->getName();
    }
    $allowed_values = $node->field_event_location_choice->getFieldDefinition()->getSetting('allowed_values');
    $choice_value = $node->get('field_event_location_choice')->value;

    if (count($node->field_event_location_details->getValue()) > 0) {
      $location_details = $node->field_event_location_details->getValue()[0]['value'];
    }
    // -END- code for Event Form Restructure 9/2023
    $date = $this->formatEventDate($node);

    $output = '<p><strong><span style="font-size:18px">';
    $output .= '<a href="' . $href . '" style="color: ' . $color . ';">' . $title . '</a>';
    $output .= '</span></strong><br>';
    $output .= $blurb . "</p>\n";
    $output .= '<p>';
    // code for Event Form Restructure 9/2023
    if (($choice_value !== 'on_campus') && ($choice_value !== 'hybrid_on_campus')) {
      $output .= $allowed_values[$choice_value] . ' Event<br>';
    }
    if ($location) {
      $output .= $location . '<br>';
    }
    if (isset($location_details)) {
      $output .= $location_details . '<br>';
    }
    // -END- code for Event Form Restructure 9/2023
    $output .= '<strong>' . $date . '</strong></p>' . "\n";
    $output .= '<hr>' . "\n\n";
    return $output;
  }

  /**
   * @param $node
   * @return array
   * @throws \Exception
   */
  private function dailyDigestFormatEventPreview($node): array {
    $event = [];
    $event['#title'] = $node->getTitle();
    $event['#href'] = $href = $node->toUrl('canonical', ['absolute' => TRUE])->toString();
    $event['#blurb'] = $node->field_event_blurb->getValue()[0]['value'];
    // code for Event Form Restructure 9/2023
    if (isset($node->field_event_location->getValue()[0]['target_id'])) {
      $location = Term::load($node->field_event_location->getValue()[0]['target_id'])->getName();
    }
    $allowed_values = $node->field_event_location_choice->getFieldDefinition()->getSetting('allowed_values');
    $choice_value = $node->get('field_event_location_choice')->value;
    if (($choice_value !== 'on_campus') && ($choice_value !== 'hybrid_on_campus')) {
      $event['#location_choice']= $allowed_values[$choice_value];
    }
    if ($location) {
      $event['#location'] = Term::load($node->field_event_location->getValue()[0]['target_id'])->getName();
    }
    // -END- code for Event Form Restructure 9/2023
    if (count($node->field_event_location_details->getValue()) > 0) {
      $event['#location_details'] = $node->field_event_location_details->getValue()[0]['value'];
    }
    $event['#date'] = $this->formatEventDate($node);

    return $event;
  }

  /**
   *
   */
  private function getUTCDateTime(string $date_time_string, string $offset = NULL) {
    $date_time = new \DateTime($date_time_string);
    $date_time->setTimezone(new \DateTimeZone('UTC'));
    if ($offset) {
      $date_time = $date_time->add(new \DateInterval($offset));
    }
    return $date_time;
  }

  /**
   * Checks whether an Event takes place on a given start date.
   *
   * @param object $node
   *   The Event node object.
   * @param object $start_date
   *   The start date to check the Event node against.
   *
   * @return bool
   *   Whether the Event takes place on the start date.
   */
  private function eventIsOnStartDate(object $node, $start_date): bool {
    $ddigest_start_date_time = $this->getUTCDateTime($start_date, 'P1D');
    $ddigest_start_date_time->setTimezone(new \DateTimeZone('America/New_York'));
    $ddigest_start_date = $ddigest_start_date_time->format('Y-m-d\TH:i:s');
    $node_start_date_field_value = $node->field_event_date->getValue()[0]['value'];
    $nodeStartDate = new \DateTime($node_start_date_field_value, new \DateTimeZone('UTC'));
    $nodeStartDate->setTimezone(new \DateTimeZone('America/New_York'));
    $node_start_date_local = $nodeStartDate->format('Y-m-d\TH:i:s');
    return ($node_start_date_local <= $ddigest_start_date);
  }

  /**
   * Formats the Event date in the format needed for the Daily Digest.
   *
   * @param object $node
   *   The Event node object.
   *
   * @return string
   *   The formatted date string.
   *
   * @throws \Exception
   */
  private function formatEventDate(object $node): string {

    $date = bmc_custom_preprocess_smart_date_field($node);
    if ($date['multi_day']) {
      if ($date['end_year']) {
        $date_string = $date['start_month'] . ' ' . $date['start_date'] . ', ' . $date['start_year'] . ' - '
        . $date['end_month'] . ' ' . $date['end_date'] . ', ' . $date['end_year'];
      }
      elseif ($date['end_month']) {
        $date_string = $date['start_month'] . ' ' . $date['start_date'] . ' - ' . $date['end_month'] . ' '
          . $date['end_date'] . ', ' . $date['start_year'];
      }
      else {
        $date_string = $date['start_month'] . ' ' . $date['start_date'] . ' - ' . $date['end_date'] . ', '
        . $date['start_year'];
      }
    }
    elseif ($date['all_day']) {
      $date_string = $date['start_month'] . ' ' . $date['start_date'] . ', ' . $date['start_year'];
    }
    else {
      $date_string = $date['start_month'] . ' ' . $date['start_date'] . ', ' . $date['start_year'] . ' '
        . $date['start_time'] . ' - ' . $date['end_time'];
    }

    return $date_string;
  }

}
