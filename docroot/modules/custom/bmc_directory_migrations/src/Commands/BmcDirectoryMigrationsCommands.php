<?php

namespace Drupal\bmc_directory_migrations\Commands;

use Drupal\bmc_directory_migrations\BmcDirectoryMigrationsUtilInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\State\StateInterface;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class BmcDirectoryMigrationsCommands extends DrushCommands {

  protected BmcDirectoryMigrationsUtilInterface $migrationUtil;

  protected ConfigFactoryInterface $configFactory;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $migrationConfig;

  protected StateInterface $state;

  /**
   *
   */
  public function __construct(
    BmcDirectoryMigrationsUtilInterface $migrationUtil,
    ConfigFactoryInterface $configFactory,
    StateInterface $state
  ) {
    $this->migrationUtil = $migrationUtil;
    $this->configFactory = $configFactory;
    $this->migrationConfig = $this->configFactory->get('bmc_directory_migrations.settings');
    $this->state = $state;
  }

  /**
   * Command description here.
   *
   * @usage bmc_directory_migrations:run_migrations
   *   Usage description
   *
   * @command bmc_directory_migrations:run_migrations
   * @aliases bmc_directory_migrations_run
   */
  public function runMigrations() {
    $this->migrationUtil->runBmcDirectoryMigrations();
  }

  /**
   * Command description here.
   *
   * @usage bmc_directory_migrations:run_migrations
   *   Usage description
   *
   * @command bmc_directory_migrations:reset_cron
   * @aliases bmc_directory_migrations_reset_cron
   */
  public function resetCron() {
    $this->state->delete('bmc_directory_migrations.cron_last_run_date');
  }

  /**
   * Command description here.
   *
   * @usage bmc_directory_migrations:run_migrations
   *   Usage description
   *
   * @command bmc_directory_migrations:getInfo
   * @aliases bmc_directory_migrations_get_info
   */
  public function getInfo() {
    $this->output()->writeln('BMC Directory Migrations current configuration:');
    $this->output()->write(var_dump($this->migrationConfig->get()));
    $this->output()->writeln('BMC Directory Migrations cron last run date');
    $last_run_date = $this->state->get('bmc_directory_migrations.cron_last_run_date');
    if (!is_null($last_run_date)) {
      $this->output()
        ->writeln(($this->state->get('bmc_directory_migrations.cron_last_run_date:')));
    }
    else {
      $this->output->writeln('The system does not believe BMC Migrations Drupal cron has been run.');
    }
  }

}
