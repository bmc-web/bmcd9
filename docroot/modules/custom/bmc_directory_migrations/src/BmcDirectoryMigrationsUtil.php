<?php

namespace Drupal\bmc_directory_migrations;

use Drupal\migrate\MigrateMessage;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManager;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate_tools\MigrateExecutable;
use Drupal\bmc_csv\CSVImporterInterface;

/**
 * Service to provide helper methods for the Bmc Directory Migrations.
 */
class BmcDirectoryMigrationsUtil implements BmcDirectoryMigrationsUtilInterface {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The config for the module.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * The Migration plugin manager.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface|\Drupal\migrate\Plugin\MigrationPluginManager
   */
  protected MigrationPluginManagerInterface $migrationPluginManager;

  /**
   * The bmc_csv.importer service.
   *
   * @var \Drupal\bmc_csv\CSVImporterInterface
   */
  protected CSVImporterInterface $csvImporter;

  /**
   * Constructs a BmcDirectoryMigrationUtils object.
   */
  public function __construct(
    FileSystemInterface $file_system,
    Connection $database,
    ConfigFactoryInterface $configFactory,
    EntityTypeManagerInterface $entityTypeManager,
    StateInterface $state,
    MigrationPluginManager $migrationPluginManager,
    CSVImporterInterface $csvImporter
  ) {
    $this->fileSystem = $file_system;
    $this->database = $database;
    $this->configFactory = $configFactory;
    $this->config =
      $this->configFactory->get('bmc_directory_migrations.settings');
    $this->entityTypeManager = $entityTypeManager;
    $this->state = $state;
    $this->migrationPluginManager = $migrationPluginManager;
    $this->csvImporter = $csvImporter;
  }

  /**
   * Unpublishes directory nodes not in the import.
   */
  public function postMigrationcleanup() {
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    // During the migration, we store a list of peoplesoft ids that correspond to rows
    // in the psfeeds. Here, we make sure only that list is published.
    $imported_psids = $this->state->get('bmc_directory_migrations.psids');
    $nids_to_unpublish = $nodeStorage->getQuery()
      ->accessCheck(FALSE)
      ->condition('status', 1)
      ->condition('type', 'directory')
      ->condition('field_dir_peoplesoft_id', $imported_psids, 'NOT IN')
      ->execute();
    $nodes_to_unpublish = $nodeStorage->loadMultiple($nids_to_unpublish);
    foreach ($nodes_to_unpublish as $node) {
      $node->setUnpublished()->save();
    }
  }

  /**
   * Build the source for bmc_directory_assignments.
   *
   * The csv for bmc_directory_assignments does not have a uniqyue key. So, we
   * need convert the csv into a table that can be used as the source for
   * the migration.
   */
  public function importDirectoryAssignments() {
    // Import the psfeed into a custom table, without transforming.
    $path_to_csv = $this->fileSystem->realpath(
      $this->config->get('directory_department_assignments_path'));
    $table_name = 'ps2_directory_dpt_assignments_raw';
    $column_names = ['peoplesoft_id', 'dcode', 'scode'];
    $truncate = TRUE;
    $skip_first_line = $this->config->get('directory_department_assignments_skip_first_line');
    $separator = "\t";

    $importDefinition = $this->csvImporter->definition()
      ->name('bmc_directory_migrations.directory_dpt_assignments_raw')
      ->path($path_to_csv)
      ->tableName($table_name)
      ->columnNames($column_names)
      ->truncate($truncate)
      ->skipFirstLine($skip_first_line)
      ->separator($separator)
      ->build();

    $this->csvImporter->import($importDefinition);

    // Transform the data into a format acceptable for the Migrate API,
    // and populate a second custom table with the transformed data.
    $this->database->truncate('ps2_directory_dpt_assignments')->execute();
    $raw_data_rows = $this->database->query(
      'SELECT peoplesoft_id, dcode, scode from {ps2_directory_dpt_assignments_raw}')->fetchAll(\PDO::FETCH_ASSOC);
    foreach ($raw_data_rows as $data) {
      $dept_code = isset($data['scode']) ? $this->generateDeptCode($data['dcode'], $data['scode']) : $data['dcode'];
      if ($dept_code !== NULL) {
        if ($dept_code !== $data['dcode']) {
          $this->database->insert('ps2_directory_dpt_assignments')->fields([
            'peoplesoft_id' => $data['peoplesoft_id'],
            'dept_code' => $data['dcode'],
          ])->execute();
        }
        $this->database->insert('ps2_directory_dpt_assignments')->fields([
          'peoplesoft_id' => $data['peoplesoft_id'],
          'dept_code' => $dept_code,
        ])->execute();
      }
    }
  }

  /**
   * Powers the drush command the cron.
   */
  public function runBmcDirectoryMigrations() {
    $migration_ids = $this->config->get('migrations');
    foreach ($migration_ids as $migration_id) {
      $this->runMigration($migration_id);
    }
  }

  /**
   * A helper function for transforming the DirectoryAssignments data.
   */
  private function generateDeptCode($dcode, $scode) {
    if (is_null($scode) || strlen($scode) == 0) {
      return $dcode;
    }
    else {
      return implode('_', [$dcode, $scode]);
    }
  }

  /**
   * Runs an individual migration.
   */
  private function runMigration(string $migration_id) {
    $migration = $this->migrationPluginManager->createInstance($migration_id);
    if ($migration != FALSE) {
      if ($migration->getStatus() !== MigrationInterface::STATUS_IDLE) {
        $migration->setStatus(MigrationInterface::STATUS_IDLE);
      }

      $migration->getIdMap()->prepareUpdate();
      $executable = new MigrateExecutable($migration, new MigrateMessage());
      $executable->import();
    }
  }

}
