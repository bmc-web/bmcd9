<?php

namespace Drupal\bmc_directory_migrations;

/**
 * Service to provide helper methods for the Bmc Directory Migrations.
 */
interface BmcDirectoryMigrationsUtilInterface {

  /**
   * Run all migrations defined in config. Useful for drush.
   */
  public function runBmcDirectoryMigrations();

  /**
   * Unpublishes directory nodes not in the import.
   */
  public function postMigrationcleanup();

  /**
   * Build the source for bmc_directory_assignments.
   *
   * The csv for bmc_directory_assignments does not have a uniqyue key. So, we
   * need convert the csv into a table that can be used as the source for
   * the migration.
   */
  public function importDirectoryAssignments();

}
