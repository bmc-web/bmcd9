<?php

namespace Drupal\bmc_directory_migrations\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\State\StateInterface;
use Drupal\bmc_directory_migrations\BmcDirectoryMigrationsUtilInterface;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigrateImportEvent;

/**
 * BMC Directory Migrations event subscriber.
 */
class BmcDirectoryMigrationsSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * @var \Drupal\bmc_directory_migrations\BmcDirectoryMigrationsUtilInterface
   */
  protected BmcDirectoryMigrationsUtilInterface $directoryMigrationsUtil;

  /**
   * Constructs event subscriber.
   */
  public function __construct(
    StateInterface $state,
    BmcDirectoryMigrationsUtilInterface $directoryMigrationsUtil
  ) {
    $this->state = $state;
    $this->directoryMigrationsUtil = $directoryMigrationsUtil;
  }

  /**
   * @return array
   */
  public static function getSubscribedEvents() {
    $events[MigrateEvents::PRE_IMPORT][] = ['onMigratePreImport'];
    $events[MigrateEvents::POST_IMPORT][] = ['onMigratePostImport'];
    $events[MigrateEvents::POST_ROW_SAVE] = ['onMigratePostRowSave'];
    return $events;
  }

  public function onMigratePreImport(MigrateImportEvent $event) {
    $migration_id = $event->getMigration()->getBaseId();
    switch ($migration_id) {
      case 'bmc_directory_nodes':
        // before running this migration, clear out the list of
        // imported nodes from the state API
        $this->bmcDirectoryNodesPreImport();
        break;
      case 'bmc_directory_department_assignments':
        // before running this migration, use bmc_csv.import to build
        // the source table
        $this->bmcDirectoryDepartmentAssignmentsPreImport();
        break;
      default:
        break;
    }
  }

  public function onMigratePostImport(MigrateImportEvent $event) {
    $migration_id = $event->getMigration()->getBaseId();
    switch ($migration_id) {
      case 'bmc_directory_nodes':
        // after running this migration, unpublish all nodes not
        // corresponding to a row in the migration
        $this->bmcDirectoryNodesPostImport();
        break;
      default:
        break;
    }
  }

  public function onMigratePostRowSave(MigratePostRowSaveEvent $event) {
    $migration_id = $event->getMigration()->getBaseId();
    switch ($migration_id) {
      case 'bmc_directory_nodes':
        // maintain a list of nodes imported during the migration
        $this->bmcDirectoryNodesPostRowSave($event);
        break;
      default:
        break;
    }
  }

  /**
   *
   */
  private function bmcDirectoryNodesPreImport() {
    $this->state->set('bmc_directory_migrations.psids', []);
  }

  /**
   * @param \Drupal\migrate\Event\MigratePostRowSaveEvent $event
   */
  private function bmcDirectoryNodesPostRowSave(MigratePostRowSaveEvent $event) {
    $imported_psids = $this->state->get('bmc_directory_migrations.psids');
    $row = $event->getRow();
    if (isset($row->getDestination()['field_dir_peoplesoft_id'])) {
      $psid = $row->getDestination()['field_dir_peoplesoft_id'];
      if (!in_array($psid, $imported_psids)) {
        $imported_psids[] = $psid;
        $this->state->set('bmc_directory_migrations.psids', $imported_psids);
      }
    }
  }

  /**
   *
   */
  private function bmcDirectoryNodesPostImport() {
    $this->directoryMigrationsUtil->postMigrationCleanup();
  }

  private function bmcDirectoryDepartmentAssignmentsPreImport() {
    $this->directoryMigrationsUtil->importDirectoryAssignments();
  }

}
