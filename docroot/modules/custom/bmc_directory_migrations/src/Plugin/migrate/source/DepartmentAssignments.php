<?php

namespace Drupal\bmc_directory_migrations\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * The 'bmc_directory_migrations_department_assignments' source plugin.
 *
 * @MigrateSource(
 *   id = "bmc_directory_migrations_department_assignments",
 *   source_module = "bmc_directory_migrations"
 * )
 */
class DepartmentAssignments extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('ps2_directory_dpt_assignments', 't')->fields('t', ['peoplesoft_id']);
    $query->addExpression('GROUP_CONCAT(dept_code)', 'dept_code');
    $query->groupBy('peoplesoft_id');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'peoplesoft_id' => $this->t('peoplesoft_id'),
      'dept_code' => $this->t('The codes fpr the departments to which the person belongs'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'peoplesoft_id' => [
        'type' => 'integer',
        'alias' => 't',
      ],
    ];
  }

  /**
   * Prepares the row.
   */
  public function prepareRow(Row $row) {
    return parent::prepareRow($row);
  }

}
