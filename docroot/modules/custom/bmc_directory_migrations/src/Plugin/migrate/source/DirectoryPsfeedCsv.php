<?php

namespace Drupal\bmc_directory_migrations\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_source_csv\Plugin\migrate\source\CSV;

/**
 * The 'bmc_directory_migrations_directory_psfeed_csv' source plugin.
 *
 * @MigrateSource(
 *   id = "bmc_directory_migrations_directory_psfeed_csv",
 *   source_module = "bmc_directory_migrations"
 * )
 */
class DirectoryPsfeedCsv extends CSV {

  /**
   * Implements prepareRow.
   */
  public function prepareRow(Row $row) {
    // Calculate and set the title on the row.
    $title = $this->calculateName(
      $row->getSourceProperty('firstname'),
      $row->getSourceProperty('lastname'),
      $row->getSourceProperty('middlename'),
    );
    $row->setSourceProperty('title', $title);
    // Clean up the person type property.
    if (!is_null($row->getSourceProperty('person_type'))) {
      $row->setSourceProperty('person_type', $this->stripLineEnding($row->getSourceProperty('person_type')));
    }
    // Clean up the onleave property.
    if (!is_null($row->getSourceProperty('onleave'))) {
      $row->setSourceProperty('onleave', $this->stripLineEnding($row->getSourceProperty('onleave')));
    }
    return parent::prepareRow($row);
  }

  /**
   * Create the node title.
   *
   * Concatenate the first, middle, and last name, and then run through the
   * psfeeds encoding filter.
   */
  private function calculateName(string $first, string $last, string $middle = ''): string {
    $name = $first;
    if ($middle != '') {
      $name .= ' ' . $middle;
    }
    $name .= ' ' . $last;
    return \Drupal::service('bmc_csv.psfeeds_fixes')->doAllFiexes($name);
  }

  /**
   * Cleans up the string.
   */
  private function stripLineEnding($string) {
    return str_replace(["\n", "\r"], '', $string);
  }

}
