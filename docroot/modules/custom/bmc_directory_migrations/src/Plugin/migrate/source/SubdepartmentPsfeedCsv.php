<?php

namespace Drupal\bmc_directory_migrations\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_source_csv\Plugin\migrate\source\CSV;

/**
 * The 'bmc_directory_migrations_directory_psfeed_csv' source plugin.
 *
 * @MigrateSource(
 *   id = "bmc_directory_migrations_subdepartment_psfeed_csv",
 *   source_module = "bmc_directory_migrations"
 * )
 */
class SubdepartmentPsfeedCsv extends CSV {

  /**
   * Implements prepareRow.
   */
  public function prepareRow(Row $row) {
    $code = $row->getSourceProperty('code');
    $dcode = $row->getSourceProperty('dcode');
    $row->setSourceProperty('code', $this->calculateSubdepartmentCode($dcode, $code));

    return parent::prepareRow($row);
  }

  /**
   * Helps with the calculation.
   */
  private function calculateSubdepartmentCode(string $dcode, string $code) {
    return implode('_', [$dcode, $code]);
  }

}
