<?php

namespace Drupal\bmc_directory_migrations\Plugin\migrate\process;

use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bmc_csv\PsfeedsFixesInterface;

/**
 * Provides a bmc_directory_fix_psfeed_encoding plugin.
 *
 * Usage:
 *
 * @code
 * process:
 *   bar:
 *     plugin: bmc_directory_fix_psfeed_encoding
 *     source: foo
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "bmc_directory_fix_psfeed_encoding"
 * )
 *
 * @DCG
 * ContainerFactoryPluginInterface is optional here. If you have no need for
 * external services just remove it and all other stuff except transform()
 * method.
 */
class BmcDirectoryFixPsfeedEncoding extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The transliteration service.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected $transliteration;

  /**
   * The PsfeedsFixes service.
   *
   * @var \Drupal\bmc_csv\PsfeedsFixesInterface
   */
  protected PsfeedsFixesInterface $psfeedsFixes;

  /**
   * Constructs a BmcDirectoryFixPsfeedEncoding plugin.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\bmc_csv\PsfeedsFixesInterface $psfeedsFixes
   *   The PsfeedsFixes service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PsfeedsFixesInterface $psfeedsFixes) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->psfeedsFixes = $psfeedsFixes;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('bmc_csv.psfeeds_fixes'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    return $this->psfeedsFixes->doAllFiexes($value);
  }

}
