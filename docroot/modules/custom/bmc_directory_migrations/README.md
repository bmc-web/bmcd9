# BMC Directory Migrations

Prior to this module, Bryn Mawr Drupal would run perl scripts to import peoplesoft feeds from AIS, and then run custom
functions in the bmc_peoplesoft module in order to update nodes and taxonomy terms based on those custom tables. This
module replaces that functionality using Drupal's Migrate api.

## Running the imports with drush

Using drush, run the migrations with the command bmc_directory_migrations_run. Note: running the migrations through
drush does NOT update any of the state values that running the update through cron would.

## Running the imports with cron

We implement hook_cron in bmc_directory_migrations.module. The migrations will NOT run via drupal cron unless
settings.php contains the line `$config['bmc_directory_migrations.settings']['cron_is_active'] = TRUE;`

If `cron_is_active` is set to true, cron will run every day at the time specified by
`$config['bmc_directory_migrations.settings']['cron_run_time']`. This configuration should be a positive integer that
represents the number of seconds after midnight from would run. For example, to run this cron at 4am,
set `cron_run-time` to 14400.

This cron uses drupal's state system to record when cron was last run. To see when cron was last run, run the drush
command bmc_directory_migrations_get_info. Cron last run information will be last lines of the output. Note: running
the imports with bmc_directory_migrations_run will not update the Cron last run date information from
bmc_directory_migrations_get_info. To reser the cron last run information, use the drush command
bmc_directory_migrations_reset_cron. (The imports will run on the next drupal cron if cron_is_active = TRUE.)

## The Migrations

The migrations will be run in the order that they are listed in `bmc_directory_migrations.settings.yml`. You can
override this list in settings.php.

By default, all migrations expect their source file to be in public://psfeeds/. You can override this ins settings.php,
for example:

```
$config['bmc_directory_migrations.settings']['directory_department_assignments_path'] = 'private://psfeeds/ps2_person_assignments.txt';
$config['migrate_plus.migration.bmc_department_terms']['source']['path'] = 'private://psfeeds/ps2_department.txt';
$config['migrate_plus.migration.bmc_directory_nodes']['source']['path'] = 'private://psfeeds/ps2_person.txt';
$config['migrate_plus.migration.bmc_subdepartment_terms']['source']['path'] = 'private://psfeeds/ps2_subdepartment.txt';
```

The migrations are:
- bmc_department_terms: Updates the departments / subdepartrments vocabulary
- bmc_subdepartment_terms: Updates the departments / subdepartments vocabulary
- bmc_directory_nodes: Creates / updates directory nodes, and unpublishes nodes not in the feed
- bmc_directory_department_assignments: Associates departments / subdepartments vocabulary terms with directory nodes

The migration `bmc_directory_department_assignments` should be run after `bmc_department_terms`

The migration `bmc_directory_department_assignments` should be run last, as it depends on the previous three migrations.
The migration also requires that the bmc_csv.importer service run a migration of a psfeed into a custom table prior to
executing.

To handle foreign names not properly encoded from AIS, we run the names through the BmcDirectoryFixPsfeedEncoding.php
plugin, which uses the PsfeedFixes service from the bmc_csv module.

## Unpublishing Nodes not in the bmc_directory_nodes feed
As business requirement is that all people not in the bmc_directory_nodes feed have their corresponding node
unpublished. We do this with with state variables. before running that migration, we empty the
`bmc_directory_migrations.nids` state variables. During the migration, we put every node found into that array. After
running that migration, we unpublish all nodes that are not in that array.
See Drupal\bmc_directory_migrations\EventSubscriber\BmcDirectoryMigrationsSubscriber for more information.
