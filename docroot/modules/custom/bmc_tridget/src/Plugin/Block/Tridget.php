<?php

namespace Drupal\bmc_tridget\Plugin\Block;

use Drupal\Core\Url;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a Tridget form block.
 *
 * @Block(
 *   id = "bmc_tridget",
 *   admin_label = @Translation("Tridget Block"),
 * )
 */
class Tridget extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    return [
      '#theme' => 'tridget',
      '#attached' => [
        'library' => [
          'bmc_tridget/tridget',
        ],
      ],
    ];

  }

}
