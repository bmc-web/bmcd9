/* using a javascript closure... as defined here: https://www.drupal.org/docs/7/theming/working-with-javascript-and-jquery  */
/* this js will activate tabs with JavaScript, wait until document is fully scriptable */
(function ($) {
    Drupal.behaviors.bmcTridgetSearch = {
        attach: function (context, settings) {
        $(function() {/* using a javascript closure... as defined here: https://www.drupal.org/docs/7/theming/working-with-javascript-and-jquery  */
          /* this js will activate tabs with JavaScript, wait until document is fully scriptable */
          //Tripod Search
          $("#searchForm").submit(function() {
            // What to do if option value contains "Everything" prefix

            var query = $("#primoQueryTemp", this).val();
            $("#primoQueryTemp")
              .attr("name", "query-no-js")
              .prop("disabled", true); //change name so we are not submitting value and disable so not sent
            if ($("#primoQueryTemp-add").length > 0) {
              var val = "any,contains," + query;
              $("#primoQueryTemp-add").val(val);
            } else {
              $(this).append(
                '<input id="primoQueryTemp-add" type="hidden" name="query" value="any,contains,' +
                query +
                '">'
              );
            }
          });  // end tripod search code
          //Remove disabled attr from form field so you can resubmit
          $("#searchForm").on("click", function() {
            $("#primoQueryTemp")
              .prop("disabled", false)
              .attr("name", "query");
          });

        }()); // end function wrapper
      } // end attach wrapper
    } // end behavior wrapper
}(jQuery));
