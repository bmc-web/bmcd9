<?php declare(strict_types = 1);

namespace Drupal\bmc_cache_invalidations_to_watchdog;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * @todo Add class description.
 */
final class Invalidator implements CacheTagsInvalidatorInterface {

  protected $logger;

  /**
   * Constructs an Invalidator object.
   */
  public function __construct(
    private readonly LoggerChannelFactoryInterface $loggerFactory,
  ) {
    $this->logger = $loggerFactory->get('Hello world');
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateTags(array $tags) {
    try {
       $this->logger->debug(t('Invalidating the following tags: @tags', ['@tags' => implode(' ', array_unique($tags))]));
    }
    catch (DatabaseExceptionWrapper $e) {}  }

}
