"use strict";

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

(function ($) {
  var app = new Vue({
    el: "#program-finder",
    delimiters: ["${", "}"],
    data: function data() {
      return {
        searchTerms: '',
        programs: [],
        interests: [],
        types: [],
        selectedInterests: [],
        selectedTypes: [],
        showFilters: true
      };
    },
    created: function created() {
      var _this = this;

      this.$nextTick(function () {
        $(window).on('resize load', _this.toggleShowFilters);
      }); // Get data from endpoint

      $.getJSON("/api/program/all", function (json) {
        _this.programs = json.map(function (a) {
          a.types = a.types.split(",");
          a.interests = a.interests.split(",");
          return a;
        });
      });
      $.getJSON("/api/tax/all", function (json) {
        _this.interests = json.filter(function (item) {
          return item.type === "prog_interests";
        }).map(function (interest) {
          interest.active = true;
          return interest;
        }).sort(function (a, b) {
          if (a.label.toLowerCase() > b.label.toLowerCase()) {
            return 1;
          }

          if (a.label.toLowerCase() < b.label.toLowerCase()) {
            return -1;
          }

          return 0;
        });
        _this.types = json.filter(function (item) {
          return item.type === "prog_type";
        }).map(function (type) {
          type.active = true;
          return type;
        });
      });
    },
    mounted: function mounted() {
      // Update filters based on query params on page load
      function getQueryVariable() {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        var pairs = [];

        for (var i = 0; i < vars.length; i++) {
          var title = vars[i].split("=")[0];
          pairs.push(_defineProperty({}, title, vars[i].split("=")[1]));
        }

        return pairs;
      }

      var params = getQueryVariable();

      if (params.length > 1) {
        // Set search filter
        if (params[0].query !== "") {
          this.searchTerms = decodeURIComponent(params[0].query);
          this.$refs.searchInput.value = this.searchTerms;
        } // Set type filter


        if (params[1].type != '') {
          this.selectedTypes = params[1].type.split(",");
        } // Set interests filter


        if (params[2].interests != '') {
          this.selectedInterests = params[2].interests.split(",");
        }
      }
    },
    computed: {
      // Get Filtered Results to display
      filteredResults: function filteredResults() {
        var _this2 = this;

        function intersection(arrA, arrB) {
          return arrA.filter(function (x) {
            return arrB.indexOf(x) !== -1;
          });
        } // param item -> item to check filter on


        var matchInterest = function matchInterest(item) {
          if (intersection(_this2.selectedInterests, item.interests).length > 0) {
            return true;
          }

          return false;
        };

        var matchType = function matchType(item) {
          if (intersection(_this2.selectedTypes, item.types).length > 0) {
            return true;
          }

          return false;
        }; // // Check if matches search filter
        // // param item -> item to check filter on


        var matchSearch = function matchSearch(item) {
          var lowerItemSearchContent = item.search.toLowerCase();

          var lowerSearchTerm = _this2.searchTerms.toLowerCase();

          if (lowerItemSearchContent.indexOf(lowerSearchTerm) !== -1) {
            return true;
          }

          return false;
        };

        var filterConds = [{
          func: matchInterest,
          cond: this.selectedInterests
        }, {
          func: matchType,
          cond: this.selectedTypes
        }, {
          func: matchSearch,
          cond: this.searchTerms
        }];
        return this.programs.filter(function (item) {
          for (var i = 0; i < filterConds.length; i++) {
            if (filterConds[i].cond.length > 0) {
              if (filterConds[i].func(item) === false) {
                return false;
              }
            }
          }

          return true;
        });
      },
      // To disable filters, get all the interest and type values of the filtered results
      activeInterests: function activeInterests() {
        var activeInterests = [];
        var arrToUse = this.filteredResults; // Do not disable inputs in own category if no other categories are selected

        if (this.selectedTypes.length === 0 && this.searchTerms === '') {
          arrToUse = this.programs;
        }

        arrToUse.forEach(function (program) {
          var interests = program.interests;
          interests.forEach(function (interest) {
            if (activeInterests.indexOf(interest) === -1) {
              activeInterests.push(interest);
            }
          });
        });
        return activeInterests;
      },
      // To disable filters, get all the interest and type values of the filtered results
      activeTypes: function activeTypes() {
        var activeTypes = [];
        var arrToUse = this.filteredResults; // Do not disable inputs in own category if no other categories are selected

        if (this.selectedInterests.length === 0 && this.searchTerms === '') {
          arrToUse = this.programs;
        }

        arrToUse.forEach(function (program) {
          var types = program.types;
          types.forEach(function (type) {
            if (activeTypes.indexOf(type) === -1) {
              activeTypes.push(type);
            }
          });
        });
        return activeTypes;
      },
      typeFilters: function typeFilters() {
        var _this3 = this;

        return this.types.map(function (type) {
          if (!_this3.activeTypes.includes(type.id)) {
            var tempType = _objectSpread({}, type);

            tempType.active = false;
            return tempType;
          }

          return type;
        });
      },
      interestFilters: function interestFilters() {
        var _this4 = this;

        return this.interests.map(function (interest) {
          if (_this4.activeInterests.indexOf(interest.id) === -1) {
            var tempInterest = _objectSpread({}, interest);

            tempInterest.active = false;
            return tempInterest;
          }

          return interest;
        });
      },
      getType: function getType() {
        var _this5 = this;

        return function (type) {
          return _this5.types.find(function (item) {
            return item.id === type;
          });
        };
      },
      showResultsButton: function showResultsButton() {
        return this.selectedInterests.length > 0 || this.selectedTypes.length > 0;
      }
    },
    watch: {
      // Update URL params whenever filteredResults Update
      filteredResults: function filteredResults() {
        window.history.pushState(null, document.title, "?query=".concat(this.searchTerms, "&type=").concat(this.selectedTypes.join(","), "&interests=").concat(this.selectedInterests.join(",")));
      }
    },
    methods: {
      toggleShowFilters: function toggleShowFilters() {
        if ($(window).width() <= 700) {
          this.showFilters = false;
        } else {
          this.showFilters = true;
        }
      },
      updateSearch: function updateSearch() {
        this.searchTerms = this.$refs.searchInput.value;
      },
      clearFilters: function clearFilters() {
        this.selectedInterests = [];
        this.$refs.searchInput.value = '';
        this.selectedTypes = [];
      },
      showTooltip: function showTooltip(event, target) {
        var $this = $(event.target);
        var $tooltip = $this.closest(".program__result__types").find("#type-" + target);
        $('.program__identifier').not($this).removeClass('expanded').attr('aria-expanded', 'false');
        $tooltip.slideToggle('300');
        $('.program__identifier__tooltip').not($tooltip).hide();
        $this.toggleClass('expanded');

        if ($this.hasClass('expanded')) {
          $this.attr('aria-expanded', 'true');
        } else {
          $this.attr('aria-expanded', 'false');
        }
      }
    }
  }); // Implement style change to sticky card when stuck/unstuck

  window.addEventListener('scroll', function () {
    var stickyEl = document.querySelector('.program-filter__panels__results');

    if (stickyEl != null) {
      // Check if card intersects the bottom of .basic-container__main-col,
      // Then toggle class is-stuck
      var observer = new IntersectionObserver(function (_ref) {
        var _ref2 = _slicedToArray(_ref, 1),
            e = _ref2[0];

        e.target.classList.toggle('is-stuck', e.intersectionRatio === 0);
      }, {
        root: document.querySelector('.program-filter__panels'),
        rootMargin: '-' + (stickyEl === null || stickyEl === void 0 ? void 0 : stickyEl.clientHeight) + 'px',
        threshold: [1]
      });
      observer.observe(stickyEl);
    }
  });
})(jQuery);