(function($) {
  const app = new Vue({
    el: "#program-finder",
    delimiters: ["${", "}"],
    data() {
      return {
        searchTerms: '',
        programs: [],
        interests: [],
        types: [],
        selectedInterests: [],
        selectedTypes: [],
        showFilters: true,
      };
    },
    created() {
      this.$nextTick(() => {
        $(window).on('resize load', this.toggleShowFilters)
      });
      // Get data from endpoint
      $.getJSON("/api/program/all", json => {
        this.programs = json.map(a => {
          a.types = a.types.split(",");
          a.interests = a.interests.split(",");
          return a;
        });
      });
      $.getJSON("/api/tax/all", json => {
        this.interests = json
          .filter(item => item.type === "prog_interests")
          .map(interest => {
            interest.active = true;
            return interest;
          })
          .sort((a, b) => {
            if (a.label.toLowerCase() > b.label.toLowerCase()) {
              return 1;
            }
            if (a.label.toLowerCase() < b.label.toLowerCase()) {
              return -1;
            }
            return 0;
          });
        this.types = json
          .filter(item => item.type === "prog_type")
          .map(type => {
            type.active = true;
            return type;
          })
      });
    },
    mounted() {
      // Update filters based on query params on page load
      function getQueryVariable() {
        const query = window.location.search.substring(1);
        const vars = query.split("&");
        const pairs = [];
        for (let i = 0; i < vars.length; i++) {
          const title = vars[i].split("=")[0];
          pairs.push({ [title]: vars[i].split("=")[1] });
        }
        return pairs;
      }
      const params = getQueryVariable();

      if (params.length > 1) {
        // Set search filter
        if (params[0].query !== "") {
          this.searchTerms = decodeURIComponent(params[0].query);
          this.$refs.searchInput.value = this.searchTerms;
        }
        // Set type filter
        if (params[1].type != '') {
          this.selectedTypes = params[1].type.split(",");
        }

        // Set interests filter
        if (params[2].interests != '') {
          this.selectedInterests = params[2].interests.split(",");
        }
      }
    },
    computed: {
      // Get Filtered Results to display
      filteredResults() {
        function intersection(arrA, arrB) {
          return arrA.filter(x => arrB.indexOf(x) !== -1);
        }
        // param item -> item to check filter on
        const matchInterest = item => {
          if (intersection(this.selectedInterests, item.interests).length > 0) {
            return true;
          }
          return false;
        };

        const matchType = item => {
          if (intersection(this.selectedTypes, item.types).length > 0) {
            return true;
          }
          return false;
        };
        // // Check if matches search filter
        // // param item -> item to check filter on
        const matchSearch = item => {
          const lowerItemSearchContent = item.search.toLowerCase();
          const lowerSearchTerm = this.searchTerms.toLowerCase();
          if (lowerItemSearchContent.indexOf(lowerSearchTerm) !== -1) {
            return true;
          }
          return false;
        };

        const filterConds = [
          {
            func: matchInterest,
            cond: this.selectedInterests
          },
          {
            func: matchType,
            cond: this.selectedTypes
          },
          {
            func: matchSearch,
            cond: this.searchTerms
          }
        ];

        return this.programs.filter(item => {
          for (let i = 0; i < filterConds.length; i++) {
            if (filterConds[i].cond.length > 0) {
              if (filterConds[i].func(item) === false) {
                return false;
              }
            }
          }
          return true;
        });
      },
      // To disable filters, get all the interest and type values of the filtered results
      activeInterests() {
        const activeInterests = [];
        let arrToUse = this.filteredResults;

        // Do not disable inputs in own category if no other categories are selected
        if (this.selectedTypes.length === 0 && this.searchTerms === '') {
          arrToUse = this.programs;
        }
        arrToUse.forEach(program => {
          const { interests } = program;
          interests.forEach(interest => {
            if (activeInterests.indexOf(interest) === -1) {
              activeInterests.push(interest);
            }
          });
        });
        return activeInterests;
      },
      // To disable filters, get all the interest and type values of the filtered results
      activeTypes() {
        const activeTypes = [];
        let arrToUse = this.filteredResults;
        // Do not disable inputs in own category if no other categories are selected
        if (this.selectedInterests.length === 0 && this.searchTerms === '') {
          arrToUse = this.programs;
        }
        arrToUse.forEach(program => {
          const { types } = program;
          types.forEach(type => {
            if (activeTypes.indexOf(type) === -1) {
              activeTypes.push(type);
            }
          });
        });
        return activeTypes;
      },
      typeFilters() {
        return this.types.map(type => {
          if (!this.activeTypes.includes(type.id)) {
            const tempType = { ...type };
            tempType.active = false;
            return tempType;
          }
          return type;
        });
      },
      interestFilters() {
        return this.interests.map(interest => {
          if (this.activeInterests.indexOf(interest.id) === -1) {
            const tempInterest = { ...interest };
            tempInterest.active = false;
            return tempInterest;
          }
          return interest;
        });
      },
      getType() {
        return type =>
          this.types.find(item => {
            return item.id === type;
          });
      },
      showResultsButton() {
        return this.selectedInterests.length > 0 || this.selectedTypes.length > 0;
      },
    },
    watch: {
      // Update URL params whenever filteredResults Update
      filteredResults() {
        window.history.pushState(
          null,
          document.title,
          `?query=${this.searchTerms}&type=${this.selectedTypes.join(
            ","
          )}&interests=${this.selectedInterests.join(",")}`
        );
      }
    },
    methods: {
      toggleShowFilters() {
        if ($(window).width() <= 700) {
          this.showFilters = false;
        } else {
          this.showFilters = true;
        }
      },
      updateSearch() {
        this.searchTerms = this.$refs.searchInput.value;
      },
      clearFilters() {
        this.selectedInterests = [];
        this.$refs.searchInput.value = '';
        this.selectedTypes = [];
      },
      showTooltip(event, target) {
        const $this = $(event.target);
        const $tooltip = $this
          .closest(".program__result__types")
          .find("#type-" + target);
        $('.program__identifier')
          .not($this)
          .removeClass('expanded')
          .attr('aria-expanded','false');
        $tooltip.slideToggle('300');
        $('.program__identifier__tooltip').not($tooltip).hide();
        $this.toggleClass('expanded');
        if ($this.hasClass('expanded')) {
          $this.attr('aria-expanded','true');
        } else {
          $this.attr('aria-expanded','false');
        }
      }
    }
  });
  // Implement style change to sticky card when stuck/unstuck
  window.addEventListener('scroll', () => {
    const stickyEl = document.querySelector('.program-filter__panels__results');
    if (stickyEl != null) {
      // Check if card intersects the bottom of .basic-container__main-col,
      // Then toggle class is-stuck
      const observer = new IntersectionObserver(
        ([e]) => {
          e.target.classList.toggle('is-stuck', e.intersectionRatio === 0);
        },
        {
          root: document.querySelector('.program-filter__panels'),
          rootMargin: '-' + (stickyEl?.clientHeight) + 'px',
          threshold: [1]
        }
      );
      observer.observe(stickyEl);
    }
  });

})(jQuery);
