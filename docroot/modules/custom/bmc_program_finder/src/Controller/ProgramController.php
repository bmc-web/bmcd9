<?php

namespace Drupal\bmc_program_finder\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\media\Entity\Media;

/**
 * Handle our endpoints.
 */
class ProgramController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function all() {

    $response_array = $this->getPrograms();

    $cache_metadata = new CacheableMetadata();
    $cache_metadata->setCacheTags(['node_list']);

    // Create the JSON response object and add the cache metadata.
    $response = new CacheableJsonResponse($response_array);
    $response->addCacheableDependency($cache_metadata);

    return $response;

  }

  /**
   * Get all published programs.
   */
  public function getPrograms() {

    $query = $this->entityTypeManager()->getStorage('node')->getQuery();

    // We need to bypass the access check to ensure we don't
    // have an early render error.
    // https://drupal.stackexchange.com/questions/251864/logicexception-the-controller-result-claims-to-be-providing-relevant-cache-meta
    // https://www.drupal.org/project/drupal/issues/2638686
    $program_query = $query->condition('status', 1)
      ->condition('type', 'program')
      ->sort('title', 'ASC')
      ->accessCheck(false)
      ->execute();

    $nodes = $this->entityTypeManager()->getStorage('node')->loadMultiple($program_query);

    foreach ($nodes as $node) {

      $search_string =
        $node->title->value
        . ' '
        . $node->field_prog_intro->value
        . ' '
        . $node->field_prog_full_body->value;

      // Removing line breaks as this is just used for search, not display.
      $search = trim(preg_replace('/\s+/', ' ', $search_string));

      $response = [
        'title' => $node->title->value,
        'id' => $node->id(),
        'interests' => $this->getTaxIdString($node->field_prog_interest),
        'types' => $this->getTaxIdString($node->field_prog_type),
        'path' => \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $node->id()),
        'search' => $search,
      ];



      if (isset($node->field_prog_img_listing->target_id)) {
        $response['image'] = bmc_custom_get_image_vars(
          $node->field_prog_img_listing->target_id,
          'program_listing'
        );
      }

      $response_array[] = $response;
    }



    // Alphabetize by Title.
    usort($response_array, function ($a, $b) {
      return strcasecmp($a['title'], $b['title']);
    });

    return $response_array;
  }

  /**
   * Takes a node tax field, returns comma delimited list of term id's.
   */
  public function getTaxIdString($tax_field): string {
    $ids = [];
    foreach ($tax_field as $term) {
      if ($term->entity) {
        $ids[] = $term->target_id;
      }
    }
    return implode(",", $ids);
  }

}
