<?php

namespace Drupal\bmc_program_finder\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\taxonomy\Entity\Term;

/**
 * Handle our endpoints.
 */
class TaxController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function all() {

    $response_array = $this->getTerms(
      [
        'prog_interests',
        'prog_type',
      ]
    );

    $cache_metadata = new CacheableMetadata();
    $cache_metadata->setCacheTags(['taxonomy_term_list']);

    // Create the JSON response object and add the cache metadata.
    $response = new CacheableJsonResponse($response_array);
    $response->addCacheableDependency($cache_metadata);

    return $response;
  }

  /**
   * Helper function to return all terms for a given vocab.
   */
  public function getTerms($vid) {
    $term_array = [];
    $query = $this->entityTypeManager()->getStorage('taxonomy_term')->getQuery();
    $tids = $query->accessCheck()->condition('vid', $vid, 'IN')
      ->sort('weight')
      ->execute();

    $terms = Term::loadMultiple($tids);

    foreach ($terms as $term) {
      $term_instance = [
        'type' => $term->bundle(),
        'id' => $term->id(),
        'label' => $term->toLink()->getText(),
        'order' => $term->getWeight(),
      ];

      if ($term->bundle() === 'prog_type') {
        $term_instance['css_class'] = $term->field_prog_type_class[0]->value;
        $term_instance['tooltip'] = $term->field_prog_type_tooltip[0]->value;
      }

      $term_array[] = $term_instance;
    }
    return $term_array;
  }

}
