<?php

namespace Drupal\bmc_program_finder\Plugin\Block;

use Drupal\config_pages\Entity\ConfigPages;
use Drupal\Core\Url;
use Drupal\Core\Block\BlockBase;
use Drupal\media\Entity\Media;

/**
 * Provides Program Finder Vue.js application.
 *
 * @Block(
 *   id = "bmc_program_finder",
 *   admin_label = @Translation("Program Finder (bmc program finder)"),
 * )
 */
class ProgramFinder extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $parent_node_title = '';
    $program_finder_hero_img = [];

    // Get Parent node title.
    $node = \Drupal::routeMatch()->getParameter('node');
    //if ($node instanceof NodeInterface) {
      $parent_node_title = $node->getTitle();
   // }

    // Get Program Finder Hero Image.
    $listing_page_config = ConfigPages::config('listing_page_settings');
    if (!empty($listing_page_config->field_cp_listpage_prog_img->target_id)) {
      $program_finder_hero_img =
        bmc_custom_get_image_vars(
          $listing_page_config->field_cp_listpage_prog_img->target_id,
          'program_hero'
        );
    }

    return [
      '#theme' => 'program_finder',
      '#program_finder_hero_img' => $program_finder_hero_img,
      '#parent_node_title' => $parent_node_title,
      '#attached' => [
        'library' => [
          'bmc_program_finder/program_finder',
        ],
      ],
      '#cache' => [
        'tags' => [
          'taxonomy_term_list',
          'node_list',
        ],
      ],
    ];
  }

}
