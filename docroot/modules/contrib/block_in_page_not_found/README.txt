CONTENTS OF THIS FILE

* Introduction
* Requirements
* Installation
* Configuration
* Troubleshooting
* FAQ
* Maintainers


INTRODUCTION
------------

Block In Page Not Found module allow to place a block in 404 page or page not found.


REQUIREMENTS
------------

This module requires block module of Drupal core.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
  https://www.drupal.org/node/1897420
  for further information.
* After installation, Go to /admin/structure/block.
* Choose a block and and check show on page not found.

CONFIGURATION
-------------

*
*
*

TROUBLESHOOTING
---------------

*
*
*

FAQ
---------------

*
*
*

MAINTAINERS
-----------

* Mohamed Anis Taktak (matio89) - https://www.drupal.org/u/matio89 

