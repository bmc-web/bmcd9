<?php

namespace Drupal\focal_point\Plugin\ImageEffect;

use Drupal\Core\Image\ImageInterface;
use Drupal\focal_point\FocalPointEffectBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Scales and crops image while keeping its focal point close to centered.
 *
 * @ImageEffect(
 *   id = "focal_point_scale_and_crop",
 *   label = @Translation("Focal Point Scale and Crop"),
 *   description = @Translation("Scales and crops image while keeping its focal point close to centered.")
 * )
 */
class FocalPointScaleAndCropImageEffect extends FocalPointEffectBase {

  /**
   * Upscale when image is too small.
   */
  public const UPSCALE = 'upscale';

  /**
   * Do not upscale before cropping but keep aspect ratio.
   */
  public const NO_UPSCALE_CROP = 'no_upscale_crop';

  /**
   * Do not upscale or crop. Aspect ratio not maintained.
   */
  public const NO_UPSCALE_NO_CROP = 'no_upscale_no_crop';

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image) {
    parent::applyEffect($image);

    // Handle where up-scaling is not selected.
    $crop_dimensions = [
      'width' => $image->getWidth(),
      'height' => $image->getHeight(),
    ];
    $this->transformDimensions($crop_dimensions, $image->getSource());

    // Now, attempt to resize the image.
    $originalDimensions = $this->getOriginalImageSize();
    $resize_data = static::calculateResizeData($originalDimensions['width'], $originalDimensions['height'], $crop_dimensions['width'], $crop_dimensions['height']);
    if (!$image->resize($resize_data['width'], $resize_data['height'])) {
      $this->logger->error(
        'Focal point scale and crop failed while resizing using the %toolkit toolkit on %path (%mimetype, %dimensions)',
        [
          '%toolkit' => $image->getToolkitId(),
          '%path' => $image->getSource(),
          '%mimetype' => $image->getMimeType(),
          '%dimensions' => $image->getWidth() . 'x' . $image->getHeight(),
        ]
      );
      return FALSE;
    }

    // Next, attempt to crop the image.
    $crop = $this->getCrop($image, $crop_dimensions);
    return $this->applyCrop($image, $crop);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    // Include a `crop_type` so that the crop module can act on images
    // generated using this effect.
    // @see crop_file_url_alter()
    // @see https://www.drupal.org/node/2842260
    return parent::defaultConfiguration() + [
      'crop_type' => 'focal_point',
      'upscale' => self::UPSCALE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['upscale'] = [
      '#type' => 'select',
      '#title' => t('Upscaling behavior'),
      '#default_value' => $this->configuration['upscale'] ? $this->configuration['upscale'] : self::UPSCALE,
      '#options' => $this->getUpscaleOptions(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['upscale'] = $form_state->getValue('upscale');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $summary = [
      '#theme' => 'focal_point_scale_and_crop_summary',
      '#data' => array_merge($this->configuration, [
        'upscale' => $this->getUpscaleOptions()[$this->configuration['upscale'] ? $this->configuration['upscale'] : self::UPSCALE],
      ]),
    ];
    $summary += parent::getSummary();

    return $summary;
  }

  /**
   * Get options for upscaling.
   */
  private function getUpscaleOptions() {
    return [
      self::UPSCALE => t('Upscale and crop'),
      self::NO_UPSCALE_CROP => t("Don't upscale but still crop"),
      self::NO_UPSCALE_NO_CROP => t("Don't upscale and don't crop"),
    ];
  }

  /**
   * Adjust the dimensions to match the source height.
   *
   * @param array $dimensions
   *   The dimensions to alter.
   */
  private function adjustDimensionsToMatchSourceHeight(array &$dimensions): void {
    if (!empty($dimensions['width']) && !empty($dimensions['height'])) {
      if ($this->configuration['upscale'] === self::NO_UPSCALE_CROP) {
        $target_ratio = $this->configuration['width'] / $this->configuration['height'];
        $dimensions['width'] = (int) ceil($dimensions['height'] * $target_ratio);
      }
      else {
        $difference = $this->configuration['width'] / $dimensions['width'];
        $dimensions['height'] = (int) ceil($dimensions['height'] * $difference);
      }
    }
  }

  /**
   * Adjust the dimensions to match source width.
   *
   * @param array $dimensions
   *   The dimensions to alter.
   */
  private function adjustDimensionsToMatchSourceWidth(array &$dimensions): void {
    if (!empty($dimensions['width']) && !empty($dimensions['height'])) {
      if ($this->configuration['upscale'] === self::NO_UPSCALE_CROP) {
        $target_ratio = $this->configuration['width'] / $this->configuration['height'];
        $dimensions['height'] = (int) ceil($dimensions['width'] / $target_ratio);
      }
      else {
        $difference = $this->configuration['height'] / $dimensions['height'];
        $dimensions['width'] = (int) ceil($dimensions['width'] * $difference);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function transformDimensions(array &$dimensions, $uri) {
    if ($this->configuration['upscale'] !== self::UPSCALE && !empty($dimensions['width']) && !empty($dimensions['height'])) {
      $target_width = $this->configuration['width'];
      $target_height = $this->configuration['height'];

      // If there's not enough image to completely fill the target dimensions,
      // then shrink the target dimensions so that it does fit. Without doing so
      // focal point will upscale the image to fit within the target dimensions,
      // and we want to avoid that.
      if ($target_width > $dimensions['width'] && $target_height > $dimensions['height']) {
        // If we don't want to keep the aspect ratio, just return.
        if ($this->configuration['upscale'] === self::NO_UPSCALE_NO_CROP) {
          return;
        }

        $target_ratio = $target_width / $target_height;
        $source_ratio = $dimensions['width'] / $dimensions['height'];
        // Depending on how the aspect ratios, compare between the source and
        // target, figure out if we need to adjust the target's width or height.
        if ($target_ratio > $source_ratio) {
          $this->adjustDimensionsToMatchSourceWidth($dimensions);
        }
        else {
          $this->adjustDimensionsToMatchSourceHeight($dimensions);
        }
      }
      // The second case is if just the image width is smaller than the target
      // width.
      elseif ($target_width > $dimensions['width']) {
        $this->adjustDimensionsToMatchSourceWidth($dimensions);
      }
      // And the final case is if just the image height is smaller than the
      // target height.
      elseif ($target_height > $dimensions['height']) {
        $this->adjustDimensionsToMatchSourceHeight($dimensions);
      }
      // The target height and width are less than the provided dimensions, so
      // pass to the parent class since the new image will have the exact
      // dimensions as defined for the effect.
      else {
        parent::transformDimensions($dimensions, $uri);
      }
    }
    else {
      // Upscaling is allowed, use the parent's implementation.
      parent::transformDimensions($dimensions, $uri);
    }
  }

}
