<?php

/**
 * @file
 * Post update functions for Focal Point.
 */

use Drupal\Core\Config\Entity\ConfigEntityUpdater;
use Drupal\focal_point\Plugin\ImageEffect\FocalPointScaleAndCropImageEffect;
use Drupal\image\ImageStyleInterface;

/**
 * Add upscale config option to focal point scale and crop image effects.
 */
function focal_point_post_update_upscale_scale_crop(array &$sandbox = NULL): void {
  \Drupal::classResolver(ConfigEntityUpdater::class)->update($sandbox, 'image_style', function (ImageStyleInterface $image_style): bool {
    $changed = FALSE;

    $effects = $image_style->getEffects();
    foreach ($effects as &$effect) {
      if ($effect instanceof FocalPointScaleAndCropImageEffect) {
        // Force re-save to add default upscale configuration.
        $changed = TRUE;
      }
    }

    return $changed;
  });
}
