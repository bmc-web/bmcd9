<?php

namespace Drupal\paragraphs_report;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Link;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\path_alias\AliasManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Class ParagraphsReport.
 *
 * Control various module logic like batches, lookups, and report output.
 *
 * @package Drupal\paragraphs_report
 */
class ParagraphsReport {

  use StringTranslationTrait;

  /**
   * The key used in the Drupal state api.
   */
  const STATE_NAME = 'paragraph_report.report_data';

  /**
   * Configuration settings.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Alias manager.
   *
   * @var \Drupal\path_alias\AliasManager
   */
  protected $aliasManager;

  /**
   * Class property to get config settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $paraSettings;

  /**
   * Class property to set config settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $paraEditSettings;

  /**
   * Pager class.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * Class property to get paragraph use counts.
   *
   * @var array
   */
  protected $paraCounts;

  /**
   * Existing report data.
   *
   * @var array
   */
  protected $paraReport;

  /**
   * All the paragraph types.
   *
   * @var array
   */
  protected $paraNames;

  /**
   * Request stack instance.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Account Proxy.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */

  protected $user;

  /**
   * Module Extension List Service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * Constructs the controller object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration settings.
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   *   Field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPath
   *   Current path.
   * @param \Drupal\path_alias\AliasManager $aliasManager
   *   Alias manager.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pagerManager
   *   Pager manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request Stack.
   * @param \Drupal\Core\State\StateInterface $state
   *   State API Container.
   * @param \Drupal\Core\Session\AccountProxy $currentUser
   *   The current user.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   The module extension list service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    EntityFieldManager $entityFieldManager,
    EntityTypeManagerInterface $entityTypeManager,
    CurrentPathStack $currentPath,
    AliasManager $aliasManager,
    PagerManagerInterface $pagerManager,
    RequestStack $requestStack,
    StateInterface $state,
    AccountProxy $currentUser,
    ModuleExtensionList $moduleExtensionList,
  ) {
    $this->configFactory = $configFactory;
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->currentPath = $currentPath;
    $this->aliasManager = $aliasManager;
    $this->pagerManager = $pagerManager;
    $this->moduleExtensionList = $moduleExtensionList;

    $this->paraSettings = $this->configFactory->get('paragraphs_report.settings');
    $this->paraEditSettings = $this->configFactory->getEditable('paragraphs_report.settings');
    $this->state = $state;
    $this->paraReport = $this->fetchReport();
    $this->paraNames = $this->getParaTypes();
    $this->paraCounts = $this->getParaUseCounts();
    $this->request = $requestStack;

    $this->user = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('path.current'),
      $container->get('path_alias.manager'),
      $container->get('pager.manager'),
      $container->get('request_stack'),
      $container->get('state'),
      $container->get('current_user'),
      $container->get('extension.list.module')
    );
  }

  /**
   * Setup batch array var.
   *
   * @param array $nids
   *   Node ids.
   *
   * @return array
   *   Batches ready to run
   */
  public function batchPrep(array $nids = []) {
    $totalRows = count($nids);
    $rowsPerBatch = $this->paraSettings->get('import_rows_per_batch') ?: 10;
    $batchesPerImport = ceil($totalRows / $rowsPerBatch);
    // Put x-amount of rows into operations array slots.
    $operations = [];
    for ($i = 0; $i < $batchesPerImport; $i++) {
      $offset = ($i == 0) ? 0 : $rowsPerBatch * $i;
      $batchNids = array_slice($nids, $offset, $rowsPerBatch);
      $operations[] = ['batch_get_para_fields', [$batchNids]];
    }
    // Full batch array.
    $batch = [
      'init_message' => $this->t('Executing a batch...'),
      'progress_message' => $this->t('Operation @current out of @total batches, @perBatch per batch.',
        ['@perBatch' => $rowsPerBatch]
      ),
      'progressive' => TRUE,
      'error_message' => $this->t('Batch failed.'),
      'operations' => $operations,
      'finished' => 'batch_save',
      'file' => $this->moduleExtensionList->getPath('paragraphs_report') . '/paragraphs_report.batch.inc',
    ];
    return $batch;
  }

  /**
   * Get paragraph fields from a bundle/type.
   *
   * @param string $bundle
   *   Entity bundle like 'node'.
   * @param string $type
   *   Entity type like 'page'.
   *
   * @return array
   *   Array of paragraph fields on a node.
   */
  public function getParaFieldsOnType($bundle = '', $type = '') {
    $paraFields = [];
    $fields = $this->entityFieldManager->getFieldDefinitions($bundle, $type);
    foreach ($fields as $field_name => $field_definition) {
      if (!empty($field_definition->getTargetBundle()) && $field_definition->getSetting('target_type') == 'paragraph') {
        $paraFields[] = $field_name;
      }
    }
    return $paraFields;
  }

  /**
   * Get paragraph fields for selected content types.
   *
   * @return array
   *   Paragraph fields by content type key
   */
  public function getParaFieldDefinitions() {
    // Loop through the fields for chosen content types to get paragraph fields.
    // Example content_type[] = field_name.
    $paraFields = [];
    foreach ($this->getTypes() as $contentType) {
      $fields = $this->entityFieldManager->getFieldDefinitions('node', $contentType);
      foreach ($fields as $field_name => $field_definition) {
        if (!empty($field_definition->getTargetBundle()) && $field_definition->getSetting('target_type') == 'paragraph') {
          $paraFields[$contentType][] = $field_name;
        }
      }
    }
    return $paraFields;
  }

  /**
   * Get list of content types chosen from settings.
   *
   * @return array
   *   Array of content types labels.
   */
  public function getTypes() {
    $data = $this->paraSettings->get('content_types') ?? [];
    return array_filter($data);
  }

  /**
   * Query db for nodes to check for paragraphs.
   *
   * @return array|int
   *   Nids to check for para fields or empty.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getNodes() {
    $contentTypes = array_filter($this->paraSettings->get('content_types'));
    if (!$contentTypes) {
      return [];
    }
    $query = $this->entityTypeManager->getStorage('node');
    $nids = $query->getQuery()
      ->accessCheck(TRUE)
      ->condition('type', $contentTypes, 'IN')
      ->execute();
    return $nids;
  }

  /**
   * Pass node id, return paragraphs report data as array.
   *
   * @param string $nid
   *   Node id.
   * @param array $current
   *   Paragraphs to append new ones to.
   *
   * @return array
   *   Array of paragraphs report data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getParasFromNid($nid = '', array $current = []) {
    // Pass any current array items into array.
    $arr = $current;
    /** @var \Drupal\Core\Entity\ContentEntityInterface $node */
    $node = $this->entityTypeManager->getStorage('node')->load($nid);
    // In some situations, load returns null when the node isn't available.
    if (empty($node)) {
      return $arr;
    }
    // Get and loop through first level paragraph fields on the node.
    $paraFields = $this->getParaFieldsOnType('node', $node->bundle());
    foreach ($paraFields as $paraField) {
      // Get paragraph values (target_ids).
      $paras = $node->get($paraField)->getValue();
      foreach ($paras as $para) {
        // Load paragraph from target_id.
        $p = Paragraph::load($para['target_id']);
        // Only add the paragraph if it has loaded successfully.
        if (!empty($p)) {
          $arr[$p->bundle()]['top'][] = $nid;
          // Check if the top level paragraph has sub-paragraph fields.
          $arr = $this->getParaSubFields($node, $p, $arr);
        }
      }
    }
    return $arr;
  }

  /**
   * Helper recursive method to find embedded paragraphs.
   *
   * Send a paragraph, check fields for sub-paragraph fields recursively.
   *
   * @param object $node
   *   Node to parse.
   * @param object $paragraph
   *   Paragraph object to parse.
   * @param array $reports
   *   Array to store parsed paragraph report data.
   *
   * @return array
   *   Paragraph values.
   */
  public function getParaSubFields($node, $paragraph, array $reports) {
    // Get fields on paragraph and check field type.
    $fields = $this->entityFieldManager->getFieldDefinitions('paragraph', $paragraph->bundle());
    foreach ($fields as $field_name => $field_definition) {
      // Check if this field a paragraph type.
      if (!empty($field_definition->getTargetBundle()) && $field_definition->getSetting('target_type') == 'paragraph') {
        // Get paragraphs on this field.
        $paras = $paragraph->get($field_name)->getValue();
        foreach ($paras as $para) {
          // If yes, add this field to report and check for more sub-fields.
          // Example arr[main component][parent] = alias of node.
          if ($p = Paragraph::load($para['target_id'])) {
            $reports[$p->bundle()][$paragraph->bundle()][] = $node->id();
            $reports = $this->getParaSubFields($node, $p, $reports);
          }
        }
      }
    }
    return $reports;
  }

  /**
   * Get a list of the paragraph components and return as lookup array.
   *
   * @return array
   *   Machine name => label.
   */
  public function getParaTypes() {
    $paras = paragraphs_type_get_types();
    $names = [];
    foreach ($paras as $machine => $obj) {
      $names[$machine] = $obj->label();
    }
    return $names;
  }

  /**
   * Pass array of path data to save for the report.
   *
   * @param array $arr
   *   Paragraph->parent->path data.
   */
  public function configSaveReport(array $arr = []) {
    $this->state->set(self::STATE_NAME, $arr);
  }

  /**
   * Remove a node path from report data.
   *
   * @param string $removeNid
   *   Remove from report data.
   *
   * @return array
   *   Updated encoded data.
   */
  public function configRemoveNode($removeNid = '') {
    $report = $this->fetchReport();
    // Search for nid and remove from array.
    // Remove item from array.
    $new = [];
    foreach ($report as $para => $sets) {
      foreach ($sets as $parent => $nids) {
        // Remove nid from array.
        $tmp = [];
        foreach ($nids as $nid) {
          if ($nid != $removeNid) {
            $tmp[] = $nid;
          }
        }
        $new[$para][$parent] = $tmp;
      }
    }
    // Save updated array.
    $this->configSaveReport($new);
    return $new;
  }

  /**
   * Check that node meets conditions to use in report data.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object to check.
   *
   * @return bool
   *   True or false if content type of entity should be watched.
   */
  public function checkWatch(EntityInterface $entity) {
    if ($this->paraSettings->get('watch_content')
      && in_array($entity->bundle(), $this->getTypes())) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Add report data from new node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Node to check.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function insertParagraphs(EntityInterface $entity) {
    if ($this->checkWatch($entity)) {
      // Send node to get parsed for paragraph fields/sub-fields.
      $updated = $this->getParasFromNid($entity->id(), $this->fetchReport());
      $this->configSaveReport($updated);
    }
  }

  /**
   * Update report data with paragraph changes in node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Node to check.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function updateParagraphs(EntityInterface $entity) {
    if ($this->checkWatch($entity)) {
      // Send node to get parsed for paragraph fields/sub-fields.
      $report = $this->configRemoveNode($entity->id());
      $updated = $this->getParasFromNid($entity->id(), $report);
      $this->configSaveReport($updated);
    }
  }

  /**
   * Remove deleted node path from report data.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Node to check.
   */
  public function deleteParagraphs(EntityInterface $entity) {
    if ($this->checkWatch($entity)) {
      $report = $this->configRemoveNode($entity->id());
      $this->configSaveReport($report);
    }
  }

  /**
   * Build quick paragraphs type drop down form.
   *
   * @return string
   *   HTML used for select form element.
   */
  public function filterForm() {
    // Build filter form.
    // Check and set filters.
    $hideParas = $this->paraSettings->get('hide_paras') ?? [];
    $paraCounts = $this->paraCounts;
    $current_path = $this->currentPath->getPath();
    $filterForm = '<form method="get" action="' . $current_path . '">';
    // Filter.
    $filterForm .= 'Filter by Type: <select name="ptype">';
    $filterForm .= '<option value="">All</option>';
    foreach ($this->paraNames as $machine => $label) {
      // Skip any paras marked to hide from report.
      if (isset($hideParas[$machine]) && empty($hideParas[$machine])) {
        $count = isset($paraCounts[$machine]) ? ' (' . $paraCounts[$machine] . ')' : '';
        $selected = isset($_GET['ptype']) && $_GET['ptype'] == $machine ? ' selected' : '';
        $filterForm .= '<option name="' . $machine . '" value="' . $machine . '"' . $selected . '>' . $label . $count . '</option>';
      }
    }
    $filterForm .= '</select> <input type="submit" value="Go"></form><br>';
    return $filterForm;
  }

  /**
   * Format the stored config into a rendered table.
   *
   * @return array
   *   Table render array.
   */
  public function formatTable() {
    $tabular = $this->getTabularData();
    // Setup pager.
    $per_page = 10;
    $current_page = $this->pagerManager
      ->createPager($tabular['total'], $per_page)
      ->getCurrentPage();
    // Split array into page sized chunks, if not empty.
    $chunks = !empty($tabular['rows']) ? array_chunk($tabular['rows'], $per_page, TRUE) : [];
    // Table output.
    $table['table'] = [
      '#type' => 'table',
      '#title' => $this->t('Paragraphs Report'),
      '#header' => $tabular['header'],
      '#sticky' => TRUE,
      '#rows' => $chunks[$current_page] ?? [],
      '#empty' => $this->t('No components found. <br /> You may need an Admin user to enable content types on the "Settings" tab and click the "Update Report Data" button.'),
    ];
    $table['pager'] = [
      '#type' => 'pager',
    ];
    return $table;
  }

  /**
   * Return a rendered table ready for output.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   Table markup returned.
   */
  public function showReport() {
    $table = $this->formatTable();
    $btn['export_button'] = [
      '#type' => 'markup',
      '#markup' => Markup::create('<div style="float:right; margin-right: 20px;">' . $this->getExportLink()
          ->toString() . '</div>'),
    ];
    // Only show this button to users w/proper access or super user.
    if ($this->user->hasPermission('update report data')) {
      $confirm_message = $this->t('Update the report data with current node info?');
      $button_label = $this->t('Update Report Data');
      $btn['run_button'] = [
        '#type' => 'markup',
        '#markup' => Markup::create('<div style="float:right">
          <a class="button" href="/admin/reports/paragraphs-report/update" onclick="return confirm(\'' . $confirm_message . '\')">' . $button_label . '</a>
        </div>'),
      ];
    }
    $filters = [];
    $filters['filter'] = [
      '#type' => 'markup',
      '#markup' => Markup::create($this->filterForm()),
      '#allowed_tags' => array_merge(Xss::getHtmlTagList(),
        ['form', 'option', 'select', 'input', 'br']),
    ];
    return [
      $btn,
      $filters,
      $table,
    ];
  }

  /**
   * Generate the export link.
   *
   * @return \Drupal\Core\Link
   *   Returns a Link object for the export.
   */
  protected function getExportLink($include_filters = TRUE) {
    $attributes = ['class' => ['button']];
    $params = [];
    if ($include_filters) {
      $params = $this->request->getCurrentRequest()->query->all();
    }
    $url = Url::fromRoute('paragraphs_report.export', $params, ['attributes' => $attributes]);
    return Link::fromTextAndUrl('Export to CSV', $url);
  }

  /**
   * Fetches the stored report data.
   *
   * @return array
   *   Returns an array representing the report.
   */
  protected function fetchReport() {
    $state_data = $this->state->get(self::STATE_NAME, []);

    // Force type to be array.
    return !empty($state_data) && is_array($state_data) ? $state_data : [];
  }

  /**
   * Parses the report data into tabular data.
   *
   * @return array
   *   Returns an array of tabular data.
   */
  protected function getTabularData($show_links = TRUE) {
    $filter = isset($_GET['ptype']) ? trim($_GET['ptype']) : '';

    // Get paragraphs label info, translate machine name to label.
    // Loop results into the table.
    $total = 0;
    $rows = [];

    if (!empty($this->paraReport)) {
      foreach ($this->paraReport as $name => $set) {
        // Skip if we are filtering out all but one.
        if (!empty($filter) && $filter != $name) {
          continue;
        }
        // Be mindful of the parent field.
        foreach ($set as $parent => $nids) {
          // Turn duplicates into counts.
          if (!empty($nids)) {
            $counts = array_count_values($nids);
            foreach ($counts as $nid => $count) {
              $node = Node::load($nid);
              $content_type_id = $node->getType();
              $contentType = $this->entityTypeManager->getStorage('node_type')
                ->load($content_type_id);
              $content_type_label = $contentType->label();
              $alias = $this->aliasManager->getAliasByPath('/node/' . $nid);
              $link = $this->t('<a href="@alias">@alias</a>', ['@alias' => $alias]);
              $label = $this->paraNames[$name];
              $rows[] = [
                $label,
                $parent,
                $nid,
                $content_type_label,
                ($show_links ? $link : $alias),
                $count,
              ];
              $total++;
            }
          }
        }
      }
    }

    $header = [
      $this->t('Paragraph'),
      $this->t('Parent'),
      $this->t('Nid'),
      $this->t('Content Type'),
      $this->t('Path'),
      $this->t('Count'),
    ];

    return compact('header', 'rows', 'total');
  }

  /**
   * Parses the report data to get counts of paragraph usage.
   *
   * @param string $filter
   *   Return all by default, or only unused paras.
   *
   * @return array
   *   Returns an array of paragraphs and use counts.
   */
  public function getParaUseCounts($filter = 'all') {
    $paraCounts = [];
    if (!empty($this->paraReport)) {
      foreach ($this->paraReport as $name => $set) {
        // Be mindful of the parent field.
        foreach ($set as $nids) {
          if (isset($paraCounts[$name])) {
            // Increment existing.
            $paraCounts[$name] += !empty($nids) ? count($nids) : 0;
          }
          else {
            // Set initial count.
            $paraCounts[$name] = !empty($nids) ? count($nids) : 0;
          }
        }
      }
    }
    // Get array keys/values.
    $allKeys = array_keys($this->paraNames);
    $currentKeys = array_keys($paraCounts);
    // Figure out what isn't in current keys.
    $missingKeys = array_diff($allKeys, $currentKeys);
    // Loop missing keys and append with empty array count.
    foreach ($missingKeys as $missingKey) {
      $paraCounts[$missingKey] = 0;
    }
    return ($filter === 'empty') ? $missingKeys : $paraCounts;
  }

  /**
   * Returns a CSV file with rendered data.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Returns a CSV file for download.
   */
  public function exportReport() {
    // Package up the data into rows for the CSV.
    $export_data = $this->getTabularData(FALSE);
    // Serve the CSV file.
    return $this->serveExportFile($export_data);
  }

  /**
   * Generates the file and returns the response.
   *
   * @param array $data
   *   A multidimensional array containing the CSV data.
   * @param string $filename
   *   The name of the file to create. Will auto-generate a filename if none is
   *   provided.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Returns a Response object formatted for the CSV.
   */
  protected function serveExportFile($data, $filename = NULL) {
    if (empty($filename) || !is_string($filename)) {
      $filename = 'paragraphs-report-export--' . date('Y-M-d-H-i-s') . '.csv';
    }

    $formatted_data = $this->formatExportData($data);
    if (empty($formatted_data)) {
      // @todo Add in some error handling and possibly watchdog warnings.
      $formatted_data = '';
    }

    // Generate the CSV response to serve up to the browser.
    $response = new Response();
    $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename);
    $response->headers->set('Content-Disposition', $disposition);
    $response->headers->set('Content-Type', 'text/csv');
    $response->headers->set('Expires', 0);
    $response->headers->set('Content-Transfer-Encoding', 'binary');
    $response->setContent($formatted_data);

    return $response;
  }

  /**
   * Convert a multidimensional array into a string for CSV export purposes.
   *
   * @param array $raw
   *   A multidimensional array containing the CSV data.
   *
   * @return string|bool
   *   Returns the data compacted into a single string for CSV export purposes.
   *   If the raw data was not an array or empty, returns FALSE.
   */
  protected function formatExportData($raw) {
    if (!is_array($raw) || empty($raw)) {
      return FALSE;
    }

    $data = [];
    if (!empty($raw['header'])) {
      $data[] = '"' . implode('","', $raw['header']) . '"';
    }

    foreach ($raw['rows'] as $row) {
      if (is_array($row)) {
        $data[] = '"' . implode('","', $row) . '"';
      }
    }

    return implode("\n", $data);
  }

}
