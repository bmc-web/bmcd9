# Guzzle Cache for Drupal

Provides a Drupal cache backend for
[Kevinrob/guzzle-cache-middleware](https://github.com/Kevinrob/guzzle-cache-middleware).

## Automatic caching for all HTTP requests

To enable the caching layer for all HTTP requests automatically, enable the `guzzle_cache_middleware` sub-module.
