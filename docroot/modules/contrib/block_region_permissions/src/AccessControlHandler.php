<?php

namespace Drupal\block_region_permissions;

use Drupal\block\Entity\Block;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for the block region permissions.
 */
class AccessControlHandler implements ContainerInjectionInterface {

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('theme_handler')
    );
  }

  /**
   * AccessControlHandler constructor.
   *
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler.
   */
  public function __construct(ThemeHandlerInterface $theme_handler) {
    $this->themeHandler = $theme_handler;
  }

  /**
   * Returns the service container.
   *
   * This method is marked private to prevent sub-classes from retrieving
   * services from the container through it. Instead,
   * \Drupal\Core\DependencyInjection\ContainerInjectionInterface should be used
   * for injecting services.
   *
   * @return \Symfony\Component\DependencyInjection\ContainerInterface
   *   The service container.
   */
  private function container() {
    return \Drupal::getContainer();
  }

  /**
   * Returns the current user.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   The current user.
   */
  protected function currentUser() {
    if (!$this->currentUser) {
      $this->currentUser = $this->container()->get('current_user');
    }
    return $this->currentUser;
  }

  /**
   * Access check for the block edit and delete forms.
   *
   * @param \Drupal\block\Entity\Block $block
   *   The block.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   An access result.
   */
  public function blockFormAccess(Block $block) {
    $account = $this->currentUser();
    $theme = $block->get('theme');
    $region = $block->get('region');
    // Check if the user has the proper permissions.
    $access = AccessResult::allowedIfHasPermission($account, "administer $theme $region");

    return $access;
  }

  /**
   * Access check for block library page.
   *
   * @param string $theme
   *   Theme name.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   An access result.
   */
  public function blockAdminLibraryAccess($theme) {
    if ($this->themeHandler->themeExists($theme)) {
      $theme_object = $this->themeHandler->getTheme($theme);
      // Get regions for this theme.
      foreach ($theme_object->info['regions'] as $region => $label) {
        $access = AccessResult::allowedIfHasPermission($this->currentUser(), "administer $theme $region");
        if ($access->isAllowed()) {
          // Allow access if user can administer at least one region in a theme.
          return $access;
        }
      }
    }

    return AccessResult::neutral();
  }

}
