<?php

namespace Drupal\Tests\file_to_media\Functional;

use Drupal\field\FieldConfigInterface;
use Drupal\file\Entity\File;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\views\Entity\View;

/**
 * Defines a class for testing file to media.
 *
 * @group file_to_media
 */
class FileToMediaTest extends BrowserTestBase {

  use MediaTypeCreationTrait;
  use TestFileCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'views',
    'file_to_media',
    'media',
  ];

  /**
   * Tests file to media creation.
   */
  public function testFileToMedia(): void {
    $view = View::load('files');
    $displays = $view->get('display');
    // Add in the file_to_media field.
    $displays['default']['display_options']['fields']['file_to_media'] = [
      'id' => 'file_to_media',
      'table' => 'file_managed',
      'field' => 'file_to_media',
      'relationship' => 'none',
      'group_type' => 'group',
      'admin_label' => '',
      'label' => 'Create media',
      'exclude' => FALSE,
      'alter' => [
        'alter_text' => FALSE,
        'text' => '',
        'make_link' => FALSE,
        'path' => '',
        'absolute' => FALSE,
        'external' => FALSE,
        'replace_spaces' => FALSE,
        'path_case' => 'none',
        'trim_whitespace' => FALSE,
        'alt' => '',
        'rel' => '',
        'link_class' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'nl2br' => FALSE,
        'max_length' => '0',
        'word_boundary' => TRUE,
        'ellipsis' => TRUE,
        'more_link' => FALSE,
        'more_link_text' => '',
        'more_link_path' => '',
        'strip_tags' => FALSE,
        'trim' => FALSE,
        'preserve_tags' => '',
        'html' => FALSE,
      ],
      'element_type' => '',
      'element_class' => '',
      'element_label_type' => '',
      'element_label_class' => '',
      'element_label_colon' => TRUE,
      'element_wrapper_type' => '',
      'element_wrapper_class' => '',
      'element_default_classes' => TRUE,
      'empty' => '',
      'hide_empty' => FALSE,
      'empty_zero' => FALSE,
      'hide_alter_empty' => TRUE,
      'entity_type' => 'file',
      'plugin_id' => 'file_to_media',
    ];
    $view->set('display', $displays);
    $view->save();
    $image_type = $this->createMediaType('image');
    $oembed_type = $this->createMediaType('oembed:video');

    $image = $this->getTestFiles('image')[0];
    $file = File::create([
      'uri' => $image->uri,
      'status' => 1,
    ]);
    $file->save();

    $assert = $this->assertSession();

    // Check user needs 'access file overview' permission.
    $this->drupalGet('/file/to-media/' . $file->id() . '/' . $image_type->id());
    $assert->statusCodeEquals(403);
    $this->drupalLogin($this->createUser(['access files overview']));

    // Check user needs 'create image type' permission.
    $this->drupalGet('/file/to-media/' . $file->id() . '/' . $image_type->id());
    $assert->statusCodeEquals(403);
    $this->drupalLogin($this->createUser([
      'access files overview',
      sprintf('create %s media', $image_type->id()),
      'access content',
      sprintf('create %s media', $oembed_type->id()),
    ]));
    $this->drupalGet('/file/to-media/' . $file->id() . '/' . $image_type->id());
    $assert->statusCodeEquals(200);

    // Check media-type must support file fields.
    $this->drupalGet('/file/to-media/' . $file->id() . '/' . $oembed_type->id());
    $assert->statusCodeEquals(404);

    // Check media-type file field must have matching extension.
    $field = $image_type->getSource()->getSourceFieldDefinition($image_type);
    $this->assertInstanceOf(FieldConfigInterface::class, $field);
    $extensions = $field->getSetting('file_extensions');
    $field->setSetting('file_extensions', 'txt');
    $field->save();
    $this->drupalGet('/file/to-media/' . $file->id() . '/' . $image_type->id());
    $assert->statusCodeEquals(404);

    // Reset field file extensions.
    $field->setSetting('file_extensions', $extensions);
    $field->save();

    // Check view.
    $this->drupalGet('admin/content/files');
    $assert->linkByHrefExists('/file/to-media/' . $file->id() . '/' . $image_type->id());
    $link_title = sprintf('Create %s', $image_type->label());
    $assert->linkExists($link_title);

    $this->clickLink($link_title);
    $this->submitForm([
      'field_media_image[0][alt]' => $this->randomMachineName(),
    ], 'Save');

    $media = \Drupal::entityTypeManager()->getStorage('media')
      ->loadByProperties([
        'field_media_image.target_id' => $file->id(),
      ]);
    $this->assertNotEmpty($media);

    // Check that once there is a usage, the link is no longer present.
    $this->drupalGet('admin/content/files');
    $assert->linkNotExists($link_title);
  }

}
