# ABOUT

_Custom Theme_ is a subtheme of the Drupal core theme
[Classy](https://www.drupal.org/docs/8/core/themes/classy-theme) which provides
a starting point for iFactory drupal projects.

## ABOUT DRUPAL THEMING

See [https://www.drupal.org/docs/8/theming](https://www.drupal.org/docs/8/theming)
for more information on Drupal theming.

## Libraries

_Custom Theme_ makes use of libraries in bmc_d9.libraries.yml to pull in
Javascript only in places where it is required on the site.

CSS is compiled into a single file and does not need to be broken out into
individual libraries unless there is a specific use case to do so.

## Node/Gulp Setup

[Node.js](https://nodejs.org) is required for development of this theme.

_Custom Theme_ uses [Gulp.js](https://gulpjs.com/) to automate the build
workflow.

### Configuration

1. Install all required modules (run from the root of the project):
`npm install`

2. Copy `default.gulpconf.js` to `gulpconf.js` and update for your local
environment and preferred settings. All custom Gulp configuration should
live inside of this file.

### Default tasks

By default Gulp comes configured with the following tasks:

- **images**
  - Minimize and optimize theme images
- **css**
  - Pre-process CSS for development
- **js**
  - Pre-process JS for development
- **watch**
  - Run build tasks for development including BrowserSync
- **default**
  - Runs watch task

### Javascript setup

Our js on this site can be divided into two categories: global js and component-specific js

global js is handled in file(s) in the global js folder.

any js that can be applied to every page should be added to this folder, and the compiled output can be added to bmc_d9.libraries globally to the theme.

component-specific js is in the all other folders
those js files are included in the bmc_d9.libraries file as libraries. You can add a new library for each component following the format for the accordion library
These libraries are then added to template files so that they only get loaded on pages that use that component. See templates/paragraphs/paragraph--acc.html.twig for an example

### Bulletin "Theme"

The bulletin section uses unique styles and a separate visual design from the rest of the site. As such, we've done a CSS and JS reset by removing the main library from the site, and adding in a new bulletin library. there's a bulletin.scss file that works the same as main.scss for including all styles for the bulletin theme. There is also a separate mixins, variables, and fonts files (including icon fonts) that were grabbed directly from the d7 site server to use as a base. Any additional variables and mixins needed can be added to those files.

