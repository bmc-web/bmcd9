/**
 * 2022-04-18 Michael Harris
 * Chosen poses accessibility barriers, so we shouldn't use it for anon-facing forms
 * Leaving this file as a reference, however.
 */

// const bulletinExploreFilters = $ => {
//   /**
//    *.
//    *
//    * @type {Drupal~behavior}
//    *
//    * @prop {Drupal~behaviorAttach} attach
//    *
//    */
//   Drupal.behaviors.bulletinExploreFilters = {
//     attach(context) {
//       $(document).ajaxComplete(function(){
//         $('.view-filters select')
//           .not('.chosen-processed')
//           .chosen({ width: '200px' });
//       });
//     },
//   };
// };
//
// bulletinExploreFilters(jQuery);
