"use strict";

var campaignScroll = function campaignScroll($) {
  function campaignScrollVisibility() {
    if ($(window).scrollTop() > 100) {
      $('.campaign-banner').addClass('hide');
      $('#block-defy-expectation-campaign-banner .campaign-banner__cta').focus(function () {
        $('.campaign-banner').removeClass('hide');
      });
    } else {
      $('.campaign-banner').removeClass('hide');
    }
  }
  /**
   *
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */


  Drupal.behaviors.campaignScroll = {
    attach: function attach(context) {
      if ($('home__carousel')) {
        $('#block-defy-expectation-campaign-banner').insertAfter('.home__carousel');
      }

      $(window, context).on('scroll', campaignScrollVisibility);
    }
  };
};

campaignScroll(jQuery);