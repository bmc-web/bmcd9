"use strict";

var shareThisStickScroll = function shareThisStickScroll($) {
  function shareThisStickScrollVisibility() {
    if ($(window).scrollTop() > 100 && $(window).width() > 699) {
      $('#st-1.sharethis--bulletin').addClass('stuck');
    } else {
      $('#st-1.sharethis--bulletin').removeClass('stuck');
    }
  }
  /**
     *
     *
     * @type {Drupal~behavior}
     *
     * @prop {Drupal~behaviorAttach} attach
     *
     */


  Drupal.behaviors.campaignScroll = {
    attach: function attach(context) {
      $(window, context).on('scroll', shareThisStickScrollVisibility);
    }
  };
};

shareThisStickScroll(jQuery);