"use strict";

var filterStayFocus = function filterStayFocus($) {
  var input = null;
  var viewname = null;
  Drupal.behaviors.filterStayFocus = {
    attach: function attach(context) {
      $('input, select').change(function () {
        input = $(this).attr('data-drupal-selector');
      });
      viewname = $('.filter-focus-view-block').attr('data-viewname');
      $('.filter-focus-view-block .pagerer-container a').click(function () {
        input = $(this).attr('data-filterfocus-target');
      });
      $(document).on('ajaxComplete', function (event, xhr, settings) {
        setTimeout(function () {
          if (input === 'edit-date') {
            $('#liveRegion2').attr('tabindex', '-1').focus();
          } else if (input === 'pager') {
            if (viewname === 'event-listing') {
              $('.filter-focus-view-block .view-header').attr('tabindex', '-1').focus();
            } else if (viewname === 'event-tag-listing') {
              $('.views-row:first-of-type h3').attr('tabindex', '-1').focus();
            } else if (viewname === 'admissions-officer-listing') {
              $('.filter-focus-view-block .view-header').attr('tabindex', '-1').focus();
            } else if (viewname === 'directory-listing') {
              $('.filter-focus-view-block .view-header').attr('tabindex', '-1').focus();
            } // put more or conditions here

          } else {
            $("input[data-drupal-selector=\"".concat(input, "\"], select[data-drupal-selector=\"").concat(input, "\"]")).focus();
          }
        }, 500);
      });
    }
  };
};

filterStayFocus(jQuery);