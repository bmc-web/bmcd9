"use strict";

var search = function search($) {
  Drupal.behaviors.search = {
    attach: function attach(context) {
      var searchMapping = [{
        path: 'gsas',
        tabText: 'gsas'
      }, {
        path: 'inside',
        tabText: 'inside bryn mawr'
      }, {
        path: 'socialwork',
        tabText: 'socialwork'
      }, {
        path: 'postbac',
        tabText: 'postbac'
      }, {
        path: 'bulletin',
        tabText: 'bulletin'
      }, {
        path: 'search',
        tabText: 'all'
      }];
      var searchBlock = document.getElementById('google-cse-results');
      var siteSection = window.location.pathname.split('/');
      var path = siteSection[1];
      var tabTextIndex = searchMapping.findIndex(function (a) {
        return a.path === path;
      });
      var tabTextToClick = searchMapping[tabTextIndex].tabText;
      var mutations = {
        childList: true
      };

      var addClasses = function addClasses() {
        setTimeout(function () {
          var $tabs = $('.gsc-refinementHeader');
          $('.gsc-resultsbox-visible').attr('role', 'tablist');
          $tabs.each(function () {
            $(this).attr({
              id: "tab-".concat($(this).index() / 2)
            });
          });
          $tabs.attr('aria-label', null);
          $(once('bmc-gsc-refinementHeader', '.gsc-refinementHeader', context)).on('keydown', function (e) {
            if (e.key === 'ArrowRight') {
              var nextTab = $(this).next().next();
              var nextNextTab = nextTab.next().next();

              if (nextTab.attr('tabindex') === '0') {
                nextTab.focus();
              } else if (nextNextTab.attr('tabindex') === '0') {
                nextNextTab.focus();
              }
            }

            if (e.key === 'ArrowLeft') {
              var prevTab = $(this).prev().prev();
              var prevPrevTab = prevTab.prev().prev();

              if (prevTab.attr('tabindex') === '0') {
                prevTab.focus();
              } else if (prevPrevTab.attr('tabindex') === '0') {
                prevPrevTab.focus();
              }
            }
          });
          $tabs.attr('aria-selected', 'false');
          $('.gsc-refinementhActive').attr('aria-selected', 'true');
          $('.gsc-result').each(function () {
            $(this).attr({
              role: 'tabpanel',
              tabindex: '0',
              'aria-labelledby': "tab-".concat($(this).index())
            });
          });

          if ($tabs.length) {
            $tabs.each(function () {
              var textFormatted = $(this).text().trim().toLowerCase();
              $(this).attr('data-path', textFormatted);
            });
            $("[data-path=\"".concat(tabTextToClick, "\"]")).trigger('click');
            $('.gsc-cursor').attr({
              role: 'Navigation',
              'aria-label': 'Pagination'
            });
            $('.gsc-cursor-current-page').attr('aria-current', 'true');

            if (!$('.gsc-cursor-current-page span').length > 0) {
              $('.gsc-cursor-current-page').prepend('<span class="visually-hidden">Current Page</span>');
            }
          } // Change result title tags from div to H2.


          var $resultHeader = $('.gsc-thumbnail-inside div.gs-title');

          if ($resultHeader) {
            $resultHeader.replaceWith(function () {
              return "<h2 class=\"gs-title\">".concat($(this).html(), "</h2>");
            });
          } // Add role to filter link


          var $filterLink = $('.gs-per-result-labels .gs-label');

          if ($filterLink) {
            $filterLink.attr('role', 'button');
          }
        }, 500);
      };

      var observer = new MutationObserver(addClasses);
      observer.observe(searchBlock, mutations);
      $('#edit-sa').val('submit');
    }
  };
};

search(jQuery);