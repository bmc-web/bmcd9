"use strict";

(function ($) {
  var TreeMenu = Vue.component('TreeMenu', {
    template: "<ul class=\"menu\">\n    <li :class=\"{active: item.isActive}\" v-for=\"item in nodes\">\n        <a :aria-current=\"item.isCurrent ? page : false\" :href=\"item.absolute\">{{ item.title }} </a>\n        <button v-on:click=\"showNewLevel(item)\" class=\"menu--section__button\" v-if=\"item.below\">\n          <span class=\"visually-hidden\">toggle submenu</span>\n        </button>\n        <transition name=\"fade\">\n          <TreeMenu v-if=\"item.below && item.isActive\" :nodes=\"item.below\"></TreeMenu>\n        </transition>\n      </li>\n\n     \n    </ul>",
    props: ['nodes'],
    methods: {
      showNewLevel: function showNewLevel(item) {
        item.isActive = !item.isActive;
      }
    }
  });
  var app = new Vue({
    el: '#marketing-grad-menu',
    data: function data() {
      return {
        items: [],
        parentStored: [],
        active: null,
        current: null,
        found: false,
        breadcrumbText: []
      };
    },
    template: "\n      <div class=\"section-navigation\">\n        <span class=\"active-link-display\"></span>\n        <div class=\"section-nav__panel\">\n            <button :disabled=\"allCollapsed\" v-on:click=\"collapse()\" class=\"section-nav__collapse\">Collapse</button>\n            <TreeMenu :nodes=\"items\"></TreeMenu>\n        </div>\n      </div>\n    ",
    created: function created() {
      var _this2 = this;

      var _this = this;

      var activeURL = encodeURI(window.location.href);
      var menuName = $('nav[data-section-name]').data('section-name');
      setTimeout(function () {
        $('.active-link-display').text($('.header__menu--theme-switcher .menu a.is-active').text());
      }, 500);
      $('.breadcrumb li').each(function () {
        _this.breadcrumbText.push($(this).text().trim());
      });

      function recursiveEdit(items) {
        items.forEach(function (a) {
          a.isActive = false;

          if (activeURL.indexOf(a.absolute) != -1) {
            a.isActive = true;
          }

          if (a.absolute === activeURL) {
            a.isCurrent = true;
            a.isActive = false;
          } else {
            a.isCurrent = false;
          }

          if (a.below) {
            recursiveEdit(a.below);
          }
        });
      }

      $.getJSON('/api/menu_items/' + menuName, function (json) {
        // this.findCurrentChild(activeURL, json);
        recursiveEdit(json);
        _this2.items = json;
      });
    },
    computed: {
      allCollapsed: function allCollapsed() {
        return this.items.every(function (a) {
          return a.isActive === false;
        });
      }
    },
    methods: {
      collapse: function collapse() {
        function recursiveEdit(items) {
          items.forEach(function (a) {
            a.isActive = false;

            if (a.below) {
              recursiveEdit(a.below);
            }
          });
        }

        recursiveEdit(this.items);
      }
    }
  });
})(jQuery);