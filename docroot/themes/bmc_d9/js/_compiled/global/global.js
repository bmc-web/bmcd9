"use strict";

/**
 * @file
 * Global interactions and functinoality.
 */

/**
 * @file
 * Provides interactions and accessibility.
 */
var header = function header($) {
  function cutOffMenuitems() {
    var $liWidth = 0;
    $('.header__menu--top .menu-item').each(function () {
      $liWidth += $(this).width() + 30;

      if ($liWidth > $('.header__menu--top').width()) {
        $(this).hide();
      } else {
        $(this).show();
      }
    });
  }

  function toggleAccessibility() {
    var $parent = $(this).parent();
    var $this = $(this);
    $this.attr({
      'aria-expanded': false
    });
    $parent.hover(function setHover() {
      if ($(window).width() >= 1024) {
        $parent.addClass('focused');
        $parent.siblings().removeClass('focused');
        $this.attr('aria-expanded', 'true');
        $parent.removeClass('remove-delay');
      }
    }, function setHoverOff() {
      if ($(window).width() >= 1024) {
        $parent.removeClass('focused');
        $parent.siblings().removeClass('focused');
        $this.attr('aria-expanded', 'false');
        $parent.removeClass('remove-delay');
      }
    });
    $(this).on('click', function (e) {
      $parent.toggleClass('focused');
      $parent.siblings().removeClass('focused');

      if ($parent.hasClass('focused')) {
        $this.attr('aria-expanded', 'true');
        $parent.removeClass('remove-delay');
      } else {
        $this.attr('aria-expanded', 'false');
        $parent.addClass('remove-delay');
      }

      e.preventDefault();
    });
    $(document).on('keydown', function (e) {
      if (e.which === 27) {
        var $active = $('.focused');
        $parent.removeClass('remove-delay');
        $parent.removeClass('focused');
        $this.attr('aria-expanded', 'false');
        $active.find($this).focus();
      }
    });
    $parent.focusout(function () {
      if ($(window).width() > 1024) {
        $parent.removeClass('focused').addClass('remove-delay');
        $this.attr('aria-expanded', 'false');
      }
    });
    $this.next().focusin(function () {
      if ($(window).width() > 1024) {
        $parent.addClass('focused');
        $parent.removeClass('remove-delay');
        $this.attr('aria-expanded', 'true');
      }
    });
  }

  function toggleSubMenu() {
    var $submenu = $(this).next();
    $submenu.slideToggle();
    $(this).toggleClass('expanded');

    if ($(this).hasClass('expanded')) {
      $(this).attr('aria-expanded', 'true');
    } else {
      $(this).attr('aria-expanded', 'false');
    }
  }

  function setScrollBarWidth() {
    var $outer = $('<div>').css({
      visibility: 'hidden',
      width: 100,
      overflow: 'scroll'
    }).appendTo('body');
    var widthWithScroll = $('<div>').css({
      width: '100%'
    }).appendTo($outer).outerWidth();
    $outer.remove();
    $('body').css('--scrollbar-width', "".concat(100 - widthWithScroll, "px"));
  }

  ;

  function toggleInsideSubMenu() {
    var $submenu = $(this).next();
    var $parent = $(this).parent();
    var $this = $(this);

    if ($(window).width() < 1023) {
      $submenu.slideToggle();
      $(this).toggleClass('expanded');

      if ($(this).hasClass('expanded')) {
        $(this).attr('aria-expanded', 'true');
      } else {
        $(this).attr('aria-expanded', 'false');
      }
    } else {
      $(document).on('keydown', function (e) {
        if ($(document.activeElement).closest($parent).length) {
          if (e.which === 27) {
            $parent.removeClass('remove-delay');
            $parent.removeClass('focused');
            $this.attr('aria-expanded', 'false');
            $this.focus();
          }
        }
      });
      $parent.toggleClass('focused');
      $(this).toggleClass('expanded');

      if ($(this).hasClass('expanded')) {
        $(this).attr('aria-expanded', 'true');
      } else {
        $(this).attr('aria-expanded', 'false');
      }

      $parent.focusout(function (e) {
        if ($(e.relatedTarget).closest('.menu-item--expanded').index() !== $parent.index()) {
          $parent.removeClass('focused').addClass('remove-delay');
          $this.attr('aria-expanded', 'false');
        }
      });
    }
  }

  function toggleMenu() {
    var $this = $(this);
    var $offsetAlertHeight = $('.alert:visible').outerHeight() || 0;
    $('body').toggleClass('open-menu');

    if ($('body').hasClass('open-menu')) {
      $this.attr('aria-expanded', 'true');
      $('main, footer').attr('aria-hidden', 'true');
      $('.header-container__menu').css('margin-top', $('header .block-bmc-custom').offset().top - $(window).scrollTop());
    } else {
      $this.attr('aria-expanded', 'false');
      $('main, footer').attr('aria-hidden', 'false');
    }

    $(document).on('keydown click', function (e) {
      if ($('body').hasClass('open-menu')) {
        if (e.which === 27 || e.type === 'click') {
          $('body').removeClass('open-menu');
          $this.attr('aria-expanded', 'false');
          $('main, footer').attr('aria-hidden', 'false');
          $this.focus();
        }
      }
    });
    $('header').on('click', function (e) {
      e.stopPropagation();
    });
  }

  function tabloopForwards(e) {
    if ($('body').hasClass('open-menu')) {
      if ((e.keyCode || e.which) === 9 && !e.shiftKey) {
        e.preventDefault();
        e.stopPropagation();
        var $elToFocusOn = $('.header-container__nav-button');
        $elToFocusOn.focus();
      }
    }
  }

  function tabloopReverse(e) {
    if ($('body').hasClass('open-menu')) {
      if ((e.keyCode || e.which) === 9 && e.shiftKey) {
        e.preventDefault();
        e.stopPropagation();
        $('.header-container__search input').focus();
      }
    }
  }

  function toggleAccessibilityInside() {
    var $parent = $(this).parent();
    var $this = $(this);
    $parent.hover(function setHover() {
      $parent.addClass('focused');
      $parent.siblings().removeClass('focused');
      $parent.removeClass('remove-delay');
    }, function setHoverOff() {
      $parent.removeClass('focused');
      $parent.siblings().removeClass('focused');
      $parent.removeClass('remove-delay');
    });
    $(document).on('keydown', function (e) {
      if (e.which === 27) {
        var $active = $('.focused');
        $parent.removeClass('remove-delay');
        $parent.removeClass('focused');
      }
    });
  }
  /**
   *
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */


  Drupal.behaviors.header = {
    attach: function attach(context) {
      $(window, context).on('load', setScrollBarWidth);
      $(once('bmc-theme-switcher-button', '.toggle-theme--switcher', context)).on('click', toggleSubMenu);
      $(window, context).on('load resize', cutOffMenuitems);
      $(once('bmc-header-container-menu-toggleaccessibility', '.header-container__menu--left .accordion__toggle', context)).each(toggleAccessibility);
      $('.menu--inside .menu-item--expanded a').each(toggleAccessibilityInside);
      $(once('bmc-menu-inside-button', '.menu--inside__button', context)).on('click', toggleInsideSubMenu);
      $(once('bmc-header-container-nav-button', '.header-container__nav-button', context)).on('click', toggleMenu);
      $('.header-container__search input[type="submit"]').on('keydown', tabloopForwards);
      $('.header-container__nav-button').on('keydown', tabloopReverse);

      if ($(window).width() < 1024) {
        $('.accordion--directions').addClass('focused');
        $('.accordion--directions button').attr('aria-expanded', 'true');
      }

      $('.menu--inside li').each(function () {
        if ($(this).find('li').length > 20) {
          $(this).addClass('long-menu');
        }

        if ($(this).find('li').length > 10) {
          $(this).addClass('twocol-menu');
        }
      });
      setTimeout(function () {
        $(once('bmc-theme-switcher-accessibility', '.menu--theme-switcher .menu-item--expanded > button', context)).each(toggleAccessibility);
      }, 500);
    }
  };
};

header(jQuery);

var search = function search($) {
  function openSearch() {
    $(this).toggleClass('expanded');

    if ($(this).hasClass('expanded')) {
      $(this).attr('aria-expanded', 'true');
      setTimeout(function () {
        $('input[type="search"]').focus();
      }, 300);
    } else {
      $(this).attr('aria-expanded', 'false');
    }
  }
  /**
   *.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */


  Drupal.behaviors.search = {
    attach: function attach(context) {
      $(once('bmc-opensearch', '.header-container__search-button', context)).on('click', openSearch);
    }
  };
};

search(jQuery);

var backToTop = function backToTop($) {
  function backToTopScroll() {
    if ($(window).scrollTop() > 500) {
      $('.btt').addClass('visible-scroll');
    } else {
      $('.btt').removeClass('visible-scroll');
    }

    if ($(window).scrollTop() > parseInt($('.footer-container').offset().top - ($(window).height() - 40), 10)) {
      $('.btt').removeClass('visible-scroll');
    }

    var footerPos = parseInt($('footer').offset().top - ($(window).height() - 0), 10);

    if ($(window).scrollTop() > footerPos) {
      $('.block-content--type-fab').css('top', $('footer').offset().top).addClass('visible-scroll');
    } else {
      $('.block-content--type-fab').css('top', 'auto').removeClass('visible-scroll');
    }
  }

  function scrollBackToTop() {
    $('html, body').animate({
      scrollTop: 0
    });
    $('.header__logo a').focus();
  }
  /**
   *.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */


  Drupal.behaviors.backToTop = {
    attach: function attach(context) {
      $(window, context).on('scroll', backToTopScroll);
      $(once('bmc-btt', '.btt', context)).on('click', scrollBackToTop);
    }
  };
};

backToTop(jQuery);

var sectionNav = function sectionNav($) {
  /**
   *.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */
  function toggleSubmenu() {
    var $parentLi = $(this).parent();
    var $currentActive = $('.current-active');
    var $topUl = $('.top-level'); // Hide Top level links

    $topUl.addClass('is-open');
    $currentActive.addClass('animate');
    setTimeout(function () {
      $('.animate').removeClass('animate'); // current active class determines visible links via CSS

      $currentActive.removeClass('current-active');
    }, 200);
    setTimeout(function () {
      $parentLi.addClass('current-active').addClass('opened');
    }, 200);
    setTimeout(function () {
      $('.current-active > ul a:first').focus();
    }, 900);
  }

  function toggleSectionNav() {
    $('body').toggleClass('open-section-nav');

    if ($(window).width() > 699) {
      $('.section-nav__panel').slideToggle(300);
    }

    if ($('body').hasClass('open-section-nav')) {
      $(this).attr('aria-expanded', 'true');
    } else {
      $(this).attr('aria-expanded', 'false');
    }
  }

  function closeSectionNav() {
    if ($('body').hasClass('open-section-nav')) {
      $('body').removeClass('open-section-nav');
      $('.section-nav__toggle').attr('aria-expanded', 'false');
      $('.section-nav__toggle').focus();
    }
  }

  function tabloopForwards(e) {
    if ($('body').hasClass('open-section-nav')) {
      var lastvisible = $('.current-active >ul>li>a:visible, .current-active >ul>li>button:visible').last();

      if ((e.keyCode || e.which) === 9 && !e.shiftKey && e.target === lastvisible[0]) {
        e.preventDefault();
        e.stopPropagation();
        var $elToFocusOn = $('.section-nav__toggle');
        $elToFocusOn.focus();
      }
    }
  }

  function tabloopReverse(e) {
    if ($('body').hasClass('open-section-nav')) {
      if ((e.keyCode || e.which) === 9 && e.shiftKey) {
        var $elToFocusOn = $('.current-active >ul>li>a:visible, .current-active >ul>li>button:visible').last();
        e.preventDefault();
        e.stopPropagation();
        $elToFocusOn.focus();
      }
    }
  }

  Drupal.behaviors.sectionNav = {
    attach: function attach(context) {
      var firstSectionNavItem = $('.menu--inside .section-nav__panel > .menu > .menu-item--active-trail');

      if (firstSectionNavItem.length > 0) {
        var sectionTitle = $('.breadcrumb li:nth-child(2) a').text() || $('.menu--inside .section-nav__panel > .menu > .menu-item--active-trail > a').text();
        $('.section-nav__toggle').text(sectionTitle);

        if (!firstSectionNavItem.has('ul').length > 0) {
          $('.menu--inside .section-nav__panel > .menu').addClass('show-all-children');
          $(once('bmc-menu-inside-wrapinner', '.menu--inside .section-nav__panel .menu', context)).wrapInner('<li class="sec-duplicate"><ul>');
          var breadcrumbLast = $('.breadcrumb li:last-child a').clone();
          $(once('bmc-prepend-breadcrumblist', '.sec-duplicate', context)).prepend(breadcrumbLast);
        }
      } // Add class to body if there's no section nav for styling


      if ($('.section-nav__toggle').length === 0) {
        $('body').addClass('no-sec-nav');
      } else {
        $('body').addClass('has-sec-nav');
      }

      $('.is-active').attr('aria-current', 'true'); // Add <div> around top ul for styling

      $('.region-content-top >.navigation >ul, .section-nav__panel > ul', context).addClass('top-level'); // Add in duplicate parent links to ul below

      $('.section-nav__panel li:has(ul)', context).each(function () {
        var url = $(this).find('a:first').attr('href');
        var text = $(this).find('a:first').text();
        var $class = $(this).find('a:first')[0].classList[0] || '';
        var $aria = $(this).find('a:first').hasClass('is-active') ? 'aria-current="true"' : '';
        var dupHTML = "<li class=\"duplicate\"><h3><a class=\"".concat($class, "-dup\" ").concat($aria, " href=\"").concat(url, "\">").concat(text, "</a></h3></li>");
        $(this).children('ul').children('li.section-item:first').before(dupHTML);
      }); // Add in duplicate parent links to ul below

      $('.section-nav__panel .menu-item--0:not(.menu-item--expanded)', context).each(function () {
        var url = $(this).find('a:first').attr('href');
        var text = $(this).find('a:first').text();
        var $class = $(this).find('a:first')[0].classList[0];
        $(this).append("<ul class=\"menu\"><li class=\"duplicate\"><h3><a class=\"".concat($class, "-dup\" href=\"").concat(url, "\">").concat(text, "</a></h3></li></ul>"));
      });
      $('.section-nav__panel a', context).each(function () {
        $(this).wrapInner('<span />');
      }); // Add custom class to first active link if it appears twice in the sec nac

      if ($('.section-nav__panel .is-active, .section-nav__panel .is-active-dup').length > 1) {
        $('.section-nav__panel .is-active').addClass('first-active');
      } // Add Current Active class on page load to drill down into menu
      // Menu will open to current page


      if ($('.section-nav__panel .is-active').parent().has('ul').length > 0) {
        // if it has children, show current page + children
        $('.is-active').parent().addClass('current-active');
        $('.top-level').addClass('is-open');
      } else {
        // else, show parent page + parent pages children
        $('.is-active').parent().parent().closest('li').addClass('current-active');
        $('.top-level').addClass('is-open');
      }

      $('.current-active a, .current-active button').css('transition', 'none');
      $('.marketing-grad-menution-menu').css('opacity', '1');
      setTimeout(function () {
        $('.current-active a, .current-active button').css('transition', '');
      }, 300);
      $(once('bmc-toggle-submenu', '.menu--section__button', context)).on('click', toggleSubmenu);
      $(once('bmc-toggle-section-nav', '.section-nav__toggle', context)).on('click', toggleSectionNav);
      $(once('bmc-section-nav-back', '.section-nav__back', context)).on('click', function () {
        var $currentActive = $('.current-active');
        var $closestUl = $('.current-active').closest('ul');
        $currentActive.addClass('animate-reverse');
        setTimeout(function () {
          $currentActive.removeClass('current-active');
          $('.animate-reverse').removeClass('animate-reverse');
          $currentActive.removeClass('opened');
          $closestUl.closest('li').addClass('current-active opened');
        }, 200);
        setTimeout(function () {
          $('.current-active > ul a:first').focus();
        }, 900);
      });
      $('.section-nav__panel a').on('keydown', tabloopForwards);
      $('.section-nav__toggle').on('keydown', tabloopReverse);
      $(document).on('keydown', function (e) {
        if (e.which === 27) {
          closeSectionNav();
        }
      });
    }
  };
};

sectionNav(jQuery);

var breadCrumbReplace = function breadCrumbReplace($) {
  function breadCrumbReplace() {
    var hrefValue = $('.breadcrumb li:last-child a').attr('href');
    $('.breadcrumb-replace').attr('href', hrefValue);
  }
  /**
   *.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */


  Drupal.behaviors.breadCrumbReplace = {
    attach: function attach(context) {
      $(window, context).on('load', breadCrumbReplace);
    }
  };
};

breadCrumbReplace(jQuery);