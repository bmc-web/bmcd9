"use strict";

var alert = function alert($) {
  function alertClose() {
    var $lastUpdatedDate = $('.alert__date').text();

    if (sessionStorage.alert !== $lastUpdatedDate) {
      sessionStorage.alert = $lastUpdatedDate;
    }

    var alertHeight = $('.alert').outerHeight();
    $('.alert').slideUp();
    $('.open-menu .header-container__menu').animate({
      marginTop: "-=".concat(alertHeight)
    });
  }

  Drupal.behaviors.alert = {
    attach: function attach(context) {
      $(once('bmc-alert-close', '.alert__close', context)).on('click', alertClose);
      var $lastUpdatedDate = $('.alert__date').text();

      if (sessionStorage.alert !== $lastUpdatedDate) {
        $('.alert').show();
      }
    }
  };
};

alert(jQuery);