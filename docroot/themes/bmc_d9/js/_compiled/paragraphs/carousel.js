"use strict";

/**
 * @file
 * Provides carousel interactions and accessibility.
 */
var carousel = function carousel($) {
  /**
   * Attaches the component behavior to carousel components.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */
  function carouselInit() {
    var $el = $(this);
    var $label = $el.attr('aria-label');
    var $totalSlides = $el.find('.slide').length;
    $el.parent().next().text("Slide 1 of ".concat($totalSlides, " is active"));
    $el.slick({
      regionLabel: $label,
      fade: true,
      rows: 0,
      responsive: [{
        breakpoint: 699,
        settings: {
          adaptiveHeight: true
        }
      }]
    });
    $el.on('beforeChange', function beforeChange(event, slick, currentSlide, nextSlide) {
      $(this).find('.slick-slide').removeClass('fade-in');
      $(this).find('.slick-slide').addClass('fade-out');
    });
    $el.on('afterChange', function afterChange(event, slick, currentSlide) {
      $(this).find('.slick-slide').removeClass('fade-in');
      $(this).find('.slick-slide').eq(currentSlide).addClass('fade-in');
      $el.parent().next().text("Slide ".concat(currentSlide + 1, " of ").concat($totalSlides, " is active"));
    });
    $el.on('setPosition', function init(slick) {
      $el.attr({
        role: null
      });
    });
  }

  Drupal.behaviors.carousel = {
    attach: function attach(context) {
      $('.field--name-field-p-carousel-slide', context).each(carouselInit);
    }
  };
};

carousel(jQuery);