"use strict";

/**
 * @file
 * Provides Video interactions and accessibility.
 */
var video = function video($) {
  var $lastOpen = null;

  function updateHTML() {
    var $entity = $(this).closest('.embedded-entity');
    var $trigger = $entity.find('.video-embed-field-launch-modal');
    var $caption = $entity.siblings('figcaption');
    $entity.append('<button class="play-video"><span class="element-invisible">Play </span><span class="video-title">Video</button>');
    var $playBtn = $entity.children('.play-video');
    $playBtn.on('click', function () {
      $lastOpen = $(this);
      $trigger.trigger('click');
    });

    if ($caption.length > 0) {
      var $captionText = $caption.text();
      $caption.text('');
      $playBtn.prependTo($entity.siblings('figcaption'));
      $playBtn.append("<span class=\"caption-text\">".concat($captionText, "</span>"));
    }
  }

  function onOpen() {
    $('#colorbox').focus();
  }

  function onClose() {
    setTimeout(function () {
      if ($lastOpen) {
        $lastOpen.focus();
        $lastOpen = null;
      }
    }, 100); // w/o a settimeout, subsequent cbox opens do not focus the colorbox
  }
  /**
   * Attaches the video behavior to video components.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   * Add Video attributes and interaction to caption
   */


  Drupal.behaviors.video = {
    attach: function attach(context) {
      $(once('bmc-media-remote', '.media--type-remote-video', context)).each(updateHTML);
      $(document, context).on('cbox_open', onOpen);
      $(document, context).on('cbox_closed', onClose);
    }
  };
};

video(jQuery);