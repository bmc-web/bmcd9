"use strict";

/**
 * @file
 * Provides columns interactions and accessibility.
 */
var columns = function columns($) {
  function lessThanThreeCol() {
    var numOfColumns = $(this).find('.columns__items--item').length;

    if (numOfColumns < 3) {
      $(this).addClass('less-than-three');
    }
  }
  /**
   * Attaches the component behavior to ___ components.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *  Bind addclass to coumns.
   */


  Drupal.behaviors.columns = {
    attach: function attach(context) {
      $('.paragraph--type--columns', context).each(lessThanThreeCol);
    }
  };
};

columns(jQuery);