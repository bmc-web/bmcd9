"use strict";

/**
 * @file
 * Provides interactions and accessibility.
 */
var stat = function stat($) {
  function wrapSpanPuncutation() {
    $(this).text($(this).text().trim());
    $(this).html($(this).text().replace(/\W$/, function (match) {
      return "<span>".concat(match, "</span>");
    }));
  }
  /**
   *.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */


  Drupal.behaviors.stat = {
    attach: function attach(context) {
      $('.fstat__num', context).each(wrapSpanPuncutation);
    }
  };
};

stat(jQuery);