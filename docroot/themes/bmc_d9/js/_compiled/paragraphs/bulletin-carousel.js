"use strict";

/**
 * @file
 * Provides carousel interactions and accessibility.
 */
var carousel = function carousel($) {
  /**
   * Attaches the component behavior to carousel components.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */
  function carouselInit() {
    var $el = $(this);
    $el.slick({
      dots: true,
      nextArrow: '<button class="arrows-button slick-next slick-arrow">➡<span class="slick-next-icon" aria-hidden="true"></span>' + '<span class="slick-sr-only">Next</span></button>',
      prevArrow: '<button class="arrows-button slick-prev slick-arrow">⬅<span class="slick-prev-icon" aria-hidden="true"></span>' + '<span class="slick-sr-only">Previous</span></button>',
      fade: true,
      regionLabel: 'Gallery',
      rows: 0
    });
    $el.on('beforeChange', function beforeChange(event, slick, currentSlide, nextSlide) {
      $(this).find('.slick-slide').removeClass('fade-in');
      $(this).find('.slick-slide').addClass('fade-out');
    });
    $el.on('afterChange', function afterChange(event, slick, currentSlide) {
      $(this).find('.slick-slide').removeClass('fade-in');
      $(this).find('.slick-slide').eq(currentSlide).addClass('fade-in');
    });
  }

  Drupal.behaviors.carousel = {
    attach: function attach(context) {
      $('.bulletin-carousel__items', context).each(carouselInit);
    }
  };
};

carousel(jQuery);