"use strict";

/**
 * @file
 * Provides events list interactions and accessibility.
 */
var clearBtn = function clearBtn($) {
  function clearButton() {
    var params = window.location.search;

    if (params !== '') {
      $('input[value="Clear"]').attr('disabled', 'true');
      params.split('&').forEach(function (a) {
        var value = a.split('=')[1];

        if (value !== '' && value !== 'All' && value !== 'today') {
          $('input[value="Clear"]').attr('disabled', null);
        }
      });
    } else {
      $('input[value="Clear"]').attr('disabled', 'true');
    }
  }

  function overrideClear(e) {
    e.preventDefault();
    window.location = window.location.pathname;
  }
  /**
   *
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */


  Drupal.behaviors.clearBtn = {
    attach: function attach(context) {
      clearButton();
      $('input[value="Clear"]').on('click', overrideClear);
    }
  };
};

clearBtn(jQuery);