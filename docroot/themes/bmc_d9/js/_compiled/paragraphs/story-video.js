"use strict";

/**
 * @file
 * Provides Video interactions and accessibility.
 */
var storyVideo = function storyVideo($) {
  function onClose() {
    setTimeout(function () {
      $('.video-embed-field-launch-modal').focus();
    }, 100); // w/o a settimeout, subsequent cbox opens do not focus the colorbox
  }
  /**
   * Attaches the video behavior to video components.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   * Add Video attributes and interaction to caption
   */


  Drupal.behaviors.storyVideo = {
    attach: function attach(context) {
      $(document, context).on('cbox_closed', onClose);
    }
  };
};

storyVideo(jQuery);