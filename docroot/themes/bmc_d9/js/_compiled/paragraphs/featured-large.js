"use strict";

/**
 * @file
 * Provides interactions and accessibility.
 */
var largeFeature = function largeFeature($) {
  function setMargin() {
    $('.feat-lg__item, .feat-md__item, .home-ftmd__item').each(function () {
      var h = $(this).find('.paragraph').outerHeight();
      $(this).css('margin-top', -h / 2);
    });
    $('.home-ftmd__item').each(function () {
      var h = $(this).find('.paragraph').outerHeight();
      $(this).css('margin-bottom', -h / 2);
    });
  }

  function toggleAccessibility() {
    var $parent = $(this).parent();
    var $this = $(this);
    $parent.hover(function setHover() {
      if ($(window).width() > 1024) {
        $parent.addClass('focused');
        $parent.siblings().removeClass('focused');
        $this.attr('aria-expanded', 'true');
        $parent.removeClass('remove-delay');
      }
    }, function setHoverOff() {
      if ($(window).width() > 1024) {
        $parent.removeClass('focused');
        $parent.siblings().removeClass('focused');
        $this.attr('aria-expanded', 'false');
        $parent.removeClass('remove-delay');
      }
    });
    $(this).on('click', function (e) {
      $parent.toggleClass('focused');

      if ($parent.hasClass('focused')) {
        $this.attr('aria-expanded', 'true');
        $parent.removeClass('remove-delay');
      } else {
        $this.attr('aria-expanded', 'false');
        $parent.addClass('remove-delay');
      }
    });
    $(document).on('keydown', function (e) {
      if (e.which === 27) {
        var $active = $('.focused');
        $parent.removeClass('remove-delay');
        $parent.removeClass('focused');
        $this.attr('aria-expanded', 'false');
        $active.find($this).focus();
      }
    });
    $parent.focusout(function () {
      $parent.removeClass('focused').addClass('remove-delay');
      $this.attr('aria-expanded', 'false');
    });
    $this.next().focusin(function () {
      $parent.addClass('focused');
      $parent.removeClass('remove-delay');
      $this.attr('aria-expanded', 'true');
    });
  }
  /**
   * Attaches the component behavior to large Feature components.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */


  Drupal.behaviors.largeFeature = {
    attach: function attach(context) {
      $(window, context).on('load resize', setMargin);
      $(once('bmc-fdrop-toggle', '.fdrop__toggle', context)).each(toggleAccessibility);
    }
  };
};

largeFeature(jQuery);