"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

(function ($) {
  var app = new Vue({
    el: '#office-service-app',
    delimiters: ['${', '}'],
    data: function data() {
      return {
        searchTerms: '',
        activeLetter: 'all',
        offices: [],
        activeLetters: []
      };
    },
    created: function created() {
      var _this = this;

      // Get data from endpoint
      $.getJSON('/api/ofc-ser/all', function (json) {
        var offices = json.map(function (item) {
          // merge all fields into a single search field
          item.search = Object.values(item).join(' ');
          return item;
        });
        _this.offices = offices;

        _this.updateActiveLetters(_this.offices);
      });
    },
    mounted: function mounted() {
      // Update filters based on query params on page load
      function getQueryVariable() {
        var query = window.location.search.substring(1);
        var vars = query.split('&');
        var pairs = [];

        for (var i = 0; i < vars.length; i++) {
          var title = vars[i].split('=')[0];
          pairs.push(_defineProperty({}, title, vars[i].split('=')[1]));
        }

        return pairs;
      }

      var params = getQueryVariable();

      if (params.length > 1) {
        // Set search filter
        if (params[0].query !== '') {
          this.searchTerms = decodeURIComponent(params[0].query);
          this.$refs.searchInput.value = this.searchTerms;
        } // Set alpha filter


        if (params[1].letter !== '') {
          this.activeLetter = params[1].letter;
          $('.alpha-search button').removeClass('active').attr('aria-current', 'false');
          $('.alpha-search button').filter(function () {
            return $(this).text().toLowerCase() === params[1].letter;
          }).addClass('active').attr('aria-current', 'true');
        }
      }
    },
    computed: {
      // Get Filtered Results to display
      filteredResults: function filteredResults() {
        var _this2 = this;

        // Setup functions to check if selected filters match the office items
        // Check if matches alpha filter
        // param item -> item to check filter on
        var matchLetter = function matchLetter(item) {
          if (item.office_title.charAt(0).toLowerCase() === _this2.activeLetter.toLowerCase()) {
            return true;
          }

          return false;
        }; // Check if matches search filter
        // param item -> item to check filter on


        var matchSearch = function matchSearch(item) {
          var lowerItemSearchContent = item.search.toLowerCase();

          var lowerSearchTerm = _this2.searchTerms.toLowerCase();

          if (lowerItemSearchContent.indexOf(lowerSearchTerm) !== -1) {
            return true;
          }

          return false;
        }; // Only do the amount of filtering necessary, based on 'all' values


        if (this.activeLetter === 'all' && this.searchTerms === '') {
          // if no filters are active, return all offices
          return this.offices;
        }

        return this.offices.filter(function (item) {
          if (_this2.activeLetter === 'all') {
            return matchSearch(item); // eslint-disable-next-line no-else-return
          } else if (_this2.searchTerms === '') {
            return matchLetter(item);
          } else {
            return matchSearch(item) && matchLetter(item);
          }
        });
      },
      // Helper to get all letters of the alphabet instead of hardcoding
      letters: function letters() {
        var alphaArr = [];

        for (var i = 0; i < 26; i++) {
          var letter = (i + 10).toString(36); // Add disabled property, check for letter in activeLetters array

          var isDisabled = true;

          if (this.activeLetters.indexOf(letter) > -1) {
            isDisabled = false;
          }

          alphaArr.push({
            letter: letter,
            disabled: isDisabled
          });
        }

        return alphaArr;
      }
    },
    watch: {
      // Update URL params whenever filteredResults Update
      filteredResults: function filteredResults() {
        window.history.pushState(null, document.title, "?query=".concat(this.searchTerms, "&letter=").concat(this.activeLetter));

        if (this.searchTerms !== '') {
          this.updateActiveLetters(this.filteredResults);
        }
      }
    },
    methods: {
      // Executes on click of an alpha filter, updates this.activeletter
      // Actual filtering logic is in computed property FilteredResults
      setAlphaFilter: function setAlphaFilter(filter, event) {
        this.activeLetter = filter;
        $('.alpha-search button').removeClass('active').attr('aria-current', 'false');
        $(event.target).addClass('active').attr('aria-current', 'true');
      },
      // Executes on submit of search form, updates this.searchTerms
      // Actual filtering logic is in computed property FilteredResults
      updateSearch: function updateSearch() {
        this.searchTerms = this.$refs.searchInput.value;
      },
      clearFilters: function clearFilters() {
        this.searchTerms = '';
        this.$refs.searchInput.value = '';
        $('.alpha-search button').removeClass('active');
        $('.alpha-search button.all').addClass('active');
        this.activeLetter = 'all';
        this.updateActiveLetters(this.offices);
      },
      updateActiveLetters: function updateActiveLetters(arrToUse) {
        var activeLetters = [];
        arrToUse.forEach(function (a) {
          var firstLetter = a.office_title.charAt(0);

          if (activeLetters.indexOf(firstLetter) === -1) {
            activeLetters.push(firstLetter.toLowerCase());
          }
        });
        this.activeLetters = activeLetters;
      }
    }
  });
})(jQuery);