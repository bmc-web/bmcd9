"use strict";

/**
 * @file
 * Provides interactions and accessibility.
 */
var socialIcons = function socialIcons($) {
  function wrapInA() {
    var shareWrapper = document.querySelector('.sharethis-wrapper');
    var mutations = {
      childList: true,
      subtree: true
    };

    var adjustButtonHTML = function adjustButtonHTML() {
      if ($('.st-btn a').length === 0) {
        $('.st-btn').wrapInner('<a href="#">');
      }

      $('.st-btn').each(function () {
        var iconType = $(this).data('network');
        var capType = iconType.substr(0, 1).toUpperCase() + iconType.substr(1);
        var share = '';
        var shareMethod = '';

        if (iconType !== 'print') {
          share = 'Share ';
          if (iconType !== 'email') shareMethod = 'on ';else shareMethod = 'via ';
        }

        $(this).find('img').prop('alt', share + shareMethod + capType);
      });
    };

    var observer = new MutationObserver(adjustButtonHTML);
    observer.observe(shareWrapper, mutations);
  }
  /**
   *.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */


  Drupal.behaviors.socialIcons = {
    attach: function attach(context) {
      wrapInA();
    }
  };
};

socialIcons(jQuery);