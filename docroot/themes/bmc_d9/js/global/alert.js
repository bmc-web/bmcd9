const alert = $ => {
  function alertClose() {
    const $lastUpdatedDate = $('.alert__date').text();
    if (sessionStorage.alert !== $lastUpdatedDate) {
      sessionStorage.alert = $lastUpdatedDate;
    }

    const alertHeight = $('.alert').outerHeight();

    $('.alert').slideUp();
    $('.open-menu .header-container__menu').animate({
      marginTop: `-=${alertHeight}`,
    });
  }

  Drupal.behaviors.alert = {
    attach(context) {
      $(once('bmc-alert-close','.alert__close', context)).on('click', alertClose);
      const $lastUpdatedDate = $('.alert__date').text();
      if (sessionStorage.alert !== $lastUpdatedDate) {
        $('.alert').show();
      }
    },
  };
};

alert(jQuery);
