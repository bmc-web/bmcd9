
(function($, drupalSettings) {
  const TreeMenu = Vue.component('TreeMenu', {
    template: `<ul class="menu">
    <li :class="{active: item.isActive}" v-for="item in nodes">
        <a :aria-current="item.isCurrent ? 'page' : 'false'" :href="item.absolute">{{ item.title }} </a>
        <button v-on:click="showNewLevel(item)" class="menu--section__button" v-if="item.below">
          <span class="visually-hidden">toggle submenu</span>
        </button>
        <transition name="fade">
          <TreeMenu v-if="item.below && item.isActive" :nodes="item.below"></TreeMenu>
        </transition>
      </li>


    </ul>`,

    props: [ 'nodes' ],
    methods: {
      showNewLevel(item) {
        item.isActive = !item.isActive;
      },
    }
  })
  const app = new Vue({
    el: '#marketing-grad-menu',
    data() {
      return {
        items: [],
        parentStored: [],
        active: null,
        current: null,
        found: false,
        breadcrumbText: [],

      };
    },
    template: `
      <div class="section-navigation">
        <span class="active-link-display"></span>
        <div class="section-nav__panel">
            <button :disabled="allCollapsed" v-on:click="collapse()" class="section-nav__collapse">Collapse</button>
            <TreeMenu :nodes="items"></TreeMenu>
        </div>
      </div>
    `,
    created() {
      const _this = this;
      const activeURL = encodeURI(window.location.href);
      const menuName = $('nav[data-section-name]').data('section-name');
      setTimeout(() => {
        $('.active-link-display').text($('.header__menu--theme-switcher .menu a.is-active').text())
      }, 500);

      $('.breadcrumb li').each(function() {
        _this.breadcrumbText.push($(this).text().trim());
      });

      function recursiveEdit(items) {
        items.forEach(a => {
          a.isActive = false;
          if (activeURL.indexOf(a.absolute) != -1) {
            a.isActive = true;
          }

          if (a.absolute === activeURL) {
            a.isCurrent = true;
          } else {
            a.isCurrent = false
          }
          if (a.below) {
            recursiveEdit(a.below)
          }
        })
      }

      // $.getJSON('/api/menu_items/'+menuName, json => {
      //   console.log(json);
      //   console.log(drupalSettings['marketing-grad-menu']);
      //   // this.findCurrentChild(activeURL, json);
      //   recursiveEdit(json);
      //   this.items = json;
      // });
      let json = drupalSettings['marketing-grad-menu'];
      recursiveEdit(json);
      this.items = json;
    },
    computed: {
      allCollapsed: function() {
        return this.items.every(a => {
          return a.isActive === false;
        });
      }
    },
    methods: {
      collapse() {
        function recursiveEdit(items) {
          items.forEach(a => {
            a.isActive = false;

            if (a.below) {
              recursiveEdit(a.below)
            }
          })
        }

        recursiveEdit(this.items);
      },

    }
  });
})(jQuery, drupalSettings);
