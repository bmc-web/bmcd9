const bulletinHeaderSearch = $ => {
  function openSearch() {
    $(this).toggleClass('expanded');
    $(this)
      .next()
      .find('form')
      .toggleClass('visible');

    if ($(this).hasClass('expanded')) {
      $(this).attr('aria-expanded', 'true');
      setTimeout(() => {
        $('input[type="search"]').focus();
      }, 300);
    } else {
      $(this).attr('aria-expanded', 'false');
    }
  }

  /**
   *.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */
  Drupal.behaviors.bulletinHeaderSearch = {
    attach(context) {
      $('.open-search', context).on('click', openSearch);
    },
  };
};

bulletinHeaderSearch(jQuery);

const backToTop = $ => {
  function backToTopScroll() {
    if ($(window).scrollTop() > 100) {
      $('.btt').addClass('visible-scroll');
    } else {
      $('.btt').removeClass('visible-scroll');
    }
  }

  function scrollBackToTop() {
    $('html, body').animate({ scrollTop: 0 });
    $('.header__logo').focus();
  }

  /**
   *.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */
  Drupal.behaviors.backToTop = {
    attach(context) {
      $(window, context).on('scroll', backToTopScroll);
      $('.btt').on('click', scrollBackToTop);
    },
  };
};

backToTop(jQuery);

const menu = $ => {
  function openMenu() {
    $(this).toggleClass('expanded');
    $(this)
      .next()
      .toggleClass('visible');

    if ($(this).hasClass('expanded')) {
      $(this)
        .closest('.header-container__bulletin')
        .addClass('open-menu');
      $(this).attr('aria-expanded', 'true');
    } else {
      $(this).attr('aria-expanded', 'false');
      $(this)
        .closest('.header-container__bulletin')
        .removeClass('open-menu');
    }
  }

  /**
   *.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */
  Drupal.behaviors.menu = {
    attach(context) {
      $('.meanmenu-reveal', context).on('click', openMenu);
    },
  };
};

menu(jQuery);
