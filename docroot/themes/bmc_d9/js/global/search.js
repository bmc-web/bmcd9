const search = $ => {
  Drupal.behaviors.search = {
    attach(context) {
      const searchMapping = [
        {
          path: 'gsas',
          tabText: 'gsas',
        },
        {
          path: 'inside',
          tabText: 'inside bryn mawr',
        },
        {
          path: 'socialwork',
          tabText: 'socialwork',
        },
        {
          path: 'postbac',
          tabText: 'postbac',
        },
        {
          path: 'bulletin',
          tabText: 'bulletin',
        },
        {
          path: 'search',
          tabText: 'all',
        },
      ];
      const searchBlock = document.getElementById('google-cse-results');
      const siteSection = window.location.pathname.split('/');
      const path = siteSection[1];
      const tabTextIndex = searchMapping.findIndex(a => a.path === path);
      const tabTextToClick = searchMapping[tabTextIndex].tabText;
      const mutations = { childList: true };
      const addClasses = () => {
        setTimeout(() => {
          const $tabs = $('.gsc-refinementHeader');
          $('.gsc-resultsbox-visible').attr('role', 'tablist');
          $tabs.each(function() {
            $(this).attr({
              id: `tab-${$(this).index() / 2}`,
            });
          });
          $tabs
            .attr('aria-label', null);

          $(once('bmc-gsc-refinementHeader', '.gsc-refinementHeader',context))
            .on('keydown', function(e) {
              if (e.key === 'ArrowRight') {
                const nextTab = $(this)
                  .next()
                  .next();
                const nextNextTab = nextTab.next().next();
                if (nextTab.attr('tabindex') === '0') {
                  nextTab.focus();
                } else if (nextNextTab.attr('tabindex') === '0') {
                  nextNextTab.focus();
                }
              }
              if (e.key === 'ArrowLeft') {
                const prevTab = $(this)
                  .prev()
                  .prev();
                const prevPrevTab = prevTab.prev().prev();
                if (prevTab.attr('tabindex') === '0') {
                  prevTab.focus();
                } else if (prevPrevTab.attr('tabindex') === '0') {
                  prevPrevTab.focus();
                }
              }
            });

          $tabs.attr('aria-selected', 'false');
          $('.gsc-refinementhActive').attr('aria-selected', 'true');
          $('.gsc-result').each(function() {
            $(this).attr({
              role: 'tabpanel',
              tabindex: '0',
              'aria-labelledby': `tab-${$(this).index()}`,
            });
          });
          if ($tabs.length) {
            $tabs.each(function() {
              const textFormatted = $(this)
                .text()
                .trim()
                .toLowerCase();
              $(this).attr('data-path', textFormatted);
            });

            $(`[data-path="${tabTextToClick}"]`).trigger('click');

            $('.gsc-cursor').attr({
              role: 'Navigation',
              'aria-label': 'Pagination',
            });

            $('.gsc-cursor-current-page').attr('aria-current', 'true');
            if (!$('.gsc-cursor-current-page span').length > 0) {
              $('.gsc-cursor-current-page').prepend(
                '<span class="visually-hidden">Current Page</span>',
              );
            }
          }
          // Change result title tags from div to H2.
          const $resultHeader = $('.gsc-thumbnail-inside div.gs-title');
          if ($resultHeader) {
            $resultHeader.replaceWith(function() {
              return `<h2 class="gs-title">${$(this).html()}</h2>`;
            });
          }
          // Add role to filter link
          const $filterLink = $('.gs-per-result-labels .gs-label');
          if ($filterLink) {
            $filterLink.attr('role', 'button');
          }
        }, 500);
      };

      const observer = new MutationObserver(addClasses);
      observer.observe(searchBlock, mutations);

      $('#edit-sa').val('submit');
    },
  };
};

search(jQuery);
