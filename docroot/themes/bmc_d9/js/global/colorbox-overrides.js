/**
 * @file
 * Provides Colorbox interactions and accessibility.
 */

const colorboxOverrides = $ => {
  function tabloopForwards(e) {
    if ((e.keyCode || e.which) === 9 && !e.shiftKey) {
      $('#colorbox').focus();
      e.preventDefault();
      e.stopPropagation();
    }
  }

  function tabloopReverse(e) {
    if ((e.keyCode || e.which) === 9 && e.shiftKey) {
      $('#cboxClose').focus();
      e.preventDefault();
      e.stopPropagation();
    }
  }

  function onOpen() {
    $('#cboxOverlay').css('visibility', 'hidden');
    $('#cboxOverlay').animate(
      {
        visibility: 'visible',
        opacity: 0.8,
        cursor: 'pointer',
      },
      300,
    );
    $.colorbox.settings.trapFocus = false;
    $('#colorbox')
      .attr('tabindex', '0')
      .css('opacity', '0')
    $('.dialog-off-canvas-main-canvas, .skip-link').attr('aria-hidden', true);
  }

  function onComplete() {
    $('#cboxContent').on('keydown', e => {
      e.stopPropagation();
    });
    $('#colorbox')
      .css('opacity', '1')
    $(once('bmc-cbox-loopforward', '#cboxClose'))
      .on('keydown', tabloopForwards);
    $(once('bmc-cbox-loopreverse', '#colorbox'))
      .on('keydown', tabloopReverse);


    const iframe = $('#cboxLoadedContent').find('iframe');
    if (iframe.length > 0) {
      const src = iframe.attr('src');
      const newSrc = src.replace('autoplay=0', 'autoplay=1');
      iframe.attr('src', newSrc);
    }
  }

  function onClose() {
    $('.dialog-off-canvas-main-canvas, .skip-link').attr('aria-hidden', false);
  }

  /**
   * Attaches the component behavior to colorbox components.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *  Bind/Add x to y.
   */
  Drupal.behaviors.colorboxOverrides = {
    attach(context) {
      $(document, context).on('cbox_open', onOpen);
      $(document, context).on('cbox_complete', onComplete);
      $(document, context).on('cbox_closed', onClose);

    },
  };
};

colorboxOverrides(jQuery);
