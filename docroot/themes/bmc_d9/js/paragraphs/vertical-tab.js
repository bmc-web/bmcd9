/**
 * @file
 * Provides Vertical Tab interactions and accessibility.
 */

const verticaltab =  ($) => {
  /**
   * keyboard navigation with tabs.
   * arrow up or right => open next tab
   * arrow down or bottom => open previous tab
   **/

  $(".vert_tab__items").each(function() {
    // do tabby things on each vert_tab__items element
    const tablists = this.querySelectorAll('[role=tablist] [role=tab]');
    this.tabListNode = tablists;

    const drawerlists = this.querySelectorAll('.tab_drawer_heading[role=tab]');
    this.drawerListNode = drawerlists;

    // creates lists to use below to bind functions and events
    const tablist = $(this.tabListNode);
    const drawerList = $(this.drawerListNode);

    // sets all tabs to tabindex -1
    tablist.each(function(){
      $(this).attr('tabindex', '-1');
    });

    // sets all drawers to tabindex -1
    drawerList.each(function(){
      $(this).attr('tabindex', '-1');
    });

    // sets first-of-type to visible/active, aria-selected true and tabindex 0 on page load
    const tabFirst = $("[role='tablist']").find("li:first-child button[role='tab']");
    const panelFirst = $("[role='tabpanel']:first-of-type");
    const drawerFirst = $('.vert_tab__items .tab_drawer_heading:first-child');

    panelFirst.addClass('visible').attr('tabindex', '0');
    tabFirst.addClass('active').attr("aria-selected", "true").attr('tabindex', '0');
    drawerFirst.addClass('d_active').attr("aria-selected", "true").attr('tabindex', '0');

    // function to handle click behavior for vert tabs
    function tabsClickHandler(){
      const thisTabId = $(this).attr('id');
      const thisTabList = $(this).parents("[role='tablist']").find("[role='tab']");
      // forEach for tabs to set tabpanels visible/active tabindex and aria-selected on or off
      thisTabList.each(function(){
        const thisTabPanel = $("[role='tabpanel'][id='"+$(this).attr('aria-controls')+"']");
        if($(this).attr('id') === thisTabId){
          thisTabPanel.addClass("visible").attr('tabindex', '0');
          $(this).attr('aria-selected', true)
            .attr('tabindex', '0')
            .addClass("active");
        } else {
          thisTabPanel.removeClass("visible").attr('tabindex', '-1');
          $(this).attr('aria-selected', false)
            .attr('tabindex', '-1')
            .removeClass("active");
        }
      });
    }
    // function to handle click behavior for drawer accordion effect
    function drawersClickHandler(){
      const thisDrawerId = $(this).attr('id');
      const thisDrawerList = $(this).parents(".tabs-details").find("[role='tab']");
      // forEach for drawers to set tabpanels visible/d_active tabindex and aria-selected on or off
      thisDrawerList.each(function(){
        const thisTabPanel = $("[role='tabpanel'][id='"+$(this).attr('aria-controls')+"']");
        if($(this).attr('id') === thisDrawerId){
          thisTabPanel.addClass("visible").attr('tabindex', '0');
          $(this).attr('aria-selected', true)
            .attr('tabindex', '0')
            .addClass("d_active");
        } else {
          thisTabPanel.removeClass("visible").attr('tabindex', '-1');
          $(this).attr('aria-selected', false)
            .attr('tabindex', '-1')
            .removeClass("d_active");
        }
      });
    }
// function to handle keyboard behavior for vert tabs
    function tabsHandler(event){
// indexes tabs in the tablist
      let index = tablist.index(this);
      let numbTabs = tablist.length;
      let nextId;

      if(numbTabs>1){
        if(event.keyCode === 40 || event.keyCode === 39) { // DOWN or RIGHT
          nextId = tablist.eq(index+1);
          event.stopPropagation();
          event.preventDefault();
          if(index === numbTabs-1){ // if it is the last not empty tab, then go to first not empty tab
            nextId = tablist.eq(0);
          }
          nextId.focus(); // focus on next tab
          nextId.click();
        }

        if(event.keyCode === 38 || event.keyCode === 37) { // UP or LEFT
          nextId = tablist.eq(index-1);
          event.stopPropagation();
          event.preventDefault();
          if(index === 0){ // if it is the last not empty tab, then go to first not empty tab
            nextId = tablist.eq(numbTabs-1);
          }
          nextId.focus(); // focus on next tab
          nextId.click();
        }
      }
    }
    // function to handle keyboard behavior for drawers for accordion effect
    function drawersHandler(event){
// indexes buttons in the drawerList
      let index = drawerList.index($(this));
      let numbTabs = drawerList.length;
      let nextId;

      if(numbTabs>1){
        if(event.keyCode === 40 || event.keyCode === 39) { // DOWN or RIGHT
          nextId = drawerList.eq(index+1);
          event.stopPropagation();
          event.preventDefault();
          if(index === numbTabs-1){ // if it is the last not empty tab, then go to first not empty tab
            nextId = drawerList.eq(0);
          }
          nextId.focus(); // focus on next tab
          nextId.click();
        }

        if(event.keyCode === 38 || event.keyCode === 37) { // UP or LEFT
          nextId = drawerList.eq(index-1);
          event.stopPropagation();
          event.preventDefault();

          if(index === 0){ // if it is the last not empty tab, then go to first not empty tab
            nextId = drawerList.eq(numbTabs-1);
          }
          nextId.focus(); // focus on next tab
          nextId.click();
        }
      }
    }
        // binds functions to events
        // for vert tabs
        tablist.on('keydown', tabsHandler);
        tablist.on('click', tabsClickHandler);
        // for drawer buttons
        drawerList.on('keydown', drawersHandler);
        drawerList.on('click', drawersClickHandler);
    });
  };

verticaltab(jQuery);
