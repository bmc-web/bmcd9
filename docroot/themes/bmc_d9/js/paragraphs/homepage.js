/**
 * @file
 * Provides carousel interactions and accessibility.
 */

const homepage = $ => {
  /**
   * Attaches the component behavior to carousel components.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */

  function carouselInit() {
    const $el = $(this);
    const $label = $el.attr('aria-label');
    const $totalSlides = $el.find('.home-slide').length,
      rand = Math.floor( Math.random() * $totalSlides ); // random number;
    $el
      .next()
      .text(`Slide 1 of ${$totalSlides} is active`)
    $el.slick({
      regionLabel: $label,
      fade: true,
      rows: 0,
      responsive: [
        {
          breakpoint: 699,
          settings: {
            adaptiveHeight: true,
          }
        },
      ]
    });
    $el.slick('slickGoTo', rand);
    //this is also works $('.my-slider')[0].slick.slickGoTo(rand);

    $el.on('beforeChange', function beforeChange(
      event,
      slick,
      currentSlide,
      nextSlide,
    ) {
      $(this)
        .find('.slick-slide')
        .removeClass('fade-in');
      $(this)
        .find('.slick-slide')
        .addClass('fade-out');
    });

    $el.on('afterChange', function afterChange(event, slick, currentSlide) {
      $(this)
        .find('.slick-slide')
        .removeClass('fade-in');
      $(this)
        .find('.slick-slide')
        .eq(currentSlide)
        .addClass('fade-in');
      $el
        .next()
        .text(`Slide ${currentSlide + 1} of ${$totalSlides} is active`)
    });

    $el.on('setPosition', function init(slick) {
      $el.attr({
        role: null,
      });
    });
  }

  Drupal.behaviors.homepage = {
    attach(context) {
      if ($('.home__carousel').length) {
        $('.home__carousel', context).each(carouselInit);
        $('body').addClass('has-home-hero');
      }
    },
  };
};

homepage(jQuery);
