/**
 * @file
 * Provides columns interactions and accessibility.
 */

const gallery = $ => {
  /**
   * Attaches the component behavior to ___ components.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *  Bind addclass to coumns.
   */

  function colorboxInit() {
    const $el = $(this)
      .parent()
      .next();
    $(this).colorbox({
      inline: true,
      href: $el,
      transition: 'fade',
      className: 'gallery',
      fixed: true,
      onComplete() {
        $el.slick({
          fade: true,
          regionLabel: 'Gallery',
        });
        $('#cboxLoadedContent').append('<div aria-live="polite" class="visually-hidden gallery-live-region"> <div>')
        const $totalSlides = $el.find('.image-gallery__slides__slide').length;
        $('.gallery-live-region').text(`Slide 1 of ${$totalSlides} is active`)

        $el.on('beforeChange', function beforeChange(
          event,
          slick,
          currentSlide,
          nextSlide,
        ) {
          $(this)
            .find('.slick-slide')
            .removeClass('fade-in');
          $(this)
            .find('.slick-slide')
            .addClass('fade-out');
        });

        $el.on('afterChange', function afterChange(event, slick, currentSlide) {
          $(this)
            .find('.slick-slide')
            .removeClass('fade-in');
          $(this)
            .find('.slick-slide')
            .eq(currentSlide)
            .addClass('fade-in');
            
          $('.gallery-live-region').text(`Slide ${currentSlide + 1} of ${$totalSlides} is active`)
        });

        $el.on('setPosition', function init(slick) {
          $el.attr({
            role: null,
            'aria-label': null,
          });
        });
      },
      onClosed() {
        $el.slick('unslick');
      },
    });
  }
  Drupal.behaviors.gallery = {
    attach(context) {
      $('.image-gallery__open', context).each(colorboxInit);
    },
  };
};

gallery(jQuery);
