/**
 * @file
 * Provides interactions and accessibility.
 */

const socialIcons = $ => {
  function wrapInA() {
    const shareWrapper = document.querySelector('.sharethis-wrapper');
    const mutations = { childList: true, subtree: true };
    const adjustButtonHTML = () => {
      if ($('.st-btn a').length === 0) {
        $('.st-btn').wrapInner('<a href="#">');
      }
      $('.st-btn').each(function() {
        const iconType = $(this).data('network');
        const capType =
          iconType.substr(0, 1).toUpperCase() + iconType.substr(1);
        let share = '';
        let shareMethod = '';
        if (iconType !== 'print') {
          share = 'Share ';
          if (iconType !== 'email') shareMethod = 'on ';
          else shareMethod = 'via ';
        }
        $(this)
          .find('img')
          .prop('alt', share + shareMethod + capType);
      });
    };

    const observer = new MutationObserver(adjustButtonHTML);
    observer.observe(shareWrapper, mutations);
  }

  /**
   *.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */
  Drupal.behaviors.socialIcons = {
    attach(context) {
      wrapInA();
    },
  };
};

socialIcons(jQuery);
