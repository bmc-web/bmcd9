/**
 * @file
 * Provides story video interactions and accessibility.
 */

const component = $ => {
  function addTabIndex() {
    $(this).attr({ tabindex: '0', role: 'button', 'aria-label': 'Play Video' });

    $(this).on('keydown', function (e) {
      if (e.which === 13) {
        $(this).trigger('click');
      }
    });
  }

  function onClose() {
    setTimeout(() => {
      $('.video-embed-field-launch-modal').focus();
    }, 100); // w/o a settimeout, subsequent cbox opens do not focus the colorbox
  }

  /**
   * Attaches the component behavior to story video components.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *  Add tabindex to story video.
   */
  Drupal.behaviors.component = {
    attach(context) {
      $(document, context).on('cbox_closed', onClose);
      $('.video-embed-field-launch-modal', context).each(addTabIndex);
    },
  };
};

component(jQuery);
