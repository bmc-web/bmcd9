/**
 * @file
 * Provides scroll animations.
 */

const scrollAnimation = $ => {
  function inview() {
    $(this).waypoint(
      function(direction) {
        $(this.element).addClass('inview');
      },
      {
        offset: '70%',
      },
    );
  }

  /**
   * Attaches the component behavior to ___ components.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *  All scroll animation to feature items.
   */
  Drupal.behaviors.scrollAnimation = {
    attach(context) {
      $('.feature-item, .home-ftmd__item, .homepage-feature-story__story--quote, .timeline__item', context).each(inview);
    },
  };
};

scrollAnimation(jQuery);
