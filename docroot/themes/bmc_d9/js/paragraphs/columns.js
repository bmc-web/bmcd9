/**
 * @file
 * Provides columns interactions and accessibility.
 */

const columns = $ => {
  function lessThanThreeCol() {
    const numOfColumns = $(this).find('.columns__items--item').length;
    if (numOfColumns < 3) {
      $(this).addClass('less-than-three');
    }
  }

  /**
   * Attaches the component behavior to ___ components.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *  Bind addclass to coumns.
   */
  Drupal.behaviors.columns = {
    attach(context) {
      $('.paragraph--type--columns', context).each(lessThanThreeCol);
    },
  };
};

columns(jQuery);
