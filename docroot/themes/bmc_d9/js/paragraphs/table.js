/**
 * @file
 * Provides Table interactions and accessibility.
 */

const table = $ => {
  function tableNext() {
    const $tableScroll = $(this)
      .siblings('table')
      .find('.table-scroller');
    const elWidth = $(this)
      .siblings('table')
      .parent()
      .width();

    $tableScroll.animate(
      {
        scrollLeft: `+=${elWidth}`,
      },
      300,
      'swing',
    );
  }

  function tablePrev() {
    const $tableScroll = $(this)
      .siblings('table')
      .find('.table-scroller');
    const elWidth = $(this)
      .siblings('table')
      .parent()
      .width();
    $(this)
      .siblings('table')
      .find($tableScroll)
      .animate(
        {
          scrollLeft: `-=${elWidth}`,
        },
        300,
        'swing',
      );
  }

  function tableScrollableCheck() {
    // Checks if table should have sticky header
    // and/or be scrollable based on dimensions
    $('table').each(function makeScrollable() {
      const $tableWrap = $(this).closest('.table__inner');
      const wrapWidth = $tableWrap.outerWidth();
      const tableWidth = $tableWrap.find('tr').outerWidth();

      // Handle arrow visibility
      if (wrapWidth < tableWidth) {
        setTimeout(() => {
          $tableWrap.addClass('js-scroll--right');
          $tableWrap.find('thead').attr('tabindex', '0');
        }, 100);
        $(this)
          .find('td, th')
          .each(function addWidths() {
            $(this).css('min-width', $(this).outerWidth());
          });
      } else {
        setTimeout(() => {
          $tableWrap.removeClass('js-scroll--left js-scroll--right');
        }, 100);
      }

      // Handle sticky headere
      if ($(this).height() > $(window).height()) {
        $(this).addClass('sticky-header');
      } else {
        $(this).removeClass('sticky-header');
      }
    });
  }

  function tableUpdateScroll() {
    const $thisWrapper = $(this).closest('.table__inner');
    const scrollLeft = $(this).scrollLeft();
    const scrollWidth = $(this)[0].scrollWidth - $thisWrapper[0].clientWidth;
    if (scrollLeft === 0 && scrollWidth > 0) {
      // If at the begin scroll pos
      $thisWrapper.addClass('js-scroll--right');
      $thisWrapper.removeClass('js-scroll--left');
    } else if (scrollLeft === scrollWidth && scrollWidth > 0) {
      // If at the end scroll pos
      $thisWrapper.removeClass('js-scroll--right').addClass('js-scroll--left');
    } else {
      $thisWrapper.addClass('js-scroll--right js-scroll--left');
    }
  }

  function tableInit(i) {
    const $tableWrapper = $(this).parent();

    $(this)
      .find('thead')
      .addClass('table-scroller syncscroll')
      .attr('name', `table-${i}`)
      .on('scroll', tableUpdateScroll);
    $(this)
      .find('tbody')
      .addClass('table-scroller syncscroll')
      .attr('name', `table-${i}`)
      .on('scroll', tableUpdateScroll);
    // eslint-disable-next-line no-undef
    syncscroll.reset();

    $tableWrapper.prepend(
      '<button class="table-block__prev" aria-hidden="true">Scroll Table left</button>\n' +
        '<button class="table-block__next" aria-hidden="true">Scroll Table right</button>',
    );

    $(this)
      .siblings('.table-block__next')
      .on('click', tableNext);
    $(this)
      .siblings('.table-block__prev')
      .on('click', tablePrev);
  }

  /**
   * Attaches the table behavior to table components.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *  Binds events to tables
   */
  Drupal.behaviors.table = {
    attach(context) {
      $(window, context).on('load resize', tableScrollableCheck);
      $(once('bmc-table', 'table', context)).each(tableInit);
    },
  };
};

table(jQuery);
