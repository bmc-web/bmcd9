/**
 * @file
 * Provides events list interactions and accessibility.
 */

const events = $ => {
  function inview() {
    $('.events__filters').waypoint(
      function(direction) {
        if (direction === 'down') {
          $(this.element)
            .find('.view-header')
            .addClass('is-stuck');
        } else {
          $(this.element)
            .find('.view-header')
            .removeClass('is-stuck');
        }
      },
      {
        offset: 'bottom-in-view',
      },
    );
  }

  function checkboxes() {
    const checkedCount = $('input[type="checkbox"]:checked').length;
    $('.open-events-filter span').text(checkedCount);
    if (checkedCount > 0) {
      $('.view-header').addClass('visible');
    } else {
      $('.view-header').removeClass('visible');
    }
  }

  function inputValue() {
    const inputField = $('#views-exposed-form-events-listing .bef-datepicker');
    window.addEventListener('load', function() {
      inputField.attr('placeholder', 'MM/DD/YYYY');
      inputField.attr('value', '');
      inputField.css('color', '#949494');
    });
  }

  function openFilters() {
    $(this).toggleClass('expanded');
    $(this)
      .next()
      .slideToggle();

    if ($(this).hasClass('expanded')) {
      $(this).attr('aria-expanded', 'true');
    } else {
      $(this).attr('aria-expanded', 'false');
    }
  }

  function clearButton() {
    const params = window.location.search;
    if (params != '') {
      $('input[value="Clear"]').attr('disabled', 'true');
      params.split('&').forEach(a => {
        const value = a.split('=')[1];
        if (value !== '' && value !== 'All' && value !== 'today') {
          $('input[value="Clear"]').attr('disabled', null);
        }
      });
    } else {
      $('input[value="Clear"]').attr('disabled', 'true');
    }
  }

  function setAria() {
    if ($(window).width() < 700) {
      $('.open-events-filter').attr('aria-expanded', 'false');
      $('.open-events-filter').removeClass('expanded');
    }
  }

  /**
   *
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */
  Drupal.behaviors.events = {
    attach(context) {
      inview();
      checkboxes();
      $(once('bmc-events-checkboxes', 'input[type="checkbox"]', context)).on('change', checkboxes);
      $(once('bmc-events-openfilters', '.open-events-filter', context)).on('click', openFilters);
      inputValue();
      clearButton();
      setAria();

      $(document).ajaxComplete(function() {
        $('.events__filters').show();
      });
    },
  };
};

events(jQuery);

const formCal = $ => {
  Drupal.behaviors.formCal = {
    attach(context) {
      // $(document).on('load', function() {
      if ($('.js-form-item-date').length) {
        $(once('bmc-js-form-item-date', '.js-form-item-date', context)).after(
          '<div id="liveRegion2" role="log" aria-live="assertive" aria-atomic="true" aria-relevant="additions" class="visually-hidden"></div>',
        );
        // Select the node that will be observed for mutations
        const targetNode = document.querySelector('body');

        // Options for the observer (which mutations to observe)
        const config = { attributes: true, childList: true, subtree: true };

        // Callback function to execute when mutations are observed
        const callback = function(mutationsList, observer) {
          if ($('.ui-state-hover').html() !== undefined) {
            const message = ` ${$('.ui-state-hover').html()} ${$(
              '.ui-datepicker-month',
            ).html()} ${$('.ui-datepicker-year').html()}`;

            if (message !== $('#liveRegion2').text()) {
              $('#liveRegion2').text(message);
            }
          }
        };

        // Create an observer instance linked to the callback function
        const observer = new MutationObserver(callback);

        // Start observing the target node for configured mutations
        observer.observe(targetNode, config);
      }
      // });
    },
  };
};

formCal(jQuery);
