/**
 * @file
 * Provides interactions and accessibility.
 */

const stat = $ => {
  function wrapSpanPuncutation() {
    $(this).text(
      $(this)
        .text()
        .trim(),
    );
    $(this).html(
      $(this)
        .text()
        .replace(/\W$/, match => {
          return `<span>${match}</span>`;
        }),
    );
  }

  /**
   *.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */
  Drupal.behaviors.stat = {
    attach(context) {
      $('.fstat__num', context).each(wrapSpanPuncutation);
    },
  };
};

stat(jQuery);
