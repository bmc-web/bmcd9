(function($) {
  const app = new Vue({
    el: '#office-service-app',
    delimiters: ['${', '}'],
    data() {
      return {
        searchTerms: '',
        activeLetter: 'all',
        offices: [],
        activeLetters: [],
      };
    },
    created() {
      // Get data from endpoint
      $.getJSON('/api/ofc-ser/all', json => {
        const offices = json.map(item => {
          // merge all fields into a single search field
          item.search = Object.values(item).join(' ');
          return item;
        });
        this.offices = offices;
        this.updateActiveLetters(this.offices);
      });
    },
    mounted() {
      // Update filters based on query params on page load
      function getQueryVariable() {
        const query = window.location.search.substring(1);
        const vars = query.split('&');
        const pairs = [];
        for (let i = 0; i < vars.length; i++) {
          const title = vars[i].split('=')[0];
          pairs.push({ [title]: vars[i].split('=')[1] });
        }
        return pairs;
      }
      const params = getQueryVariable();

      if (params.length > 1) {
        // Set search filter
        if (params[0].query !== '') {
          this.searchTerms = decodeURIComponent(params[0].query);
          this.$refs.searchInput.value = this.searchTerms;
        }
        // Set alpha filter
        if (params[1].letter !== '') {
          this.activeLetter = params[1].letter;
          $('.alpha-search button')
            .removeClass('active')
            .attr('aria-current', 'false');
          $('.alpha-search button')
            .filter(function() {
              return $(this).text().toLowerCase() === params[1].letter;
            })
            .addClass('active')
            .attr('aria-current', 'true');
        }
      }
    },
    computed: {
      // Get Filtered Results to display
      filteredResults() {
        // Setup functions to check if selected filters match the office items

        // Check if matches alpha filter
        // param item -> item to check filter on
        const matchLetter = item => {
          if (
            item.office_title.charAt(0).toLowerCase() ===
            this.activeLetter.toLowerCase()
          ) {
            return true;
          }
          return false;
        };
        // Check if matches search filter
        // param item -> item to check filter on
        const matchSearch = item => {
          const lowerItemSearchContent = item.search.toLowerCase();
          const lowerSearchTerm = this.searchTerms.toLowerCase();
          if (lowerItemSearchContent.indexOf(lowerSearchTerm) !== -1) {
            return true;
          }
          return false;
        };

        // Only do the amount of filtering necessary, based on 'all' values
        if (this.activeLetter === 'all' && this.searchTerms === '') {
          // if no filters are active, return all offices
          return this.offices;
        }

        return this.offices.filter(item => {
          if (this.activeLetter === 'all') {
            return matchSearch(item);
            // eslint-disable-next-line no-else-return
          } else if (this.searchTerms === '') {
            return matchLetter(item);
          } else {
            return matchSearch(item) && matchLetter(item);
          }
        });
      },
      // Helper to get all letters of the alphabet instead of hardcoding
      letters() {
        const alphaArr = [];
        for (let i = 0; i < 26; i++) {
          const letter = (i + 10).toString(36);

          // Add disabled property, check for letter in activeLetters array
          let isDisabled = true;
          if (this.activeLetters.indexOf(letter) > -1) {
            isDisabled = false;
          }
          alphaArr.push({ letter, disabled: isDisabled });
        }
        return alphaArr;
      },
    },
    watch: {
      // Update URL params whenever filteredResults Update
      filteredResults() {
        window.history.pushState(
          null,
          document.title,
          `?query=${this.searchTerms}&letter=${this.activeLetter}`,
        );
        if (this.searchTerms !== '') {
          this.updateActiveLetters(this.filteredResults);
        }
      },
    },
    methods: {
      // Executes on click of an alpha filter, updates this.activeletter
      // Actual filtering logic is in computed property FilteredResults
      setAlphaFilter(filter, event) {
        this.activeLetter = filter;
        $('.alpha-search button')
          .removeClass('active')
          .attr('aria-current', 'false');
        $(event.target)
          .addClass('active')
          .attr('aria-current', 'true');
      },
      // Executes on submit of search form, updates this.searchTerms
      // Actual filtering logic is in computed property FilteredResults
      updateSearch() {
        this.searchTerms = this.$refs.searchInput.value;
      },
      clearFilters() {
        this.searchTerms = '';
        this.$refs.searchInput.value = '';
        $('.alpha-search button').removeClass('active');
        $('.alpha-search button.all').addClass('active');
        this.activeLetter = 'all';
        this.updateActiveLetters(this.offices);
      },
      updateActiveLetters(arrToUse) {
        const activeLetters = [];
        arrToUse.forEach(a => {
          const firstLetter = a.office_title.charAt(0);
          if (activeLetters.indexOf(firstLetter) === -1) {
            activeLetters.push(firstLetter.toLowerCase());
          }
        });
        this.activeLetters = activeLetters;
      },
    },
  });
})(jQuery);
