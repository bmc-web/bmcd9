<?php

/**
 * @file
 * Local simplesamlphp.config.local.php file.
 */

$config['store.type'] = 'sql';
$config['store.sql.dsn'] = 'mysql:host=database;dbname=main_db;port=3306';
$config['store.sql.username'] = 'main_user';
$config['store.sql.password'] = 'main_pass';
$config['store.sql.prefix'] = 'SimpleSAMLphp';

// Force server port to 443 with HTTPS environments when behind a load balancer
// which is a requirement for SimpleSAML with OKTA when providing a
// redirect path.
// @see https://github.com/simplesamlphp/simplesamlphp/issues/450
if ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] === 'on') {
  $_SERVER['SERVER_PORT'] = 443;
}
