describe('Site Editor password protected', function() {
	context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
		beforeEach(() => {
			cy.viewport(1280, 720);
		});


		it('SITE EDITOR check for auth, goto protected pages, edit protected page password', function() {
      cy.loginViaUi('iftest_site-editor', 'LightSlateGray3348!');
      cy.visit('/user');
			cy.url().should('include', '/user/196');
			cy.get('.header__logo img')
				.should('be.visible');

        cy.visit('/user');
        // click config page menu item
        cy.adminToolbarSelect('protected-pages-list-pages');


  // visit protected pages
        cy.get('h1.page-title')
          .should('include.text', 'Protected Pages');
        cy.get('tbody tr').eq(8).within(() => {
          cy.get('li.edit-protected-page').click();
        });
        cy.wait(3000);
      cy.get('h1.page-title')
        .should('include.text', 'Edit Protected Page');
 // edit password so anon protected password test will pass
      cy.get('.form-item-password-pass1 input').type('lantern');
      cy.get('.form-item-password-pass2 input').type('lantern');
      cy.get('.password-confirm-message').contains( 'yes');
      cy.get('input#edit-submit').click();

    });

    it('ANON password protected page - correct password', function() {
      cy.visit('/inside/offices-services/alumnaei-relations-development/volunteering/alumnaei-admissions-representatives/aar-resources-tools');
      cy.get('#block-bmc-d9-content')
        .should('not.include.text', 'AAR Resources and Tools');
      cy.get('form.protected-pages-enter-password').within(() => {
        cy.get('.protected_pages_description')
          .should('include.text', 'The page you are trying to view is password protected. Please enter the password below to proceed.');
        cy.get('input#edit-password')
          .focus()
          .type('lantern');
        cy.get('input#edit-submit')
          .focus()
          .click();
      }); // END protected page form within
      cy.get('h1.page-title')
        .should('include.text', 'AAR Resources and Tools');
    });

  });
});

