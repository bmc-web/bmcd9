describe('Site Editor - Daily Digest report generate', function() {
	context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
		beforeEach(() => {
			cy.viewport(1280, 720);
      cy.loginViaUi('iftest_site-editor', 'LightSlateGray3348!');
		});

		it('check for auth', function() {
      cy.visit('/user');
			cy.url().should('include', '/user/196');
			cy.get('.header__logo img')
				.should('be.visible');
		});

    it('create program with Feature Large, Feature Medium, Story 1up and Vertical Tab', function() {
      cy.visit('/node/add/program');
      // Start new program create
      cy.get('h1.page-title')
        .should('include.text', 'Create Program');
      // ENTER title
      cy.get('input#edit-title-0-value').type('001 Test Program');
      cy.wait(1000);
      // IMAGES
      cy.get('details#edit-group-images')
        .should('contain.text', 'Images')
        .click();
      cy.wait(1000);
       // DETAIL IMAGE
      cy.get('.field--name-field-prog-img-detail input.button.form-submit').eq(0)
        .click();
      cy.wait(3000);
      // enter entity-browser form iframe
      cy.modalImageLibraryChoice('media41');

       // LISTING IMAGE
      cy.get('.field--name-field-prog-img-listing input.button.form-submit').eq(0)
        .click();
      cy.wait(3000);
      // enter entity-browser form iframe
      cy.modalImageLibraryChoice('media26');
      // INTRODUCTION
      cy.get('.field--name-field-prog-intro textarea').type('This is the intro for this test program.');

      // PROGRAM INTEREST
      cy.get('input#edit-field-prog-interest-4061').click({force:true});

      // PROGRAM TYPE
      cy.get('input#edit-field-prog-type-1621').click({force:true});

      // FLEX CONTENT:

      // add Feature Medium flex item
      // open flex item list
      cy.flexItemSelect('prog-body','feat-md');

      // Feature Medium Heading
      cy.get('.field--name-field-p-feat-md-head input').type('Feature Medium Test - Heading');

      // Feature Medium image
      cy.get('.field--name-field-p-feat-md-img input.button.form-submit').eq(0)
        .click();
      cy.wait(3000);
      // enter entity-browser form iframe
      cy.modalImageLibraryChoice('media56');

      // Feature Medium Intro
      cy.get('.field--name-field-p-feat-md-intro textarea').type('Medium feature intro. Ta-da!');

      // Medium Feature CTA
      cy.get('.field--name-field-p-feat-md-cta input[data-drupal-selector="edit-field-prog-body-0-subform-field-p-feat-md-cta-0-uri"]')
        .type('Full Catering Policy (16046)');
      cy.get('.field--name-field-p-feat-md-cta input[data-drupal-selector="edit-field-prog-body-0-subform-field-p-feat-md-cta-0-title"]')
        .type('Green Week!');

      cy.childFlexItemSelect('feat-md-item','fquote');
      cy.get('div.field--name-field-p-fquote-quote input').type('to be or not to be, that is a hairball');
      cy.get('div.field--name-field-p-fquote-attr input').type('One Very Hairy Cat');

      // Feature Large

      // add Feature Large flex item
      // open flex item list
      cy.flexItemSelect('prog-body','feat-lg');

      // Feature Large Heading
      cy.get('.field--name-field-p-feat-lg-head input').type('Feature Large Test - Heading');

      // Feature Large Intro
      cy.get('.field--name-field-p-feat-lg-intro textarea').type('Feature Large Intro! Ta-da!');

      // Feature Large image
      cy.get('.field--name-field-p-feat-lg-img input.button.form-submit').eq(0)
        .click();
      cy.wait(3000);
      // enter entity-browser form iframe
      cy.modalEntityLibraryChoice('media_image_browser', 'media-image-browser', 'media26');

      // Feature Large CTA
      cy.get('.field--name-field-p-feat-lg-cta input[data-drupal-selector="edit-field-prog-body-1-subform-field-p-feat-lg-cta-0-uri"]')
        .type('Step Sing at Reunion (20566)');
      cy.get('.field--name-field-p-feat-lg-cta input[data-drupal-selector="edit-field-prog-body-1-subform-field-p-feat-lg-cta-0-title"]')
        .type('Step Sing!');

         // add Dropdown Link Item
      cy.get('.field--name-field-p-feat-lg-drop input.field-add-more-submit').click();
      cy.wait(3000);
      cy.get('.field--name-field-p-fdrop-title input').type('Campus Trees');
      cy.get('.field--name-field-p-fdrop-link .form-item-field-prog-body-1-subform-field-p-feat-lg-drop-0-subform-field-p-fdrop-link-0-uri input')
        .type('Beeches and Cherry Trees on Campus (36196)');
      cy.get('.form-item-field-prog-body-1-subform-field-p-feat-lg-drop-0-subform-field-p-fdrop-link-0-title input')
        .type('Some really cool trees');

         // add Statistic
      cy.childFlexItemSelect('feat-lg-item','fstat');
      cy.get('div.field--name-field-p-fstat-num input').type('1');
      cy.get('div.field--name-field-p-fstat-desc input').type('Shady Campus');

      // Story 1up

      // add Story 1up flex item
      // open flex item list
      cy.flexItemSelect('prog-body','story-1-up');
      cy.get('.field--name-field-p-story-manual-item input.button.form-submit').click();
      cy.wait(5000);
      cy.modalEntityLibraryChoice('story_nodes', 'story-nodes', 'node50546');
      cy.wait(3000);
      // Vertical Tab

      // add Vertical Tab flex item
      // open flex item list
      cy.flexItemSelect('prog-body','vert-tab');
      cy.get('.field--name-field-p-vert-tab-heading input').type('Vertical Tab Awesomeness');

      // Vertical Tab intro wysiwyg
      cy.get('.field--name-field-p-vert-tab-intro');
      cy.getIframeBody('[id*=field-p-vert-tab-intro-0-value] iframe.cke_wysiwyg_frame')
        .type('This is the intro for the awesome vertical tab! Wa-Hoo!');
      cy.wait(3000);

      // Vertical Tab image
      cy.get('.field--name-field-p-vert-tab-bkgd-image input.button.form-submit')
        .click();
      cy.wait(5000);

      // enter entity-browser form iframe
      cy.modalImageLibraryChoice('media56');

      // First Vertical Tab Item
        // Title
      cy.get('.field--name-field-vert-panel-item-title input[data-drupal-selector*=subform-field-p-vert-tab-items-0-subform-field-vert-panel-item-title-0-value]')
        .type('Vert Tab #1');
        // Detail
      cy.get('.field--name-field-vert-panel-item-detail');
      cy.getIframeBody('[id*=subform-field-p-vert-tab-items-0-subform-field-vert-panel-item-detail-0-value] iframe.cke_wysiwyg_frame')
        .type('Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. ' +
          'Suspendisse urna nibh viverra non semper suscipit posuere a pede.\n' +
          '    // Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.');
      cy.wait(3000);

      // add Second Vertical Tab Item
      cy.get('.field--name-field-p-vert-tab-items .paragraphs-add-wrapper input.field-add-more-submit').click();
      cy.wait(3000);

      // Second Vertical Tab Item
      // Title
      cy.get('.field--name-field-vert-panel-item-title input[data-drupal-selector*=subform-field-p-vert-tab-items-1-subform-field-vert-panel-item-title-0-value]')
        .type('Vert Tab #2');
      // Detail
      cy.get('.field--name-field-vert-panel-item-detail');
      cy.getIframeBody('[id*=subform-field-p-vert-tab-items-1-subform-field-vert-panel-item-detail-0-value] iframe.cke_wysiwyg_frame')
        .type('this is just a link');
      // add link to text in Detail wysiwyg
      cy.getIframeBody('[id*=subform-field-p-vert-tab-items-1-subform-field-vert-panel-item-detail-0-value] iframe.cke_wysiwyg_frame')
        .find('p').type('{selectall}');
      cy.get('a.cke_button__drupallink').eq(2).click();
      cy.wait(3000);
      cy.get('div#drupal-modal form.editor-link-dialog').within(() => {
        cy.get('div.form-type-linkit input.form-text').type('/node/59246');
        cy.get('details[data-drupal-selector=edit-advanced]').click();
        cy.get('input[data-drupal-selector=edit-attributes-class]').type('cta--button--secondary');
      }); // end linkit form within
// save link for text in wysiwyg
      cy.modalButtonClick('Save');

      // add Third Vertical Tab Item
      cy.get('.field--name-field-p-vert-tab-items .paragraphs-add-wrapper input.field-add-more-submit').click();
      cy.wait(3000);

      // Third Vertical Tab Item
      // Title
      cy.get('.field--name-field-vert-panel-item-title input[data-drupal-selector*=subform-field-p-vert-tab-items-2-subform-field-vert-panel-item-title-0-value]')
        .type('Vert Tab #3');
      // Detail
      cy.get('.field--name-field-vert-panel-item-detail').eq(2).within(() => {
        cy.get('a.cke_button__source').click();
        cy.get('textarea.cke_source')
          .type('<h2>Paragraphs</h2><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros</p><p>Nullam malesuada erat ut turpis. Suspendisse urna nibh viverra non semper suscipit posuere a pede.Donec nec justo eget felis facilisis fermentum.</p><p> Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>');
      }); // end vert tab detail form field within

      // save node
      cy.saveNode('001 Test Program');

      // Test Page Contents
      cy.get('nav.breadcrumb li:last')
        .should('include.text', 'Majors, Minors, and Concentrations');
      cy.get('.field--name-field-p-fquote-attr').contains('One Very Hairy Cat');
      cy.get('.feat-lg__intro p').contains('Feature Large Intro! Ta-da!');
      cy.get('.story-1-up__cta a').should('have.attr', 'href', '/stories/joi-dallas');
      cy.get('.vert_tab__intro').contains('This is the intro for the awesome vertical tab! Wa-Hoo!');
      cy.get('.vert_tab__items').within(() => {
        cy.get('.vert_tab__image')
          .should('be.visible');
        cy.get('li button.vert-tabs-list.active').contains('Vert Tab #1');
        cy.get('.vert-tab-detail.visible').should('include.text', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.');
        cy.get('li.vert_tab__list-item button.vert-tabs-list').eq(1)
          .should('not.have.class', 'active')
          .click();
        cy.wait(1000);
        cy.get('li.vert_tab__list-item button.vert-tabs-list').eq(1)
          .should('have.class', 'active');
        cy.get('.vert-tab-detail').eq(1)
          .should('have.class', 'visible');
        cy.get('.vert-tab-detail.visible').should('include.text', 'this is just a link')
          .and('include.html', '<a class="cta--button--secondary" href="/node/59246">');
      });  // end vert_tab__items within
    });
    it('checks listing pg for new program, edits new program to unpub then verifies that on listing pg', function() {
      cy.visit('/academics/majors-minors-concentrations?query=&type=&interests=');
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('h1')
        .should('contain.text', 'Majors, Minors, and Concentrations');
      cy.countItemCompare('.program__result', '.program__results-count');
      cy.get('.program__result h2 a').contains('001 Test Program')
        .focus()
        .click();
      cy.wait(1000);
      // click edit tab to edit node
      cy.get('.tabs.primary li a').eq(1)
        .focus()
        .click();
      cy.wait(2000);
      // unpublish node
      cy.get('input#edit-status-value').uncheck();
      // save node
      cy.saveNode('001 Test Program');
      cy.wait(2000);
      cy.get('nav.breadcrumb ol li:last-of-type')
        .should('include.text', 'Majors, Minors, and Concentrations')
        .click();
      cy.get('h1')
        .should('contain.text', 'Majors, Minors, and Concentrations');
      cy.countItemCompare('.program__result', '.program__results-count');
    });
  });
});

