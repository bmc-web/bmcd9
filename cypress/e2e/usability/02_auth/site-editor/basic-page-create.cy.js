describe('Site Editor - Basic Page create', function() {
  context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
    beforeEach(() => {
      cy.viewport(1280, 720);
      cy.loginViaUi('iftest_site-editor', 'LightSlateGray3348!');
    });

    it('check for auth', function() {
      cy.visit('/user');
      cy.url().should('include', '/user/196');
      cy.get('.header__logo img')
        .should('be.visible');
    });

    it('goto content, create page', function() {
      cy.visit('/user');
      // click add basic page menu  item
      cy.adminToolbarSelect('admin-toolbar-tools-extra-linksnode-add-page');

      // Start new page create
      cy.get('h1.page-title')
        .should('include.text', 'Create Basic page');
      // ENTER title
      cy.get('input#edit-title-0-value').type('001 Test Page');
// CREATE Hero Image
      // open image modal window
      cy.get('input#edit-field-page-hero-img-entity-browser-entity-browser-open-modal').click();
      cy.wait(3000);

      // enter entity-browser form iframe
      // UPLOAD IMAGE
      cy.modalImageLibraryUpload('media', '001TEST-image', 'cypress/fixtures/images/drupal-site-editor.jpeg', 'site-editor cartoon');




      // checks for hero img
      cy.wait(3000);
      cy.get('.field--name-field-media-image img')
        .should('have.attr', 'alt', 'site-editor cartoon');
      // Intro wysiwyg
      cy.get('#cke_edit-field-page-intro-0-value');
      cy.getIframeBody('#cke_edit-field-page-intro-0-value iframe.cke_wysiwyg_frame').type('This is my Test Intro');
      cy.wait(3000);

      // add wysiwyg flex item
      cy.flexwysiwyg();


      // intereact with wysiwyg iframe
      cy.getIframeBody('.field--name-field-wysiwyg-body iframe.cke_wysiwyg_frame').type('This is my Test WYSIWYG.\n' +
        '\n' +
        'Why do we test these things?\n' +
        '\n' +
        'Because we can!\n' +
        '\n');
      cy.get('.field--name-field-wysiwyg-body').within(() => {
        cy.get('a.cke_button__image').click();
      }); // end wysiwyg body within
      cy.wait(3000);
// WYSIWYG IMAGE
      // opens wysiwyg image entity browser
      // UPLOAD IMAGE
      cy.modalImageLibraryUpload('wysiwyg', '002TEST-image', 'cypress/fixtures/images/drupal-goat-02.jpeg', 'baby goat 2');

      // checks img embed form for alt text
      cy.wait(3000);
      cy.get('div#drupal-modal form.entity-embed-dialog.entity-embed-dialog-step--embed').within(() => {
        cy.get('div#data-entity-embed-display-settings-wrapper input.form-text')
          .should('have.attr', 'placeholder', 'baby goat 2');
      }); // end embed form within
      // submit embed form for wysiwyg
      cy.modalButtonClick('Embed');

      // check wysiwyg iframe for embedded image with correct alt text
      cy.getIframeBody('.field--name-field-wysiwyg-body iframe.cke_wysiwyg_frame')
        .find('div.cke_widget_wrapper .cke_widget_element img')
        .should('have.attr', 'alt', 'baby goat 2');

  // CAROUSEL
      // create carousel
      // open flex item list
      cy.flexItemSelect('body','carousel');

      // create slide

      cy.get('.field--name-field-p-slide-title').eq(0)
        .type('Test Slide 1');
      // upload slide image
      // open image modal window
      cy.get('.field--name-field-p-slide-image input.button.form-submit')
        .click();
      cy.wait(3000);
      // enter entity-browser form iframe
      // UPLOAD IMAGE
      cy.modalImageLibraryUpload('media', '003TEST-image', 'cypress/fixtures/images/drupal-mountain-01.jpeg', 'mountain 1');

      cy.get('.field--name-field-p-slide-cta').eq(0).within(() => {
        cy.autocompleteInputField('Lantern Shuttle (17911)', 'Test link 1');

      }); // end cta form within



// add second slide
      cy.get('.field--name-field-p-carousel-slide .paragraphs-add-wrapper input.field-add-more-submit').click();
      cy.wait(3000);

      cy.get('.field--name-field-p-slide-title').eq(1)
        .type('Test Slide 2');
      // select slide image from library
      // upload slide image
      // open image modal window
      cy.get('.field--name-field-p-slide-image  input.button.form-submit').eq(2)
        .click();
      cy.wait(3000);
      // enter entity-browser form iframe
      //select image from library
      cy.modalImageLibraryChoice('media36');



      cy.get('.field--name-field-p-slide-cta').eq(1).within(() => {
        cy.autocompleteInputField('First Fridays (6781)', 'Test link 2');

      }); // end cta form within

// CALLOUT
// create callout
      // add callout flex item
      // open flex item list
      cy.flexItemSelect('body','callout');

// Callout Heading
      cy.get('.field--name-field-p-callout-heading input').type('Callout Test - Heading');
// Callout image
      cy.get('.field--name-field-p-callout-image input.button.form-submit')
        .click();
      cy.wait(3000);
      // enter entity-browser form iframe
      cy.modalImageLibraryChoice('media56');


// callout wysiwyg
      cy.get('.field--name-field-p-callout-body');
      cy.getIframeBody('.field--name-field-p-callout-body iframe.cke_wysiwyg_frame')
        .type('This is my Test callout body. Writing more than one sentence. Ta-da!');
      cy.wait(3000);
// callout cta

      // cta 1
      cy.get('.field--name-field-p-callout-cta tbody tr').eq(0).within(() => {
        cy.autocompleteInputField('The Great Tradition (36206)', 'Test Callout CTA 1');

      }); // end cta form within
      // cta 2
      cy.get('.field--name-field-p-callout-cta tbody tr').eq(1).within(() => {
        cy.autocompleteInputField('Digital Collections (18316)', 'Test Callout CTA 2');
        cy.get('summary').contains('Advanced settings')
          .click();
        cy.wait(1000);
        cy.get('option[value="callout__cta--secondary"]')
          .click({force:true});

      }); // end cta form within
//
      // FLEX CONTENT:
      // // use the code: flexItemSelect({contentType},{flexItem});
      //
      // use the code: childFlexItemSelect({childItem},{itemChoice}); in test where there is a child select dropdown to add more
      //
//
// COLUMNS
//
// EMBED
//
// FEATURE QUOTE
//
// IMAGE DETAIL LARGE
//
// NEWS CONTACT

// save node
      cy.saveNode('001 Test Page');
    });


  });
});

