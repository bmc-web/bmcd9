describe('Site Editor', function() {
	context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
		beforeEach(() => {
			cy.viewport(1280, 720);
      cy.loginViaUi('iftest_site-editor', 'LightSlateGray3348!');
		});

		it('check for auth', function() {
      cy.visit('/user');
			cy.url().should('include', '/user/196');
			cy.get('.header__logo img')
				.should('be.visible');
		});

      it('goto config pages, edit config page', function() {
        cy.visit('/user');
        // click config page menu item
        cy.adminToolbarSelect('entity-config-pages-collection-tab');


  // See config pages list
        // Then I should see h1.page-title contains the text "Config pages library”
        cy.get('h1.page-title')
          .should('include.text', 'Config pages library');

// And then I should see the text “Bulletin Current Issue” in a td
// And then I should see li.dropbutton-action
// // And then I should see a with href "/admin/structure/config_pages/bulletin_current_issue/edit"
// // And the a should contain the text “Edit”
// And when I click on that a I should be on URL admin/structure/config_pages/bulletin_current_issue/edit
        cy.get('td').contains('Bulletin Current Issue')
          .siblings().find('li a')
          .should('have.attr', 'href', '/admin/structure/config_pages/bulletin_current_issue/edit')
          .and('have.text', 'Edit').click();
        cy.wait(1000);
// And then I should see div#edit-field-cp-bulletin-current-issue--description with the text “The selected Bulletin
// Issue taxonomy term must have a value in it's URL field, or the current issue will not update.”
        cy.get('#edit-field-cp-bulletin-current-issue--description')
          .should('include.text', 'The selected Bulletin Issue taxonomy term must have a value in it\'s URL field, or the current issue will not update.');

      });

    it('goto blocks layout pages, adds block, edits block', function() {
      cy.visit('/user');
      // click blocks menu item
      cy.adminToolbarSelect('block-admin-display');

      // See Block layout list
      // Then I should see h1.page-title contains the text "Block layout” /admin/structure/block
      cy.get('h1.page-title')
        .should('include.text', 'Block layout');
      cy.url()
        .should('include', 'admin/structure/block');

      // And then I should see the table#blocks tbody tr td contains the text “Content bottom”
      cy.get('tr.region-title-content_bottom')
        .should('include.text', 'Content bottom');

      // And I should see an li.tabs__tab that contains the text “Custom block library”
      // And I should click it
      cy.adminToolbarSelect('admin-toolbar-tools-extra-linksentity-block-content-collection');

      // And the URL should be /admin/content/block
      cy.url()
        .should('include', 'admin/content/block');

  // And I should see edit button for first custom block
      cy.get('td.views-field-operations').eq(0).within(() => {
      cy.get('ul li').eq(0)
        .should('have.text', 'Edit');
        }); // END td.views-field-operations within

      // And I should see a ul.action-links li a with an href of /block/add?destination=/admin/content/block
      // And I should click it
      cy.get('ul.action-links li a')
        .should('have.attr', 'href', '/block/add?destination=/admin/content/block')
        .click();

      //   And the URL should be block/add?destination=/admin/structure/block/block-content
      cy.get('h1.page-title')
        .should('include.text', 'Add content block');
      cy.url()
        .should('include', 'block/add?destination=/admin/content/block');

      // And I should see a ul.adminlist with three li
      cy.get('ul.admin-list li').each((item, index, list) => {
        expect(list).to.have.length(3);
      }); // END item each

      // And the 2nd li should contain the text “In Context Block”
      // And I should click on it
      cy.get('ul.admin-list li a').eq(1)
        .should('include.text', 'In Context Block')
        .click();

      // And the URL should be block/add/in_context_block?destination=/admin/structure/block/block-content
      cy.get('h1.page-title')
        .should('include.text', 'Add In Context Block content block');
      cy.url()
        .should('include', 'block/add/in_context_block?destination=/admin/content/block');
    });

  });
});

