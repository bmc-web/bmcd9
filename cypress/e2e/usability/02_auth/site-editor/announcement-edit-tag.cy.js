describe('Site Editor - edit Announcement, add 4up and check tag page', function() {
  context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
    beforeEach(() => {
      cy.viewport(1280, 720);
      cy.loginViaUi('iftest_site-editor', 'LightSlateGray3348!');
    });

    it('check for auth', function() {
      cy.visit('/user');
      cy.url().should('include', '/user/196');
      cy.get('.header__logo img')
        .should('be.visible');
    });

    Cypress._.times(5, (k) => {
      it(`adding LITS tag ${k + 1} / 5`, () => {
        cy.visit('/admin/content?title=&type=anncmnt&status=1&uid&label=&main_theme=All&field_section_target_id=All&items_per_page=50&page=4');
        cy.get('table.views-table tr').eq(2).within(() => {
          cy.get('li.edit a')
            .click();
        }); // END within

        cy.wait(2000);
        cy.url()
          .should('include', 'edit?');
        // changes expiry date to far in future
        cy.get('#edit-field-anncmnt-nolonger-timely-0-value-date').type('2048-06-22');
        // selects option value from a select ID
        cy.get('select#edit-field-tags-0-target-id')
          .select('4326');
        cy.get('input#edit-submit').click();
      }); // END it
    }); // END times loop

    it('check announcements tagged listing page', function() {
      cy.visit('/inside/latest/announcements?tags=4326');
      cy.get('h1.page-title')
        .should('contain.text', 'LITS Announcements');
      cy.get('.anncmnt--teaser')
        .should('have.length.at.least', 5);
      cy.get('.anncmnt_teaser--content').eq(0).should('be.visible');
      cy.get('.anncmnt_teaser--link').eq(0).should('be.visible');
      cy.get('.anncmnt_teaser--title').eq(0).should('be.visible');
      cy.get('.anncmnt_teaser--blurb').eq(0).should('be.visible');
      cy.get('.anncmnt_teaser--date').eq(0).should('be.visible');

      cy.anyPagerer('.view-id-announcements', 'Announcements');
    });

    it('create basic page with Announcement 4up', function() {
      cy.visit('/node/add/page');

      cy.get('h1.page-title')
        .should('include.text', 'Create Basic page');
      // ENTER title
      cy.get('input#edit-title-0-value').type('002 Test Page 4up');
      cy.wait(2000);
      // create announcement 4up
      // open flex item list
      cy.flexItemSelect('body','anncmnt-4-up');

      cy.get('[data-drupal-selector="edit-field-body-0-subform-field-p-anncmnt-head-0-value"]')
        .type('Test Announcement 4up - Tag LITS');
      // selects option value from a select ID
      cy.get('[data-drupal-selector="edit-field-body-0-subform-field-p-anncmnt-tag-0-target-id"]')
        .select('4326');
// save node
      cy.saveNode('002 Test Page 4up');
      cy.get('.anncmnt_4_up__head').should('include.text', 'Test Announcement 4up - Tag LITS');
      cy.get('.anncmnt--teaser')
        .should('have.length', 4);
      cy.get('.anncmnt_teaser--content').eq(0).should('be.visible');
      cy.get('.anncmnt_teaser--link').eq(0).should('be.visible');
      cy.get('.anncmnt_teaser--title').eq(0).should('be.visible');
      cy.get('.anncmnt_teaser--blurb').eq(0).should('be.visible');
      cy.get('.anncmnt_teaser--date').eq(0).should('be.visible');

      cy.get('.anncmnt_4_up__more_link a.cta--button--primary')
        .should('have.text', 'More Announcements').and('have.attr', 'href', '/inside/latest/announcements?tagged=4326');
    });

  });
});

