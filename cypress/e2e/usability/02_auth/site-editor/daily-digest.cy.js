describe('Site Editor - Daily Digest report generate', function() {
	context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
		beforeEach(() => {
			cy.viewport(1280, 720);
      cy.loginViaUi('iftest_site-editor', 'LightSlateGray3348!');
		});

		it('check for auth', function() {
      cy.visit('/user');
			cy.url().should('include', '/user/196');
			cy.get('.header__logo img')
				.should('be.visible');
		});

    it('goto daily digest, filter for results', function() {
      cy.visit('/admin/reports/daily-digest');
    });

  });
});

