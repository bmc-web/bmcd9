describe('Site Editor - Homepage create', function() {
  context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
    beforeEach(() => {
      cy.viewport(1280, 720);
      cy.loginViaUi('iftest_site-editor', 'LightSlateGray3348!');
    });

    it('check for auth', function() {
      cy.visit('/user');
      cy.url().should('include', '/user/196');
      cy.get('.header__logo img')
        .should('be.visible');
    });

    it('create homepage via admin toolbar', function() {
      cy.visit('/user');
      // click add basic page menu  item
      cy.adminToolbarSelect('admin-toolbar-tools-extra-linksnode-add-home');

// Start new homepage create
      cy.get('h1.page-title')
        .should('include.text', 'Create Homepage');
      // ENTER title
      cy.get('input#edit-title-0-value').type('001 Test Homepage');
// CREATE Hero Slide
      // open slide
      cy.get('input#field-home-carousel-slide-homepage-hero-add-more').click();
      cy.wait(3000);

      // create slide

      cy.get('.form-item-field-home-carousel-0-subform-field-p-home-slide-title-0-value input')
        .type('Test Hero Slide 1');
      cy.get('.form-item-field-home-carousel-0-subform-field-p-home-slide-supertitle-0-value input')
        .type('Supertitle One');
      // upload slide image
      // open image modal window
      cy.get('.field--name-field-p-home-slide-img input.button.form-submit')
        .click();
      cy.wait(3000);

      // UPLOAD IMAGE
      cy.modalImageLibraryUpload('media', '003HP-test-image', 'cypress/fixtures/images/drupal-mountain-02.jpeg', 'mountain 2');

      // ADD CTA
      cy.get('.field--name-field-p-home-slide-cta').eq(0).within(() => {
        cy.autocompleteInputField('Lantern Shuttle (17911)', 'Test link 1');

      }); // end cta form within


      cy.wait(4000);
// CALLOUT
// create callout
      // add callout flex item
      // open flex item list
      cy.flexItemSelect('home-body','callout');

// Callout Heading
      cy.get('.field--name-field-p-callout-heading input').type('Callout Test - Heading');
// Callout image
      cy.get('.field--name-field-p-callout-image input.button.form-submit')
        .click();
      cy.wait(3000);
      // enter entity-browser form iframe
      cy.modalImageLibraryChoice('media56');


// callout wysiwyg
      cy.get('.field--name-field-p-callout-body');
      cy.getIframeBody('.field--name-field-p-callout-body iframe.cke_wysiwyg_frame')
        .type('This is my Test callout body. Writing more than one sentence. Ta-da!');
      cy.wait(3000);
// callout cta

      // cta 1
      cy.get('.field--name-field-p-callout-cta tbody tr').eq(0).within(() => {
        cy.autocompleteInputField('The Great Tradition (36206)', 'Test Callout CTA 1');

      }); // end cta form within
      // cta 2
      cy.get('.field--name-field-p-callout-cta tbody tr').eq(1).within(() => {
        cy.autocompleteInputField('Digital Collections (18316)', 'Test Callout CTA 2');
        cy.get('summary').contains('Advanced settings')
          .click();
        cy.wait(1000);
        cy.get('option[value="callout__cta--secondary"]')
          .click({force:true});

      }); // end cta form within

// FLEX CONTENT:
      // // use the code: flexItemSelect({contentType},{flexItem});
      //
      // use the code: childFlexItemSelect({childItem},{itemChoice}); in test where there is a child select dropdown to add more
      //
//
// Homepage Feature Large
      // add homepage-feature-large flex item
      // open flex item list
      cy.flexItemSelect('home-body','homepage-feature-large');
      // HEADING
      cy.get('.field--name-field-p-fhlrg-heading input').type('Feature Large Heading');

      // INTRO
      cy.get('.field--name-field-p-fhlrg-intro textarea').type('Feature large intro text here.');

      // CTA
             cy.get('.field--name-field-p-fhlrg-cta tbody tr').eq(0).within(() => {
               cy.autocompleteInputField('First Fridays (6781)', 'FL Test link 1');

             }); // end cta form within

      // IMAGE

          // media entity browser
      cy.get('.field--name-field-p-fhlrg-img input.button.form-submit')
        .click();
      cy.wait(3000);
      // enter entity-browser form iframe
      cy.modalImageLibraryChoice('media56');



      // DETAIL TITLE
        // LEAVE AS DEFAULT
          // do nothing
        // CHANGE
            // use Feature and New Text like so: cy.detailTitleChange('fhlrg', 'New Title Here');
        // DELETE DEFAULT/LEAVE BLANK
           // use cy.detailTitleChange('fhlrg');

      cy.detailTitleChange('fhlrg');


      // MAIN LIST ITEM
        // click add feature child main list item
          cy.get('.field--name-field-p-fhlrg-mnlst input.field-add-more-submit').click();
          cy.wait(2000);

          //MAIN LIST URL
          //MAIN LIST LINK TEXT

      cy.get('.field--name-field-p-fhlrg-mnlst').within(() => {
        cy.get('.field--name-field-p-fhome-mlink').within(() => {
          cy.autocompleteInputField('First Fridays (6781)', 'Main List test link 1');
        }); // end cta form within

        //MAIN LIST DESCRIPTION
        cy.get('.field--name-field-p-fhome-mdesc input').type('This is the Main List Description');

      });


      // ADDITIONAL LIST ITEM eq(0)
            // URL
            // LINK TEXT
             cy.get('.field--name-field-p-fhlrg-alink tbody tr').eq(0).within(() => {
               cy.autocompleteInputField('Workshop: LifeLines: How Are We Free? (30306)', 'Additional Link #1');
             }); // end cta form within

      // ADDITIONAL LIST ITEM eq(1)
            // URL
            // LINK TEXT
             cy.get('.field--name-field-p-fhlrg-alink tbody tr').eq(1).within(() => {
               cy.autocompleteInputField('Book Traces (36846)', 'Additional Link #2');
             }); // end cta form within

      // ITEM BLOCK
        // fhlrg-item = child item
        // SEE BELOW FOR EACH CHOICE = item choice
        // childFlexItemSelect({childItem},{itemChoice});
      // cy.childFlexItemSelect('fhlrg-item','fcap'); //<-- for caption
       cy.childFlexItemSelect('fhlrg-item','fquote'); // <-- for quote
      // cy.childFlexItemSelect('fhlrg-item','fstat');// <-- for statistic
      // cy.childFlexItemSelect('fhlrg-item','fvid'); //<-- for video


      // ADD FEATURE CHILD SPECIAL CAPTION - fcap
          // CAPTION FIELD
            // cy.get('.field--name-field-p-fcap-caption input').type('');
        // ADD FEATURE CHILD SPECIAL QUOTE - fquote
          // QUOTE FIELD
            // .field--name-field-p-fquote-quote input
          // ATTRIBUTION FIELD
             // .field--name-field-p-fquote-attr input
          cy.get('.field--name-field-p-fquote-quote input').type('You are what you are and you ain\'t what you ain\'t');
          cy.get('.field--name-field-p-fquote-attr input').type('A wiseguy');
        // ADD FEATURE CHILD SPECIAL STATISTIC - fstat
          //NUMBER FIELD
            //  cy.get('field--name-field-p-fstat-num input')type('');
          //DESCRIPTION FIELD
            // cy.get('.field--name-field-p-fstat-desc input').type('');
        // ADD FEATURE CHILD SPECIAL VIDEO - fvid
            // VIDEO ENTITY BROWSER SELECT
              // .field--name-field-p-fvid-video iframe#entity_browser_iframe_wysiwyg_video_browser
            // TITLE FIELD cy.get('.field--name-field-p-fvid-title input').type('');
// Homepage Feature Medium
      // add homepage-feature-medium flex item
      // open flex item list
      cy.flexItemSelect('home-body','homepage-feature-medium');
      // HEADING
      cy.get('.field--name-field-p-fhmed-heading input').type('Feature Medium Heading');
      // INTRO
      cy.get('.field--name-field-p-fhmed-intro textarea').type('Feature medium intro text here.');
      // CTA
      cy.get('.field--name-field-p-fhmed-cta tbody tr').eq(0).within(() => {
        cy.autocompleteInputField('The Trees of Bryn Mawr (36156)', 'FM Test link 1');
      }); // end cta form within

      // IMAGE
      // media entity browser
      cy.get('.field--name-field-p-fhmed-img input.button.form-submit')
        .click();
      cy.wait(3000);
      // enter entity-browser form iframe
      cy.modalImageLibraryChoice('media26');

      // IMAGE ALIGNMENT
       cy.get('.field--name-field-p-fhmed-imgalign [id*="subform-field-p-fhmed-imgalign"]')
         .select('right');
      // DROPDOWN
        // click add feature child dropdown
           cy.get('.field--name-field-p-fhmed-dropdown input.button.form-submit').click();
          // TITLE FIELD .field--name-field-p-fdrop-title
          cy.get('.field--name-field-p-fdrop-title').type('FM Dropdown Title');
          // LINKS URL FIELD
          // LINKS LINK TEXT FIELD
      // CTA
            cy.get('.field--name-field-p-fdrop-link tbody tr').eq(0).within(() => {
               cy.autocompleteInputField('Book Traces (36846)', 'FM Drop link 1');
            }); // end cta form within
        // click add another item
        cy.get('.field--name-field-p-fdrop-link input.field-add-more-submit').click();
        cy.wait(2000);
          // LINKS URL FIELD
          // LINKS LINK TEXT FIELD
      // CTA
            cy.get('.field--name-field-p-fdrop-link tbody tr').eq(1).within(() => {
               cy.autocompleteInputField('The Trees of Bryn Mawr (36156)', 'FM Drop link 2');
             }); // end cta form within
      //* DETAIL TITLE
        // LEAVE AS DEFAULT
        // CHANGE
          cy.detailTitleChange('fhmed', 'Nifty Stuff Here');
        // DELETE DEFAULT/LEAVE BLANK

      // MAIN LIST ITEM
      // click add feature child main list item
        cy.get('.field--name-field-p-fhmed-mnlst input.field-add-more-submit').click();
        cy.wait(2000);

      //MAIN LIST URL
      //MAIN LIST LINK TEXT
      cy.get('.field--name-field-p-fhmed-mnlst').within(() => {
        cy.get('.field--name-field-p-fhome-mlink').within(() => {
          cy.autocompleteInputField('Gillian Claire Baillie (1221)', 'FM Main List test link 1');
        }); // end cta form within

        //MAIN LIST DESCRIPTION
        cy.get('.field--name-field-p-fhome-mdesc input').type('This is the FM Main List Description');
      });

      // ADDITIONAL LIST ITEM eq(0)
      // URL
      // LINK TEXT
      cy.get('.field--name-field-p-fhmed-alink tbody tr').eq(0).within(() => {
        cy.autocompleteInputField('Workshop: LifeLines: How Are We Free? (30306)', 'Additional Link #1');
      }); // end cta form within

      // ADDITIONAL LIST ITEM eq(1)
      // URL
      // LINK TEXT
      cy.get('.field--name-field-p-fhmed-alink tbody tr').eq(1).within(() => {
        cy.autocompleteInputField('Book Traces (36846)', 'Additional Link #2');
      }); // end cta form within

// ITEM BLOCK
      // fhmed-item = child item
      // SEE BELOW FOR EACH CHOICE = item choice
      // childFlexItemSelect({childItem},{itemChoice});
      // cy.childFlexItemSelect('fhmed-item','fcap');// <-- for caption
      //cy.childFlexItemSelect('fhmed-item','fquote'); // <-- for quote
       cy.childFlexItemSelect('fhmed-item','fstat');// <-- for statistic
      // cy.childFlexItemSelect('fhmed-item','fvid'); //<-- for video

   // ADD FEATURE CHILD SPECIAL CAPTION - fcap
      // CAPTION FIELD
        // cy.get('.field--name-field-p-fcap-caption input').type('');
   // ADD FEATURE CHILD SPECIAL QUOTE - fquote
      // QUOTE FIELD
        // .field--name-field-p-fquote-quote input
      // ATTRIBUTION FIELD
       // .field--name-field-p-fquote-attr input
        //  cy.get('.field--name-field-p-fquote-quote input').type('You are what you are and you ain\'t what you ain\'t');
        //  cy.get('.field--name-field-p-fquote-attr input').type('A wiseguy');
   // ADD FEATURE CHILD SPECIAL STATISTIC - fstat
        // NUMBER FIELD
        // DESCRIPTION FIELD
          cy.get('.field--name-field-p-fstat-num input').type('17');
          cy.get('.field--name-field-p-fstat-desc input').type('ways to fly');
   // ADD FEATURE CHILD SPECIAL VIDEO - fvid
      // VIDEO ENTITY BROWSER SELECT
        // .field--name-field-p-fvid-video iframe#entity_browser_iframe_wysiwyg_video_browser
        // TITLE FIELD cy.get('.field--name-field-p-fvid-title input').type('');

// Homepage Feature Story
      // add homepage-feature-story flex item
      // open flex item list
        cy.flexItemSelect('home-body','homepage-feature-story');
      // HEADING
        cy.get('.field--name-field-p-fhstory-heading input').type('Feature Story Heading');
      // INTRO
        cy.get('.field--name-field-p-fhstory-intro textarea').type('Feature story intro text here.');
      // CTA
        cy.get('.field--name-field-p-fhstory-cta').eq(0).within(() => {
         cy.autocompleteInputField('The Trees of Bryn Mawr (36156)', 'FS Test link 1');

        }); // end cta form within
      // expand story
      cy.get('.field--name-field-p-fhstory-story summary').click();
      cy.wait(1000);
      cy.get('div[id*="subform-field-p-fhstory-story-entity-browser"] input.button.form-submit')
        .click();
      cy.wait(4000);
        // select story item
          // story entity browser
      cy.modalEntityLibraryChoice('story_nodes', 'story-nodes', 'node96296');

      // STORY TITLE FIELD
        cy.get('.field--name-field-p-fhstory-stitle').type('Test HP Feature Story');
      // QUOTE ALIGNMENT
        // Default on left -- do nothing here
      // expand The Details
      cy.get('.paragraph-type--homepage-feature-story').within(() => {
        cy.get('summary').eq(1).click();
      });

        //* DETAIL TITLE
          // LEAVE AS DEFAULT
          // CHANGE
          // DELETE DEFAULT/LEAVE BLANK -- opting to do nothing so Default will show

      // MAIN LIST ITEM
        //MAIN LIST URL
        //MAIN LIST LINK TEXT
          cy.get('.field--name-field-p-fhstory-mlink').within(() => {
           cy.autocompleteInputField('Gillian Claire Baillie (1221)', 'FS Main List test link 1');
          }); // end cta form within

        //MAIN LIST DESCRIPTION
          cy.get('.field--name-field-p-fhstory-mdesc textarea').type('This is the FS Main List Description');

      // ADDITIONAL LIST ITEM eq(0)
      // URL
      // LINK TEXT
        cy.get('.field--name-field-p-fhstory-alink tbody tr').eq(0).within(() => {
          cy.autocompleteInputField('Workshop: LifeLines: How Are We Free? (30306)', 'Additional FS Link #1');
        }); // end cta form within

      // ADDITIONAL LIST ITEM eq(1)
      // URL
      // LINK TEXT
        cy.get('.field--name-field-p-fhstory-alink tbody tr').eq(1).within(() => {
          cy.autocompleteInputField('Book Traces (36846)', 'Additional FS Link #2');
        }); // end cta form within

// SELECT MAIN THEME
      cy.get('select#edit-field-theme-main').select('5');
// save node
      cy.get('input#edit-submit').click();

// check node for elements

      // slide title
      cy.get('h2.home-slide-title').should('have.text', 'Test Hero Slide 1');
      // slide link
      cy.get('.home-slide__ctas a').should('have.attr', 'href', '/inside/offices-services/campus-safety/lantern-shuttle');

      // callout heading
      cy.get('h2.callout__section-title').contains('Callout Test - Heading');

      // callout link 1
      cy.get('div.callout__cta a').eq(1).should('have.text', 'Test Callout CTA 2');

      // FL heading
      cy.get('h2.feat-lg__section-title').should('have.text', 'Feature Large Heading');
      // FL CTA
      cy.get('.feat-lg__cta.cta--primary a').should('have.attr', 'href', '/admissions/first-friday');
      // FL should.be The Details text
      cy.get('h3.feat-lg__mlist-title').should('include.text', 'The Details');
      // FL Main List link
      cy.get('.feat-lg__mnlst .field--name-field-p-fhome-mlink a').should('have.text', 'Main List test link 1');
      // FL Additional link #2
      cy.get('.field--name-field-p-fhlrg-alink a').eq(1).should('have.text', 'Additional Link #2');

      // FL Quote attr
      cy.get('.field--name-field-p-fhlrg-item .field--name-field-p-fquote-attr').should('have.text', 'A wiseguy');

      // FM heading
      cy.get('h2.home-ftmd__section-title').should('have.text', 'Feature Medium Heading');
      // FM CTA
      cy.get('.field--name-field-p-fhmed-cta a').should('have.text', 'FM Test link 1');
      // FM should.not.be The Details text
      cy.get('h3.home-ftmd__mlist-title')
        .should('contain.text', 'Nifty Stuff Here')
        .and('not.contain.text', 'The Details');
      // FM Main List description
      cy.get('.field--name-field-p-fhmed-mnlst .home-fhome__mdesc').should('include.text', 'This is the FM Main List Description')
      // FM Dropdown title
      cy.get('.home-ftmd__dropdown .field--name-field-p-fdrop-title').should('have.text', 'FM Dropdown Title');
      // FM dropdown link #2
      cy.get('.home-ftmd__dropdown ul.fdrop__links li.fdrop__link').eq(1).should('have.text', 'FM Drop link 2');
      // FM Additional link #1
      cy.get('.field--name-field-p-fhmed-alink a').eq(0).should('have.text', 'Additional Link #1');

      // FM Stat number
      cy.get('.field--name-field-p-fhmed-item .fstat__num').should('include.text', '17');

      // FS heading
      cy.get('h2.homepage-feature-story__heading').should('have.text', 'Feature Story Heading');
      // FS CTA
      cy.get('.field--name-field-p-fhstory-cta a').should('have.text', 'FS Test link 1');
      // FS should.be The Details text
      cy.get('h3.homepage-feature-story__details--title').should('include.text', 'The Details');
      // FS Main List description
      cy.get('.homepage-feature-story__details--desc p').should('include.text', 'This is the FS Main List Description');
      // FS Additional link #1
      cy.get('.field--name-field-p-fhstory-alink a').eq(0).should('have.text', 'Additional FS Link #1');

      // FS Quote
      cy.get('.homepage-feature-story__story--quote').should('have.length.at.least', 1);
        // Story title
      cy.get('h3.homepage-feature-story__story--title').should('have.text', 'Test HP Feature Story');
        // Story CTA
      cy.get('.homepage-feature-story__story a.cta--secondary').should('be.visible');

    });
  });
});

