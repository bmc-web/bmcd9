describe('EMS Events Create', function() {
  context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
    beforeEach(() => {
      cy.viewport(1280, 720);
      cy.loginViaUi('iftest_site-editor', 'LightSlateGray3348!');
    });


    it('check for auth', function() {
      cy.visit('/user');
      cy.url().should('include', '/user/196');
      cy.get('.header__logo img')
        .should('be.visible');
    });


    it('edit events from report and publish', function() {
      // this should have Site Editor edit and publish three events
      // visits event and announcement report
      cy.visit('/admin/reports/events-and-announcements?status=0&type=3&field_event_date_value=&field_event_date_end_value=&field_ddigest_date_value_1%5Bmin%5D=&field_ddigest_date_value_1%5Bmax%5D=&field_conference_events_approved_value=2&items_per_page=50&order=created&sort=desc');
      cy.get('h1.page-title')
        .should('include.text', 'Events and Announcements');
      // change the approved filter to YES, click apply
      cy.get('#edit-field-conference-events-approved-value')
        .select('1');
      cy.get('div#edit-actions #edit-submit-event-management')
        .click();
      cy.wait(1000);

      cy.get('.views-table tbody').within(() => { // Only yield inputs within
        // see three "✔" in the
        // .views-field views-field-field-conference-events-approved column
        cy.get('.views-field-field-conference-events-approved:contains("✔")')
          .should('have.length.at.least', 3);
      }); // end within block

      // will find row, check event title, edit event make sure correct event then publish and save
      cy.eventPublish(2);
      cy.eventPublish(1);
      cy.eventPublish(0);
    });


    it('Daily Digest check', function() {
      cy.visit('/admin/reports/daily-digest');
      cy.get('h1.page-title')
        .should('include.text', 'Daily Digest');
      cy.get('input#edit-start-date').type('2026-06-29');
      cy.get('input#edit-end-date').type('2026-07-03');
      cy.get('input#edit-submit').click();
      cy.wait(1000);
      cy.get('#edit-new-preview').within(() => {
        cy.get('h1').contains('All Campus').should('be.visible');
        cy.get('p a').contains('Test Event EMS 1')
          .and('have.attr', 'href', 'https://bmcd9.lndo.site/inside/latest/events/test-event-ems-1');
        cy.get('h1').contains('Faculty and Staff').should('be.visible');
        cy.get('p a').contains('Test Event EMS 2')
          .and('have.attr', 'href', 'https://bmcd9.lndo.site/inside/latest/events/test-event-ems-2');
      }); // end within
    });


  });
});
