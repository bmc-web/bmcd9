describe('EMS Events Create', function() {
  context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
    beforeEach(() => {
      cy.viewport(1280, 720);
      cy.loginViaUi('iftest_events', 'SiennaTomato488#');
    });

    it('check for auth', function() {
      cy.visit('/user');

      cy.url().should('include', '/user/211');
      cy.get('.header__logo img')
        .should('be.visible');
    });

    it('go to content view', function() {
      cy.visit('/user');
      cy.get('.toolbar-tab a#toolbar-item-administration');

      cy.get('.toolbar-tab a#toolbar-item-administration').should('have.class', 'is-active');
      cy.get('.toolbar-menu-administration li a.toolbar-icon.toolbar-icon-system-admin-content')
        .click();
      cy.wait(1000);
      cy.get('h1.page-title')
        .should('include.text', 'Content');
    });


    it('create hybrid off-campus event', function() {
      cy.visit('/inside/events/submit');
      cy.get('h1.page-title')
        .should('include.text', 'Create Event');
      cy.get('form.node-event-form').within(() => { // Only yield inputs within form
        cy.get('input#edit-title-0-value').type('Test Event EMS 1');
        cy.get('input#edit-field-event-date-0-time-wrapper-value-date').type('2026-07-04');
        cy.get('input#edit-field-event-date-0-time-wrapper-value-time').type('13:00');
        cy.get('input#edit-field-event-date-0-time-wrapper-end-value-time').type('20:00');

        cy.get('input#edit-field-event-location-choice-hybrid-off-campus').click({force:true});

        cy.get('input#edit-field-virtual-event-access-link-0-uri')
          .type('https://brynmawr-edu.zoom.us/meeting/register/tJUtc-GtrzorHtG3PafivgKuh9CV5YEkrhky');

        cy.get('input#edit-field-event-location-details-0-value')
          .type('Test Location');

        cy.get('input#edit-field-event-type-7').click({force:true});
        cy.get('input#edit-field-event-audience-12').click({force:true});
        cy.get('input#edit-field-event-submitter-email-0-value').type('me@brynmawr.edu');
        cy.get('input#edit-field-event-contact-name-0-value').type('Test Submitter');
        cy.get('input#edit-field-event-contact-email-0-value').type('me@swarthmore.edu');
        cy.get('textarea#edit-field-event-blurb-0-value').type('This is my blurb');
        cy.get('.field--name-field-event-body').scrollIntoView();
        // intereact with wysiwyg iframe
        cy.getIframeBody('.field--name-field-event-body iframe.cke_wysiwyg_frame').type('This is the body of the event. We are making it longer so that we can really test how the event looks with some text in it. On our historic and verdant suburban campus 11 miles from Philadelphia, a world-class faculty guides and challenges 1,300 undergraduate women and more than 400 graduate women and men from 45 states and 62 countries. Bryn Mawr is a place where teaching is valued, each student is known, and students and faculty members form close bonds. ');
        cy.get('table#field-ddigest-date-values').scrollIntoView();
        cy.get('input#edit-field-ddigest-date-0-value-date').type('2026-06-30');
        cy.get('input#edit-field-ddigest-date-1-value-date').type('2026-07-03');
        cy.get('.form-item-field-conference-events-approved-value label')
          .should('include.text', 'Conference & Events Approved').and('be.visible');
        cy.get('#edit-status-value--description')
          .should('include.text', 'You can not change the Publish status.').and('be.visible');
        // SAVE node
        cy.get('input#edit-submit')
          .focus()
          .click();
      }); // end within block
      // checks success message on node
      cy.get('li.messages__item').eq(0)
        .should('include.text', 'An email notification has been sent to me@brynmawr.edu');
      cy.get('li.messages__item').eq(2)
        .contains('Thank you for completing the Event/Daily Digest submission form!');
      cy.get('h1.page-title')
        .should('include.text', 'Events');
      cy.get('h2.event-listing-page__main_listing')
        .should('include.text', 'All Upcoming Events');
    });

    it('create virtual only event', function() {
      cy.visit('/inside/events/submit');
      cy.get('h1.page-title')
        .should('include.text', 'Create Event');
      cy.get('form.node-event-form').within(() => { // Only yield inputs within form
        cy.get('input#edit-title-0-value').type('Test Event EMS 2');
        cy.get('input#edit-field-event-date-0-time-wrapper-value-date').type('2026-07-04');
        cy.get('input#edit-field-event-date-0-time-wrapper-value-time').type('13:00');
        cy.get('input#edit-field-event-date-0-time-wrapper-end-value-time').type('20:00');

        cy.get('input#edit-field-event-location-choice-virtual-only').click({force:true});

        cy.get('input#edit-field-virtual-event-access-link-0-uri')
          .type('https://brynmawr-edu.zoom.us/meeting/register/tJUtc-GtrzorHtG3PafivgKuh9CV5YEkrhky');

        cy.get('input#edit-field-event-type-3981').click({force:true});
        cy.get('input#edit-field-event-audience-21').click({force:true});
        cy.get('input#edit-field-event-submitter-email-0-value').type('me@brynmawr.edu');
        cy.get('input#edit-field-event-contact-name-0-value').type('Test Submitter');
        cy.get('input#edit-field-event-contact-email-0-value').type('me@swarthmore.edu');
        cy.get('textarea#edit-field-event-blurb-0-value').type('This is my blurb');
        cy.get('.field--name-field-event-body').scrollIntoView();
        // intereact with wysiwyg iframe
        cy.getIframeBody('.field--name-field-event-body iframe.cke_wysiwyg_frame').type('This is the body of the event. We are making it longer so that we can really test how the event looks with some text in it. On our historic and verdant suburban campus 11 miles from Philadelphia, a world-class faculty guides and challenges 1,300 undergraduate women and more than 400 graduate women and men from 45 states and 62 countries. Bryn Mawr is a place where teaching is valued, each student is known, and students and faculty members form close bonds. ');
        cy.get('table#field-ddigest-date-values').scrollIntoView();
        cy.get('input#edit-field-ddigest-date-0-value-date').type('2026-06-30');
        cy.get('input#edit-field-ddigest-date-1-value-date').type('2026-07-03');
        cy.get('.form-item-field-conference-events-approved-value label')
          .should('include.text', 'Conference & Events Approved').and('be.visible');

        cy.get('#edit-status-value--description')
          .should('include.text', 'You can not change the Publish status.').and('be.visible');
        // SAVE node
        cy.get('input#edit-submit')
          .focus()
          .click();
      }); // end within block
      // checks success message on node
      cy.get('li.messages__item').eq(0)
        .should('include.text', 'An email notification has been sent to me@brynmawr.edu');
      cy.get('li.messages__item').eq(2)
        .contains('Thank you for completing the Event/Daily Digest submission form!');
      cy.get('h1.page-title')
        .should('include.text', 'Events');
      cy.get('h2.event-listing-page__main_listing')
        .should('include.text', 'All Upcoming Events');
    });

  });
});
