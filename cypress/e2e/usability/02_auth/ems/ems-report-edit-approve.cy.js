describe('EMS Events Create', function() {
  context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
    beforeEach(() => {
      cy.viewport(1280, 720);
      cy.loginViaUi('iftest_events', 'SiennaTomato488#');
    });


    it('check for auth', function() {
      cy.visit('/user');
      cy.url().should('include', '/user/211');
      cy.get('.header__logo img')
        .should('be.visible');
    });


    it('go to content view', function() {
      cy.visit('/user');
      cy.get('.toolbar-tab a#toolbar-item-administration');
      cy.get('.toolbar-tab a#toolbar-item-administration').should('have.class', 'is-active');
      cy.get('.toolbar-menu-administration li a.toolbar-icon.toolbar-icon-system-admin-content')
        .click();
      cy.wait(1000);
      cy.get('h1.page-title')
        .should('include.text', 'Content');
    });


    it('check event report', function() {
      cy.visit('/admin/reports/events-and-announcements?status=0&type=3&field_event_date_value=&field_event_date_end_value=&field_ddigest_date_value_1%5Bmin%5D=&field_ddigest_date_value_1%5Bmax%5D=&field_conference_events_approved_value=2&items_per_page=50&order=created&sort=desc');
      cy.get('h1.page-title')
        .should('include.text', 'Events and Announcements');
      cy.get('.views-table tbody').within(() => { // Only yield inputs within
        // check first row contains "Test Event EMS 2"
        cy.get('tr').eq(0).contains('Test Event EMS 2');
        // count rows then count all .views-field views-field-status that have "No" and all the
        // .views-field-field-conference-events-approved that have "✖" should be same amount
        let $row = cy.get('tr');
        let $pubStatus = cy.get('.views-field-status:contains("No")');
        let $apprvStatus = cy.get('.views-field-field-conference-events-approved:contains("✖")');
          if ($row.length === ($pubStatus.length && $apprvStatus.length))  {
            cy.log('All Events are Unpublished and Need Approval');
          } else {
            cy.log('The Numbers don\'t match!');
          }
      }); // end within block
    });


    it('edit events from report', function() {
      // this should have EMS edit and approve three events
      // visits event and announcement report
      cy.visit('/admin/reports/events-and-announcements?status=0&type=3&field_event_date_value=&field_event_date_end_value=&field_ddigest_date_value_1%5Bmin%5D=&field_ddigest_date_value_1%5Bmax%5D=&field_conference_events_approved_value=2&items_per_page=50&order=created&sort=desc');
      cy.get('h1.page-title')
        .should('include.text', 'Events and Announcements');
      // will find row, check event title, edit event make sure correct event then approve and save
      cy.eventApprove(2);
      cy.eventApprove(1);
      cy.eventApprove(0);
      // change the approved filter to any, click apply
      cy.get('#edit-field-conference-events-approved-value')
        .select('All');
      cy.get('div#edit-actions #edit-submit-event-management')
        .click();
      cy.wait(1000);

      cy.get('.views-table tbody').within(() => { // Only yield inputs within
        // see three "✔" in the
        // .views-field views-field-field-conference-events-approved column
        cy.get('.views-field-field-conference-events-approved:contains("✔")')
          .should('have.length.at.least', 3);
      }); // end within block
    });
  });
});
