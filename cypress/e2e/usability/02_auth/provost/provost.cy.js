describe('Provost', function() {
  context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
    beforeEach(() => {
      cy.viewport(1280, 720);
      cy.loginViaUi('iftest_steward', 'SpringGreen334433@');
    });

    it('check for auth', function() {
      cy.visit('/user');

      cy.url().should('include', '/user/201');
      cy.get('.header__logo img')
        .should('be.visible');
    });

    it('go to content view, edit akaldrovic bio to add/remove pdf', function() {
      cy.visit('/user');
      cy.get('.toolbar-tab a#toolbar-item-administration');

      cy.get('.toolbar-tab a#toolbar-item-administration').should('have.class', 'is-active');
      cy.get('.toolbar-menu-administration li a.toolbar-icon.toolbar-icon-system-admin-content')
        .click();
      cy.wait(1000);
      cy.get('h1.page-title')
        .should('include.text', 'Content');
      // find kaldrovics bio
      cy.get('input#edit-title').type('kaldrovics');
      cy.get('select#edit-type').select('Directory').should('have.value', 'directory');
      cy.get('input#edit-submit-content').click();
      cy.wait(2000);
      cy.get('ul.dropbutton li.dropbutton-action a')
        .should('have.attr', 'href')
        .and('include', '/node/2841/edit');
      // edit kaldrovics bio
      cy.get('ul.dropbutton li.dropbutton-action a').click();
      cy.url()
        .should('include', '/node/2841/edit');
      // upload pdf
      cy.get('div#edit-field-dir-cv-0 input#edit-field-dir-cv-0-upload')
        .selectFile('cypress/fixtures/akaldrovicCV.pdf');
      // affirm pdf uploaded
      cy.get('span.file--application-pdf')
        .should('contain', 'akaldrovicCV');
      // save node
      cy.get('input#edit-submit').click();
      cy.visit('/node/2841');
      cy.get('h1.page-title')
        .should('contain.text', 'Andrea Kaldrovics');
      // affirm areas of focus title and pdf icon/text
      cy.get('.directory__contact-cv').find('a')
        .invoke('attr', 'href')
        .should('contain', 'akaldrovicCV');
      // edit node
      cy.visit('/node/2841/edit');
      // remove pdf
      cy.get('input#edit-remove-cv-from-directory').click({force: true});
      // save node
      cy.saveNode('Andrea Kaldrovics');
      // affirm no pdf icon
      cy.get('.directory__contact-cv').should('not.exist');

    });
    it('edit Michael H Allen bio to change AoF label', function() {

      // edit Michael H Allen bio to change AoF label field
      cy.visit('/node/1131');
      cy.get('.directory__def-focus h2').should('contain','Areas of Focus');
      cy.visit('/node/1131/edit');
     // cy.get('select#edit-field-dir-rsrch-intrst-label option[value="6036"]').click({force: true});
      cy.get('select#edit-field-dir-rsrch-intrst-label').select('Ask Me About').should('have.value', '6036');

      cy.saveNode('Michael H. Allen');
      // affirm areas of focus label is now 'Ask Me About'
         cy.get('.directory__def-focus h2').should('contain','Ask Me About');
      // edit Michael H Allen bio  to change AoF label field back
      cy.visit('/node/1131/edit');
       cy.get('select#edit-field-dir-rsrch-intrst-label').select('- None -').should('have.value', '_none');

      cy.saveNode('Michael H. Allen');
      // affirm areas of focus label is now back to 'Areas of Focus'
      cy.get('.directory__def-focus h2').should('contain','Areas of Focus');
    });
  });
});

