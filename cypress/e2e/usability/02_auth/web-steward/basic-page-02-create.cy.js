describe('Web Steward', function() {
  context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
    beforeEach(() => {
      cy.viewport(1280, 720);
      cy.loginViaUi('drupalInstructor', 'Manually_migrate2D9!');
    });


    it('check for auth', function() {
      cy.visit('/user');
      cy.url().should('include', '/user/671');
      cy.get('.header__logo img')
        .should('be.visible');
    });


    it('goto Math group, create page', function() {
// Visit group
      cy.visit('/inside/group/mathematics');
// click add basic page item
      cy.get('.group__add-links .field__item a').eq(1)
        .should('have.attr', 'href', '/group/866/content/create/group_node:page')
        .click();
 // Start new page create
      cy.get('h1.page-title')
        .should('include.text', 'Add Group: Group node (Basic page)');
// check breadcrumb
      cy.get('div.region-breadcrumb nav.breadcrumb ol li').eq(1)
        .should('include.text', 'Mathematics');
// ENTER title
      cy.get('input#edit-title-0-value').type('Test Page 1 MATH');
// add wysiwyg flex item
      cy.flexwysiwyg();
// type in  wysiwyg iframe
      cy.getIframeBody('.field--name-field-wysiwyg-body iframe.cke_wysiwyg_frame').type('This is my Test WYSIWYG.');
 // add link to text in wysiwyg
      cy.getIframeBody('.field--name-field-wysiwyg-body iframe.cke_wysiwyg_frame')
        .find('p').type('{selectall}');
        cy.get('a.cke_button__drupallink').eq(1).click();
      cy.wait(3000);
      cy.get('div#drupal-modal form.editor-link-dialog').within(() => {
        cy.get('div.form-type-linkit input.form-text').type('#thisPageAnchor');
      }); // end embed form within
// save link for text in wysiwyg
      cy.modalButtonClick('Save');

// add wysiwyg flex item
      cy.flexwysiwyg();
      cy.getIframeBody('.field--name-field-wysiwyg-body iframe.cke_wysiwyg_frame');
      cy.get('.field--name-field-wysiwyg-body').eq(1).within(() => {
        cy.get('a.cke_button__video').click();
      }); // end wysiwyg body within
      cy.wait(3000);
 // ADD VIDEO TO WYSIWYG
   // opens wysiwyg video entity browser
      cy.getIframeBody('iframe#entity_browser_iframe_wysiwyg_video_browser')
        .find('form#entity-browser-wysiwyg-video-browser-form').within(() => {
        cy.get('input#edit-entity-browser-select-media45421').click();
        cy.get('input#edit-submit').click();
        cy.wait(3000);
      }); // end browser form within
      cy.wait(3000);
   // click next in modal window
      cy.modalButtonClick('Next');
   // add caption text for video
      cy.get('div#drupal-modal form.entity-embed-dialog.entity-embed-dialog-step--embed').within(() => {
        cy.get('div.form-item-attributes-data-caption textarea')
          .type('this is my video caption');
      }); // end embed form within
    // click embed form for wysiwyg
      cy.modalButtonClick('Embed');
    // check wysiwyg iframe for embedded image with correct caption and video

      cy.wait(3000);
      cy.getIframeBody('.field--name-field-wysiwyg-body [id^=cke_edit-field-body-1-] iframe.cke_wysiwyg_frame')
        .find('div.cke_widget_wrapper .cke_widget_element')
        .should('have.attr', 'data-embed-button', 'video')
        .should('have.attr','data-caption', 'this is my video caption');

      // FLEX CONTENT:
      // // use the code: flexItemSelect({contentType},{flexItem});
      //
      // use the code: childFlexItemSelect({childItem},{itemChoice}); in test where there is a child select dropdown to add more
      //
 // ACCORDION
      //

 // CONTACT
      //
      // And I scroll h4 with the text “Flexible Content ”into view
      // And I select li.add-more-button-contact  input.field-add-more-submit
      // And I should scroll to tr.paragraph-type--contact
      // And I fill in “Contact Test” for input data-drupal-selector="edit-field-body-6-subform-field-p-contact-heading-0-value"
      // And I fill in “Department of Tests” for input data-drupal-selector="edit-field-body-6-subform-field-p-contact-title-0-value"
    // IMAGE
      //    And I should press the    input data-drupal-selector="edit-field-body-6-subform-field-p-contact-image-entity-browser-entity-browser-open-modal"
      // And I focus on iframe#entity_browser_iframe_media_image_browser
      // And then I click the checkbox for input.form-checkbox with the value “media:26”
      // And I press input#edit-submit with value "Embed"
      // And I wait for AJAX to finish
      // And I should see article. media--view-mode-default with an img element
      // And I scroll to textarea data-drupal-selector="edit-field-body-7-subform-field-p-contact-body-0-value"
      // And I enter the iframe.cke_wysiwyg_frame body.cke_editable
      // And I fill in "Mr. Test
      //  101 Main St.
      // Anytown, Pennsylvania 19101
      // 215-555-1212
      // test@abc.edu"
      //
// EVENT 3UP
      //  ******* [ TAG: Staff Association ]
      //

// FEATURE IMAGE GRID
      // And I scroll h4 with the text “Flexible Content ”into view
      // And I select li.add-more-button-feat-grid  input.field-add-more-submit
      // And I should scroll to tr.paragraph-type--feat-grid
      // And I fill in “Test Image Grid” for input data-drupal-selector="edit-field-body-4-subform-field-p-featgrid-heading-0-value"
      // IMAGE
      //    And I should press the    input data-drupal-selector="edit-field-body-4-subform-field-p-featgrid-items-0-subform-field-p-featgrid-iimg-entity-browser-entity-browser-open-modal"
      // And I focus on iframe#entity_browser_iframe_media_image_browser
      // And then I click the checkbox for input.form-checkbox with the value “media:1”
      // And I press input#edit-submit with value "Embed"
      // And I wait for AJAX to finish
      // And I should see article. media--view-mode-default with an img element
      // And I should fill in “Our Buildings (13471)” for input data-drupal-selector="edit-field-body-4-subform-field-p-featgrid-items-0-subform-field-p-featgrid-icta-0-uri"
      // And I should fill in “Buildings” for input data-drupal-selector="edit-field-body-4-subform-field-p-featgrid-items-0-subform-field-p-featgrid-icta-0-title"
      // And I should click div data-drupal-selector="edit-field-body-4-subform-field-p-featgrid-items-add-more"   input value “Add Feature: Grid Item”
      // IMAGE
      //    And I should press the    input data-drupal-selector="edit-field-body-4-subform-field-p-featgrid-items-1-subform-field-p-featgrid-iimg-entity-browser-entity-browser-open-modal"
      // And I focus on iframe#entity_browser_iframe_media_image_browser
      // And then I click the checkbox for input.form-checkbox with the value “media:56”
      // And I press input#edit-submit with value "Embed"
      // And I wait for AJAX to finish
      // And I should see article. media--view-mode-default with an img element
      // And I should fill in “Academic/Student Use Buildings (13476)” for input data-drupal-selector="edit-field-body-4-subform-field-p-featgrid-items-1-subform-field-p-featgrid-icta-0-uri"
      // And I should fill in “More Buildings” for input data-drupal-selector="edit-field-body-4-subform-field-p-featgrid-items-1-subform-field-p-featgrid-icta-0-title"
      // ADD Embed that has the following code: <a href="/node/61456#myAnchor">Visit my Image List item on the ak test flex page /node/61456 </a>
      //
// IMAGE LIST
      // **** with one item that has thisPageAnchor as an anchor name

   // add image list flex item
   // open flex item list
      cy.flexItemSelect('body','imglist');

  // Image List Heading
      cy.get('.field--name-field-p-imglist-head input').type('Image List Test - Heading');

   // add First Image Item
     // Image List Title
      cy.get('.field--name-field-p-imglist-item-title input').eq(0)
        .type('Image List Test - Title #1');
    // Image List Anchor
      cy.get('.field--name-field-p-imglist-item-anchor input').eq(0)
        .type('thisPageAnchor');
    // Image List image
      cy.get('.field--name-field-p-imglist-item-img input.button.form-submit').eq(0)
        .click();
      cy.wait(3000);
    // enter entity-browser form iframe
      cy.modalImageLibraryChoice('media56');

    // Image List wysiwyg
      cy.get('.field--name-field-p-imglist-item-body').eq(0);
      cy.getIframeBody('.field--name-field-p-imglist-item-body iframe.cke_wysiwyg_frame')
        .type('This is my Test Image Item #1 body. Writing more than one sentence. Ta-da!');
      cy.wait(3000);
    // Image List cta
      // cta 1
      cy.get('.field--name-field-p-imglist-item-cta').eq(0).within(() => {
        cy.autocompleteInputField('/node/61456#myAnchor', 'Test Image List CTA - visit a different page');
      }); // end cta form within

  // add Second Image List Item
      cy.get('.field--name-field-p-imglist-items .paragraphs-add-wrapper input.field-add-more-submit').click();
      cy.wait(3000);

  // Image List Title
      cy.get('.field--name-field-p-imglist-item-title input').eq(1)
        .type('Image List Test - Title #2');
  // Image List image
      cy.get('.field--name-field-p-imglist-item-img input.button.form-submit').eq(1)
        .click();
      cy.wait(3000);
      // enter entity-browser form iframe
      cy.modalImageLibraryChoice('media66');

  // Image List wysiwyg
      cy.get('.field--name-field-p-imglist-item-body');
      cy.getIframeBody('[id^=cke_edit-field-body-2-subform-field-p-imglist-items-1-subform-field-p-imglist-item-body-0-value--] iframe.cke_wysiwyg_frame')
        .type('This is my Test Image Item #2 body. Writing more than one sentence. Ta-da!');
      cy.wait(3000);


// NEWS-3UP
      //    *******  [ TAG: Faculty Publication ]
      // And I scroll h4 with the text “Flexible Content ”into view
      // And I select li.add-more-button-news-3-up  input.field-add-more-submit
      // And I should scroll to tr.paragraph-type--news-3-up
      // And I fill in "Test News" on the Heading input data-drupal-selector="edit-field-body-2-subform-field-p-news3up-heading-0-value"
      // And I select “Election 2020” option value 3966 for select data-drupal-selector="edit-field-body-2-subform-field-p-news3up-tag"
      //
// PEOPLE LIST
      // ***** DEPARTMENT =  LITS
      //

// SOCIAL MEDIA CALLOUT
      //
      // And I scroll h4 with the text “Flexible Content ”into view
      // And I select li.add-more-button-smcallout  input.field-add-more-submit
      // And I should scroll to tr.  paragraph-type--smcallout
      // And I fill in “Follow this test on social!” for input data-drupal-selector="edit-field-body-5-subform-field-p-smcallout-head-0-value"
      // And I select option value sm-callout--facebook for select data-drupal-selector="edit-field-body-5-subform-field-p-smcallout-channels-0-subform-field-p-smcalloutlink-service"
      // And I fill in “https://facebook.com” for input data-drupal-selector="edit-field-body-5-subform-field-p-smcallout-channels-0-subform-field-p-smcalloutlink-url-0-uri"
      // And I click input value “Add Social Media: Callout Link” in div data-drupal-selector="edit-field-body-5-subform-field-p-smcallout-channels-add-more"
      // And I select option value “sm-callout--instagram” for select data-drupal-selector="edit-field-body-5-subform-field-p-smcallout-channels-1-subform-field-p-smcalloutlink-service"
      // And I fill in “https://instagram.com” for input data-drupal-selector="edit-field-body-5-subform-field-p-smcallout-channels-1-subform-field-p-smcalloutlink-url-0-uri"
      //
      //
// SPECIAL LIST SMALL
      // ******   [ with link to /node/61456#myAnchor ]
      // And I scroll h4 with the text “Flexible Content ”into view
      // And I select li.add-more-button-special-list-small  input.field-add-more-submit
      // And I should scroll to tr.paragraph-type--special-list-small
      // And I fill in "This is a Special List Small" for div.form-item-field-body-3-subform-field-p-slistsm-heading-0-value input
      // And I fill in "Web Services (53286)" for input data-drupal-selector="edit-field-body-3-subform-field-p-slistsm-items-0-uri"
      // And I fill in "link text web services" for input data-drupal-selector="edit-field-body-3-subform-field-p-slistsm-items-0-title"
      // And then I click “Add another item” input data-drupal-selector="edit-field-body-3-subform-field-p-slistsm-items-add-more"
      // And I fill in "Libraries (13481)" for input data-drupal-selector="edit-field-body-3-subform-field-p-slistsm-items-1-uri"
      // And I fill in "link text libraries" for input data-drupal-selector="edit-field-body-3-subform-field-p-slistsm-items-1-title"
      // And then I click “Add another item” input data-drupal-selector="edit-field-body-3-subform-field-p-slistsm-items-add-more"
      // And I fill in "Core Competencies (18836)" for input data-drupal-selector="edit-field-body-3-subform-field-p-slistsm-items-2-uri"
      // And I fill in "link text competencies" for input data-drupal-selector="edit-field-body-3-subform-field-p-slistsm-items-2-title"


// save node
      cy.saveNode('Test Page 1 MATH');

// TEST PAGE CONTENT

      // And I should see an h2 with the text “This is a callout heading”
      // And I should see li.special-list-small__list--item a with the text “link text libraries”
      // And I should see an h2 with the text “Test Image Grid”
      // And I should see a.feat_grid__link with the text “More Buildings”
      //  And I should see an h2.smcallout__section-title with the text “Follow this test on social!”
      // And I should see h2 with text “Contact Test”
      // And I should see h3 with text “Department of Tests”
      //  And I should see the text "Mr. Test"


      // And I should see “This is my Test WYSIWYG.”

      cy.get('p').contains('This is my Test WYSIWYG.')
        .should('have.attr', 'href', '#thisPageAnchor')
        .then((link) => {
        cy.log(link.prop('href'));
      });
      //
// IMAGE LIST ANCHOR NAME TEST
      cy.get('p').contains('This is my Test WYSIWYG.')
        .click();
      cy.wait(2000);
      cy.url()
        .should('include', '#thisPageAnchor');
      cy.wait(2000);
        cy.get('.field--name-field-p-imglist-item-cta a')
          .should('have.attr', 'href', '/inside-119#myAnchor')
          .click();
      cy.wait(2000);
      cy.url()
        .should('include', '#myAnchor');
      cy.get('.imglist__item a').eq(0)
        .should('have.attr', 'id','myAnchor');



    }); // end create page

    it('go to content view', function() {
      cy.visit('/user');
      cy.get('.toolbar-tab a#toolbar-item-administration');

      cy.get('.toolbar-tab a#toolbar-item-administration').should('have.class', 'is-active');
      cy.get('.toolbar-menu-administration li a.toolbar-icon.toolbar-icon-system-admin-content')
        .click();
      cy.wait(1000);
      cy.get('h1.page-title')
        .should('include.text', 'Content');
    });

  });
});

