describe('Web Steward', function() {
  context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
    beforeEach(() => {
      cy.viewport(1280, 720);
      cy.loginViaUi('drupalInstructor', 'Manually_migrate2D9!');
    });

    it('check for auth', function() {
      cy.visit('/user');

      cy.url().should('include', '/user/671');
      cy.get('.header__logo img')
        .should('be.visible');
    });

    it('go to content view', function() {
      cy.visit('/user');
      cy.get('.toolbar-tab a#toolbar-item-administration');

      cy.get('.toolbar-tab a#toolbar-item-administration').should('have.class', 'is-active');
      cy.get('.toolbar-menu-administration li a.toolbar-icon.toolbar-icon-system-admin-content')
        .click();
      cy.wait(1000);
      cy.get('h1.page-title')
        .should('include.text', 'Content');
    });

    it('create auth announcement', function() {
      cy.visit('/inside/announcements/submit');
      // Start new page create
      cy.get('h1.page-title')
        .should('include.text', 'Create Announcement');
      // ENTER title
      cy.get('input#edit-title-0-value').type('Auth Announcement');
        cy.get('textarea#edit-field-anncmnt-blurb-0-value').type('this blurb is shorter than most blurbs');
        cy.get('#edit-field-anncmnt-url-0-uri')
          .type('https://askathena.brynmawr.edu');
        cy.get('#edit-field-anncmnt-url-0-title')
          .type('Ask Athen articles');
        cy.get('input#edit-field-anncmnt-submitter-0-value').type('me@brynmawr.edu');
        cy.get('.field--name-field-anncmnt-body')
        cy.getIframeBody('.field--name-field-anncmnt-body iframe.cke_wysiwyg_frame')
          .type('This is my Test announcement body. I am logged in when filling this out. Writing more than one sentence. Ta-da!');
        cy.wait(3000);
      cy.get('select#edit-field-tags-0-target-id')
        .select('4326');
        cy.get('select#edit-field-ddigest-sec')
          .select('campus');
        cy.get('input#edit-field-ddigest-date-0-value-date').type('2035-08-15');
        cy.get('input#edit-field-ddigest-date-1-value-date').type('2035-08-17');
        cy.get('#edit-status-value--description')
          .should('include.text', 'You can not change the Publish status.').and('be.visible');
        // SAVE node
        cy.get('input#edit-submit')
          .focus()
          .click();
        cy.wait(2000);
      // checks success message on node
      cy.get('.messages').wait(2000)
        .then((el) => {
          if (Cypress.dom.isVisible(el)) {
            cy.wrap(el).children()
              .then(($msgParts) => {
                if ($msgParts.find('li.messages__item').length) {
                  // success message was found
                  cy.log( 'found submission success message!');
                  cy.get('li.messages__item').eq(0)
                    .should('include.text', 'An email notification has been sent to me@brynmawr.edu');
                  cy.get('li.messages__item').eq(2)
                    .contains('Thank you for completing the Announcement/Daily Digest submission form!');
                  cy.get('h1.page-title')
                    .should('include.text', 'Submit Your Announcement');
                  cy.get('.field--name-field-wysiwyg-body h2').eq(0)
                    .should('include.text', 'Announcement Options');
                } else {
                  cy.log( 'NO success message - uninstalling simplesaml_auth locally allows success message to appear for anon and logged in submissions');
                  cy.wait(3000);
                }
              }); // end then msgParts
             } // end if isVisible
           }); // end then el
    });
  });
});

