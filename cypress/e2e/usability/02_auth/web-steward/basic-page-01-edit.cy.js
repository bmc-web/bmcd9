describe('Web Steward', function() {
  context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
    beforeEach(() => {
      cy.viewport(1280, 720);
      cy.loginViaUi('drupalInstructor', 'Manually_migrate2D9!');
    });

    it('check for auth', function() {
      cy.visit('/user');
      cy.url().should('include', '/user/671');
      cy.get('.header__logo img')
        .should('be.visible');
    });


    it('edit a page', function() {
      cy.visit('/node/61456/edit');
      cy.get('h1.page-title')
        .should('include.text', 'Edit Basic page ak test flex');
      // add  anchor name to an existing image list

      // edit image list
      cy.get('tr.paragraph-type--imglist td').eq(1).within(() => {
        cy.get('.paragraphs-actions input[value="Edit"]')
          .click();
        cy.wait(4000);
        cy.get('[data-drupal-selector="edit-field-body-3-subform"]').within(() => {
          cy.get('table[id^=field-p-imglist-items-values--] tr.paragraph-type--imglist-item [id^=field-body-3-subform-field-p-imglist-items-0-item-wrapper--]')
            .within(() => {
            cy.get('.paragraphs-actions input[value="Edit"]')
              .click();
         //   cy.get('[id^=edit-field-body-3-subform-field-p-imglist-items-0-subform--] .field--name-field-p-imglist-item-anchor input')
         //     .type('myAnchor');
              cy.get('[id^=edit-field-body-3-subform-field-p-imglist-items-0-subform--] .field--name-field-p-imglist-item-anchor').within(() => {
                if ('input [value=^"myAnchor"]') {
                  cy.get('[type="text"]').clear();
                }
                cy.get('input').type('myAnchor');
              });

          }); // end image list item within
        }); // end image list within
      }); // end flex-item td within

      // create carousel
      // open flex item list
      cy.flexItemSelect('body','carousel');

      // create slide
      cy.get('.field--name-field-p-slide-title').eq(0)
        .type('Test Slide 1');
      cy.get('.field--name-field-p-slide-supertitle input').eq(0)
        .type('This is the supertitle');

      // select slide image from library
      // upload slide image
      // open image modal window
      cy.get('.field--name-field-p-slide-image  input.button.form-submit').eq(0)
        .click();
      cy.wait(3000);
      // enter entity-browser form iframe
      cy.modalImageLibraryChoice('media66');

      cy.get('.field--name-field-p-slide-cta').eq(0).within(() => {
        cy.autocompleteInputField('Lantern Shuttle (17911)', 'Test link 1');
      }); // end cta form within

// add second slide
      cy.get('.field--name-field-p-carousel-slide .paragraphs-add-wrapper input.field-add-more-submit').click();
      cy.wait(3000);

      cy.get('.field--name-field-p-slide-title').eq(1)
        .type('Test Slide 2');
      // select slide image from library
      // upload slide image
      // open image modal window
      cy.get('.field--name-field-p-slide-image  input.button.form-submit').eq(1)
        .click();
      cy.wait(3000);
      // enter entity-browser form iframe
      cy.modalImageLibraryChoice('media36');

      cy.get('.field--name-field-p-slide-cta').eq(1).within(() => {
        cy.autocompleteInputField('Trees on Campus (20796)', 'Test link 2');
      }); // end cta form within

// save node
      cy.saveNode('ak test flex');
      cy.get('div.carousel h2').eq(0)
        .should('include.text', 'Test Slide 1');
      // find image list
      cy.get('h2.imglist__section-title')
        .should('include.text', 'Dance!');
    });

  });
});

