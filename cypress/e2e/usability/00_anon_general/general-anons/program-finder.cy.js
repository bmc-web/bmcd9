describe('Program Finder - test list page, search and detail node', function() {
	context('Desktop', () => {
		beforeEach(() => {
			cy.viewport(1280, 720);
		});

		it('visit program finder list page, filter by checkbox, clear result', function() {
			cy.visit('/academics/majors-minors-concentrations?query=&type=&interests=');
			cy.get('.header__logo img')
				.should('be.visible');
      cy.get('h1')
        .should('contain.text', 'Majors, Minors, and Concentrations');
      // counts program items and compares count to number in results count
      cy.countItemCompare('.program__result', '.program__results-count');
      // filter with checkboxes
      cy.get('.header__logo img').scrollIntoView();
      cy.get('div.program__interests input#1641').click({force:true});
      cy.get('div.program__interests input#1661').click({force:true});
      cy.get('div.program__interests input#4051').click({force:true});
      cy.get('.program-filter__panels__results')
        .should('have.text', '22 Programs See Results');

      // counts program items and compares count to number in results count
      cy.countItemCompare('.program__result', '.program__results-count');
      // clear results
      cy.get('.program-filter__panels').scrollIntoView();
      cy.get('button.program__clear').click();
      cy.wait(1000);
      // counts program items and compares count to number in results count
      cy.countItemCompare('.program__result', '.program__results-count');
		});


    it('test tool tip functionality', function() {
      cy.visit('/academics/majors-minors-concentrations?query=&type=&interests=');
      cy.get('.program__result').eq(0).scrollIntoView();
      cy.get('.program__label button.program__identifier[aria-describedby=type-051416]')
        .click();
      cy.get('div.program__result__type div#type-051416.program__identifier__tooltip')
        .should('have.attr', 'style', 'display: block;');
      cy.get('.program__label button.program__identifier[aria-describedby=type-051416]')
        .click();
      cy.get('div.program__result__type div#type-051416.program__identifier__tooltip')
        .should('have.attr', 'style', 'display: none;');
      cy.get('.program__result').eq(10).scrollIntoView();
      cy.get('.program__label button.program__identifier[aria-describedby=type-051476]')
        .click();
      cy.wait(1000);
      cy.get('div.program__result__type div#type-051476.program__identifier__tooltip')
        .should('have.attr', 'style', 'display: block;');
      cy.get('.program__label button.program__identifier[aria-describedby=type-051476]')
        .click();
      cy.get('div.program__result__type div#type-051476.program__identifier__tooltip')
        .should('have.attr', 'style', 'display: none;');
      cy.get('.program__result').eq(14).scrollIntoView();
      cy.get('.program__label button.program__identifier[aria-describedby=type-251521]')
        .click();
      cy.wait(1000);
      cy.get('div.program__result__type div#type-251521.program__identifier__tooltip')
        .should('have.attr', 'style', 'display: block;');
      cy.get('.program__label button.program__identifier[aria-describedby=type-251521]')
        .click();
      cy.get('div.program__result__type div#type-251521.program__identifier__tooltip')
        .should('have.attr', 'style', 'display: none;');
    });
    it('search by program type', function() {
      cy.visit('/academics/majors-minors-concentrations?query=&type=&interests=');
      cy.get('#program-finder form').within(() => {
        cy.get('input#program-search').type('french') // Only yield inputs within form
      })
      cy.get('div.search-field input[type=submit]').click({force:true});
      // counts program items and compares count to number in results count
      cy.countItemCompare('.program__result', '.program__results-count');

    });

      it('visit program detail node', function() {
      cy.visit('/academics/majors-minors-and-concentrations/classical-near-eastern-archaeology');
      cy.get('.header__logo img')
        .should('be.visible');
        cy.get('h1.page-title')
          .should('contain.text', 'Classical and Near Eastern Archaeology');
        cy.get('a.fab__cta--btn').eq(0)
          .should('contain.text', 'Apply');
        cy.get('h2.special-list-small__section-title')
          .should('contain.text', 'Key Information');
        cy.get('li.special-list-small__list--item a').eq(2)
          .should('contain.text', 'Faculty and Staff');
        cy.get('.paragraph--type--slistlg').eq(0).scrollIntoView();
        cy.get('.paragraph--type--slistlg').eq(0).within(() => {
          cy.get('h2.slistlg__section-title')// Only yield elements within parent
            .should('contain.text', 'Degree Options');
          cy.get('h3.slistlg__items--item--title')
            .should('contain.text', 'Concentration in Geoarchaeology');
          cy.get('.slistlg__items--item--body')
            .should('contain.text', 'The Department of Anthropology participates with Classical and Near Eastern Archaeology and Geology in offering a concentration within the major in geoarchaeology');
        }); // end special list large within
    });

  it('visit link in callout', function() {
    cy.visit('/academics/majors-minors-and-concentrations/classical-near-eastern-archaeology');
    cy.get('.paragraph--type--callout').scrollIntoView();
    cy.get('a.callout__cta--primary').click();
    cy.url()
      .should('include', '/phd-program-classical-near-eastern-archaeology');
    cy.get('.header__logo img')
      .should('be.visible').and('have.attr', 'alt').and('eq', 'Graduate School of Arts and Sciences of Bryn Mawr College');
    cy.get('nav.breadcrumb li:last')
      .should('contain.text', 'Academics');
  });
	});
});
