describe('Stories - List and Node Pages', function() {
  context('Desktop', () => {
    beforeEach(() => {
      cy.viewport(1280, 720);
    })

    it('visit listing page', function() {
      cy.visit('/stories');
      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Bryn Mawr College');
      cy.get('h1.page-title')
        .should('include.text', 'Bryn Mawr Stories');
      cy.get('.node--view-mode-teaser-1-up').should('be.visible');
      cy.wait(3000);
      cy.get('.story-feature__right--quote blockquote')
        .should('be.visible');
      cy.get('div.view-display-id-listing .views-row').eq(8).within(() => {
        cy.get('a .story-listing__cta .field--name-field-story-cta').should('be.visible');
      });
      cy.get('div.view-display-id-listing .views-row').eq(9).within(() => {
        cy.get('[class$=label--text]').should('be.visible');
      });
      cy.get('div.view-display-id-listing .views-row').eq(11).within(() => {
        cy.get('.node__content.story-listing__items--item a').should('have.attr', 'href')
          .and('include', 'stories');
        cy.get('.node__content.story-listing__items--item a')
          .click();
      });
    });

    it('use pager', function() {
      cy.visit('/stories');
      cy.get('.pager__item.pager__item.pager__item--previous').scrollIntoView
      cy.get('.pager__item--next a')
        .click();
      cy.url()
        .should('include', 'page=1'); cy.get('.pager__item.pager__item.pager__item--previous').scrollIntoView
      cy.get('.pager__item--previous a')
        .focus();
    });

    it('visit detail page - video', function() {
      cy.visit('/stories/meagan-murray-bruce-20');
      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Bryn Mawr College');
      cy.get('h1.page-title')
        .should('include.text', 'Meagan Murray-Bruce \'20');
      cy.get('.video-embed-field-launch-modal')
        .click();
      cy.wait(3000);
      cy.get('#colorbox .video-embed-field-provider-youtube iframe')
        .should('be.visible');
      cy.wait(2000);
      cy.get('button#cboxClose')
        .click();
      cy.wait(3000);
      cy.get('div.story__tags').scrollIntoView();
      cy.get('div.story__tags ul li').eq(0)
        .should('include.text', 'Mathematics')
        .click()
      cy.url()
        .should('include', '?tagged=');
    });

    it('visit detail page - feature person', function() {
      cy.visit('/stories/amara-gregorek-23');

      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Bryn Mawr College');
      cy.get('div.content__page-header__inner').within(() => {
        cy.get('h1.page-title')
          .should('include.text', 'Amara Gregorek \'23');
        cy.get('div.featured-full-image picture img')
          .should('be.visible');
        cy.get('blockquote.quote--title.story__header--quote')
          .should('include.text', 'In high school I volunteered at DOROT');
      });
      cy.get('article.story').scrollIntoView();
      cy.get('.story__header--intro-text')
        .should('include.text', 'Mawrters define success on their own terms');
      cy.get('div.story__tags').scrollIntoView();
      cy.get('div.story__tags ul li').eq(0)
        .should('include.text', 'Literatures in English')
        .and('be.visible');
    });

    it('visit detail page - feature story', function() {
      cy.visit('/stories/closer-look-360deg-course-clusters');
      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Bryn Mawr College');
      cy.get('div.content__page-header__inner').within(() => {
        cy.get('h1.page-title')
          .should('include.text', 'A Closer Look at 360° Course Clusters');
        cy.get('div.featured-full-image picture img')
          .should('be.visible');
        cy.get('blockquote.quote--title.story__header--quote')
          .should('include.text', 'Pictured: Students in the 360°: Textiles in Context');
      });
      cy.get('article.story').scrollIntoView();
      cy.get('.story__header--intro-text')
        .should('include.text', '360° offers an interdisciplinary experience for students and faculty');
      cy.get('div.story__tags').scrollIntoView();
      cy.get('div.story__tags ul li').eq(0)
        .should('include.text', '360')
        .and('be.visible');
      cy.get('.special-list-small').scrollIntoView();
      cy.get('.special-list-small__section-title')
        .should('include.text','Learn More');
      cy.get('li.special-list-small__list--item a').eq(3)
        .should('have.text', 'Previous Clusters')
        .focus().click();
      cy.wait(6000);
      cy.url()
        .should('include', '/inside/academic-information/special-academic-programs/360-program/previous-clusters');
    });

    it('visit tag listing page via detail page', function() {
      cy.visit('/stories/gabrielle-farrell-12');

      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Bryn Mawr College');
      cy.get('h1.page-title')
        .should('include.text', 'Gabrielle Farrell \'12');
      cy.get('article.story').scrollIntoView();
      cy.get('div.story__header--img img')
        .should('be.visible');
      cy.get('blockquote.story__header--quote')
        .should('include.text', 'The culture at Bryn Mawr: the Honor Code');
      cy.get('div.story__tags ul li').eq(1)
        .should('include.text', 'Alumnae/i')
        .click();
      cy.wait(3000);
      cy.url()
        .should('include', '?tagged=4021');
      cy.get('h1.page-title')
        .should('include.text', 'Alumnae/i Stories');
      cy.get('.views-row article.story-listing').eq(0).within(() => {
        cy.viewsRowLink('a .story-listing__cta.cta-button');
        cy.viewsRowLink('.story-listing__story-label--text');
      });
    });

  });
});
