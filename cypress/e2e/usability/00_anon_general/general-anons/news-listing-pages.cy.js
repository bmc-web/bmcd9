describe('News List Pages - all themes + flex list display', function() {
  context('Desktop', () => {
    beforeEach(() => {
      cy.viewport(1280, 720);
    });

    it('visit listing page - main', function() {
      cy.visit('/news');
      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Bryn Mawr College');
      cy.get('h1.page-title')
        .should('include.text', 'News');
      cy.get('div.view-display-id-1up article.news-feature')
        .should('be.visible');
      cy.get('div.view-display-id-listing .views-row').eq(0).within(() => {
        cy.get('article').should('have.class', 'news-teaser-3up');
      });
    });

    it('use pager', function() {
      cy.visit('/news');
      cy.get('.pager__item.pager__item.pager__item--previous').scrollIntoView
      cy.get('.pager__item--next a')
        .click();
      cy.url()
        .should('include', 'page=1');
      cy.get('.pager__item.pager__item.pager__item--previous').scrollIntoView
      cy.get('.pager__item--next a')
        .click();
      cy.url()
        .should('include', 'page=2');
    });

    it('visit listing page - inside', function() {
      cy.visit('/inside/latest/news');
      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Inside Bryn Mawr');
      cy.get('h1.page-title')
        .should('include.text', 'News');
      cy.get('div.view-display-id-1up article.news-feature')
        .should('be.visible');
      cy.get('div.view-display-id-listing .views-row').eq(0).within(() => {
        cy.get('article').should('have.class', 'news-teaser-3up');
      });
      cy.get('.pager__item.pager__item.pager__item--previous').scrollIntoView();
      cy.get('.pager__item--next a')
        .click();
      cy.url()
        .should('include', 'page=1');
    });

    it('visit listing page - gsas', function() {
      cy.visit('/gsas/news');
      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Graduate School of Arts and Sciences of Bryn Mawr College');
      cy.get('h1.page-title')
        .should('include.text', 'News');
      cy.get('div.view-display-id-1up article.news-feature')
        .should('be.visible');
      cy.get('div.view-display-id-listing .views-row').eq(0).within(() => {
        cy.get('article').should('have.class', 'news-teaser-3up');
      });
      cy.get('.pager__item.pager__item.pager__item--previous').scrollIntoView();
      cy.get('.pager__item--next a')
        .click();
      cy.url()
        .should('include', 'page=1');
    });

    it('visit listing page - socialwork', function() {
      cy.visit('/socialwork/news');
      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Graduate School of Social Work and Social Research of Bryn Mawr College');
      cy.get('h1.page-title')
        .should('include.text', 'News');
      cy.get('div.view-display-id-1up article.news-feature')
        .should('be.visible');
      cy.get('div.view-display-id-listing .views-row').eq(0).within(() => {
        cy.get('article').should('have.class', 'news-teaser-3up');
      });
      cy.get('.pager__item.pager__item.pager__item--previous').scrollIntoView();
      cy.get('.pager__item--next a')
        .click();
      cy.url()
        .should('include', 'page=1');
    });

    it('visit listing page - postbac', function() {
      cy.visit('/postbac/news');
      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Postbaccalaureate Premedical Program of Bryn Mawr College');
      cy.get('h1.page-title')
        .should('include.text', 'News');
    });

    it('visit tagged listing - main', function() {
      cy.visit('/news?tagged=3671');
      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Bryn Mawr College');
      cy.get('h1.page-title')
        .should('include.text', 'Faculty Publications Articles');
      cy.get('div.news-tagged__header--tags__cta-button a.cta--button--primary')
        .should('have.attr', 'href', '/news').and('contain.text', 'All College News');

      cy.get('div.view-display-id-listing .views-row').eq(0).within(() => {
        cy.get('article').should('have.class', 'news-teaser-3up');
      });
      cy.get('.pager__item.pager__item.pager__item--previous').scrollIntoView
      cy.get('.pager__item--next a')
        .click();
      cy.url()
        .should('include', 'page=1');
    });

    it('visit tagged listing - inside', function() {
      cy.visit('/inside/latest/news?tagged=4436');
      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Inside Bryn Mawr');
      cy.get('h1.page-title')
        .should('include.text', 'Blended Learning Articles');

      cy.get('div.view-display-id-listing .views-row').eq(0).within(() => {
        cy.get('article').should('have.class', 'news-teaser-3up');
      });
      cy.get('.pager__item.pager__item.pager__item--previous').scrollIntoView
      cy.get('.pager__item--next a')
        .click();
      cy.url()
        .should('include', 'page=1');
    });

    it('visit tagged listing - gsas', function() {
      cy.visit('/gsas/news?tagged=4096');
      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Graduate School of Arts and Sciences of Bryn Mawr College');
      cy.get('h1.page-title')
        .should('include.text', 'Graduate School of Arts and Sciences Articles');
      cy.get('div.news-tagged__header--tags__cta-button a.cta--button--primary')
        .should('have.attr', 'href', '/gsas/news').and('contain.text', 'All GSAS News');
      cy.get('div.view-display-id-listing .views-row').eq(0).within(() => {
        cy.get('article').should('have.class', 'news-teaser-3up');
      });
      cy.get('.pager__item.pager__item.pager__item--previous').scrollIntoView
      cy.get('.pager__item--next a')
        .click();
      cy.url()
        .should('include', 'page=1');
    });

    it('visit tagged listing - socialwork', function() {
      cy.visit('/socialwork/news?tagged=13');
      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Graduate School of Social Work and Social Research of Bryn Mawr College');
      cy.get('h1.page-title')
        .should('include.text', 'Graduate School of Social Work and Social Research Articles');
      cy.get('div.news-tagged__header--tags__cta-button a.cta--button--primary')
        .should('have.attr', 'href', '/socialwork/news').and('contain.text', 'All GSSWSR News');
      cy.get('div.view-display-id-listing .views-row').eq(0).within(() => {
        cy.get('article').should('have.class', 'news-teaser-3up');
      });
      cy.get('.pager__item.pager__item.pager__item--previous').scrollIntoView();
      cy.get('.pager__item--next a')
        .click();
      cy.url()
        .should('include', 'page=1');
    });

    it('news flex on basic page display', function() {
      cy.visit('/inside/academic-information/departments-programs/computer-science/about-computer-science/news-events');
      cy.get('h1.page-title')
        .should('be.visible');
      cy.get('ul.menu.top-level.is-open li.current-active')
        .should('contain.text', 'About Computer Science');
      cy.get('ul.menu.top-level.is-open li.current-active ul li.menu-item--active-trail')
        .should('contain.text', 'News');
      cy.get('.news-3-up.paragraph').scrollIntoView();
      cy.get('.news-3-up__more a')
        .should('have.attr', 'href', '/inside/latest/news?tagged=4216');
      cy.get('h3.news-listing__title').eq(0)
        .should('be.visible');
    });

  });
});
