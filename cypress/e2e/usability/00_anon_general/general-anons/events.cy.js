describe('Inside: Events', function() {
  context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
    beforeEach(() => {
      cy.viewport(1280, 720);
    });

    it('visit events calendar', function() {
      cy.visit('/inside/latest/events');
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('h1.page-title')
        .should('contain.text', 'Events');
    });

    it('use pager', function() {
      cy.visit('/inside/latest/events');
      cy.get('h2.event-listing-page__main_listing').scrollIntoView();
      cy.get('.form-item-date input[id^=edit-date]')
        .focus()
        .type('03/06/2023{enter}{enter}');
      cy.wait(3000);
      cy.get('.form-item-date .form-item__wrap').click();
      // tests if pagerer, pagerer function and filterfocus js
      cy.eventPagerer();
    });

    it('filter by event type and clear filter', function() {
      cy.visit('/inside/latest/events');
      cy.get('h2.event-listing-page__main_listing').scrollIntoView();
      cy.get('div#edit-type div.form-checkboxes input#edit-type-326').click({force:true})
      cy.url()
        .should('include', '?type[326]=326');
      cy.wait(2000);
      cy.get('.views-row h4.event-teaser-3up__title').contains( 'Ashwini Ramaswamy');
      cy.wait(2000);
      cy.get('.form-actions [id^=edit-reset--]')
        .click();
    });

    it('filter by start date', function () {
      cy.visit('/inside/latest/events');
      cy.get('h2.event-listing-page__main_listing').scrollIntoView();
      cy.get('.form-item-date input[id^=edit-date]')
        .focus();
      cy.get('#ui-datepicker-div')
        .should('be.visible');
      cy.get('.ui-datepicker-prev')
        .click();
      cy.get('.ui-datepicker-calendar tbody a')
        .first()
        .click();
      cy.url()
        .should('include', '?date');
    });

    it('visit event detail page', function() {
      cy.visit('/inside/latest/events/staff-picnic');
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('h1.page-title.event__title')
        .should('contain.text', 'Staff Picnic');
      cy.get('.event__title--wrap__location')
        .should('include.text', 'Outdoor Space, Sunken Garden');
      cy.get('span.event__audience--label').scrollIntoView();
      cy.get('.event__audience')
        .should('include.text', 'For Faculty and Staff');
      cy.get('.field--name-field-event-contact-email a')
        .should('have.text', 'mpyle@brynmawr.edu');
      cy.get('.event__disclaimer')
        .should('include.text', 'Should you wish to request a disability-related accommodation for this event');
      // back to top
      cy.get('button.btt').click();
      cy.wait(2000);
      cy.focused().should('have.focus');
    });

    it('visit tag event page', function() {
      cy.visit('/inside/latest/events/tagged?tags=4226');
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('h1.page-title')
        .should('contain.text', 'Dance Events');
      cy.get('nav.breadcrumb li:last')
        .should('include.text', 'Events');
      cy.get('.event-tagged__header--tags__cta-button a')
        .should('have.text', 'All Events').and('have.attr', 'href', '/inside/latest/events');
      cy.get('article.event-teaser-3up h3 a').eq(0).focus().click();
      cy.wait(2000);
      cy.get('.event__contact').scrollIntoView();
      cy.get('div.event__tags.tagged-as')
        .should('include.text', 'Dance');
    });

    it('visit 3up event page', function() {
      cy.visit('/node/53216'); // web services all content basic page
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('h1.page-title')
        .should('contain.text', 'Basic Page');
      cy.get('nav.breadcrumb li:last')
        .should('include.text', 'Drupal Guides');
      cy.get('ul.menu.show-all-children.top-level.is-open li.sec-duplicate.current-active a')
        .should('include.text', 'Drupal Guides');
      cy.get('ul.menu.show-all-children.top-level.is-open li.sec-duplicate.current-active ul li.menu-item.menu-item--active-trail a.is-active')
        .should('include.text', 'Basic Page Style Guide');
      cy.get('h2.event-3-up__section-heading')
        .should('contain.text', 'Event 3up');
      cy.get('div.event-3-up__intro').should( 'be.visible');
      cy.get('article.event-teaser-3up h3.event-teaser-3up__title').eq(0).should('be.visible');
      cy.get('article.event-teaser-3up h3.event-teaser-3up__title').eq(1).scrollIntoView();
      cy.get('.event-3-up__more a.cta--primary')
        .should('have.text', 'Events Home').and('have.attr', 'href', '/inside/latest/events')
        .focus()
        .click();
      cy.wait(2000);
      cy.url()
        .should('include', '/inside/latest/events');
      cy.get('h1.page-title')
        .should('include.text', 'Events');
      cy.get('nav.breadcrumb li:last')
        .should('include.text', 'The Latest');
      cy.get('.event-teaser-3up__date').eq(1).should('be.visible');

      // tests if pagerer and pagerer function
      cy.anyPagerer('[data-viewname="event-listing"]', 'Events');

    });

    it('create anonymous event', function() {
      cy.visit('/inside/events/submit');
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('h1.page-title')
        .should('include.text', 'Submit Event');
      cy.get('nav.breadcrumb li:first')
        .should('include.text', 'Inside Bryn Mawr');
      cy.get('form.node-event-form').within(() => { // Only yield inputs within form
        cy.get('input#edit-title-0-value').type('Test Event 1')
        cy.get('input#edit-field-event-date-0-time-wrapper-value-date').type('2026-07-04');
        cy.get('input#edit-field-event-date-0-time-wrapper-value-time').type('13:00');
        cy.get('input#edit-field-event-date-0-time-wrapper-end-value-time').type('20:00');

        cy.get('input#edit-field-event-location-choice-hybrid-on-campus').click({force:true});
        cy.get('input#edit-field-event-reservation-number-0-value').type('111111');

        cy.get('select#edit-field-event-location')
          .select('106');

        cy.get('input#edit-field-virtual-event-access-link-0-uri')
          .type('https://brynmawr-edu.zoom.us/meeting/register/tJUtc-GtrzorHtG3PafivgKuh9CV5YEkrhky');

        cy.get('input#edit-field-event-location-details-0-value')
          .type('Test Location');

        cy.get('input#edit-field-event-type-316').click({force:true});
        cy.get('input#edit-field-event-audience-21').click({force:true});
        cy.get('input#edit-field-event-submitter-email-0-value').type('me@brynmawr.edu');
        cy.get('input#edit-field-event-contact-name-0-value').type('Test Submitter');
        cy.get('input#edit-field-event-contact-email-0-value').type('me@swarthmore.edu');
        cy.get('textarea#edit-field-event-blurb-0-value').type('This is my blurb');
        cy.get('.field--name-field-event-body').scrollIntoView();
        cy.get('#edit-field-event-body-0-value').type('This is the body of the event. We are making it longer so that we can really test how the event looks with some text in it. On our historic and verdant suburban campus 11 miles from Philadelphia, a world-class faculty guides and challenges 1,300 undergraduate women and more than 400 graduate women and men from 45 states and 62 countries. Bryn Mawr is a place where teaching is valued, each student is known, and students and faculty members form close bonds. ');
        cy.get('table#field-ddigest-date-values').scrollIntoView();
        cy.get('input#edit-field-ddigest-date-0-value-date').type('2026-06-30');
        cy.get('input#edit-field-ddigest-date-1-value-date').type('2026-07-03');
        cy.get('.captcha').scrollIntoView();
        // captcha calculation

        cy.captchaMath();

        cy.get('#edit-status-value--description')
          .should('include.text', 'You can not change the Publish status.').and('be.visible');
        // SAVE node
        cy.get('input#edit-submit')
          .focus()
          .click();
      }); // end within block
      // checks success message on node
      cy.get('li.messages__item').eq(0)
        .should('include.text', 'An email notification has been sent to me@brynmawr.edu');
      cy.get('li.messages__item').eq(2)
        .contains('Thank you for completing the Event/Daily Digest submission form!');
      cy.get('h1.page-title')
        .should('include.text', 'Events');
      cy.get('h2.event-listing-page__main_listing')
        .should('include.text', 'All Upcoming Events');
    });
  });
});
