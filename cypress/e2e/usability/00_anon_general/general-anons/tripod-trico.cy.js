describe('Anonymous - Trico+Tripod', function() {
	context('Desktop', () => {
		beforeEach(() => {
			cy.viewport(1280, 720);
		});

		it('trico course guide search form, view results, visit page from results', function() {
			cy.visit('/inside/academic-information/registrar/tri-co-course-search');
			cy.get('.header__logo img')
				.should('be.visible');
      cy.get('form#trico-course-guide-search-form h1')
        .should('contain.text', 'Tri-Co Course Search');
      cy.get('input#edit-keyword')
        .type('art')
        .should('have.value', 'art');
      cy.get('div#edit-semester input.form-checkbox ').eq(3).check({force: true});
      cy.get('input#edit-submit')
        .should('have.value', 'Search').click();
      // trico course results
      cy.url()
        .should('include', 'results?');
      cy.get('h1')
        .should('contain.text', 'Course Search Results');
      cy.get('table')
        .should('have.class','sticky-header' );
      cy.get('table tr.odd td:first-child a').eq(8)
        .click({force: true});
      // trico single course page
      cy.get('h1')
        .should('contain.text', 'Course Info');
      cy.get('td')
        .should('contain.text', 'Additional Course Info');
      cy.get('p.trico-pager').scrollIntoView();
      cy.get('p.trico-pager a').eq(1)
        .should('contain.text', 'Return to Results')
        .click();
      cy.get('p.trico-pager').scrollIntoView();
      cy.get('p.trico-pager a').eq(0)
        .should('contain.text', 'New Search')
        .click();
      cy.get('form#trico-course-guide-search-form h1')
        .should('contain.text', 'Tri-Co Course Search');
    });

    it('tripod search form test', function(){
      cy.visit('/inside/offices-services/library-information-technology-services');
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('form#searchForm');
      cy.get('input#primoQueryTemp')
        .focus()
        .type('art');
      cy.get('button#submit')
        .should('contain.text','Search Tripod')
        .click();
     });
	});
});
