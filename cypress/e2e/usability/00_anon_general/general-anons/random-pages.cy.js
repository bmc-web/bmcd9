describe('Random: tests pages for protected, sticky, embed and incontext blocks', function() {
	context('Desktop', () => {
		beforeEach(() => {
			cy.viewport(1280, 720);
		});

		it('visit campus map via inside homepage menu', function() {
      cy.visit('/inside');
      cy.get('.header-container__menu--left').within(() => {
        // inside menu-left is really the right side icons in menu
        // accordion-directions
        cy.get('.accordion__item.accordion--directions').within(() => {
          cy.get('button.accordion__toggle')
            .should('have.attr', 'aria-expanded', 'false')
            .focus()
            .type('{enter}');
          //  this uses the keyboard to select the focused item by using the enter key to expand menu
          cy.get('button.accordion__toggle')
            .should('have.attr', 'aria-expanded', 'true');

          cy.get('.inside__panel div#block-menu-inside-location ul.menu li.menu-item a').eq(1)
            .should('include.text', 'Interactive Campus Map')
            .and('have.attr', 'href', '/campusmap/')
            .focus()
            .click();
        }); // END directions within
		  }); // END menu-left within
      cy.wait(6000);
      cy.get('iframe')
        .should('be.visible')
        .and('have.attr', 'id', 'map_frame');
    });

    it('incontext block on LITS page', function() {
      cy.visit('/inside/offices-services/library-information-technology-services');
      cy.get('h1.page-title')
        .should('include.text', 'Library & Information Technology Services');

      cy.get('.region-content-bottom-wrapper').within(() => {
       cy.get('#block-ic-lits-hours').within(() => {
         cy.get('h3')
           .should('include.text', 'Today\'s Hours');
         cy.get('div#api_hours_today_iid1479_lid0').should('be.visible');
       }); // END lits embed block within

        cy.get('#block-litscontact').within(() => {
          cy.get('h2.contact-wrap__section-heading')
            .should('include.text', 'Contact Us');
          cy.get('h3.contact-wrap__title')
            .should('include.text', 'Library and Information Technology Services');
          cy.get('.contact-wrap__body .field--name-field-p-contact-body p').eq(1)
            .should('include.text', 'Office of the CIO:').and('contain', '610-526-5271');
        }); // END contact block within
      }); // END content-bottom within
    });

    it('table sticky header', function() {
      cy.visit('/inside/offices-services/transportation/blue-bus-bi-co');
      cy.get('h1.page-title')
        .should('include.text', 'Blue Bus: Bi-Co');
      cy.get('.table.table--wysiwyg').eq(1).within(() => {
       cy.get('table')
         .should('have.class','sticky-header');
       cy.get('thead')
         .should('have.attr', 'name', 'table-1')
         .and('have.class', 'table-scroller syncscroll');
        cy.get('tbody')
          .should('have.attr', 'name', 'table-1')
          .and('have.class', 'table-scroller syncscroll');
      }); // END 1st table-wysiwyg within

    });

    it('password protected page - wrong password', function() {
      cy.visit('/inside/offices-services/alumnaei-relations-development/volunteering/alumnaei-admissions-representatives/aar-resources-tools');
      cy.get('#block-bmc-d9-content')
        .should('not.include.text', 'AAR Resources and Tools');
      cy.get('form.protected-pages-enter-password').within(() => {
       cy.get('.protected_pages_description')
         .should('include.text', 'The page you are trying to view is password protected. Please enter the password below to proceed.');
       cy.get('input#edit-password')
         .focus()
         .type('password');
       cy.get('input#edit-submit')
         .focus()
         .click();
      }); // END protected page form within
      cy.get('#block-bmc-d9-content')
        .should('not.include.text', 'AAR Resources and Tools');
      cy.get('.region-help div.messages.messages--error')
        .should('include.text','Incorrect password!');
    });

    // Moved correct protected password test to site-editor/protected-page-test.cy.js

    it('embed as page', function() {
      cy.visit('/inside/offices-services/access-services/document-converter');
      cy.get('nav.breadcrumb ol li:last-of-type')
        .should('include.text', 'Access Services');
      cy.get('h1.page-title')
        .should('include.text', 'Document Converter');
      cy.get('.field.field--name-field-p-embed-body').within(() => {
       cy.get('p').eq(0)
         .should('include.text', 'The Document Converter automates the conversion of documents into a range of alternative formats');
       cy.get('p').eq(1).children('iframe').should('be.visible');
       cy.get('h3').should('include.text', 'Getting Started');
        cy.get('p').eq(3).children('iframe').should('be.visible');
      }); // END embed within
    });

	});
});
