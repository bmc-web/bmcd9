describe('News detail pages', function() {
  context('Desktop', () => {
    beforeEach(() => {
      cy.viewport(1280, 720);
    });

    it('visit detail page', function() {
      cy.visit('/news/emily-shein-22-awarded-fulbright-eta');
      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Bryn Mawr College');
      cy.get('h1.page-title')
        .should('include.text', 'Emily Shein \'22 Awarded Fulbright ETA');
      cy.get('figure.news__detail-image')
        .should('be.visible');
      cy.get('figure.news__detail-image figcaption.news__detail-image--caption')
        .should('include.text', 'Emily Shein \'22');
      cy.get('div.field--name-field-news-body').contains('After her Fulbright, Shein plans to attend graduate school to pursue a Ph.D. in 18th-19th century German philosophy.');
      cy.get('div.news__tags').scrollIntoView();
      cy.get('div.news__tags ul li').eq(1)
        .should('include.text', 'German and German Studies');
      // verify sharethis rendered
      cy.get('[data-network="twitter"] a')
        .focus();
    });

    it('visit news with all content and check tagged content', function() {
      cy.visit('/node/52296');
      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Bryn Mawr College');
      cy.get('h1.page-title')
        .should('include.text', 'News with all content - Web Services All Styles');
      cy.get('.news__date').contains('November 29, 2021');
      cy.get('.news__author').contains('Andrea Kaldrovics');
      cy.get('figure.news__detail-image.detail_wide img')
        .should('have.attr', 'alt', 'campus outdoor');
      cy.get('[data-network="facebook"] a')
        .focus();
      cy.get('[data-network="facebook"] a img')
        .should('have.attr', 'alt', 'Share on Facebook');
      cy.get('div.contact').scrollIntoView();
      cy.get('div.contact__img img')
        .should('have.attr', 'alt', 'Lantern Burst');
      cy.get('h3.contact-wrap__title')
        .should('include.text', 'Department of Web Services');
      cy.get('.story-1-up.paragraph').scrollIntoView();
      cy.wait(3000);
      cy.get('.story-1-up.paragraph .quote__name')
        .should('be.visible');
      cy.get('div.story-3-up').scrollIntoView();
      cy.get('div.story-3-up__more a')
        .should('have.attr', 'href', '/stories');
      cy.get('.story-listing__story-label').eq(1)
        .should('include.text', 'New Faculty');
      cy.get('div.story-listing__quote blockquote').eq(1)
        .should('include.text', 'Over the next few years');
      cy.get('.table--wysiwyg').eq(0).scrollIntoView();
      cy.get('div.news__body--source').contains('Read more: BMC Tech Docs');
      cy.get('div.news__body--source a')
        .should('include.text', 'BMC Tech Docs')
        .and('have.attr', 'href','https://askathena.brynmawr.edu/help/web-editing');
      cy.get('div.news__body--source')
        .next('div')
        .should('have.class', 'news__body--paras');
      cy.visit('/news/faculty-publication-associate-professor-yonglin-jiang');
      cy.get('div.news__tags').scrollIntoView();
      cy.get('div.news__tags ul li').eq(0)
        .should('include.text', 'Faculty Publication')
        .click()
      cy.url()
        .should('include', '?tagged=');
    });
  });
});
