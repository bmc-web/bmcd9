describe('Anon site availability', function() {
	context('Desktop', () => {
		beforeEach(() => {
			cy.viewport(1280, 720);
		})

		it('visit login page', function() {
			cy.visit('/user');
      cy.get('input#edit-submit')
          .focus()
          .click();

      cy.url()
        .should('include', '/login');
      cy.contains('Access denied').should('not.exist');
		});

    it('enter wrong password', function() {
      cy.visit('/user/login?local_login');
      cy.get('form.user-login-form input#edit-name')
        .focus()
        .type('some user');
      cy.get('form.user-login-form input#edit-pass')
        .focus()
        .type('some pw');
      cy.get('form.user-login-form input#edit-submit')
        .focus() // Ensure submit is focusable
        .click();
      cy.get('[role="alert"]').contains('Unrecognized username or password.');
    });

    it('event and announcement forms available', function() {
      cy.visit('/inside/announcements/submit');
      cy.get('.page-title')
        .should('contain.text', 'Submit Announcement');
      cy.get('.captcha legend')
        .should('contain.text', 'CAPTCHA');
      cy.visit('/inside/events/submit');
      cy.get('.page-title')
        .should('contain.text', 'Submit Event');
      cy.get('.captcha legend')
        .should('contain.text', 'CAPTCHA');
    })

    it('anon denied auth URL access', function() {
      cy.visit('/admin/content', {failOnStatusCode: false});
      cy.get('.page-title')
        .should('contain.text', 'Error 403 -- Access Denied');
    });
	});
});
