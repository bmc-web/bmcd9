describe('Inside: Basic Page All Content', function() {
  context('Desktop', () => {
    beforeEach(() => {
      cy.viewport(1280, 720);
      cy.visit('/node/53216');
    });

    it('visit basic page with all content', function() {
     cy.get('.header__logo img')
        .should('be.visible');
      cy.get('h1.page-title')
        .should('contain.text', 'Basic Page With All Content — Web Services All Styles');
      cy.get('nav.breadcrumb li:last')
        .should('include.text', 'Drupal Guides');
      cy.get('ul.menu.top-level.is-open li.current-active')
        .should('contain.text', 'Drupal Guides');
      cy.get('ul.menu.top-level.is-open li.current-active ul li.menu-item--active-trail')
        .should('contain.text', 'Basic Page Style Guide');
    });

    it('quicklink anchor link test', function() {
      cy.get('.page__intro--quicklinks').within(() => {
        cy.get('li.special-list-small__list--item a').eq(4)
          .should('have.attr', 'href','#image-list-test-anchor')
          .click();
      });
      cy.get('.imglist__intro p').should('be.visible');
      cy.get('h2.accordion__header').scrollIntoView();
    });

    it('test accordion and anchor link to feature grid', function() {
      cy.get('.accordion__header')
        .should('be.visible');
      cy.get('.accordion__expand-all')
        .click()
      cy.get('.accordion__collapse-all')
        .click()
      cy.get('.accordion__toggle')
        .contains('How much will I love Bryn Mawr')
        .click();
        cy.get('p')
          .contains('Visit our feature grid')
        .click()
      cy.get('.feat_grid__intro').should('be.visible');
        cy.get('.feat_grid__intro')
        .contains('optional field');
      cy.get('.feat_grid__link').eq(0)
        .should('be.visible','google')
        .focus();
      cy.get('.callout__cta').scrollIntoView();
    });

    it('carousel test', function() {
      // Then I should scroll .carousel into view
      cy.get('.field--name-field-p-carousel-slide').scrollIntoView();
      // And then I should click next arrow
      cy.get('.slick-next')
        .focus()
        .click();
      // And then I should see the text slide 2
      cy.get('h2.slide__title').contains('slide 2').and('be.visible');
    });

    it('image gallery test', function() {
      // Then I should scroll .image-gallery into view
      cy.get('.image-gallery').scrollIntoView();
      // And then I should click button.image-gallery__open
      cy.get('.image-gallery__open')
        .click({force: true});
      // And then I should find the next arrow inside the #cboxContent
      cy.get('#cboxContent .image-gallery__slides').should('be.visible').within(() => {
        cy.get('.slick-next').click();
        // And then the .slick-slide aria-label=1 should have aria-hidden=true
        cy.get('.slick-slide').eq(0)
          .should('have.attr', 'aria-label', 'slide 1').and('have.attr', 'aria-hidden', 'true');
        // And aria-label=slide 2 should have class slick-current
        cy.get('.slick-slide.slick-current.slick-active')
          .should('have.attr', 'aria-label', 'slide 2').and('not.have.attr', 'aria-hidden', 'true');
      });

      cy.get('#cboxContent button#cboxClose')
        .focus()
        .click({force: true});
      cy.wait(2000);
      cy.get('.image-gallery--inner--intro').contains('This is the intro to image detail gallery,').and('be.visible');
      cy.wait(2000);
    });

    it('embed test', function() {
      cy.get('.paragraph--type--embed').eq(0).scrollIntoView();
      cy.get('.paragraph--type--embed').eq(0).within(() => {
        cy.get('.field--name-field-p-embed-body').within(() => {
          cy.get('iframe').should('be.visible');
        });
      });
    });

    it('wysiwyg media test', function() {
      cy.get('.media--view-mode-wysiwyg-video-full');
      cy.get('button.play-video').eq(1)
        .focus()
        .click();
      cy.wait(3000);
      cy.get('button#cboxClose')
        .click();
      cy.wait(3000);
      cy.get('.align-left .field--name-field-media-image.field--type-image img')
        .should('have.attr', 'alt', 'Academic Major tea');
    });
    it('news contact test', function() {
      cy.get('.news-contact').within(() => {
        cy.get('h2.news-wrap__section-heading')
          .should('have.text', 'News - tag selection covid');
        cy.get('a.news-title-only__link').eq(2)
          .should('be.visible');

        cy.get('.news-wrap__more').should('have.attr', 'href')
          .and('include', '?tagged=3771');
        cy.get('.contact-wrap h3.contact-wrap__title')
          .should('contain.text', 'Health Services');
      });

    });

    it('event 3up test', function() {
      cy.get('div.event-3-up').scrollIntoView();
      cy.get('.event-3-up__intro')
        .should('include.text', 'This is the intro to event 3up');
      cy.get('h3.event-teaser-3up__title').eq(0)
        .should('include.text', 'MLK Virtual Celebration');
    });

    it('perform search', function () {
      cy.get('.header__logo img').scrollIntoView();
      cy.get('.header-container__search-button')
        .click();
      cy.get('#headerSearch')
        .focus()
        .type('course');
      cy.get('#headerSearch + input[type=submit]')
        .focus() // Ensure submit is focusable
        .click();
    });

  });
});
