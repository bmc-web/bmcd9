describe('Directory tests: main list and search, people list and special character', function() {
	context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });

    beforeEach(() => {
      cy.viewport(1280, 720);
    });

		it('main directory list page', function() {
			cy.visit('/inside/people');
			cy.get('.header__logo img')
				.should('be.visible');
      cy.get('h1.page-title')
        .should('contain.text', 'Faculty and Staff Directory');

    });

    it('use pager', function() {
      cy.visit('/inside/people');
      cy.get('li.pager__item.is-active').scrollIntoView();
      cy.get('.pager__item--next a')
        .click();
      cy.url()
        .should('include', 'page=1');
      cy.get('li.pager__item.is-active').scrollIntoView();
      cy.get('.pager__item--next a')
        .click();
      cy.url()
        .should('include', 'page=2');
      cy.focused().should('have.class', 'view-header').and('contain.text', 'Results')

    });

    it('perform directory list page search', function() {
      cy.visit('/inside/people');
      cy.get('.header__logo img').scrollIntoView();
      cy.get('.form-select').eq(1)
        .select('2456');
      cy.get('h2.directory-teaser__name')
        .should('contain.text', 'Maria Albano');
      cy.get('.field--name-field-dir-title')
        .should('contain.text', 'Benefits and Data Coordinator');
      cy.get('div.directory-teaser__contact-building')
        .should('contain.text', 'Human Resources Office');

    });

    it('perform directory list ARD search', function() {
      cy.visit('/inside/people');
      cy.get('.header__logo img').scrollIntoView();
      cy.get('.form-select').eq(1)
        .select('2701');
      cy.wait(2000);
      cy.get('.view-header')
        .should('include.text', 'Results');
      // .node--view-mode-teaser
      cy.countItemEach('.node--view-mode-teaser');


      cy.get('.pager__item--next a')
        .click();
      cy.url()
        .should('include', 'page=1')
        .and('include', 'department=2701');

    });
    it('perform directory list Facilities search', function() {
      cy.visit('/inside/people');
      cy.get('.header__logo img').scrollIntoView();
      cy.get('.form-select').eq(1)
        .select('2286');
      cy.wait(2000);
      cy.get('.view-header')
        .should('include.text', 'Results');
      cy.countItemEach('.node--view-mode-teaser');
      cy.get('.pager__item--next a')
        .click();
      cy.url()
        .should('include', 'page=1')
        .and('include', 'department=2286');


    });

		it('people list page', function() {
      cy.visit('/node/53216');
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('h3.directory-teaser-ppllist__name').eq(2)
        .should('contain.text', 'Andrea Kaldrovics');
      });

		it('special character', function() {
      cy.visit('/inside/people/ines-arribas');
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('h1.page-title')
        .should('contain.text', 'Inés Arribas');
      cy.get('.directory__def-focus')
        .should('include.text','Language acquisition, Spanish cultural studies, slam poetry, spoken word.');
      cy.get('.field--name-field-dir-body')
        .should('include.text', 'Inés Arribas taught at Bryn Mawr in 1997-1998 and rejoined the department in fall 2001. Her field of research embraces several facets of Spanish Cultural Studies');
      cy.get('.directory__photo img').should('be.visible');
      cy.visit('/inside/people/maja-seselj');
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('h1.page-title')
        .should('contain.text', 'Maja Šešelj');
      cy.get('.directory__def-focus p')
        .should('include.text','Biological anthropology; the evolution of the modern human pattern of growth and development');
       cy.get('.directory__photo img').should('be.visible');
      cy.visit('inside/people/martin-l-gaspar');
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('h1.page-title')
        .should('contain.text', 'Martín L. Gaspar');
      cy.get('.directory__def-focus p')
        .contains('Latin American intellectual history');
      cy.get('.field--name-field-dir-body')
        .contains('Martín Gaspar’s research engages a wide range of fields that includes Latin American intellectual history since the 19th Century;');
      cy.get('.directory__photo img').should('be.visible');


    });

	});
});
