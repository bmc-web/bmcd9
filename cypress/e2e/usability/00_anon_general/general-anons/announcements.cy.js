describe('Inside: Events', function() {
  context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    })
    beforeEach(() => {
      cy.viewport(1280, 720);
    });

    it('visit announcements list page', function() {
      cy.visit('/inside/latest/announcements');
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('h1.page-title')
        .should('contain.text', 'Announcements');
    });

    it('use pager', function() {
      cy.visit('/inside/latest/announcements');
      cy.anyPagerer('.view-id-announcements', 'Announcements');
    });

    it('visit announcement detail page', function() {
      cy.visit('/inside/latest/announcements/wildfires-air-quality');
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('.anncmnt__header--intro-wrapper h1.page-title')
        .should('contain.text', 'Wildfires and Air Quality');
      cy.get('.anncmnt__date--date')
        .should('include.text','June 7, 2023');
      cy.get('.field--name-field-anncmnt-body')
        .should('be.visible');
      cy.get('.anncmnt__body--source a')
        .should('have.attr', 'href', 'https://www.airnow.gov/');
      // back to top
      cy.get('button.btt').click();
      cy.wait(2000);
      cy.focused().should('have.focus');
    });

    it('create anonymous announcement', function() {
      cy.visit('/inside/announcements/submit');
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('h1.page-title')
        .should('include.text', 'Submit Announcement');
      cy.get('nav.breadcrumb li:first')
        .should('include.text', 'Inside Bryn Mawr');

      cy.get('form.node-anncmnt-form').within(() => { // Only yield inputs within form
        cy.get('input#edit-title-0-value').type('anon announce');
        cy.get('textarea#edit-field-anncmnt-blurb-0-value').type('this blurb is short');
        cy.get('#edit-field-anncmnt-url-0-uri')
          .type('https://askathena.brynmawr.edu');
        cy.get('input#edit-field-anncmnt-submitter-0-value').type('me@brynmawr.edu');
        cy.get('#edit-field-anncmnt-submitter-0-value--description').scrollIntoView();
        cy.get('.field--name-field-anncmnt-body')
        cy.getIframeBody('.field--name-field-anncmnt-body iframe.cke_wysiwyg_frame')
          .type('This is my Test announcement body. Writing more than one sentence. Ta-da!');
        cy.wait(3000);
        cy.get('select#edit-field-ddigest-sec')
          .select('grad_schools');
        cy.get('input#edit-field-ddigest-date-0-value-date').type('2035-08-15');
        cy.get('input#edit-field-ddigest-date-1-value-date').type('2035-08-17');
        cy.get('.captcha').scrollIntoView();
       // captcha calculation

        cy.captchaMath();

        cy.get('#edit-status-value--description')
          .should('include.text', 'You can not change the Publish status.').and('be.visible');
 // SAVE node
        cy.get('input#edit-submit')
          .focus()
          .click();
      }) // end within block
 // checks success message on node
      cy.get('li.messages__item').eq(0)
       .should('include.text', 'An email notification has been sent to me@brynmawr.edu');
       cy.get('li.messages__item').eq(2)
         .contains('Thank you for completing the Announcement/Daily Digest submission form!');
      cy.get('h1.page-title')
        .should('include.text', 'Submit Your Announcement');
      cy.get('.field--name-field-wysiwyg-body')
        .should('include.text', 'What is the deadline for submitting an Announcement?');
    });
  });
});
