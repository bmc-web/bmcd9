describe('Office Service - search and node', function() {
	context('Desktop', () => {
		beforeEach(() => {
			cy.viewport(1280, 720);
		});

		it('office-service perform search and visit single page', function() {
			cy.visit('/inside/directory?query=&letter=all');
			cy.get('.header__logo img')
				.should('be.visible');
      cy.get('h1.page-title')
        .should('contain.text', 'Offices and Services')
      cy.get('input#office-search')
        .focus()
        .type('web services');
      cy.get('div.office__search-filters form')
        .find('input[type=submit]')
        .focus() // Ensure submit is focusable
        .click()
      cy.get('h3.office__result__title')
        .should('contain.text', 'Web Services')
      cy.get('div.office__result__description')
        .should('contain.text', 'Web Services collaborates with campus administrative and academic offices as well as individual community members in support of institutional, departmental, and personal web presence needs.')
      cy.get('h3.office__result__title a')
        .should('contain.text', 'Web Services')
        .focus() // Ensure submit is focusable
        .click();
      // visit single office-service page
      cy.url()
        .should('include', '/inside/offices-services/library-information-technology-services/projects-partnerships/web-services');
      cy.get('.ofc_ser__body')
        .should('include.text', 'Web Services collaborates with campus administrative and academic offices');
      cy.get('ul.menu.top-level.is-open li.menu-item.menu-item--expanded.menu-item--active-trail.current-active')
        .should('contain.text', 'Web Services');
    });

	});
});
