describe('Course Program Description - test table data display', function() {
	context('Desktop', () => {
		beforeEach(() => {
			cy.viewport(1280, 720)
		})

    it('visit course page - simple', function () {
      cy.visit('/inside/academic-information/departments-programs/chemistry/courses');
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('h1.page-title')
        .should('contain.text', 'Courses');
      cy.get('ul.menu.top-level.is-open li.current-active')
        .should('contain.text', 'Chemistry');
      cy.get('ul.menu.top-level.is-open li.current-active ul li.menu-item--active-trail')
        .should('contain.text', 'Courses');
      cy.get('div#block-course-schedule div div')
        .should('include.text', 'This page displays the schedule of Bryn Mawr courses in this department for this academic year')
        .and('include.text', 'For information about the Academic Calendar');
      cy.get('table').eq(0)
        .should('have.class','sticky-header' );
      cy.get('table thead th').eq(4)
        .should('include.text', 'Location');
      cy.get('tbody').eq(0)
        .should('have.attr', 'name', 'table-0').within(() => {
        cy.get('tr.odd td a').eq(0).focus()
          .click({force: true});
      });

      cy.get('div.bmc_course_listing__item p.courses-description').eq(0).should('be.visible')
      cy.get('div.bmc_course_listing__item p a')
        .should('have.attr', 'href', '#top').eq(0)
        .focus()
        .click();
      cy.get('h2.term')
        .should('be.visible');

    });

    it('visit course page - complex', function () {
      cy.visit('/inside/academic-information/departments-programs/greek-latin-classical-studies/courses');
      // /inside/academic-information/departments-programs/greek-latin-classical-studies/courses
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('h1.page-title')
        .should('contain.text', 'Courses');
      cy.get('ul.menu.top-level.is-open li.current-active')
        .should('contain.text', 'Greek, Latin, and Classical Studies');
      cy.get('ul.menu.top-level.is-open li.current-active ul li.menu-item--active-trail')
        .should('contain.text', 'Courses');
     // cy.get('div#block-course-schedule div div') ## COMMENTED OUT due to basic page swap for courses pg ##
      cy.get('.paragraph--type--wysiwyg p').eq(0).contains('courses in this department for this academic year');
      cy.get('.paragraph--type--wysiwyg p').eq(2).contains('For information about the Academic Calendar');
      cy.get('h2').contains('GREEK');

      cy.get('table.sticky-header').eq(0).scrollIntoView();

      cy.get('table thead th').eq(4)
        .should('include.text', 'Location');
      cy.get('tbody[name=table-0]')
        .should('have.attr', 'name', 'table-0').within(() => {
        cy.get('tr td a').eq(0).focus()
          .click({force: true});
      }); // end within

      cy.contains('LATN')
        .should('have.length', 1)
        .and('include.text', 'LATN');

      cy.contains('CSTS')
        .should('have.length', 1)
        .and('include.text', 'CSTS');

    //  cy.get('div.bmc_course_listing__item p.courses-description').eq(0).should('be.visible');  ## COMMENTED OUT due to basic page swap for courses pg ##
    //  cy.get('a#GREKB201')
      cy.get('p').contains('GREK B201 Plato and Thucydides');
  //    cy.get('div.bmc_course_listing__item p a')  ## COMMENTED OUT due to basic page swap for courses pg ##
  //      .should('have.attr', 'href', '#top').eq(0)  ## COMMENTED OUT due to basic page swap for courses pg ##
  //      .focus()  ## COMMENTED OUT due to basic page swap for courses pg ##
  //      .click();  ## COMMENTED OUT due to basic page swap for courses pg ##
  //    cy.get('h2').contains('GREEK')  ## COMMENTED OUT due to basic page swap for courses pg ##
  //      .should('be.visible');  ## COMMENTED OUT due to basic page swap for courses pg ##

    });

    it('visit program requirement page', function () {
      cy.visit('/inside/academic-information/departments-programs/greek-latin-classical-studies/program-requirements-opportunities');
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('h1.page-title')
        .should('contain.text', 'Program Requirements and Opportunities');
      cy.get('ul.menu.top-level.is-open li.current-active')
        .should('contain.text', 'Greek, Latin, and Classical Studies');
      cy.get('ul.menu.top-level.is-open li.current-active ul li.menu-item--active-trail')
        .should('contain.text', 'Program Requirements and Opportunities');
      cy.get('div.generic-description')
        .should('include.text', 'Published annually, the Course Catalog sets out the requirements of the academic programs--the majors, minors, and concentrations')
        .and('include.text', 'For more information, visit the Catalog Homepage to view the current content')
      cy.get('div.program-description h3').eq(0)
        .should('include.text', 'CLASSICAL LANGUAGES');
      cy.get('div.program-description').eq(1).scrollIntoView();
      cy.get('div.program-description').eq(1).within(() => {
        cy.get('h3').eq(1)
          .should('include.text', 'Major Requirements');
        cy.get('h3').eq(2)
          .should('include.text', 'Minor Requirements');
        cy.get('p:last-child')
          .should('include.text', '##');
      });
      cy.get('div.program-description').eq(2).scrollIntoView();
      cy.get('div.program-description').eq(2).within(() => {
        cy.get('h3').eq(0)
          .should('include.text', 'CLASSICAL CULTURE AND SOCIETY');
      });
      cy.get('.region-content-bottom div#block-classicscontact')
        .should('contain', 'Contact Us');
    });
  });
});
