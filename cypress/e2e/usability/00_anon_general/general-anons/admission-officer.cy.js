describe('Admissions Officer tests: main list and search, detail node', function() {
	context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });

    beforeEach(() => {
      cy.viewport(1280, 720);
    });

		it('meet the team list page', function() {
			cy.visit('/admissions-aid/meet-team');
			cy.get('.header__logo img')
				.should('be.visible');
      cy.get('h1.page-title')
        .should('contain.text', 'Meet the Team');
    });

    it('perform admissions officer list page search', function() {
      cy.visit('/admissions-aid/meet-team');
      cy.get('.header__logo img').scrollIntoView();
      cy.get('select#edit-field-admino-state-target-id')
        .select('541');
      cy.wait(2000);
      cy.get('div.view-header')
        .should('include.text', '1 Results');
      cy.get('.admino-teaser__title h2')
        .should('include.text', 'Caitlin Brown');
      cy.get('.header__logo img').scrollIntoView();
      cy.get('input[id^=edit-reset--]')
        .focus()
        .click();
      cy.wait(2000);

      cy.get('.admino-teaser__title a').each((item, index, list) => {
        let countOfElements = 0;
        countOfElements = list.length;
        expect(list).to.have.length(countOfElements);
        expect(item).attr('href').to.include('/admissions-aid/meet-team/');
        }); // END item each
      cy.get('.admino-teaser__title a').then($elements => {
         let countOfElements = 0;
           countOfElements = $elements.length;
           cy.get('.view-header').contains(countOfElements +' Results');
         }); // end then

      cy.get('select#edit-field-admino-country-target-id')
        .select('681');
      cy.wait(2000);
      cy.get('.view-header').contains('1 Results');
      cy.get('.admino-teaser__title h2')
        .should('include.text', 'Jennifer Russell');
    });

		it('admissions officer detail page', function() {
      cy.visit('/admissions-aid/meet-team/caitlin-brown');
      cy.get('.header__logo img')
        .should('be.visible');
      cy.get('h1.page-title')
        .should('contain.text', 'Caitlin Brown');
      cy.get('.admino__job-title')
        .should('contain.text', 'Associate Director of Admissions & Director of Enrollment Communications');
      cy.get('.admino__image img')
        .should('have.attr', 'alt', 'caitlin-brown');
      cy.get('.admino__contact__phone')
        .should('include.text', '(610) 526-5518');
      cy.get('.admino__body').scrollIntoView();
      cy.get('.field--name-field-admino-desc')
        .contains('Fun Fact!');
    });
	});
});
