describe('All Content Homepage', function() {
	context('Desktop', () => {
		beforeEach(() => {
			cy.viewport(1280, 720);
      cy.visit('/node/59246');
		});

    it('visit homepage - all content', function() {

      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Bryn Mawr College');
      cy.get('h1.visually-hidden')
        .should('include.text', 'Example Homepage with All Content');
    });

    it('operate homepage hero  carousel', function() {
      // Find Slide 1
      cy.homeCarouselSlideFind('0','1');

      // test that slides advance with next button
      cy.testCarouselNext();

      // Find and interact with Slide 2
      cy.homeCarouselSlideFind('1','2');


    });


    // HP FEATURE LARGE
    it('homepage feature large', function() {
      cy.get('.home-paragraph-feat-lg').scrollIntoView();
      cy.get('.home-paragraph-feat-lg .feat-lg__content-wrap').within(() => {
        cy.get('.feat-lg__content-inner-bg .cta--primary a').eq(1)
          .should('include.text', 'Sweetgum Tree');
        cy.get('.feat-lg__details-wrap .feat-lg__mnlst .mnlst_item.paragraph .home-fhome__mdesc')
          .should('include.text', 'Trees on Campus');
        cy.get('.feat-lg__details-wrap .feat-lg__alink a').eq(0)
          .should('include.text', 'Weeping Cherry');
        cy.get('.field--name-field-p-fhlrg-item .fquote__quote')
          .should('include.text', 'We love the trees!');
      }); // END within
    });

    // HP FEATURE MEDIUM
    it('homepage feature medium', function() {
      cy.get('.home-ftmd').scrollIntoView();
      cy.get('div.home-ftmd__cta .field__item').eq(1)
        .should('include.text', 'Earth Week!');
      cy.get('.home-ftmd__item.inview .fcap__caption')
        .should('include.text', 'Trees are extra special on Earth Day');
      cy.get('h3.home-ftmd__mlist-title').should('include.text', 'The Details');
      cy.get('.home-ftmd button.fdrop__toggle').should('have.attr', 'aria-expanded', 'false')
        .focus()
        .type('{enter}');
      //  this uses the keyboard to select the focused item by using the enter key to expand menu
      cy.get('.home-ftmd button.fdrop__toggle')
        .should('have.attr', 'aria-expanded', 'true');
      cy.get('.fdrop__content ul.fdrop__links li.fdrop__link a').eq(2)
        .should('include.text', 'Spring BBQ')
        .and('have.attr', 'href','/inside/latest/events/earth-week-spring-bbq-earthapooloza');
      cy.get('.home-ftmd__alink .field--name-field-p-fhmed-alink .field__item a').eq(0)
        .should('include.text', 'Additional list item - tree 1');
    });

    // HP STORY
    it('homepage story', function() {
      cy.get('.homepage-feature-story').scrollIntoView();
      cy.get('.homepage-feature-story__heading').should('include.text', 'Homepage Feature Story');
      cy.get('div.field--name-field-p-fhstory-cta a').should('include.text', 'Earth Week event series');
      cy.get('div.homepage-feature-story__details--desc').should('include.text', 'Here there is a link to Dance Auditions to help make this story extraordinary');
      cy.get('h3.homepage-feature-story__story--title').should('include.text', 'Homepage Feature Story Title');
      cy.get('div.homepage-feature-story__story--quote.inview').should('include.text', 'I learned so much from the district office manager');
    });

    // CALLOUT
    it('callout', function() {
      cy.get('.callout').scrollIntoView();
      cy.get('.callout .callout__img .field--name-field-p-callout-image img').should('be.visible');
      cy.get('.callout h2.callout__section-title').should('include.text','This is a Callout');
      cy.get('.callout .callout__body').should('include.text','Donec faucibus risus ut accumsan tincidunt');
      cy.get('.callout .callout__cta a').eq(0)
        .should('include.text','BalletX Master Class')
        .and('have.class', 'callout__cta--primary');
      cy.get('.callout .callout__cta a').eq(1)
        .should('include.text','Performing Arts Series')
        .and('have.class', 'callout__cta--secondary');
    });

    // COLUMNS
    it('columns', function() {
      cy.get('.columns').scrollIntoView();
      cy.get('.columns h2.columns__section-title').should('include.text', 'Columns Heading');
      cy.get('.columns .columns__intro').should('include.text', 'Donec faucibus risus ut accumsan tincidunt');
      cy.get('.columns .columns__items .columns__items--item.has-link').eq(1).within(() => {
        cy.get('.columns__items--item--img img').should('be.visible');
        cy.get('h3.columns__items--item--title a')
          .should('include.text', 'item 2')
          .and('have.attr', 'href', '/dance/dance-programs-dancing-histories-writing-dance-course');
        cy.get('.columns__items--item--body').should('include.text', 'Donec faucibus risus ut accumsan tincidunt');
      });  // END within
    });

    // FEATURE LARGE
    it('feature large', function() {
      cy.get('.feat-lg').eq(1).scrollIntoView();
      cy.get('.feat-lg').eq(1).within(() => {
        cy.get('.feat-lg__content-wrap .feat-lg__content-inner').within(() => {
          cy.get('h2.feat-lg__section-title').should('include.text', 'Feature Large');
          cy.get('.feat-lg__intro').should('include.text', 'This is the Feature Large introduction text.');
          cy.get('.field--name-field-p-feat-lg-cta .field__item a').eq(1)
            .should('include.text', 'Lanterns and Owls');
          cy.get('.field--name-field-p-feat-lg-drop button.fdrop__toggle')
            .should('include.text', 'More about lanterns');
          cy.get('.field--name-field-p-feat-lg-drop .fdrop__content ul.fdrop__links li.fdrop__link').eq(1)
            .should('include.text', 'Lantern Maker');
        });  // END within
      });  // END within

      cy.get('.feat-lg__img-wrap img').should('be.visible');
      cy.get('.feat-lg .feat-lg__content-wrap .feat-lg__item .field--name-field-p-feat-lg-item .fstat').within(() => {
        cy.get('.fstat__num').should('include.text', '1');
        cy.get('.fstat__desc').should('include.text', 'Perfect Lantern');
      });  // END within
    });

    // FEATURE MEDIUM
    it('regular feature medium', function() {
      cy.get('.feat-md').scrollIntoView();
      cy.get('.feat-md .feat-md__inner').within(() => {
        cy.get('h2.feat-md__section-title').should('include.text', 'Feature Medium');
        cy.get('.feat-md__intro').should('include.text', 'This is the introduction for the Feature Medium.');
        cy.get('.feat-md__cta .field--name-field-p-feat-md-cta .field__item a').eq(1).should('include.text', 'Pets in the Library');
        cy.get('.feat-md__media-wrap').within(() => {
          cy.get('.field--name-field-p-feat-md-img img').should('have.attr', 'alt', 'Dog');
          cy.get('.feat-md__item.pb-er-quote .field--name-field-p-feat-md-item .fquote blockquote').within(() => {
            cy.get('.fquote__attr.quote__name .field--name-field-p-fquote-attr').should('include.text', 'the cutest dog ever');
            cy.get('.fquote__quote .field--name-field-p-fquote-quote').should('include.text', 'Rub my belly!');
          });  // END blockquote within
        });  // END media within
      });  // END inner within
    });
    // FEATURE QUOTE
    it('feature quote', function() {
      cy.get('.quote').scrollIntoView();
      cy.get('.quote .quote__image .field--name-field-p-quote-image img')
        .should('have.attr', 'alt', 'Sunset on campus');
      cy.get('.quote blockquote .quote__name').should('include.text', 'Anonymous');
      cy.get('.quote blockquote .quote__quote').should('include.text', 'The campus is magical, especially at dusk.');
    });

    // IMAGE DETAIL LARGE
    it('image detail large', function() {
      cy.get('.imgdetlg').scrollIntoView();
      cy.get('.imgdetlg .field--name-field-p-imgdetlg-img img')
        .should('have.attr', 'alt', 'Old Library on a snow day');
      cy.get('.imgdetlg .imgdetlg__content-wrap').within(() => {
        cy.get('h2.imgdetlg__section-title').should('include.text', 'Image Detail Large');
        cy.wait(6000);
        // caption appears after display delay
        cy.get('.field--name-field-p-imgdetlg-item .fcap__caption')
          .should('include.text', 'Winter is exciting!');
         cy.get('.imgdetlg__intro').should('include.text', 'Image Detail Large intro text.');
        cy.get('.imgdetlg__body').should('include.text', 'Donec faucibus risus ut accumsan tincidunt');
        cy.get('.imgdetlg__body .imgdetlg__cta .field--name-field-p-imgdetlg-cta .field__item').eq(1)
          .should('include.text', 'Winter Dinner');
      });  // END within
    });

    // IMAGE LIST
    it('image list', function() {
      cy.get('.imglist').scrollIntoView();
      cy.get('h2.imglist__section-title').should('include.text', 'Image List');
      cy.get('.imglist__intro').should('include.text', 'This is the Intro text for the Image List');
      cy.get('.imglist__items .field--name-field-p-imglist-items .field__item .imglist__item').eq(1).within(() => {
        cy.get('.field--name-field-p-imglist-item-img img').should('be.visible');
        cy.get('.imglist__col--content-wrap .imglist__body').should('include.text', 'Donec faucibus risus ut accumsan tincidunt');
        cy.get('.imglist__col--content-wrap .imglist__cta .field--name-field-p-imglist-item-cta a').should('include.text', 'Girls Who Code');
      });  // END within
    });

    // NEWS 3UP
    it('news 3up', function() {
      cy.get('.news-3-up').scrollIntoView();
      cy.get('.news-3-up h2.news-3-up__section-heading').should('include.text', 'News 3up');
      cy.get('.news-3-up .news-3-up__intro-wrapper').within(() => {
        cy.get('.news-3-up__intro .field--name-field-p-news3up-intro')
          .should('include.text', 'Manually selected articles about books');
        cy.get('.news-3-up__more a.cta--primary')
          .should('have.attr', 'href', '/news')
          .and('include.text', 'More News');
      });  // END within
      cy.get('.news-3-up .news-3-up__items .field__item article.news-teaser-3up').eq(1).within(() => {
        cy.get('.news-listing__items--item .news-listing__left ').within(() => {
          cy.get('.news-listing--img .field--name-field-news-img img').should('be.visible');
        });  // END new-listing-left within
        cy.get('.news-listing__items--item .news-listing__right').within(() => {
          cy.get('.news-listing__date .field--name-field-news-date time.datetime')
            .should('have.attr', 'datetime', '2022-03-09T12:00:00Z')
            .and('include.text', 'Mar 9, 2022');
          cy.get('h3.news-listing__title').should('include.text', 'Provost Tim Harte\'s Book Wins Award');
          cy.get('.identifiers .identifier.identifier--marketing').should('include.text', 'College News');
        });  // END news-listing-right within
      });  // END news-teaser-3up within
    });

    // SOCIAL CALLOUT
    it('social media callout', function() {
      cy.get('.smcallout').scrollIntoView();
      cy.get('.smcallout .smcallout__inner').within(() => {
        cy.get('h2.smcallout__section-title')
          .should('include.text', 'Social Media Callout');
        cy.get('.smcallout__channels a.smcallout__icon').eq(0)
          .should('have.class', 'sm-callout--facebook')
          .and('have.attr', 'href', 'https://facebook.com');
      });  // END within
    });

    // SOCIAL CONNECT
    it('social media connect', function() {
      cy.get('.social-media-connect').scrollIntoView();
      cy.get('.social-media-connect .social-media-connect__inner').eq(0).within(() => {
        cy.get('h2.social-media-connect__heading')
          .should('include.text', 'Social Media Connect');
        cy.get('ul.social_media_connect li.social_media_connect_item__wrapper .social_media_connect_item--instagram').eq(0).within(() => {
          cy.get('a.social_media_connect_item__link')
            .should('have.attr', 'href', 'https://instagram.com');
          cy.get('.social_media_connect_item__img img')
            .should('have.class', 'image-style-social-connect');
          cy.get('.social_media_connect_item__handle .field--name-field-p-connect-handle')
            .should('include.text', '@BrynMawrCollege');
        });  // END instagram within
      });  // END social-media-connect-inner within
    });


    // SPECIAL LISTS
    it('special list large', function() {
      cy.get('.slistlg').scrollIntoView();
      cy.get('.slistlg .slistlg__items').within(() => {
       cy.get('.field--name-field-p-slistlg-items .field__item').eq(0).within(() => {
         cy.get('h3.slistlg__items--item--title').should('include.text', 'Item 1 - no link');
         cy.get('.slistlg__items--item--body').should('include.text', 'Donec faucibus risus ut accumsan tincidunt');
       });  // END field-item 1st within

        cy.get('.field--name-field-p-slistlg-items .field__item').eq(1).within(() => {
          cy.get('a').should('have.attr', 'href', '/lilac/volunteering-saturday-service-broad-street-food-pantry');
          cy.get('a h3.slistlg__items--item--title').should('include.text', 'Item 2');
        });  // END field-item 2nd within
      });  // END items within
    });

    it('special list small', function() {
      cy.get('.special-list-small').scrollIntoView();
      cy.get('.special-list-small h2.special-list-small__section-title')
        .should('include.text', 'Special List Small');
      cy.get('.special-list-small ul.special-list-small__list li.special-list-small__list--item a').eq(0)
        .should('have.attr', 'href', '/news/harry-potters-birthday-behind-scenes-look-bryn-mawrs-hogwarts-themed-dinner')
        .and('include.text', 'Hogwart\'s Dinner');
    });

  });
});
