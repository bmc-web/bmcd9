describe('Postbac Homepage', function() {
	context('Desktop', () => {
		beforeEach(() => {
			cy.viewport(1280, 720);
      cy.visit('/postbac');
		});

    it('visit homepage - postbac', function() {

      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Postbaccalaureate Premedical Program of Bryn Mawr College');
      cy.get('h1.visually-hidden')
        .should('include.text', 'Postbaccalaureate Premedical Program');
    });

    it('theme switcher and menu explore', function() {
      cy.get('nav#block-menu-theme-switcher').eq(0).within(() => {
        cy.get('ul.menu li.grad_top_level button')
          .should('include.text', 'Graduate & Postbac');
        cy.get('ul.menu li.grad_top_level ul.menu li.postbac a')
          .should('have.class', 'is-active');
      });
      // explore menu collapsed
      cy.get('.header__menu--top nav#block-menu-postbac-main-top ul.menu li.menu-item a').eq(2)
        .should('include.text', 'After Bryn Mawr');
      // explore menu expanded
      cy.get('.header-container__nav-button')
        .click();
      cy.wait(1000);
      cy.get('.header-container__menu')
        .should('be.visible');
      cy.get('.header-container__menu .header-container__menu-cols').within(() => {
        cy.get('.header-container__menu--right').within(() => {
          // change for theme to test
          // Postbaccalaureate Premedical Program
          cy.get('div.section-navigation .active-link-display').should('have.text', 'Postbaccalaureate Premedical Program');
          cy.get('.section-nav__panel ul.menu.top-level.is-open').within(() => {
            cy.get('li').eq(1)
              .should('include.text', 'About the Program').within(() => {
              cy.get('a').eq(0).should('have.attr', 'href').and('include','/postbac/about-program');
              cy.get('button.menu--section__button')
                .click();
              cy.wait(1000);
              cy.get('ul.menu li a').eq(1)
                .should('include.text', 'Academics')
                .and('have.attr', 'href').should('include','/postbac/about-program/academics');
              // end academics within
            });
            // end main menu within
          });
          // end right-side within
        });

        cy.get('.header-container__menu--left').within(() => {
          // ancillary menu - change for theme to test
          cy.get('nav#block-menu-postbac-ancillary ul.menu li.menu-item a').eq(1)
            .should('include.text', 'Events');
          // resources for menu - change for theme to test
          cy.get('h2').eq(1)
            .should('include.text', 'Resources for');
          cy.get('nav#block-menu-postbac-resources-for ul.menu li.menu-item a').eq(2)
            .should('include.text', 'All Faculty and Staff');
          // social menu
          cy.get('.header-container__menu--left--social a span').eq(0)
            .should('have.class', 'sm-callout--facebook')
            .and('include.text', 'Facebook');
          // end left-side within
        });
        // end menu-cols within
      });
      // close nav
      cy.get('.header-container__nav-button')
        .click({force:true});
      cy.get('.header-container__nav-button')
        .should('have.attr', 'aria-expanded', 'false');
    });

    //.fab
    it('check for fab', function() {
      cy.get('.header-container__menu-wrapper .fab-sticky').within(() => {
        cy.get('.field--name-field-b-fab div.field__item').eq(0).within(() => {
          cy.get('.fab a.fab__cta--btn') .should('include.text', 'Apply')
            .and('have.attr', 'href', '/postbac/admissions-aid/how-apply');
        });
        cy.get('.field--name-field-b-fab div.field__item').eq(1).within(() => {
          cy.get('.fab a.fab__cta--btn') .should('include.text', 'Info')
            .and('have.attr', 'href', 'https://admissions.brynmawr.edu/register/grad-request-for-information');
        });
      });
    });

    it('operate homepage hero  carousel', function() {
      // Find Slide 1
      cy.homeCarouselSlideFind('0','1');

      // test that slides advance with next button
      cy.testCarouselNext();

      // Find and interact with Slide 2
      cy.homeCarouselSlideFind('1','2');


    });

    it('footer elements', function() {
      cy.get('#block-footer-container-postbac').scrollIntoView();
      cy.get('.left-footer').within(() => {
        cy.get('.footer__logo .postbac__medical-icon img').should('have.attr', 'alt', 'Postbac medical mark');
        cy.get('.footer__address').should('include.text', 'Canwyll House East');
      });

      cy.get('.mid-footer').within(() => {
        cy.get('.social-icons a').eq(2)
          .should('have.class', 'sm-callout--twitter')
          .and('have.attr', 'href', 'https://twitter.com/brynmawrpostbac');
      });

      cy.get('.right-footer').within(() => {
        cy.get('.block-content--type-giving-footer-block').within(() => {
          cy.get('.field--name-field-b-giving-title h2').should('include.text', 'Make A Difference');
          cy.get('.field--name-field-b-giving-cta a')
            .should('include.text', 'Give Now')
            .and('have.attr', 'href', 'https://engage.brynmawr.edu/Make-a-gift');
        });
      });

      cy.get('.footer-container__bottom').within(() => {
        cy.get('.footer-container__copyright').should('include.text', 'Bryn Mawr College. All rights reserved');
        cy.get('nav#block-menu-footer-utility ul.menu li.menu-item a').eq(3)
          .should('include.text', 'Title IX')
          .and('have.attr', 'href', '/inside/policies-guidelines-handbooks/sexual-misconduct');
      });
    });

    it('pre-footer and back to top', function() {
      cy.get('.footer__prefooter').within(() => {
        cy.get('.prefooter__img .field--name-field-b-prefooter-img img').should('be.visible');
        cy.get('.prefooter__text h2.prefooter__title').should('include.text', 'Take the Next Step');
        cy.get('.field--name-field-b-prefooter-cta a').eq(1)
          .should('have.attr', 'href', 'https://admissions.brynmawr.edu/register/grad-request-for-information')
          .and('include.text', 'Request Information');
      }); // end prefooter within
      // back to top
      cy.get('.footer__prefooter').scrollIntoView();
      cy.get('button.btt.visible-scroll')
        .focus()
        .click({force: true});
      cy.wait(2000);
      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Postbaccalaureate Premedical Program of Bryn Mawr College');
      cy.get('h1.visually-hidden')
        .should('include.text', 'Postbaccalaureate Premedical Program');
    });

    it('perform search', function () {
      cy.get('.header-container__search-button')
        .click();
      cy.get('input#header-search')
        .focus()
        .type('course');
      cy.get('#header-search + input[type=submit]')
        .focus() // Ensure submit is focusable
        .click();
      // check search result page
      cy.get('h1.page-title')
        .should('include.text', 'Search: PostBac');
      cy.get('form.google-cse-search-box-form label')
        .should('include.text', 'Enter your keywords');
      cy.wait(2000);
      cy.get('#google-cse-results .gsc-refinementBlock .gsc-refinementHeader').eq(4)
        .should('have.attr', 'role', 'tab')
        .and('have.class', 'gsc-refinementhActive')
        .and('have.attr', 'data-path', 'postbac');
      cy.get('.gsc-wrapper').eq(0).within(() => {
        cy.get('.gsc-tabdActive .gsc-webResult.gsc-result').eq(0)
          .should('be.visible');
      });
      // check search result url
      cy.url()
        .should('include', '/postbac/search?keys=course');
    });
  });
});
