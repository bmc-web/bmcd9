describe('Inside Homepage', function() {
	context('Desktop', () => {
		beforeEach(() => {
			cy.viewport(1280, 720);
      cy.visit('/inside');
		});

    it('visit homepage - inside', function() {

      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Inside Bryn Mawr');
      cy.get('h1.page-title span.field--name-title.field--label-hidden')
        .should('include.text', 'Inside Bryn Mawr');
    });

    it('theme switcher and menu explore', function() {
      cy.get('nav#block-menu-theme-switcher').eq(0).within(() => {
        cy.get('ul.menu li.inside a')
          .should('have.class', 'is-active')
          .and('include.text', 'Inside Bryn Mawr');
      });

      // explore menu

      cy.get('.header-container__menu')
        .should('be.visible');
      cy.get('.header-container__menu .header-container__menu-cols').within(() => {
        cy.get('.header-container__menu--right').within(() => {
          // inside menu-right is really the left side of the menu bar
          cy.get('nav#block-menu-inside-main').within(() => {
            cy.get('ul.menu li.menu-item.menu-item--expanded').eq(1)
              .should('include.text', 'Offices and Services')
              .and('have.class', 'long-menu').within(() => {
              cy.get('a').eq(0).should('have.attr', 'href', '/inside/directory');
              cy.get('button.menu--inside__button')
                .should('have.attr', 'aria-expanded', 'false')
                .focus()
                .type('{enter}');
            //  this uses the keyboard to select the focused item by using the enter key to expand menu
              cy.get('button.menu--inside__button')
                .should('have.attr', 'aria-expanded', 'true');

              cy.get('ul.menu li.menu-item a').eq(1)
                .should('include.text', 'Access Services')
                .and('have.attr', 'href', '/inside/offices-services/access-services');
              // close menu
              cy.get('button.menu--inside__button')
                .should('have.attr', 'aria-expanded', 'true')
                .focus()
                .type('{enter}');
              cy.get('button.menu--inside__button')
                .should('have.attr', 'aria-expanded', 'false');
              // end offices and services within
            });
            // end main menu within
          });
          // end right-side within
        });

        cy.wait(2000);

        cy.get('.header-container__menu--left').within(() => {
          // inside menu-left is really the right side icons in menu
          // accordion-directions
          cy.get('.accordion__item.accordion--directions').within(() => {
            cy.get('button.accordion__toggle')
              .should('have.attr', 'aria-expanded', 'false')
              .focus()
              .type('{enter}');
            //  this uses the keyboard to select the focused item by using the enter key to expand menu
            cy.get('button.accordion__toggle')
              .should('have.attr', 'aria-expanded', 'true');

            cy.get('.inside__panel div#block-menu-inside-location ul.menu li.menu-item a').eq(2)
              .should('include.text', 'Maps and Directions')
              .and('have.attr', 'href', '/about-college/visit-guide/maps-directions');
            // close menu
            cy.get('button.accordion__toggle')
              .should('have.attr', 'aria-expanded', 'true')
              .focus()
              .type('{enter}');
            cy.get('button.accordion__toggle')
              .should('have.attr', 'aria-expanded', 'false');
            // end accordion-directions within
          });

          cy.wait(2000);

          // accordion-tools
          cy.get('.accordion__item.accordion--tools').within(() => {
            cy.get('button.accordion__toggle')
              .should('have.attr', 'aria-expanded', 'false')
              .focus({force: true})
              .type('{enter}');
            //  this uses the keyboard to select the focused item by using the enter key to expand menu
            cy.get('button.accordion__toggle')
              .should('have.attr', 'aria-expanded', 'true');

            cy.get('.inside__panel div#block-menu-inside-tools-resources ul.menu li.menu-item a').eq(2)
              .should('include.text', 'Library')
              .and('have.attr', 'href', '/inside/offices-services/library-information-technology-services');
            // close menu
            cy.get('button.accordion__toggle')
              .should('have.attr', 'aria-expanded', 'true')
              .focus()
              .type('{enter}');
            cy.get('button.accordion__toggle')
              .should('have.attr', 'aria-expanded', 'false');
            // end accordion-tools within
          });

          cy.wait(2000);

          // accordion-resources
          cy.get('.accordion__item.accordion--resources').within(() => {
            cy.get('button.accordion__toggle')
              .should('have.attr', 'aria-expanded', 'false')
              .focus()
              .type('{enter}');
            //  this uses the keyboard to select the focused item by using the enter key to expand menu
            cy.get('button.accordion__toggle')
              .should('have.attr', 'aria-expanded', 'true');

            cy.get('.inside__panel div#block-menu-inside-resources-for ul.menu li.menu-item a').eq(2)
              .should('include.text', 'Faculty and Staff')
              .and('have.attr', 'href', '/inside/faculty-staff');
            // close menu
            cy.get('button.accordion__toggle')
              .should('have.attr', 'aria-expanded', 'true')
              .focus()
              .type('{enter}');
            cy.get('button.accordion__toggle')
              .should('have.attr', 'aria-expanded', 'false');
            // end accordion-directions within
          });

          // end left-side within
        });
        // end menu-cols within
      });
    });

    it('page elements', function() {
      cy.get('.special-list-small').eq(0).within(() => {
        cy.get('h3.special-list-small__section-title').should('include.text', 'Most-Searched-For Pages');
        cy.get('ul.special-list-small__list li.special-list-small__list--item a').eq(4)
          .should('include.text', 'Health and Wellness Center');
      });
    });

    it('footer elements and back to top', function() {
      cy.get('#block-footer-container-inside').scrollIntoView();
      cy.get('.left-footer').within(() => {
        cy.get('.footer__address').should('include.text', '101 North Merion Ave.');
      }); // end left footer within

      cy.get('.mid-footer').within(() => {
        cy.get('.social-icons a').eq(2)
          .should('have.class', 'sm-callout--twitter')
          .and('have.attr', 'href', 'http://twitter.com/BrynMawrCollege');
      }); // end mid footer within

      cy.get('.right-footer').within(() => {
        cy.get('.block-content--type-giving-footer-block').within(() => {
          cy.get('.field--name-field-b-giving-title h2').should('include.text', 'Make A Difference');
          cy.get('.field--name-field-b-giving-cta a')
            .should('include.text', 'Give Now')
            .and('have.attr', 'href', 'https://engage.brynmawr.edu/Make-a-gift');
        }); // end giving within
      }); // end right footer within

      cy.get('.footer-container__bottom').within(() => {
        cy.get('.footer-container__copyright').should('include.text', 'Bryn Mawr College. All rights reserved');
        cy.get('nav#block-menu-footer-utility ul.menu li.menu-item a').eq(3)
          .should('include.text', 'Title IX')
          .and('have.attr', 'href', '/inside/policies-guidelines-handbooks/sexual-misconduct');
      }); // end footer bottom within
      // back to top
      cy.get('#block-footer-container-inside').scrollIntoView();
      cy.get('button.btt.visible-scroll')
        .focus()
        .click({force: true});
      cy.wait(2000);
      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Inside Bryn Mawr');
      cy.get('h1.page-title span.field--name-title.field--label-hidden')
        .should('include.text', 'Inside Bryn Mawr');
    });

    it('perform search', function () {
      cy.get('.header-container__search-button')
        .click();
      cy.get('input#headerSearch')
        .focus()
        .type('course');
      cy.get('#headerSearch + input[type=submit]')
        .focus() // Ensure submit is focusable
        .click();
      // check search result page
      cy.get('h1.page-title')
        .should('include.text', 'Search: Inside Bryn Mawr');
      cy.get('form.google-cse-search-box-form label')
        .should('include.text', 'Enter your keywords');
      cy.wait(2000);
      cy.get('#google-cse-results .gsc-refinementBlock .gsc-refinementHeader').eq(1)
        .should('have.attr', 'role', 'tab')
        .and('have.class', 'gsc-refinementhActive')
        .and('have.attr', 'data-path', 'inside bryn mawr');
      cy.get('.gsc-wrapper').eq(0).within(() => {
        cy.get('.gsc-tabdActive .gsc-webResult.gsc-result').eq(0)
          .should('be.visible');
      });
      // check search result url
      cy.url()
        .should('include', '/inside/search?keys=course');
    });
  });
});
