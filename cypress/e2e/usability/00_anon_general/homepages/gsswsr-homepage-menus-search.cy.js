describe('GSSWSR Homepage', function() {
	context('Desktop', () => {
		beforeEach(() => {
			cy.viewport(1280, 720);
      cy.visit('/socialwork');
		});

    it('visit homepage - gsswsr', function() {

      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Graduate School of Social Work and Social Research of Bryn Mawr College');
      cy.get('h1.visually-hidden')
        .should('include.text', 'Graduate School of Social Work and Social Research');
    });

    it('theme switcher and menu explore', function() {
      cy.get('nav#block-menu-theme-switcher').eq(0).within(() => {
        cy.get('ul.menu li.grad_top_level button')
          .should('include.text', 'Graduate & Postbac');
        cy.get('ul.menu li.grad_top_level ul.menu li.socialwork a')
          .should('have.class', 'is-active');
      });

      // explore menu collapsed

      cy.get('.header__menu--top nav#block-menu-gsswsr-main-top ul.menu li.menu-item a').eq(2)
        .should('include.text', 'Student Support');

      // explore menu expanded
      cy.get('.header-container__nav-button')
        .click();
      cy.wait(1000);
      cy.get('.header-container__menu')
        .should('be.visible');
      cy.get('.header-container__menu .header-container__menu-cols').within(() => {
        cy.get('.header-container__menu--right').within(() => {
          // change for theme to test
          // Graduate School of Social Work and Social Research
          cy.get('div.section-navigation .active-link-display').should('have.text', 'Graduate School of Social Work and Social Research');
          cy.get('.section-nav__panel ul.menu.top-level.is-open').within(() => {
            cy.get('li').eq(1)
              .should('include.text', 'Academics').within(() => {
              cy.get('a').eq(0).should('have.attr', 'href').and('include','/socialwork/academics');
              cy.get('button.menu--section__button')
                .click();
              cy.wait(1000);
              cy.get('ul.menu li a').eq(2)
                .should('include.text', 'M.S.S./M.P.H. Dual Degree Program')
                .and('have.attr', 'href').should('include','/socialwork/academics/mssmph-dual-degree-program');
              // end academics within
            });
            // end main menu within
          });
          // end right-side within
        });

        cy.get('.header-container__menu--left').within(() => {
          // ancillary menu - change for theme to test
          cy.get('nav#block-menu-gsswsr-ancillary ul.menu li.menu-item a').eq(1)
            .should('include.text', 'Events');
          // resources for menu - change for theme to test
          cy.get('h2').eq(1)
            .should('include.text', 'Resources for');
          cy.get('nav#block-menu-gsswsr-resources-for ul.menu li.menu-item a').eq(2)
            .should('include.text', 'All Faculty and Staff');
          // social menu
          cy.get('.header-container__menu--left--social a span').eq(0)
            .should('have.class', 'sm-callout--facebook')
            .and('include.text', 'Facebook');
          // end left-side within
        });
        // end menu-cols within
      });
      // close nav
      cy.get('.header-container__nav-button')
        .click({force:true});
      cy.get('.header-container__nav-button')
        .should('have.attr', 'aria-expanded', 'false');
    });

    //.fab

    it('check for fab', function() {
      cy.get('.header-container__menu-wrapper .fab-sticky').within(() => {
        cy.get('.field--name-field-b-fab div.field__item').eq(1).within(() => {
          cy.get('.fab a.fab__cta--btn') .should('include.text', 'Visit')
            .and('have.attr', 'href', '/socialwork/admissions-aid/visit-school');
        });
        cy.get('.field--name-field-b-fab div.field__item').eq(2).within(() => {
          cy.get('.fab a.fab__cta--btn') .should('include.text', 'Info')
            .and('have.attr', 'href', 'https://admissions.brynmawr.edu/register/grad-request-for-information');
        });
      });
    });


    it('operate homepage hero  carousel', function() {
      // Find Slide 1
      cy.homeCarouselSlideFind('0','1');

      // test that slides advance with next button
      cy.testCarouselNext();

      // Find and interact with Slide 2
      cy.homeCarouselSlideFind('1','2');


    });

    it('footer elements', function() {
      cy.get('#block-footer-container-gsswsr').scrollIntoView();
      cy.get('.left-footer').within(() => {
        cy.get('.footer__logo img').should('have.attr', 'alt', 'Graduate School of Social Work and Social Research of Bryn Mawr College');
        cy.get('.footer__address').should('include.text', '300 Airdale Road');
      });

      cy.get('.mid-footer').within(() => {
        cy.get('.social-icons a').eq(1)
          .should('have.class', 'sm-callout--twitter')
          .and('have.attr', 'href', 'https://twitter.com/GSSWSR_BMC');
      });

      cy.get('.right-footer').within(() => {
        cy.get('.block-content--type-giving-footer-block').within(() => {
          cy.get('.field--name-field-b-giving-title h2').should('include.text', 'Make A Difference');
          cy.get('.field--name-field-b-giving-cta a')
            .should('include.text', 'Give Now')
            .and('have.attr', 'href', 'https://engage.brynmawr.edu/Make-a-gift');
        });
      });

      cy.get('.footer-container__bottom').within(() => {
        cy.get('.footer-container__copyright').should('include.text', 'Bryn Mawr College. All rights reserved');
        cy.get('nav#block-menu-footer-utility ul.menu li.menu-item a').eq(3)
          .should('include.text', 'Title IX')
          .and('have.attr', 'href', '/inside/policies-guidelines-handbooks/sexual-misconduct');
      });
    });

    it('pre-footer and back to top', function() {
      cy.get('.footer__prefooter').within(() => {
        cy.get('.prefooter__img .field--name-field-b-prefooter-img img').should('be.visible');
        cy.get('.prefooter__text h2.prefooter__title').should('include.text', 'Take the Next Step');
        cy.get('.field--name-field-b-prefooter-cta a').eq(1)
          .should('have.attr', 'href', '/socialwork/admissions-aid/visit-school')
          .and('include.text', 'Visit');
      }); // end prefooter within
      // back to top
      cy.get('.footer__prefooter').scrollIntoView();
      cy.get('button.btt.visible-scroll')
        .focus()
        .click({force: true});
      cy.wait(2000);
      cy.get('.header__logo img')
        .should('have.attr', 'alt', 'Graduate School of Social Work and Social Research of Bryn Mawr College');
      cy.get('h1.visually-hidden')
        .should('include.text', 'Graduate School of Social Work and Social Research');
    });

    it('perform search', function () {
      cy.get('.header-container__search-button')
        .click();
      cy.get('input#header-search')
        .focus()
        .type('course');
      cy.get('#header-search + input[type=submit]')
        .focus() // Ensure submit is focusable
        .click();
      // check search result page
      cy.get('h1.page-title')
        .should('include.text', 'Search: Social Work');
      cy.get('form.google-cse-search-box-form label')
        .should('include.text', 'Enter your keywords');
      cy.wait(2000);
      cy.get('#google-cse-results .gsc-refinementBlock .gsc-refinementHeader').eq(3)
        .should('have.attr', 'role', 'tab')
        .and('have.class', 'gsc-refinementhActive')
        .and('have.attr', 'data-path', 'socialwork');
      cy.get('.gsc-wrapper').eq(0).within(() => {
        cy.get('.gsc-tabdActive .gsc-webResult.gsc-result').eq(0)
          .should('be.visible');
      });
      // check search result url
      cy.url()
        .should('include', '/socialwork/search?keys=course');
    });
  });
});
