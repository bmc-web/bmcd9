describe('Bulletin - spotlight', function() {
  context('Desktop', () => {
    beforeEach(() => {
      cy.viewport(1280, 720);
    });

    it('visit Spotlight page', function() {
      cy.visit('/bulletin/maise-shepherd-20');
      cy.get('.header__logo img')
        .should('be.visible')
        .and('have.attr', 'alt', 'Bryn Mawr Alumnae Bulletin');
      cy.get('h1.page-title span.field--name-title')
        .should('have.text', 'Maise Shepherd \'20');
      cy.get('h1.page-title span.spotlight-subhead')
        .should('have.text', 'Anassa Kata');
      cy.get('.spotlight__col.bulletin__col--content')
        .should('include.text', 'Developing commercial fusion energy')
        .and('contain', 'it had achieved a world record magnetic field strength of 20 Tesla');
      cy.get('.spotlight__col.bulletin__col--side').within(() => {
        cy.get('.spotlight-image img')
          .should('have.attr', 'alt', 'maise');
        cy.get('.spotlight-title').should('have.text', 'Maise Shepherd \'20');
      }); //END col side within
    });
  });
});
