describe('Bulletin - News featured', function() {
  context('Desktop', () => {
    beforeEach(() => {
      cy.viewport(1280, 720);
      cy.visit('/bulletin/where-math-rules');
    });

    it('check header img, h1 and sharethis', function() {
      cy.get('.header-container__bulletin .header__logo img')
        .should('be.visible')
        .and('have.attr', 'alt', 'Bryn Mawr Alumnae Bulletin');
      cy.get('.content__page-header--image').within(() => {
        cy.get('img').eq(4).should('have.attr', 'alt', 'Math Illustration');
        cy.get('.header__text h1.page-title.bulletin-news-title')
          .should('include.text', 'Where Math Rules');
        cy.get('.sharethis--bulletin .st-btn.st-first img')
          .should('have.attr', 'alt', 'Share on Facebook');
        cy.get('[data-network="twitter"] a')
          .focus();
      }); // END header image within
    });

    it('check page elements - main content', function() {
      cy.get('.bulletin-article__author')
        .should('include.text', 'By Judy Hill');
        cy.get('.bulletin-article__body.bulletin__col--content').within(() => {
          cy.get('.bulletin-article__body--text')
            .should('include.text', 'It’s not π, but it’s still pretty sweet!')
            .and('contain', 'students have been snapping up those candy bars');
          cy.get('figure.caption.caption-drupal-entity.align-left').eq(0).within(() => {
            cy.get('figcaption')
              .should('contain', 'Professor and Chair of Mathematics\n' +
                'on the William R. Kenan Jr., Chair,\n' +
                'Victor Donnay');
            cy.get('img').should('have.attr', 'alt', 'Victor Donnay');
          }); //END figure.caption within
          cy.get('h4').each((item, index, list) => {
            expect(list).to.have.length(6);
          }); // END item each
        }); // END col content within
    });

    it('check page elements - sidebar gallery', function() {
      cy.get('.bulletin-article__sidebar.bulletin__col--side').within(() => {
        cy.get('.bulletin-article__sidebar--gallery').within(() => {
          cy.get('a[data-colorbox-gallery="bulletin-sidebar-gallery"]').eq(0)
            .focus()
            .click();
          cy.wait(3000);
        }); // END sidebar gallery within FOR GALLERY to work
      }); // END col side within FOR GALLERY to work

      // STARTS Sidebar Image Gallery test
          cy.get('#cboxContent').should('be.visible').within(() => {
            cy.get('#cboxLoadedContent .bulletin-article__sidebar--lightbox img').should('have.attr', 'alt', 'Charlotte Scott');
            cy.get('#cboxCurrent').should('include.text', '1 of 3');
            cy.get('button#cboxNext')
              .focus()
              .click();
            cy.wait(3000);
            cy.get('#cboxLoadedContent .bulletin-article__sidebar--lightbox img').should('have.attr', 'alt', 'Anna Pell Wheeler');
            cy.get('#cboxCurrent').should('include.text', '2 of 3');
            cy.get('button#cboxClose')
              .focus()
              .click();
            cy.wait(3000);
          }); // END cboxContent within
    });

    it('check page elements - sidebar custom text', function() {
      cy.get('.bulletin-article__sidebar.bulletin__col--side').within(() => {
        cy.get('.bulletin-article__sidebar--custom-text').within(() => {
          cy.get('h4').each((item, index, list) => {
            expect(list).to.have.length(3);
          }); // END item each
          cy.get('h3').each((item, index, list) => {
            expect(list).to.have.length(2);
          }); // END item each
          cy.get('.field--name-field-news-bulletin-s-text')
            .should('include.text', 'From 1885 to 1935, when those three women were here, I don’t think there was a better place in the country for women mathematicians');
          cy.get('h3').eq(0).should('include.text', 'The Math Matriarchs');
        }); // END custom text within
      }); // END col side within
    });
  });
});
