describe('Bulletin - bookshelf item', function() {
  context('Desktop', () => {
    beforeEach(() => {
      cy.viewport(1280, 720);
    });

    it('visit Bookshelf page', function() {
      cy.visit('/bulletin/detroit-opera-house');
      cy.get('.header__logo img')
        .should('be.visible')
        .and('have.attr', 'alt', 'Bryn Mawr Alumnae Bulletin');
      cy.get('h1.page-title')
        .should('include.text', 'Detroit Opera House');
      cy.get('.field-name-field-full-author')
        .should('include.text', 'Marianne Weldon and Michael Hauser');
      cy.get('.bookshelf__col.bulletin__col--content').within(() => {
        cy.get('p').should('include.text', 'co-written (with Michael Hauser) by Marianne Weldon, collections manager in Bryn Mawr’s Special Collections, is part of the Images of America series');
      }); // END col-content within
      cy.get('.bulletin__col--side').within(() => {
        cy.get('div.bookshelf-image img').should('have.attr', 'alt', 'Detroit cover');
      }); // END col-side within
    });
  });
});
