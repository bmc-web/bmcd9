describe('Bulletin - News', function() {
  context('Desktop', () => {
    beforeEach(() => {
      cy.viewport(1280, 720);
      cy.visit('/bulletin/artist-orchard');
    });

    it('visit Bulletin News page', function() {
      cy.get('.header__logo img')
        .should('be.visible')
        .and('have.attr', 'alt', 'Bryn Mawr Alumnae Bulletin');
      cy.get('h1.page-title.bulletin-news-title')
        .should('include.text', 'The Artist and the Orchard');
    });

    it('verify sharethis rendered', function() {
      cy.get('.content__page-header').within(() => {
        cy.get('.sharethis--bulletin .st-btn.st-first img')
          .should('have.attr', 'alt', 'Share on Facebook');
        cy.get('[data-network="twitter"] a')
          .focus();
      }); // END content-page-header within
    });

    it('check Bulletin elements', function() {
      cy.get('.bulletin-article__summary')
        .should('include.text', 'How Linda Weiner Hoffman ’78 brought an abandoned apple orchard back to life.');
      cy.get('.bulletin-col-wrapper').within(() => {
       cy.get('.bulletin-article__body.bulletin__col--content').within(() => {
         cy.get('.bulletin-article__body--text')
           .should('include.text', 'In 2001, Linda Weiner Hoffman ’78 bought a small, run-down farm')
           .and('contain', 'A brown sofa, a dark upholstered chair, and a wooden desk floated like atolls on the hardwood floor');
         cy.get('.bulletin-article__body--text img')
           .should('have.attr', 'alt', ' The Artist and the Orchard');
       }); // END col content within
       cy.get('.bulletin-article__sidebar.bulletin__col--side').within(() => {
         cy.get('.bulletin-article__img-wrap').within(() => {
           cy.get('.field--name-field-news-img img')
              .should('have.attr', 'alt', 'Linds Hoffman');
            cy.get('p').should('have.text', 'Linda Hoffman \'78');
         }); // END img-wrap within
       }); // END col side within
      }); // END bulletin col-wrap within
      cy.get('.bulletin-article__meta--inner').within(() => {
        cy.get('.sharethis--bulletin .st-btn').eq(2)
          .should('have.attr', 'data-network', 'email');
        cy.get('.bulletin-article__more')
          .should('include.text', '\n' +
            '        More from \n' +
            '          Winter 2022\n' +
            '        ');
        cy.get('.bulletin-article__more a')
          .should('have.attr', 'href', '/bulletin/explore?field_bulletin_issue_target_id[0]=4471');
      });  // END meta-inner within

    // DISQUS COMMENTED OUT in case they want to revive it
      //  cy.get('.bulletin-disqus-comments .field--name-field-news-bulletin-disqus-cmnt #disqus_thread iframe')
    //    .should('be.visible');

    });
  });
});
