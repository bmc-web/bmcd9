describe('Bulletin: Frontpage', function() {
	context('Desktop', () => {
		beforeEach(() => {
			cy.viewport(1280, 720);
      cy.visit('/bulletin/spring-2022');
		});

		it('visit frontpage, check logo, issue and carousel', function() {
			cy.get('.header__logo img')
				.should('be.visible')
        .and('have.attr', 'alt', 'Bryn Mawr Alumnae Bulletin');
      cy.get('.bulletin-issue h3.rotated')
        .should('be.visible')
        .and('include.text', 'Spring 2022');
      cy.get('.bulletin-carousel .bulletin-carousel__item.slick-slide.slick-current.slick-active').within(() => {
        cy.get('h2.bulletin-carousel__slide-title a')
          .should('have.attr', 'href', 'https://www.brynmawr.edu/bulletin/be-well');
        cy.get('h2.bulletin-carousel__slide-title a div')
          .should('have.class', 'bulletin-carousel__slide-title-pos')
          .and('include.text', 'Be Well');
      }); // END carousel within
	  	});

    it('check In This Issue', function () {
      cy.get('.block-views-blocknews-block-1').within(() => {
        cy.get('h2').should('include.text', 'In This Issue');
        cy.get('.view-display-id-block_1').within(() => {
          cy.get('.views-row').eq(2).within(() => {
            cy.viewsRowLink('.views-field-field-bulletin-section');
            cy.viewsRowLink('.views-field-title');



          }); // END 3rd views-row within

          cy.get('.views-row').eq(3).within(() => {
            cy.viewsRowLink('.views-field-field-bulletin-section');
            cy.viewsRowLink('.views-field-title');


          }); // END 4th views-row within
          cy.get('.views-row').eq(5).within(() => {
            cy.get('.views-field-field-news-summary')
              .should('include.html', '<p>');
          }); // END 7th views-row within
          cy.get('.more-link a')
            .should('include.text', 'More Stories')
            .and('have.attr', 'href', '/bulletin/explore?field_bulletin_issue_target_id%5B0%5D=4666');
        }); // END view-display-id-block_1 within
      }); // END blocknews within
    });

    it('check Bookshelf', function () {
      cy.get('.block-views-blockbulletin-bookshelf-block-1').within(() => {
        cy.get('h2').should('include.text', 'Bookshelf');
        cy.get('.bookshelf-title')
          .should('have.length', 4)
          .eq(1)
          .should('include.text', 'Detroit Opera House');
        cy.get('.more-link a')
          .should('include.text', 'More Alumnae/i Books')
          .and('have.attr', 'href', '/bulletin/explore?field_bulletin_issue_target_id%5B0%5D=4666&field_bulletin_section_target_id%5B0%5D=1906');
      }); // END bookshelf-block within
    });

    it('check Campus News', function () {
      cy.get('.block-views-blocknews-block-2').within(() => {
        cy.get('h2').should('include.text', 'Campus News');
        cy.get('.view-display-id-block_2').within(() => {
          cy.get('.views-field-title')
            .should('have.length', 4);
          cy.get('.more-link a')
            .should('include.text', 'Read more')
            .and('have.attr', 'href', '/news');
          }); // END view-display-id-block_2 within
         }); // END bookshelf-block within
    });

    it('check footer/nav elements', function () {
      cy.get('#block-footer-container-bulletin .footer-container__bulletin').within(() => {
        cy.get('.left-footer').within(() => {
          cy.get('ul.social-icons li a').eq(0)
            .should('include.text', 'Facebook')
            .and('have.attr', 'href', 'http://www.facebook.com/BrynMawrCollege');
          cy.get('nav#block-menu-bulletin-footer-magazine').within(() => {
            cy.get('li.menu-item')
              .should('have.length', 2);
            cy.get('li.menu-item a').eq(0)
              .should('include.text', 'Past Issues')
              .and('have.attr', 'href', '/bulletin/archive');
            cy.get('li.menu-item a').eq(1)
              .should('include.text', 'Explore Stories')
              .and('have.attr', 'href', '/bulletin/explore');
          }); // END footer mag menu within
          cy.get('nav#block-menu-bulletin-footer-alumnae').within(() => {
            cy.get('li.menu-item')
              .should('have.length', 4);
            cy.get('li.menu-item a').should( items => {
              expect(items[0]).to.contain.text('Submit a Class Note').and.to.have.attr('href', '/inside/offices-services/alumnaei-relations-development/connecting/submit-class-notes');
              expect(items[1]).to.contain.text('Alumnae/i Homepage').and.to.have.attr('href', '/alumnae');
              expect(items[2]).to.contain.text('Submit a Story Idea').and.to.have.attr('href', '/bulletin/contact-us');
              expect(items[3]).to.contain.text('Giving Homepage').and.to.have.attr('href', '/giving');
            }); // END expect items
          }); // END footer alumnae menu within
        }); // END left footer within
        cy.get('.right-footer').within(() => {
          cy.get('.footer__logo img')
            .should('have.attr', 'alt', 'Bryn Mawr ');
          cy.get('nav#block-menu-bulletin-footer-bmc').within(() => {
            cy.get('li.menu-item')
              .should('have.length', 2);
            cy.get('li.menu-item a').eq(0)
              .should('include.text', 'Bryn Mawr College Homepage')
              .and('have.attr', 'href', '/');
            cy.get('li.menu-item a').eq(1)
              .should('include.text', 'News and Headlines')
              .and('have.attr', 'href', '/news');
          }); // END footer-menu within
        }); // END right footer within
      }); // END footer-container-bulletin within
    });

    it('check back to top', function () {
      cy.get('.footer-container__bulletin  button.btt')
        .focus()
        .click({force: true});
      cy.wait(2000);
      cy.get('.header__logo img')
        .should('be.visible')
        .and('have.attr', 'alt', 'Bryn Mawr Alumnae Bulletin');
    });

    it('check top nav', function () {
      cy.get('.header__menu--top').within(() => {
        cy.get('nav#block-menu-bulletin-main').within(() => {
          cy.get('li.menu-item')
            .should('have.length', 3);
          cy.get('li.menu-item a').should( items => {
            expect(items[0]).to.contain.text('Explore Stories').and.to.have.attr('href', '/bulletin/explore');
            expect(items[1]).to.contain.text('Past Issues').and.to.have.attr('href', '/bulletin/archive');
            expect(items[2]).to.contain.text('Contact Us').and.to.have.attr('href', '/bulletin/contact-us');
          }); // END expect items
        }); // END main nav within

        cy.get('.back-to-edu').within(() => {
          cy.get('a').should('include.text', 'brynmawr.edu').and('have.attr', 'href', '/');
        }); // END back-to-edu within
      }); // END header-menu-top
    });

    it('perform search', function () {
			cy.get('button.open-search')
				.click();
      cy.wait(1000);
      cy.get('button.open-search').should('have.class', 'expanded');
			cy.get('.search input[type=search]')
				.focus()
				.type('alumnae');
			cy.get('.search input[type=search] + input[type=submit]')
				.focus() // Ensure submit is focusable
				.click();
      // check search result page
      cy.get('h1.page-title.bulletin-block-title')
        .should('include.text', 'Search: Bulletin');
      cy.get('form.google-cse-search-box-form label')
        .should('include.text', 'Enter your keywords');
      cy.get('#google-cse-results .gsc-refinementBlock .gsc-refinementHeader').eq(5)
        .should('have.attr', 'role', 'tab')
        .and('have.class', 'gsc-refinementhActive')
        .and('have.attr', 'data-path', 'bulletin');
      cy.get('.gsc-wrapper').eq(0).within(() => {
        cy.get('.gsc-tabdActive .gsc-results.gsc-webResult').within(() => {
          cy.get('.gsc-webResult.gsc-result')
            .should('have.length', 10)
            .eq(3)
            .should('be.visible');
        }); // END tab active results within
      }); // END results wrapper within
      // check search result url
      cy.url()
        .should('include', '/bulletin/search?keys=alumnae');
		});
	});
});
