describe('Bulletin - Explore', function() {
  context('Desktop', () => {
    beforeEach(() => {
      cy.viewport(1280, 720);
      cy.visit('/bulletin/explore');
    });

    it('visit Explore page', function() {
      cy.get('.header__logo img')
        .should('be.visible')
        .and('have.attr', 'alt', 'Bryn Mawr Alumnae Bulletin');
      cy.get('h1.page-title.bulletin-block-title')
        .should('include.text', 'Alumnae Bulletin');
    });

    it('sidebar - display', function() {
      cy.get('aside.bulletin__explore-stories__sidebar.bulletin__col--side').within(() => {
        cy.get('h2#bulletin__explore-stores__sidebar__aside-heading')
          .should('include.text', 'Explore Stories');
        cy.get('nav.view-id-bulletin_issues').within(() => {
          cy.get('h3#bulletin__explore__sidebar__issues-heading')
            .should('include.text', 'Issues');

          cy.countListItemInclude('.views-field-name a', 'href', '/bulletin/explore?field_bulletin_issue_target_id');

     //     cy.get('.views-field-name a').each((item, index, list) => {
     //       expect(list).to.have.length(4);
    //        expect(item).attr('href').to.include('/bulletin/explore?field_bulletin_issue_target_id');
    //      }); // END item each



          cy.get('.more-link a')
            .should('include.text', 'Issue Archive')
            .and('have.attr', 'href', '/bulletin/archive');
        }); // END view-id-bulletin_issues within
        cy.get('nav.view-id-bulletin_section').within(() => {
          cy.get('h3#bulletin__explore__sidebar__sections-heading')
            .should('include.text', 'Sections');

          cy.countListItemInclude('.views-field-name a', 'href', '/bulletin/explore?field_bulletin_section_target_id');

      cy.bulletinLink(1);
          cy.bulletinLink(3);
          cy.bulletinLink(6);

          //   cy.get('.views-field-name a').eq(linkNum).then(($title) => {
          //     // finds third link, saves title's text .views-field-name a text
          //     cy.log($title.text());
          //
          //    expect(items[linkNum]).to.contain.text($title.text());
          //
          //
          //
          //   }); // end then

        //  cy.get('.views-field-name a').should( items => {
        //    expect(items[1]).to.contain.text('Archways');
        //    expect(items[3]).to.contain.text('Discourse');
        //    expect(items[6]).to.contain.text('President\'s Message');
         // }); // END expect items
        }); // END view-id-bulletin_section within
        cy.get('nav.view-id-bulletin_topics').within(() => {
          cy.get('h3#bulletin__explore__sidebar__topics-heading')
            .should('include.text', 'Topics');

          cy.countListItemInclude('.views-field-name a', 'href', '/bulletin/explore?topic');

          cy.bulletinLink(1);
          cy.bulletinLink(3);
          cy.bulletinLink(9);

       //   cy.get('.views-field-name a').should( items => {
       //     expect(items[1]).to.contain.text('Being Bryn Mawr');
       //    expect(items[3]).to.contain.text('Crowd Source');
       //     expect(items[items.length - 1]).to.contain.text('U-Curve');
       //   }); // END expect items
        }); // END view-id-bulletin_topics within
      }); // END sidebar within
    });

    it('main view - display', function() {
      cy.get('.view-id-bulletin_explore').within(() => {
        cy.get('form#views-exposed-form-bulletin-explore-block-1').within(() => {
          cy.get('label').should( items => {
            expect(items[0]).to.contain.text('Bulletin Issue').and.to.have.attr('for', 'edit-field-bulletin-issue-target-id');
            expect(items[1]).to.contain.text('Section').and.to.have.attr('for', 'edit-field-bulletin-section-target-id');
            expect(items[2]).to.contain.text('Topics').and.to.have.attr('for', 'edit-topic');
          }); // END expect items
        }); // END exposed filters within
        cy.get('.view-content').within(() => {
          cy.countItemEach('article.node--view-mode-teaser-explore');
        }); // END view-content within
      }); // END explore view within
    });

    it('exposed form - issue and section select', function() {
        cy.get('form#views-exposed-form-bulletin-explore-block-1').within(() => {
          cy.get('select#edit-field-bulletin-issue-target-id')
            .select('4666');
          cy.wait(2000);
          cy.get('input#edit-submit-bulletin-explore')
            .click();
        }); // END exposed filters within
       cy.get('.view-id-bulletin_explore').within(() => {
        cy.get('.views-row').eq(0).within(() => {
          cy.get('h2')
            .should('include.text', 'Answering the Call');
          cy.get('.bulletin__tex__more .bulletin__tex--issue')
            .should('include.text', 'Spring 2022');
          cy.get('.bulletin__tex__more .bulletin__tex--issue a')
            .should('have.attr', 'href', '/bulletin/explore?issue[]=4666');
        }); // END 1st views-row within
      }); // END explore view within
      cy.url()
        .should('include', 'explore?field_bulletin_issue_target_id=4666');
      // exposed form - section select
      cy.get('form#views-exposed-form-bulletin-explore-block-1').within(() => {
        cy.get('select#edit-field-bulletin-section-target-id')
          .select('1901');
        cy.wait(2000);
        cy.get('input#edit-submit-bulletin-explore')
          .click();
      }); // END exposed filters within
      cy.get('.view-id-bulletin_explore').within(() => {
        cy.get('.views-row').eq(0).within(() => {
          cy.get('h2')
            .should('include.text', 'On the Medical Fast Track');
          cy.get('.bulletin__tex__more .bulletin__tex--issue')
            .should('include.text', 'Spring 2022');
          cy.get('.bulletin__tex__more .bulletin__tex--issue a')
            .should('have.attr', 'href', '/bulletin/explore?issue[]=4666');
        }); // END 1st views-row within
      }); // END explore view within
      cy.url()
        .should('include', 'explore?field_bulletin_issue_target_id=4666&field_bulletin_section_target_id=1901');
      // reset form
      cy.get('form#views-exposed-form-bulletin-explore-block-1 input#edit-reset')
        .click();
      cy.wait(3000);
    });



    it('exposed form - topics select', function() {
      cy.get('form#views-exposed-form-bulletin-explore-block-1').within(() => {
        cy.get('select#edit-topic')
          .select('4866');
        cy.wait(2000);
        cy.get('input#edit-submit-bulletin-explore')
          .click();
      }); // END exposed filters within
      cy.get('.view-id-bulletin_explore').within(() => {
        cy.get('.views-row').eq(0).within(() => {
          cy.get('.bulletin__tex__more .bulletin-tex__tags')
            .should('include.text', 'Topics')
            .and('include.text', 'Featured News');
        }); // END 1st views-row within
      }); // END explore view within
      cy.url()
        .should('include', 'explore?field_bulletin_issue_target_id=All&field_bulletin_section_target_id=All&topic=4866');
    });

    it('pager - functionality  ', function() {
      cy.get('nav.pager').scrollIntoView();
      cy.get('li.pager__item.is-active')
        .should('include.text', '1');
      cy.get('li.pager__item.pager__item--ellipsis')
        .should('include.text', '…');
      cy.get('li.pager__item:last-of-type')
        .should('have.class', 'pager__item--last');
      cy.get('.pager__item--next a')
        .click();
      cy.url()
        .should('include', 'page=1');
      cy.get('li.pager__item.is-active').scrollIntoView();
      cy.get('.pager__item--next a')
        .click();
      cy.url()
        .should('include', 'page=2');
      cy.get('nav#block-menu-bulletin-main li.menu-item').eq(0)
        .should('have.class', 'menu-item--active-trail');
    });
  });
});
