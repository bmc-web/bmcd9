describe('Bulletin - Archive', function() {
  context('Desktop', () => {
    beforeEach(() => {
      cy.viewport(1280, 720);
      cy.visit('/bulletin/archive');
    });

    it('visit Past Issues page', function() {
      cy.get('.header__logo img')
        .should('be.visible')
        .and('have.attr', 'alt', 'Bryn Mawr Alumnae Bulletin');
      cy.get('h1.page-title.bulletin-block-title')
        .should('include.text', 'Past Issues');
      cy.get('.view-id-bulletin_issues').within(() => {
        cy.get('.field--name-field-media-image img').each((item, index, list) => {
          expect(list).to.have.length(12);
          expect(item).class('image-style-bulletin-archive');
        }); // END item each
      }); // END bulletin-issue within
    });

    it('pager - functionality  ', function() {
      cy.get('nav.pager').scrollIntoView();
      cy.get('li.pager__item.is-active')
        .should('include.text', '1');
      cy.get('li.pager__item:last-of-type')
        .should('have.class', 'pager__item--last');
      cy.get('li.pager__item').eq(3)
        .click();
      cy.url()
        .should('include', 'archive?page=3');
    });

  });
});
