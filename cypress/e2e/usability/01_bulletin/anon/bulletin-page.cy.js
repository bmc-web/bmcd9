describe('Bulletin - Basic Page', function() {
  context('Desktop', () => {
    beforeEach(() => {
      cy.viewport(1280, 720);
    });

    it('visit Bulletin Contact Us page', function() {
      cy.visit('/bulletin/contact-us');
      cy.get('.header__logo img')
        .should('be.visible')
        .and('have.attr', 'alt', 'Bryn Mawr Alumnae Bulletin');
      cy.get('h1.page-title.bulletin-block-title')
        .should('include.text', 'Contact Us');
      cy.get('.field--name-field-wysiwyg-body p').eq(0)
        .should('include.text', 'news roundups, regular columns, feature articles,')
        .and('contain', 'to introduce them to Mawrters making a difference in the world');
      cy.get('.field--name-field-p-embed-body').within(() => {
        cy.get('h3')
          .should('include.text', 'Contact the Bulletin');
        cy.get('iframe').should('be.visible');
      }); // END embed within
    });
  });
});
