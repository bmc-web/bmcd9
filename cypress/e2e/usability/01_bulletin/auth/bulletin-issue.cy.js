describe('Site Editor', function() {
  context('Desktop', () => {
    // Prevent known JS error from failing test
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
    beforeEach(() => {
      cy.viewport(1280, 720);
      cy.loginViaUi('iftest_site-editor', 'LightSlateGray3348!');
    });

    it('check for auth', function() {
      cy.visit('/user');

      cy.url().should('include', '/user/196');
      cy.get('.header__logo img')
        .should('be.visible');
    });

    it('go to content view', function() {
      cy.visit('/user');
      cy.get('.toolbar-tab a#toolbar-item-administration');

      cy.get('.toolbar-tab a#toolbar-item-administration').should('have.class', 'is-active');
      cy.get('.toolbar-menu-administration li a.toolbar-icon.toolbar-icon-system-admin-content')
        .click();
      cy.wait(1000);
      cy.get('h1.page-title')
        .should('include.text', 'Content');

    });

  });
});

