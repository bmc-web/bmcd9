TO BEGIN
You may have to run an npm install from the root of your site to install the Cypress node_module.

TO RUN TESTS
You will have to manually run a command from the root of the site:
./node_modules/.bin/cypress open
There is now a command in the Make file so just type : make cy-start in the CLI

The above command will open the GUI for the cypress tests.

TEST ORGANIZATION
All the tests can be found in cypress/integration/usability.
As long as the tests are in the integration directory you can organize as you would like.
https://docs.cypress.io/guides/overview/why-cypress has a ton of helpful documentation
 and can answer most questions you may have along the way.

CYPRESS CONFIG
One other thing to note is there is a cypress.json in the root
of the site where you can configure the baseURL if you would like.
This is currently set to be https://bmcd9.lndo.site.
