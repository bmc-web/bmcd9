// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })

// authenticate user
// use the code cy.loginViaUi({ username: '<type-username>', password: '<type password>' }); in test

Cypress.Commands.add('loginViaUi', (username, password) => {
  cy.session([username,password], () => {
    cy.visit('/user/login?local_login');
    cy.get('input[name=name]').clear().type(username);
    cy.get('input[name=pass]').clear().type(password);
    cy.get('input#edit-submit').click();
    cy.get('a#toolbar-item-user').contains(username);
    cy.get('h4.label').contains('Member for');
  }, {
    cacheAcrossSpecs: true
  });
});

// does math on captcha for event and announcement
// use the code cy.captchaMath(); in test

Cypress.Commands.add('captchaMath', (span) => {
  cy.get('div.form-item-captcha-response span.field-prefix').then((span) => {
    // store the field's text
    const txt = span.text();
    function addbits(s) {
      var total = 0,
        s = s.match(/[+\-]*(\.\d+|\d+(\.\d+)?)/g) || [];
      while (s.length) {
        total += parseFloat(s.shift());
      }
      return total;
    }
    const captchaNum = addbits(txt);
    cy.get('input#edit-captcha-response').type(captchaNum);
  }); // end then block

});

// make iframe accessible to cypress
// USE cy.getIframeBody('iframe#entity_browser_iframe_media_image_browser')
// where #entity_browser_iframe_media_image_browser is the id of the browser
// iframe in test for whatever iframe you need to enter
Cypress.Commands.add('getIframeBody', (iframe_id) => {
  // get the iframe > document > body
  // and retry until the body element is not empty
  cy.log('getIframeBody');

  // fill in iframe_id variable in individual tests, will be something like 'iframe#entity_browser_iframe_media_image_browser'
  return cy
    .get(iframe_id, { log: false })
    .its('0.contentDocument.body', { log: false }).should('not.be.empty')
    // wraps "body" DOM element to allow
    .then((body) => cy.wrap(body, { log: false }));
});

// flex wysiwyg
// use the code: cy.flexwysiwyg(); in test
Cypress.Commands.add('flexwysiwyg', () => {
  cy.get('div [id^=edit-field-body-add-more] li.add-more-button-wysiwyg input')
    .click();
  cy.wait(3000);

});


// modal button click
// use the code: modalButtonClick({buttonText}); in test where the buttonText is the word
// on the particular button [ex: Save or Next or Embed]
Cypress.Commands.add('modalButtonClick', (buttonText) => {
  cy.get('.ui-dialog-buttonset.form-actions button').contains(buttonText).click();
  cy.wait(3000);

});

// autocomplete input field
// use the code: autocompleteInputField({autocomText}, {linkText}); in test where the autocomText is the title
// of the particular article with its NID that usually appears in the autocomplete field [ex: First Fridays (6781)]
// and the linkText is the text you wish to appear as the link text for that item [ex: Test Link 2]
Cypress.Commands.add('autocompleteInputField', (autocomText, linkText) => {
  cy.get('input.ui-autocomplete-input').type(autocomText);
  cy.get('input').eq(1).type(linkText);
});

// modal image library select
// use the code: modalImageLibraryChoice({libraryChoice}); in test where the libraryChoice is the media# portion of the id
// of the particular image, so this example would have the libraryChoice be media36  [ex: input#edit-entity-browser-select-media36]
// entity_browser_iframe_story_nodes
Cypress.Commands.add('modalImageLibraryChoice', (libraryChoice) => {
  cy.getIframeBody('iframe#entity_browser_iframe_media_image_browser')
    .find('form#entity-browser-media-image-browser-form').within(() => {
    cy.get('input#edit-entity-browser-select-' + libraryChoice).click();
    cy.get('input#edit-submit').click();
    cy.wait(3000);
  }); // end browser form within
});

// modal image library upload
// use the code: modalImageLibraryUpload({iframeBrowserType}, {imageName}, {imageFile}, {imageAlt}); in test where the imageName is for the name field,
// imageFile is the path on the machine to upload file from, and the imageAlt is the alt-text for the image
// so example is: cy.modalImageLibraryUpload('Test Goat', 'cypress/fixtures/images/drupal-goat-02.jpeg', 'baby goat',)

Cypress.Commands.add('modalImageLibraryUpload', (iframeBrowserType, imageName, imageFile, imageAlt) => {
  // enter entity-browser form iframe
  cy.getIframeBody('iframe#entity_browser_iframe_'+iframeBrowserType+'_image_browser')
    .find('form#entity-browser-'+iframeBrowserType+'-image-browser-form').within(() => {
    cy.get('nav.eb-tabs ul li a').eq(1).click();
    cy.wait(3000);
  }); // end browser form within

  // have to call iframe again since iframe page changes
  cy.getIframeBody('iframe#entity_browser_iframe_'+iframeBrowserType+'_image_browser')
    .find('form#entity-browser-'+iframeBrowserType+'-image-browser-form').within(() => {
    cy.get('input#edit-inline-entity-form-name-0-value').type(imageName);
    cy.get('input#edit-inline-entity-form-field-media-image-0-upload')
      .selectFile(imageFile);
    cy.wait(2000);
    cy.get('div.form-item-inline-entity-form-field-media-image-0-alt input').type(imageAlt);
    cy.get('input#edit-submit').click();
  }); // end browser form within
});



// modal entity browser node select
// use the code: modalEntityLibraryChoice({nodeType}, {formType}, {libraryChoice}); in test where the nodeChoice is the {LIBRARY#} portion of the id
// of the particular image, so this example would have the libraryChoice be media36  [ex: input#edit-entity-browser-select-media36]
// nodeType:
// story_nodes from iframe#entity_browser_iframe_story_nodes
// media_image_browser from iframe#entity_browser_iframe_media_image_browser
// formType
// story-nodes from form#entity-browser-story-nodes-form
// media-image-browser from form#entity-browser-media-image-browser-form
Cypress.Commands.add('modalEntityLibraryChoice', (nodeType, formType, libraryChoice) => {
  cy.getIframeBody('iframe#entity_browser_iframe_'+nodeType)
    .find('form#entity-browser-'+formType+'-form').within(() => {
    cy.get('input#edit-entity-browser-select-' + libraryChoice).click();
    cy.get('input#edit-submit').click();
    cy.wait(3000);
  }); // end browser form within
});

// count number of same element on page
//make sure elements include the expected attribute and value
// use the code: cy.countListItemInclude('{elName}', '{elAttr}', '{elInclude}'); in test
// EXAMPLE: elName would be '.views-field-name a'
// elAttr would be 'href'
// and elInclude would be '/bulletin/explore?topic'
// LIKE SO: cy.countListItemInclude('.views-field-name a', 'href', '/bulletin/explore?topic');
Cypress.Commands.add('countListItemInclude', (elName, elAttr, elInclude) => {
  cy.get(elName).each((item, index, list) => {
    let countOfElements = 0;
    countOfElements = list.length;
    expect(list).to.have.length(countOfElements);
    expect(item).attr(elAttr).to.include(elInclude);
  }); // END item each
});

// count number of same element on page
// use the code: cy.countItemEach('{listElName}'); in test
// EXAMPLE: listElName would be '.views-field-name a'
// LIKE SO: cy.countItemEach('.views-field-name a');
Cypress.Commands.add('countItemEach', (listElName) => {


  cy.get(listElName)
    .should('be.visible')
    .its('length')
    .then((len) => {
      cy.log(len); //prints length
    });

});

// count number of same element on page
//make sure count matches value of result count, like on program finder
// use the code: cy.countItemCompare('{elName}', '{elResult}'); in test
// EXAMPLE: elName would be '.program__result'
// elResult would be '.program__results-count'
//
// LIKE SO: cy.countItemCompare('.program__result', '.program__results-count');
Cypress.Commands.add('countItemCompare', (elName, elResult) => {
  cy.get(elName).each((item, index, list) => {
    let countOfElements = 0;
    countOfElements = list.length;
    expect(list).to.have.length(countOfElements);
    cy.get(elResult).contains(list.length);
  }); // END item each
});


// event main listing page - pagerer
// use the code: cy.eventPagerer(); in test
Cypress.Commands.add('eventPagerer', () => {
  cy.get('[data-viewname="event-listing"]').wait(2000)
    .then((el) => {
      if (Cypress.dom.isVisible(el)) {
        cy.wrap(el).children()
          .then(($viewParts) => {
            if ($viewParts.find('li.pager__item.is-active').length) {
              // pagerer was found
              cy.log( 'found pagerer!');
              cy.get('li.pager__item.is-active').scrollIntoView();
              cy.get('.pager__item--next a')
                .click();
              cy.url()
                .should('include', 'page=1');
              cy.get('li.pager__item.is-active').scrollIntoView();
              cy.get('.pager__item--previous a')
                .click();
              cy.url()
                .should('include', 'page=0');
              cy.wait(2000);
              cy.focused().should('have.class', 'view-header').and('contain.text', 'Events Found');
            } else {
              cy.log( 'NO pagerer- SAD FACE');
              cy.wait(3000);
            }
          }); // end then viewParts
      }
    }); // end then el
});

// any - pagerer
// use the code: cy.anyPagerer('pagerViewname','listPage'); in test where the pagerViewname is the value of
// the unique identifier found on the element with the class that begins js-view-dom-
// for instance for the Admissions Officer page it is data-viewname="admissions-officer-listing"
// but on Announcements, there is a class .view-id-announcements
// and the listPage is the page title, so it could be Announcements or Meet the Team, etc.
Cypress.Commands.add('anyPagerer', (pagerViewname, listPage) => {
  cy.get(pagerViewname).wait(2000)
    .then((el) => {
      if (Cypress.dom.isVisible(el)) {
        cy.wrap(el).children()
          .then(($viewParts) => {
            if ($viewParts.find('li.pager__item.is-active').length) {
              // pagerer was found
              cy.log( 'found pagerer!');
              cy.get('li.pager__item.is-active').scrollIntoView();
              cy.get('.pager__item--next a')
                .click();
              cy.url()
                .should('include', 'page=1');
              cy.get('li.pager__item.is-active').scrollIntoView();
              cy.get('.pager__item--previous a')
                .click();
              cy.url()
                .should('include', 'page=0');
              cy.wait(2000);
              cy.get('h1.page-title').should('contain.text', listPage);
            } else {
              cy.log( 'NO pagerer- SAD FACE');
              cy.wait(3000);
            }
          }); // end then viewParts
      }
    }); // end then el
});

// flex items
// use the code: flexItemSelect({contentType},{flexItem}); in test where the flexItem is the last part of
// the add-more-button- class for the particular item [ex: carousel would be used for li.add-more-button-carousel]
// and contentType is the content type in which it is used, see below
// CONTENT TYPES
// [ex: body would be used for Basic Page edit-field-body-add-more ]
// [ex: prog-body would be used for Program edit-field-prog-body-add-more]
// [ex: news-paras would be used for News edit-field-news-paras-add-more]
// [ex: story-paras would be used for Story edit-field-story-paras-add-more]
Cypress.Commands.add('flexItemSelect', (contentType, flexItem) => {
  cy.get('div [id^=edit-field-'+ contentType +'-add-more] li.dropbutton-toggle button').click();
  cy.get('div [id^=edit-field-'+ contentType +'-add-more] li.add-more-button-'+ flexItem +' input')
    .click();
  cy.wait(3000);

});
// CHILD flex items
// use the code: childFlexItemSelect({childItem},{itemChoice}); in test where the childItem is the part of
// the -add-more ID for the particular item [ex: acc-items would be used for something like #edit-field-story-paras-0-subform-field-p-acc-items-add-more]
// we are only looking for a partial match, so we are using the [ * ] to find something in the middle, rather than the beginning[ ^ ] or the end [ $ ]
// see below
// and the itemChoice is the item you are going to choose. ex: for li.add-more-button-fquote the itemChoice would be fquote
// CHILD PARAS
// div [id*=subform-field-p-acc-items-add-more]  should be div [id*=subform-field-p-'+ childItem +'-add-more]
//subform-field-p-acc-items-add-more
//subform-field-p-carousel-slide-add-more
//subform-field-p-cols-items-add-more
//subform-field-p-featgrid-items-add-more
//subform-field-p-feat-lg-item-add-more
//subform-field-p-feat-md-item-add-more
//subform-field-p-imgdetlg-item-add-more
//subform-field-p-gallery-slide-add-more
//subform-field-p-imglist-items-add-more
//subform-field-p-smcallout-channels-add-more
//subform-field-p-connect-item-add-more
//subform-field-p-slistlg-items-add-more
//subform-field-p-vert-tab-items-add-more
Cypress.Commands.add('childFlexItemSelect', (childItem, itemChoice) => {
  cy.get('div [id*=-field-p-'+ childItem +'-add-more] li.dropbutton-toggle button').click();
  cy.get('div [id*=-field-p-'+ childItem +'-add-more] li.add-more-button-'+ itemChoice +' input')
    .click();
  cy.wait(3000);

});



// admin toolbar select
// use the code: adminToolbarSelect({toolbarItem}); in test where the toolbarItem is the last part of
// the toolbar-icon- class for the particular item [ex: entity-config-pages-collection-tab would be
// used for a.toolbar-icon-entity-config-pages-collection-tab]
Cypress.Commands.add('adminToolbarSelect', (toolbarItem) => {
  cy.get('.toolbar-tab a#toolbar-item-administration');
  cy.get('.toolbar-tab a#toolbar-item-administration').should('have.class', 'is-active');
  // SELECT Config Pages menu item from admin toolbar
  cy.get('.toolbar-menu-administration li a.toolbar-icon-'+ toolbarItem )
    .click({force: true});
  cy.wait(1000);
});

// save node
// use the code: saveNode({nodeTitle}); in test where the nodeTitle is the title for
// the particular node [ex: 001 Test Page ]
Cypress.Commands.add('saveNode', (nodeTitle) => {
  cy.get('input#edit-submit').click();
  cy.get('h1.page-title').contains(nodeTitle);
});


// test homepage carousel slides
// use the code: homeCarouselSlideFind(0,1); where 0 is the data-slick-index and 1 is the slide number
Cypress.Commands.add('homeCarouselSlideFind', (dataIndex, slideNum) => {
  function getTheSlide() {
    cy.get('[data-slick-index="'+ dataIndex +'"]').wait(2000)
      .then((el) => {
        if (Cypress.dom.isVisible(el)) {
          cy.log('Found slide '+ slideNum);
          cy.wrap(el).should('have.attr', 'aria-label', 'slide '+ slideNum);
          cy.wrap(el).children().children('.home-slide__image')
            .children('.field--name-field-p-home-slide-img')
            .children('.field--name-field-media-image')
            .children('.field__item')
            .children('picture').should('have.descendants', 'img').and('be.visible');
          cy.wrap(el).children().children().children('h2.home-slide-title').should('be.visible')
            .next()

            .then(($cta) => {
              // synchronously query from wrap
              // to find which element was created
              if ($cta.find('.field--name-field-p-home-slide-cta a').length) {
                // cta was found
                cy.log( 'cta!');
              } else {
                // else assume it was video
                cy.log( 'video!');
                cy.wrap($cta).children('button.home-slide__play')
                  .should('be.visible')
                  .and('include.text', 'Play Video');
                cy.get('.home-slide__play.cboxElement')
                  .click({force:true});
                // See slide video iframe and close modal
                cy.wait(3000);
                cy.get('#colorbox #cboxLoadedContent iframe')
                  .should('be.visible');
                cy.get('#colorbox #cboxContent button#cboxClose')
                  .click();
                cy.wait(3000);
              }


            });

          cy.log('Found it!');
        } else {
          cy.log('clicking next button');
          cy.wait(1000);
          cy.get('.home__carousel button.slick-next')
            .focus()
            .click()
            .wait(2000)
            .then(getTheSlide);
        }
      });
  }

  getTheSlide();
});

// test homepage carousel next button
// use the code: testCarouselNext();
Cypress.Commands.add('testCarouselNext', () => {
  // Next button
  cy.get('.home__carousel button.slick-next')
    .focus()
    .click();
  cy.wait(2000);
  // Next button
  cy.get('.home__carousel button.slick-next')
    .focus()
    .click();
  cy.wait(2000);

  // see what slide we are on

  let firstSlideNum = [];
  cy.get('.home__carousel.slick-initialized.slick-slider').next().within((ele) => {
    cy.wrap(ele).each(($value) => {
      cy.log($value.text());
      firstSlideNum.push($value.text());
    });
  });

  // Next button
  cy.get('.home__carousel button.slick-next')
    .focus()
    .click();
  cy.wait(2000);

  // compare slide numbers with previous and current
  cy.get('.home__carousel.slick-initialized.slick-slider').next().within((ele) => {
    cy.wrap(ele).each(($value,index) => {
      cy.log($value.text());
      const secondSlideNum = $value.text();
      expect(firstSlideNum[index]).to.not.equal(secondSlideNum);
    });

  });
});



// Event Approval EMS
// use the code: eventApprove({rowNum}); in test where the rowNum is the row number for
// the particular node [ex: the third row is 2; or the first row is 0 ]
Cypress.Commands.add('eventApprove', (rowNum) => {
  cy.get('.views-table tbody tr .views-field-title a').eq(rowNum).then(($title) => {
    // finds third row, saves title's text .views-field views-field-title a text
    cy.log($title.text());
    //  clicks edit .views-field-edit-node a
    cy.get('.views-field-edit-node a').eq(rowNum).click();
    cy.wait(1000);
    // in event edit screen
    // title field  #edit-title-0-value  value matches variable above
    cy.get('#edit-title-0-value').invoke('val').then(($title2) => {
      expect($title2).to.eq($title.text());
    }); // end invoke then
    // finds approval box, checks box input#edit-field-conference-events-approved-value
    cy.get('input#edit-field-conference-events-approved-value')
      .check();
    // finds publish box, checks box input#edit-status-value
    // save node
    cy.get('input#edit-submit').click();
  }); // end then
});


// Event Publish SITE EDITOR
// use the code: eventPublish({rowNum}); in test where the rowNum is the row number for
// // the particular node [ex: the third row is 2; or the first row is 0 ]
Cypress.Commands.add('eventPublish', (rowNum) => {
  cy.get('.views-table tbody tr .views-field-title a').eq(rowNum).then(($title) => {
    // finds third row, saves title's text .views-field views-field-title a text
    cy.log($title.text());
    //  clicks edit .views-field-edit-node a
    cy.get('.views-field-edit-node a').eq(rowNum).click();
    cy.wait(1000);
    // in event edit screen
    // title field  #edit-title-0-value  value matches variable above
    cy.get('#edit-title-0-value').invoke('val').then(($title2) => {
      expect($title2).to.eq($title.text());
    }); // end invoke then
    // finds publish box, checks box input#edit-status-value
    cy.get('input#edit-status-value')
      .check();
    // save node
    cy.get('input#edit-submit').click();
  }); // end then
});

// Bulletin Link Text Check
// use the code: bulletinLink({linkNum}); in test where the linkNum is the link eq number for
// the particular node [ex: the third link is 2; or the first link is 0 ]
Cypress.Commands.add('bulletinLink', (linkNum) => {
  cy.get('.views-field-name a').eq(linkNum).then(($title) => {
    // finds third link, saves title's text .views-field-name a text
    cy.log($title.text());
    cy.get('.views-field-name a').should( items => {
    expect(items[linkNum]).to.contain.text($title.text());
    }); // END expect items
    }); // end then
});

// Views Row Link Text Check
// use the code: viewsRowLink({fieldA}); in test where the fieldA is the link text for
// the particular views-row field [ex: the third link is .views-field-field-bulletin-section;
// or the first link is .views-field-title ]
Cypress.Commands.add('viewsRowLink', (fieldA) => {
    cy.get(fieldA).then(($title) => {
    // finds third link, saves title's text .views-field-name a text
    cy.log($title.text());
    cy.get(fieldA).should( 'include.text',$title.text());
  }); // end then
});

// Flex-item Feature Detail Title
// use the code: detailTitleChange({whichFeature}, {titleChange}); in test where whichFeature is the fhlrg, fhmed, etc
// and the titleChange is what you'd like to change the detail title to.
// If left blank, there should be no text in field but still display default
// TO CHANGE TITLE
// use Feature and New Text like so: cy.detailTitleChange('fhlrg', 'New Title Here');
// TO DELETE DEFAULT/LEAVE BLANK
// use Feature only like so: cy.detailTitleChange('fhlrg');
Cypress.Commands.add('detailTitleChange', (whichFeature, titleChange) => {
  cy.get('.field--name-field-p-'+whichFeature+'-detail-title').within(() => {
    if ('input [value="The Details"]') {
      cy.get('[type="text"]').clear();
    }
    if (titleChange) {
      cy.get('input').type(titleChange);
    }
  }); // end detail title within
});


//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
