<?php

namespace Drupal\bmc_section_nav\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\bmc_section_nav\BmcContextNav;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide a contextual section navigation.
 *
 * @Block(
 *  id = "bmc_section_nav",
 *  admin_label = @Translation("Section Navigation"),
 * )
 */
class SectionNav extends BlockBase implements ContainerFactoryPluginInterface {

  protected $contextNav;

  /**
   * Constructs a new Section Nav object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\bmc_section_nav\BmcContextNav $context_nav
   *   The iFactory contact nav service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BmcContextNav $context_nav) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->contextNav = $context_nav;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('bmc.context_nav')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['toggle_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include toggle button in the output?'),
      '#default_value' => isset($config['toggle_button']) ? $config['toggle_button'] : 0,
      '#description' => $this->t('You may have to clear caches after changing this.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['toggle_button'] = $values['toggle_button'];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function build() {
    $build = [
      '#theme' => 'bmc_section_nav',
      '#context_nav' => $this->contextNav->build(),
      '#toggle_button' => $this->configuration['toggle_button'],
      '#cache' => [
        'keys' => [
          'bmc_section_nav',
        ],
        'contexts' => [
          'url.path',
        ],
        'tags' => [
          'menu_link_content_list',
        ],
        'max-age' => Cache::PERMANENT,
      ],
    ];

    return $build;
  }

}
