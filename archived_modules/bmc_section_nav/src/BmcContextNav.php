<?php

namespace Drupal\bmc_section_nav;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;

/**
 * Bmc Context Nav Service.
 *
 * @package Drupal\bmc_section_nav
 */
class BmcContextNav {

  /**
   * BMC menu service.
   *
   * @var \Drupal\bmc_section_nav\BmcMenu
   */
  protected BmcMenu $bmcMenu;

  /**
   * BmcContextNav constructor.
   *
   * @param \Drupal\bmc_section_nav\BmcMenu $bmc_menu
   *   The Bmc Menu service.
   */
  public function __construct(BmcMenu $bmc_menu) {
    $this->bmcMenu = $bmc_menu;
  }

  /**
   * Build a context nav.
   *
   * @param bool $show_home_as_root
   *   Whether to include home page as the root of the menu.
   *
   * @return array
   *   A render array representing the context nav.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function build($show_home_as_root = FALSE): array {
    $main_menus = [
      'main',
      'inside',
      'socialwork',
      'postbac',
      'gsas',
    ];
    try {
      if (!empty($current_page_menu_link = $this->bmcMenu->getMenuLinkFromRoute(\Drupal::routeMatch()))) {
        $menu_name = $current_page_menu_link->getMenuName();
        // If a node is in multiple menus, and first link is not in a main menu,
        // let's force it to be more specific.
        if (!in_array($menu_name, $main_menus)) {
          $theme_to_menu = [
            '5' => 'main',
            '1' => 'bulletin',
            '2' => 'gsas',
            '3' => 'socialwork',
            '4' => 'inside',
            '6' => 'postbac',
          ];
          $node = \Drupal::request()->attributes->get('node');
          if (is_object($node)) {
            if (isset($node->field_theme_main->target_id)) {
              $menu_name = $theme_to_menu[$node->field_theme_main->target_id];
              $current_page_menu_link = $this->bmcMenu->getMenuLinkFromRoute(\Drupal::routeMatch(), $menu_name);
            }
          }
        }
        $menu_tree = $this->buildMenuTreeArray($menu_name, FALSE, $show_home_as_root);
        $current_page_mlid = $current_page_menu_link->getPluginId();

        if (!empty($menu_tree['#items'])) {
          $first_item = reset($menu_tree['#items']);
          if (count($menu_tree['#items']) == 1 && empty($first_item['below'])) {
            // No children, so get the siblings (as if on parent) instead.
            $menu_tree = $this->buildMenuTreeArray($menu_name, TRUE, $show_home_as_root);
            $this->setActiveClasses($menu_tree, $current_page_mlid);
          }
        }

        $menu_tree['#theme'] = 'bmc_context_nav';
        $menu_tree['#attributes'] = new Attribute([
          'class' => [
            'menu',
            'menu--' . $menu_name,
          ],
        ]);
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException | PluginException $e) {
    }

    $menu_tree['#cache'] = [
      'keys' => [
        'bmc_menu_context_nav',
      ],
      'contexts' => [
        'url.path',
      ],
      'tags' => [
        'menu_link_content_list',
      ],
      'max-age' => 600,
    ];

    return $menu_tree;
  }

  /**
   * Build the menu tree array.
   *
   * @param string $menu_name
   *   Name of the menu.
   * @param bool $show_siblings
   *   Whether to show home page as root of the menu.
   * @param bool $show_home_as_root
   *   Whether to include home page as the root of the menu.
   *
   * @return array
   *   Array of menu items.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function buildMenuTreeArray(string $menu_name, bool $show_siblings, bool $show_home_as_root): array {
    $current_page_depth = count($this->bmcMenu->getMenuActiveTrail($menu_name));
    if ($show_siblings) {
      $menu_tree = $this->bmcMenu->buildMenuTree($menu_name, $current_page_depth - 1, $current_page_depth, TRUE);
    }
    else {
      $menu_tree = $this->bmcMenu->buildMenuTree($menu_name, $current_page_depth, $current_page_depth + 1, TRUE);
    }

    if ($show_home_as_root === TRUE) {
      $home_below = $menu_tree['#items'];
      unset($menu_tree['#items']);
      $menu_tree['#items']['home'] = [
        'is_expanded' => TRUE,
        'is_collapsed' => FALSE,
        'in_active_trail' => TRUE,
        'title' => $this->t('Home'),
        'url' => Url::fromRoute('<front>'),
        'below' => $home_below,
      ];
    }

    if (!empty($menu_tree['#items'])) {
      $this->pruneMenuTree($menu_tree['#items']);
    }

    return $menu_tree;
  }

  /**
   * Adds active or active-trail classes to appropriate menu items.
   *
   * @param array $menu_tree
   *   Menu item tree.
   * @param string $current_page_mlid
   *   Current page menu plugin link ID.
   */
  private function setActiveClasses(array &$menu_tree, string $current_page_mlid) {
    reset($menu_tree['#items']);
    $first_item_key = key($menu_tree['#items']);
    if (!empty($menu_tree['#items'][$first_item_key]['below'])) {
      foreach ($menu_tree['#items'][$first_item_key]['below'] as $key => $item) {
        if ($item['in_active_trail'] === TRUE) {
          $menu_tree['#items'][$first_item_key]['below'][$key]['attributes']['class'] = 'active-trail';
        }
        if ($current_page_mlid == $key) {
          $menu_tree['#items'][$first_item_key]['below'][$key]['attributes']['class'] = 'active';
        }
      }
    }
  }

  /**
   * Prune menu tree.
   *
   * @param array $menu_items
   *   The menu tree to prune.
   */
  protected function pruneMenuTree(array &$menu_items) {
    foreach ($menu_items as $index => $item) {
      if ($item['in_active_trail'] === FALSE) {
        unset($menu_items[$index]);
      }
      else {
        if (!empty($menu_items[$index]['below'])) {
          $this->preserveChildrenActiveTrail($menu_items[$index]['below']);
        }
      }
    }
  }

  /**
   * Clean all submenus of links not in active trail.
   *
   * @param array $menu_items
   *   The menu tree prune.
   */
  public function preserveChildrenActiveTrail(array &$menu_items) {
    foreach ($menu_items as $index => $item) {
      if ($item['in_active_trail'] === FALSE) {
        unset($menu_items[$index]['below']);
      }
      else {
        $this->preserveChildrenActiveTrail($menu_items[$index]['below']);
      }
    }
  }

}
