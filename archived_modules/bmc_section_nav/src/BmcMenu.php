<?php

namespace Drupal\bmc_section_nav;

use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Menu\MenuLinkManager;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Bmc Menu Service.
 *
 * @package Drupal\bmc_section_nav
 */
class BmcMenu {

  /**
   * Standard EntityTypeManager service.
   *
   * @var EntityTypeManager
   *   The EntityTypeManager.
   */
  protected $entityTypeManager;

  /**
   * A menu link tree service.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuTree;

  /**
   * A menu link manager service.
   *
   * @var \Drupal\Core\Menu\MenuLinkManager
   */
  protected $linkManager;

  /**
   * BmcMenu constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *    A standard EntityTypeManager.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_link_tree
   *    A menu link tree service.
   * @param \Drupal\Core\Menu\MenuLinkManager $link_manager
   *    A menu link manager service.
   */
  public function __construct(EntityTypeManager $entity_type_manager, MenuLinkTreeInterface $menu_link_tree, MenuLinkManager $link_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->menuTree = $menu_link_tree;
    $this->linkManager = $link_manager;
  }

  /**
   * Build a menu tree.
   *
   * @param string $menu_name
   *    The machine name of the menu.
   * @param int $level
   *    The level to start the menu from.
   * @param int $depth
   *    How deep the menu should go.
   * @param bool $expanded
   *    Whether to expand the nodes of the menu.
   * @param string $parent
   *    The menu link id of the root menu item.
   *
   * @return array
   *     A render array that renders the menu.
   */
  public function buildMenuTree($menu_name, $level = 1, $depth = 0, $expanded = FALSE, $parent = NULL) {
    $parameters = $this->menuTree->getCurrentRouteMenuTreeParameters($menu_name);

    // Adjust the menu tree parameters.
    $parameters->setMinDepth($level);
    // When the depth is configured to zero, there is no depth limit. When depth
    // is non-zero, it indicates the number of levels that must be displayed.
    // Hence this is a relative depth that we must convert to an actual
    // (absolute) depth, that may never exceed the maximum depth.
    if ($depth > 0) {
      $parameters->setMaxDepth(min($level + $depth - 1, $this->menuTree->maxDepth()));
    }
    // Build the whole tree.
    // If expandedParents is empty, the whole menu tree is built.
    if ($expanded == TRUE) {
      $parameters->expandedParents = [];
    }

    // When a fixed parent item is set, root the menu true at the given ID.
    if ($menuLinkID = str_replace($menu_name . ':', '', $parent)) {
      $parameters->setRoot($menuLinkID);

      // If the starting level is 1, we always want the child links to appear,
      // but the requested tree may be empty if the tree does not contain the
      // active trail.
      if ($level === 1 || $level === '1') {
        // Check if the tree contains links.
        $tree = $this->menuTree->load(NULL, $parameters);
        if (empty($tree)) {
          // Change the request to expand all children and limit the depth to
          // the immediate children of the root.
          $parameters->expandedParents = [];
          $parameters->setMinDepth(1);
          $parameters->setMaxDepth(1);
          // Re-load the tree.
          $tree = $this->menuTree->load(NULL, $parameters);
        }
      }
    }

    // Load the tree if we haven't already.
    if (!isset($tree)) {
      $tree = $this->menuTree->load($menu_name, $parameters);
    }

    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $tree = $this->menuTree->transform($tree, $manipulators);

    return $this->menuTree->build($tree);
  }

  /**
   * Find a main menu link for the specified Url object.
   *
   * @param \Drupal\Core\Url $url
   *   A url.
   * @param string $menu_name
   *   Optional menu name.
   *
   * @return \Drupal\Core\Menu\MenuLinkInterface|false|null|object
   *   A menu link for a Url.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getMenuLink(Url $url, $menu_name = '') {
    $link = NULL;

    $query = \Drupal::entityQuery('menu_link_content')
      ->accessCheck()->condition('link__uri', '%' . $url->getInternalPath(), 'LIKE');
    if (!empty($menu_name)) {
      $query->condition('menu_name', $menu_name);
    }

    $query->sort('id', 'ASC')->range(0, 1);
    $result = $query->execute();

    if (!empty($result) && !empty($id = reset($result))) {
      $menu_link = $this->entityTypeManager->getStorage('menu_link_content')->load($id);
      $link = $this->linkManager->getInstance(['id' => $menu_link->getPluginId()]);
    }

    return $link;
  }

  /**
   * Return the menu link for the specified route (preferring main menu items).
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route
   *   A route.
   *
   * @return \Drupal\Core\Menu\MenuLinkInterface|false|object
   *   A menu link for a route.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getMenuLinkFromRoute(RouteMatchInterface $route, $menu_name = 'main') {
    $link = $this->getMenuLink(Url::fromRouteMatch($route), $menu_name);

    if (empty($link)) {
      $link = $this->getMenuLink(Url::fromRouteMatch($route));
    }

    if (empty($link) && \Drupal::service('module_handler')->moduleExists('menu_position')) {
      $link = $this->menuPositionLinkByRoute($route);
    }

    return $link;
  }

  /**
   * Get a menu position's link for the provided route.
   *
   * @param RouteMatchInterface $route
   *    A route.
   *
   * @return \Drupal\Core\Menu\MenuLinkInterface|false|null|object
   *    The menu link for the menu position.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function menuPositionLinkByRoute(RouteMatchInterface $route) {
    // TODO: Change logic so it doesn't rely on a naming convention
    // Only detects rules with machine name {ENTITY_TYPE}__{BUNDLE}.
    if (!empty($menu_positions = $this->entityTypeManager->getStorage('menu_position')->loadMultiple())) {
      foreach ($menu_positions as $mp_id => $position) {
        if (!empty($mp_meta = explode('__', $mp_id)) && count($mp_meta) == 2 && !empty($entity = $route->getParameter($mp_meta[0])) && is_object($entity) && $entity->bundle() == $mp_meta[1]) {
          return $this->linkManager->getInstance([
            'id' => $position->getMenuLink(),
          ]);
        }
      }
    }

    return NULL;
  }

  /**
   * Get the active trail menu items of a menu.
   *
   * @param string $menu_name
   *    The machine name of a menu.
   *
   * @return array
   *    An ordered array of menu links representing the active trail.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function getMenuActiveTrail($menu_name) {
    $menu_link_parents = [];
    $current_route_parameters = $this->menuTree->getCurrentRouteMenuTreeParameters($menu_name);

    if (!empty($current_route_parameters) && !empty($parents = $current_route_parameters->activeTrail)) {
      foreach ($parents as $menu_meta) {
        if (!empty($menu_meta = explode(':', $menu_meta)) && !empty($menu_meta[0]) && !empty($menu_meta[1]) && !empty($menu_link = $this->entityTypeManager->getStorage($menu_meta[0])->loadByProperties(['uuid' => $menu_meta[1]]))) {
          $menu_link_parents[] = reset($menu_link);
        }
      }
    }

    return $menu_link_parents;
  }

}
