<?php

namespace Drupal\bmc_articles_archive_migrate;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Provides functions for creating Linksets.
 */
class ProcessLinksets {

  /**
   * Create an Image Linkset paragraph bundle.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createImageLinkset($d7_links): array {
    $links = [];
    foreach ($d7_links as $d7_link) {
      $link = $this->createImageLinksetItem($d7_link);
      if (!empty($link)) {
        $links[] = $link;
      }
    }
    $parent_para = Paragraph::create([
      'type' => 'imglist',
      'field_p_imglist_intro' => [
        'value'  => '',
      ],
      'field_p_imglist_head' => [
        'value'  => '',
      ],
      'field_p_imglist_items' => $links,
    ]);

    $parent_para->save();

    return [
      'target_id' => $parent_para->id(),
      'target_revision_id' => $parent_para->getRevisionId(),
    ];
  }

  /**
   * Create an Image List paragraph entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createImageLinksetItem($link): ?array {
    $image_src = str_replace(
      'public://',
      'https://www.brynmawr.edu/sites/default/files/',
      $link['field_landing_page_image']['uri']
    );
    $media_array = [];
    try {
      $file_vars = (new CreateMediaItems)->createFileEntityfromSrc($image_src);
      $media_array = (new CreateMediaItems)->generateMediaArray($link['field_landing_page_image'], $file_vars);
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
    }

    $cta_title = $link['field_info_text'] . ' ' . $link['field_link_text'];
    if (!empty($link['ctas'][0]['url'])) {
      $cta_uri = (new TransformInternalLinks)->checkCtaLinkUri($link['ctas'][0]['url']);
      $cta = [
        'uri'  => $cta_uri,
        'title' => $cta_title,
      ];
      $child_para = Paragraph::create([
        'type' => 'imglist_item',
        'field_p_imglist_item_align' => [
          'value'  => 'left',
        ],
        'field_p_imglist_item_title' => [
          'value'  => ' ',
        ],
        'field_p_imglist_item_cta' => $cta,
        'field_p_imglist_item_img' => $media_array,
      ]);

      $child_para->save();

      return [
        'target_id' => $child_para->id(),
        'target_revision_id' => $child_para->getRevisionId(),
      ];
    }
   return NULL;

  }

  /**
   * Create a WYSIWYG entity with CTA links from field_info_links.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createInfoLinkset($links): array {
    $linkset = '<p>';
    foreach ($links as $link) {
      if (isset($link['ctas'][0]['url'])) {
        $link_text = $link['field_info_text'] . ' ' . $link['field_link_text'];
        $link_url = (new TransformInternalLinks)->checkCtaLinkUri($link['ctas'][0]['url']);
        $link_cta = '<a class="cta--button--primary" href="' . $link_url . '">' . $link_text . '</a>';
        $linkset .= $link_cta;
      }
    }
    $linkset .= '</p>';
    return $this->createWysiwygParagraphItem($linkset);
  }


  /**
   * Generates a WYSIWYG paragraph item entity.
   *
   * @param string $i
   *   Values from the row being migrated.
   *
   * @return array
   *   Target/revision IDs to pass back to the entity reference field.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createWysiwygParagraphItem($i) : array {
    $paragraph = Paragraph::create([
      'type' => 'wysiwyg',
      'field_wysiwyg_body' => [
        'value'  => $i,
        'format' => 'advanced',
      ],
    ]);

    $paragraph->save();

    return [
      'target_id' => $paragraph->id(),
      'target_revision_id' => $paragraph->getRevisionId(),
    ];
  }

}
