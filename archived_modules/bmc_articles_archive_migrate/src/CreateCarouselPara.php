<?php

namespace Drupal\bmc_articles_archive_migrate;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Provides functions for a Carousel Paragraph Entity.
 */
class CreateCarouselPara {

  /**
   * Creates a Carousel parent paragraph entity.
   *
   * @param array $d7_slides
   *   The array of D7 slides.
   *
   * @return array
   *   The parent paragraph entity reference array.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createParentPara(array $d7_slides): array {
    $slides = [];
    foreach ($d7_slides as $d7_slide) {
      $slide = $this->createChildPara($d7_slide);
      if (!empty($slide)) {
        $slides[] = $slide;
      }
    }
    $parent_para = Paragraph::create([
      'type' => 'carousel',
      'field_p_carousel_align' => [
        'value'  => 'left',
      ],
      'field_p_carousel_slide' => $slides,
    ]);

    $parent_para->save();

    return [
      'target_id' => $parent_para->id(),
      'target_revision_id' => $parent_para->getRevisionId(),
    ];
  }

  /**
   * Creates a Carousel child paragraph entity.
   *
   * @param array $slide
   *   The array of D7 slide instance values.
   *
   * @return array|null
   *   The child paragraph entity reference array.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createChildPara(array $slide): ?array {

    $image_src = str_replace('public://', 'https://www.brynmawr.edu/sites/default/files/', $slide["field_slide_image"]["uri"]);
    try {
      $file_vars = (new CreateMediaItems)->createFileEntityfromSrc($image_src);
      $media_array = (new CreateMediaItems)->generateMediaArray($slide['field_slide_image'], $file_vars);
      $ctas = [];

      foreach ($slide["ctas"] as $d7_cta) {
        $cta = [];
        $cta_title = '';
        $cta_uri = '';
        if (isset($d7_cta["url"])) {
          $cta_uri = (new TransformInternalLinks)->checkCtaLinkUri($d7_cta["url"]);
        }
        if (isset($d7_cta["title"])) {
          $cta_title = $d7_cta["title"];
        }
        if (!empty($cta_title) && !empty($cta_uri)) {
          $cta = [
            'uri'  => $cta_uri,
            'title' => $cta_title,
          ];
        }
        if (!empty($cta)) {
          $ctas[] = $cta;
        }
      }
      $child_para = Paragraph::create([
        'type' => 'slide',
        'field_p_slide_title' => [
          'value'  => $slide["field_slide_title"],
        ],
        'field_p_slide_supertitle' => [
          'value'  => $slide["field_slide_header"],
        ],
        'field_p_slide_body' => [
          'value'  => $slide["field_slide_body"],
        ],
        'field_p_slide_cta' => $ctas,
        'field_p_slide_image' => $media_array,
      ]);

      $child_para->save();

      return [
        'target_id' => $child_para->id(),
        'target_revision_id' => $child_para->getRevisionId(),
      ];
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      return NULL;
    }
  }

}
