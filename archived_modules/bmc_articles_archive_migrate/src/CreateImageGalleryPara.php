<?php

namespace Drupal\bmc_articles_archive_migrate;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Provides functions for an Image Gallery paragraph entity.
 */
class CreateImageGalleryPara {

  /**
   * Creates a Image Gallery parent paragraph entity.
   *
   * @param array $d7_slides
   *   The array of D7 slides.
   * @param string $caption
   *   The Image Gallery Caption.
   *
   * @return array
   *   The parent paragraph entity reference array.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createGalleryPara(array $d7_slides, $caption = ''): array {
    $slides = [];
    foreach ($d7_slides as $d7_slide) {
      $slide = $this->createGallerySlidePara($d7_slide);
      if (!empty($slide)) {
        $slides[] = $slide;
      }
    }
    $first_slide_para = Paragraph::load($slides[0]['target_id']);
    $first_slide_image_mid = $first_slide_para->field_p_slide_image->target_id;
    $first_slide_image = ['target_id' => $first_slide_image_mid];
    $parent_para = Paragraph::create([
      'type' => 'image_gallery',
      'field_p_gallery_intro' => [
        'value'  => $caption,
      ],
      'field_p_gallery_img' => $first_slide_image,
      'field_p_gallery_slide' => $slides,
    ]);

    $parent_para->save();

    return [
      'target_id' => $parent_para->id(),
      'target_revision_id' => $parent_para->getRevisionId(),
    ];
  }

  /**
   * Creates an Image Gallery child paragraph entity.
   *
   * @param array $slide
   *   The array of D7 slide instance values.
   *
   * @return array|null
   *   The child paragraph entity reference array.
   */
  public function createGallerySlidePara(array $slide): ?array {

    $image_src = str_replace('public://', 'https://www.brynmawr.edu/sites/default/files/', $slide["uri"]);
    try {
      $file_vars = (new CreateMediaItems)->createFileEntityfromSrc($image_src);
      $media_array = (new CreateMediaItems)->generateMediaArray($slide, $file_vars);

      $child_para = Paragraph::create([
        'type' => 'slide',
        'field_p_slide_title' => [
          'value'  => $slide['alt'],
        ],
        'field_p_slide_image' => $media_array,
      ]);

      $child_para->save();

      return [
        'target_id' => $child_para->id(),
        'target_revision_id' => $child_para->getRevisionId(),
      ];
    }
    catch (InvalidPluginDefinitionException | EntityStorageException | PluginNotFoundException $e) {
      return NULL;
    }
  }

}
