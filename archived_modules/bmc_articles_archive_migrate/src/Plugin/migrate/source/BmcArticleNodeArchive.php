<?php

namespace Drupal\bmc_articles_archive_migrate\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node as d7_node;
use Drupal\redirect\Entity\Redirect;
use PDO;

/**
 * Published nodes from the d7 database.
 *
 * @MigrateSource(
 *   id = "bmc_article_node_archive",
 *   source_module = "node"
 * )
 */
class BmcArticleNodeArchive extends d7_node {

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    $query = parent::query();

    // Only migrate published records.
    $query->condition('n.status', 1)->condition('n.created', '1535760000', '<');

    // Get the URL alias.
    $query->leftJoin('url_alias', 'ua', "ua.source = CONCAT('node/', n.nid)");
    $query->addExpression('GROUP_CONCAT(DISTINCT ua.alias)', 'php_alias');

    $query->leftJoin('users', 'u', "u.uid = n.uid");
    $query->addExpression('GROUP_CONCAT(DISTINCT u.name)', 'php_author');

    $query->leftJoin('field_data_field_department', 'fd', 'n.nid = fd.entity_id');
    $query->condition('field_department_tid', 695, '<>');

    $query->condition('n.nid', 10506, '<>');


    // Only return one row per node.
    // This is one way of solving "isn't in GROUP BY" issues with < MySQL 5.7.
    // We cannot set ONLY_FULL_GROUP_BY in Webfaction, unfortunately.
    $query->groupBy('n.nid');
    $query->groupBy('n.uid');
    $query->groupBy('n.type');
    $query->groupBy('n.language');
    $query->groupBy('n.status');
    $query->groupBy('n.created');
    $query->groupBy('n.changed');
    $query->groupBy('n.comment');
    $query->groupBy('n.promote');
    $query->groupBy('n.sticky');
    $query->groupBy('n.tnid');
    $query->groupBy('n.translate');
    $query->groupBy('nr.vid');
    $query->groupBy('nr.title');
    $query->groupBy('nr.log');
    $query->groupBy('nr.timestamp');
    $query->groupBy('nr.uid');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = $this->baseFields();
    $fields['body/format'] = $this->t('Format of body');
    $fields['body/value'] = $this->t('Full text of body');
    $fields['body/summary'] = $this->t('Summary of body');
    return $fields;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function prepareRow(Row $row): bool {
    $nid = $row->getSourceProperty('nid');
    // Get the Gallery Images.
    $this->setGalleryImages($row);
    // Get the Other fields of Node.
    $this->setNodeOtherFields($row);
    $this->setCarouselFieldCollection($row);

    $this->splitAliases($row);
    $row->setSourceProperty('php_nid', [$row->getSourceProperty('nid')]);
    return parent::prepareRow($row);
  }

  /**
   * Splits out multiple aliases into a primary alias and redirects.
   *
   * @throws \Exception
   */
  protected function splitAliases(Row $row): void {
    $aliases = explode(',', $row->getSourceProperty('php_alias'));
    if (count($aliases) > 1) {
      $row->setSourceProperty('php_alias', $aliases[0]);
      /** @var \Drupal\redirect\RedirectRepository $repository */
      $source_url = '/' . $aliases[1];
      $redirect_url = '/' . $aliases[0];
      $repository = \Drupal::service('redirect.repository');
      $redirect_exists = $repository->findBySourcePath($source_url);
      if (!isset($redirect_exists)) {
        Redirect::create([
          'redirect_source' => $source_url,
          'redirect_redirect' => $redirect_url,
          'status_code' => 301,
        ])->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function setNodeOtherFields(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        ttd.name,
        fiit.field_primary_image_type_value,
        ft.field_byline_value,
        ft.field_byline_format,
        fi.field_blurb_value,
        fi.field_blurb_format,
        fp.field_premium_value,
        fpb.field_premium_blub_value,
        fct.field_carousel_type_value,
        figc.field_image_gallery_caption_value,
        fcst.field_custom_sidebar_text_value,
        fcst.field_custom_sidebar_text_format,
        feb.field_expert_blurb_value,
        fbdi.field_bulletin_drop_in_value,
        fbdi.field_bulletin_drop_in_format,
        fld.body_value,
        fld.body_summary,
        fld.body_format,
        fm.uid,
        fm.filename,
        fm.uri,
        fm.filemime,
        fm.filesize,
        fii.field_primary_image_fid,
        fii.field_primary_image_alt,
        fii.field_primary_image_title,
        fii.field_primary_image_width,
        fii.field_primary_image_height
      FROM
        node n
      LEFT JOIN
        {field_data_field_primary_image_type} fiit
      ON
        n.nid = fiit.entity_id
      LEFT JOIN
        {field_data_field_byline} ft
      ON
        n.nid = ft.entity_id
      LEFT JOIN
        {field_data_field_blurb} fi
      ON
        n.nid = fi.entity_id
      LEFT JOIN
        {field_data_field_premium} fp
      ON
        n.nid = fp.entity_id
      LEFT JOIN
        {field_data_field_premium_blub} fpb
      ON
        n.nid = fpb.entity_id
      LEFT JOIN
        {field_data_field_carousel_type} fct
      ON
        n.nid = fct.entity_id
      LEFT JOIN
        {field_data_field_image_gallery_caption} figc
      ON
        n.nid = figc.entity_id
      LEFT JOIN
        {field_data_field_custom_sidebar_text} fcst
      ON
        n.nid = fcst.entity_id
      LEFT JOIN
        {field_data_field_bulletin_drop_in} fbdi
      ON
        n.nid = fbdi.entity_id
      LEFT JOIN
        {field_data_body} fld
      ON n.nid = fld.entity_id
      LEFT JOIN
        {field_data_field_expert_blurb} feb
      ON n.nid = feb.entity_id
      LEFT JOIN
        {field_data_field_primary_image} fii
      ON
        n.nid = fii.entity_id
      LEFT JOIN
        {file_managed} fm
      ON
        fii.field_primary_image_fid = fm.fid
      LEFT JOIN
        {field_data_field_department} fd
      ON
        n.nid = fd.entity_id
      LEFT JOIN
        {taxonomy_term_data} ttd
      ON
        fd.field_department_tid = ttd.tid
      WHERE
        n.nid = :nid', [':nid' => $nid]);

    foreach ($result as $record) {
      $row->setSourceProperty('tfield_department', $record->name);
      $row->setSourceProperty('tfield_premium', $record->field_premium_value);
      $row->setSourceProperty('tfield_premium_blub', $record->field_premium_blub_value);
      $row->setSourceProperty('tfield_carousel_type', $record->field_carousel_type_value);
      $row->setSourceProperty('tfield_image_gallery_caption', $record->field_image_gallery_caption_value);
      $row->setSourceProperty('tfield_primary_image_type', $record->field_primary_image_type_value);
      $row->setSourceProperty('tfield_expert_blurb', $record->field_expert_blurb_value);
      $row->setSourceProperty('tfield_bulletin_drop_in',
        [
          'value' => $record->field_bulletin_drop_in_value,
          'format' => $record->field_bulletin_drop_in_format
        ]
      );
      $row->setSourceProperty('tfield_blurb',
        [
          'value' => $record->field_blurb_value,
          'format' => $record->field_blurb_format
        ]
      );
      $row->setSourceProperty('tfield_byline',
        [
          'value' => $record->field_byline_value,
          'format' => $record->field_byline_format
        ]
      );
      $primary_image = [
        'uid' => $record->uid,
        'filename' => $record->filename,
        'uri' => $record->uri,
        'filemime' => $record->filemime,
        'filesize' => $record->filesize,
        'target_id' => $record->field_primary_image_fid,
        'alt' => $record->field_primary_image_alt,
        'title' => $record->field_primary_image_title,
        'width' => $record->field_primary_image_width,
        'height' => $record->field_primary_image_height,
      ];
      $row->setSourceProperty('tfield_primary_image', $primary_image);
      $row->setSourceProperty('body_value', $record->body_value);
      $row->setSourceProperty('body_summary', $record->body_summary);
      $row->setSourceProperty('body_format', $record->body_format);
    }

    $result_sidebar_text = $this->getDatabase()->query('
        SELECT
          fcst.field_custom_sidebar_text_value,
          fcst.field_custom_sidebar_text_format
        FROM
            node n
        LEFT JOIN
            {field_data_field_custom_sidebar_text} fcst
        ON
            n.nid = fcst.entity_id
        WHERE
            n.nid = :nid AND
            fcst.bundle = :bundle', [
              ':nid' => $nid,
              ':bundle' => 'article',
    ]);
    foreach ($result_sidebar_text as $record) {
      $row->setSourceProperty('tfield_custom_sidebar_text',
        [
          'value' => $record->field_custom_sidebar_text_value,
          'format' => $record->field_custom_sidebar_text_format
        ]
      );
    }

    // Get Multi Department.
    $department_multi = $this->getDatabase()->query('
      SELECT
        GROUP_CONCAT(td.name) as tnames
      FROM
        {field_data_field_department_multi} fld
      LEFT JOIN
        {taxonomy_term_data} td
      ON
        fld.field_department_multi_tid = td.tid
      WHERE
        fld.entity_id = :nid', [':nid' => $nid]);
    foreach ($department_multi as $dep) {
      if (!is_null($dep->tnames)) {
        $row->setSourceProperty('tfield_department_multi', explode(',', $dep->tnames));
      }
    }

    // Get topic term names.
    $topics = $this->getDatabase()->query('
      SELECT
        GROUP_CONCAT(td.name) as tnames
      FROM
        {field_data_field_article_terms} fld
      LEFT JOIN
        {taxonomy_term_data} td
      ON
        fld.field_article_terms_tid = td.tid
      WHERE
        fld.entity_id = :nid', [':nid' => $nid]);
    foreach ($topics as $topic) {
      if (!is_null($topic->tnames)) {
        $row->setSourceProperty('tfield_article_terms', explode(',', $topic->tnames));
      }
    }

    // Get CTA Links.
    $cta = $this->getDatabase()->query('
      SELECT
        field_cta_url,
        field_cta_title,
        field_cta_attributes
      FROM
        {field_data_field_cta}
      WHERE
        entity_id = :nid AND
        entity_type = :node AND
        bundle = :bundle
      ORDER BY delta', [
        ':nid' => $nid,
        ':node' => 'node',
        'bundle' => 'article',
      ]);
    $ctas = [];
    foreach ($cta as $cta) {
      $ctas[] = [
        'url' => $cta->field_cta_url,
        'title' => $cta->field_cta_title,
        'attributes' => $cta->field_cta_attributes
      ];
    }
    $row->setSourceProperty('tfield_cta', $ctas);

    // Get Related Links.
    $related_links = $this->getDatabase()->query('
      SELECT
        field_related_links_url,
        field_related_links_title,
        field_related_links_attributes
      FROM
        {field_data_field_related_links}
      WHERE
        entity_id = :nid
      ORDER BY delta', [':nid' => $nid]);
    $links = [];
    foreach ($related_links as $cta) {
      $links[] = [
        'url' => $cta->field_related_links_url,
        'title' => $cta->field_related_links_title,
        'attributes' => $cta->field_related_links_attributes
      ];
    }
    $row->setSourceProperty('tfield_related_links', $links);

    // Get Bullentin Related nodes.
    $bullentin_related_links = $this->getDatabase()->query('
      SELECT
        field_bulletin_related_target_id
      FROM
        {field_data_field_bulletin_related}
      WHERE
        entity_id = :nid
      ORDER BY delta', [':nid' => $nid]);
    $links = [];
    foreach ($bullentin_related_links as $cta) {
      $links[] = $cta->field_bulletin_related_target_id;
    }
    $row->setSourceProperty('tfield_bulletin_related', $links);

    // Bulletin Issue & Bulletin Section Term Names.
    $bullentin = $this->getDatabase()->query('
      SELECT
        td1.name as issue,
        td2.name as section
      FROM
        {node} n
      LEFT JOIN
        {field_data_field_bulletin_issue} fbi
      ON
        n.nid = fbi.entity_id
      LEFT JOIN
        {taxonomy_term_data} td1
      ON
        fbi.field_bulletin_issue_tid = td1.tid
      LEFT JOIN
        {field_data_field_bulletin_section} fbs
      ON
        n.nid = fbs.entity_id
      LEFT JOIN
        {taxonomy_term_data} td2
      ON
        fbs.field_bulletin_section_tid = td2.tid
      WHERE
        n.nid = :nid', [':nid' => $nid]);
    foreach ($bullentin as $result) {
      if (!is_null($result->issue)) {
        $row->setSourceProperty('tfield_bulletin_issue', $result->issue);
      }
      if (!is_null($result->section)) {
        $row->setSourceProperty('tfield_bulletin_section', $result->section);
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function setGalleryImages(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        fm.uid,
        fm.filename,
        fm.uri,
        fm.filemime,
        fm.filesize,
        fig.field_image_gallery_fid,
        fig.field_image_gallery_alt,
        fig.field_image_gallery_title,
        fig.field_image_gallery_width,
        fig.field_image_gallery_height
      FROM
        {field_data_field_image_gallery} fig
      LEFT JOIN
        {file_managed} fm
      ON
        fig.field_image_gallery_fid = fm.fid
      WHERE
        fig.entity_id = :nid', [':nid' => $nid]);
    $images = [];
    foreach ($result as $record) {
      $images[] = [
        'uid' => $record->uid,
        'filename' => $record->filename,
        'uri' => $record->uri,
        'filemime' => $record->filemime,
        'filesize' => $record->filesize,
        'target_id' => $record->field_files_fid,
        'alt' => $record->field_image_gallery_alt,
        'title' => $record->field_image_gallery_title,
        'width' => $record->field_image_gallery_width,
        'height' => $record->field_image_gallery_height,
      ];
    }
    $row->setSourceProperty('tfield_image_gallery', $images);
  }

  /**
   * {@inheritdoc}
   */
  protected function setCarouselFieldCollection(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $old_slides = $this->getDatabase()->query(
      'SELECT
        fc.field_carousel_value
      FROM
        {field_data_field_carousel} fc
      WHERE
        fc.entity_id = :nid',
      [':nid' => $nid]
    )->fetchAll(PDO::FETCH_ASSOC);
    $new_slides = [];
    if (count($old_slides) > 0) {
      $old_slide_entity_type = 'field_collection_item';
      $old_slide_bundle = 'field_carousel';
      foreach ($old_slides as $slide) {
        $new_slide = [];
        $old_slide_id =  $slide['field_carousel_value'];

        // Slide Title
        $new_slide_title = '';
        $old_slide_title_results = $this->database->query('
        SELECT
            field_slide_title_value
        FROM
            {field_data_field_slide_title} f
        WHERE
            f.entity_type = :entity_type AND
            f.bundle = :bundle AND
            f.entity_id = :fcid',
          [
            ':entity_type' => $old_slide_entity_type,
            ':bundle' => $old_slide_bundle,
            ':fcid' => $old_slide_id,
          ]
        )->fetchAll(PDO::FETCH_ASSOC);

        if (count($old_slide_title_results) > 0) {
          $new_slide_title = $old_slide_title_results[0]['field_slide_title_value'];
        }
        $new_slide['field_slide_title'] = $new_slide_title;

        // Slide Header
        $new_slide_header = '';
        $old_slide_header_results = $this->database->query('
        SELECT
            field_slide_header_value
        FROM
            {field_data_field_slide_header} f
        WHERE
            f.entity_type = :entity_type AND
            f.bundle = :bundle AND
            f.entity_id = :fcid',
          [
            ':entity_type' => $old_slide_entity_type,
            ':bundle' => $old_slide_bundle,
            ':fcid' => $old_slide_id,
          ]
        )->fetchAll(PDO::FETCH_ASSOC);

        if (count($old_slide_header_results) > 0) {
          $new_slide_title = $old_slide_header_results[0]['field_slide_header_value'];
        }
        $new_slide['field_slide_header'] = $new_slide_title;

        // Slide Body
        $new_slide_body = '';
        $old_slide_body_results = $this->database->query('
        SELECT
            field_slide_body_value
        FROM
            {field_data_field_slide_body} f
        WHERE
            f.entity_type = :entity_type AND
            f.bundle = :bundle AND
            f.entity_id = :fcid',
          [
            ':entity_type' => $old_slide_entity_type,
            ':bundle' => $old_slide_bundle,
            ':fcid' => $old_slide_id,
          ]
        )->fetchAll(PDO::FETCH_ASSOC);

        if (count($old_slide_body_results) > 0) {
          $new_slide_body = $old_slide_body_results[0]['field_slide_body_value'];
        }
        $new_slide['field_slide_body'] = $new_slide_body;

        // Slide Link
        $new_slide_share_link_url = '';
        $new_slide_share_link_title = '';
        $new_slide_share_link_attributes = '';
        $old_slide_body_results = $this->database->query('
        SELECT
            field_slide_share_link_url,
            field_slide_share_link_title,
            field_slide_share_link_attributes
        FROM
            {field_data_field_slide_share_link} f
        WHERE
            f.entity_type = :entity_type AND
            f.bundle = :bundle AND
            f.entity_id = :fcid',
          [
            ':entity_type' => $old_slide_entity_type,
            ':bundle' => $old_slide_bundle,
            ':fcid' => $old_slide_id,
          ]
        )->fetchAll(PDO::FETCH_ASSOC);

        if (count($old_slide_body_results) > 0) {
          $new_slide_share_link_url = $old_slide_body_results[0]['field_slide_share_link_url'];
          $new_slide_share_link_title = $old_slide_body_results[0]['field_slide_share_link_title'];
          $new_slide_share_link_attributes = $old_slide_body_results[0]['$new_slide_share_link_attributes'];
        }
        $new_slide['field_slide_share_link'] = [
          'url' => $new_slide_share_link_url,
          'title' => $new_slide_share_link_title,
          'attributes' => $new_slide_share_link_attributes,
        ];

        // Slide Image
        $new_slide_image_uid = '';
        $new_slide_image_filename = '';
        $new_slide_image_uri = '';
        $new_slide_image_filemime = '';
        $new_slide_image_filesize = '';
        $new_slide_image_target_id = '';
        $new_slide_image_alt = '';
        $new_slide_image_title = '';
        $new_slide_image_width = '';
        $new_slide_image_height = '';

        $old_slide_image_results = $this->database->query('
        SELECT
            fsi.field_slide_image_fid,
            fsi.field_slide_image_alt,
            fsi.field_slide_image_title,
            fsi.field_slide_image_width,
            fsi.field_slide_image_height,
            fm.uid,
            fm.filename,
            fm.uri,
            fm.filemime,
            fm.filesize
        FROM
            {field_data_field_slide_image} fsi
        LEFT JOIN
            {file_managed} fm
        ON
            fsi.field_slide_image_fid = fm.fid
        WHERE
            fsi.entity_type = :entity_type AND
            fsi.bundle = :bundle AND
            fsi.entity_id = :fcid',
          [
            ':entity_type' => $old_slide_entity_type,
            ':bundle' => $old_slide_bundle,
            ':fcid' => $old_slide_id,
          ]
        )->fetchAll(PDO::FETCH_ASSOC);

        if (count($old_slide_image_results) > 0) {
          $old_slide_image = $old_slide_image_results[0];
          $new_slide_image_uid = $old_slide_image['uid'];
          $new_slide_image_filename = $old_slide_image['filename'];
          $new_slide_image_uri = $old_slide_image['uri'];
          $new_slide_image_filemime = $old_slide_image['filemime'];
          $new_slide_image_filesize = $old_slide_image['filesize'];
          $new_slide_image_target_id = $old_slide_image['fid'];
          $new_slide_image_alt = $old_slide_image['alt'];
          $new_slide_image_title = $old_slide_image['title'];
          $new_slide_image_width = $old_slide_image['width'];
          $new_slide_image_height = $old_slide_image['height'];

          $new_slide['field_slide_image'] = [
            'uid' => $old_slide_image['uid'],
            'filename' => $old_slide_image['filename'],
            'uri' => $old_slide_image['uri'],
            'filemime' => $old_slide_image['filemime'],
            'filesize' => $old_slide_image['filesize'],
            'target_id' => $old_slide_image['field_files_fid'],
            'alt' => $old_slide_image['field_intro_image_alt'],
            'title' =>$old_slide_image['field_intro_image_title'],
            'width' => $old_slide_image['field_intro_image_width'],
            'height' => $old_slide_image['field_intro_image_height'],
          ];
        }

        // CTAs
        $new_slide_ctas = [];
        $old_slide_cta_results = $this->getDatabase()->query('
        SELECT
          field_cta_url,
          field_cta_title,
          field_cta_attributes
        FROM
          {field_data_field_cta} f
        WHERE
            f.entity_type = :entity_type AND
            f.bundle = :bundle AND
            f.entity_id = :fcid
        ORDER BY delta',
          [
            ':entity_type' => $old_slide_entity_type,
            ':bundle' => $old_slide_bundle,
            ':fcid' => $old_slide_id,
          ]
        )->fetchAll();

        foreach ($old_slide_cta_results as $old_slide_cta_result) {
          $new_slide_ctas[] = [
            'url' => $old_slide_cta_result->field_cta_url,
            'title' => $old_slide_cta_result->field_cta_title,
            'attributes' => $old_slide_cta_result->field_cta_attributes
          ];
        }
        $new_slide['ctas'] = $new_slide_ctas;

        $new_slides[] = $new_slide;
      }
    }

    $row->setSourceProperty('tfield_carousel', $new_slides);
  }

}
