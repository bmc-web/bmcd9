<?php

namespace Drupal\bmc_articles_archive_migrate\Plugin\migrate\process;

use Drupal\bmc_articles_archive_migrate\CreateMediaItems;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Migrates images from an image field.
 *
 * @MigrateProcessPlugin(
 *   id = "news_archive_process_image"
 * )
 *
 * @code
 *   field_name:
 *     source: title
 *     plugin: process_image
 * @endcode
 */
class ProcessImage extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!empty($value)) {
      try {
        $mid_array = (new CreateMediaItems)->generateMediaArray($value);
      }
      catch (InvalidPluginDefinitionException | PluginNotFoundException | EntityStorageException $e) {
      }
    }
    return $mid_array ?? NULL;
  }

}
