<?php

namespace Drupal\bmc_articles_archive_migrate\Plugin\migrate\process;

use Drupal\bmc_articles_archive_migrate\TransformInternalLinks;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Replaces internal links from Drupal 7 with site relative links.
 *
 * @MigrateProcessPlugin(
 *   id = "news_archive_transform_internal_wysiwyg_links"
 * )
 *
 * @code
 *   field_name:
 *     source: title
 *     plugin: transform_internal_wysiwyg_links
 * @endcode
 */
class TransformInternalWysiwygLinks extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (isset($value['value'])) {
      $value = $value['value'];
    }
    // Remove Edge Cases breaking the script.
    $value = str_replace('href="The Conferences &amp; Events Office at Bryn Mawr College is dedicated to working with Bryn Mawr faculty, staff, students and external organizations planning events on the campus."', 'href="https://www.brynmawr.edu"', $value);
    $value = str_replace('mailto: &quot;digicomp@brynmawr.edu&quot;', 'mailto:digicomp@brynmawr.edu', $value);
    $value = str_replace(
      'tel:https://www-dev.brynmawr.edu/admin/custom_reports/gray-box-carousel-slides?order=field_carousel&amp;sort=asc',
      'tel:610-526-7413',
      $value
    );
    $value = str_replace(
      'https://www.brynmawr.edu/career-civic/career-planning/employment-opportunities?lor=3&amp;utm_source=mass_mailer&amp;utm_medium=email&amp;utm_content=559355&amp;utm_campaign=uni_targeted_emails',
      'https://www.brynmawr.edu/career-civic/career-planning/employment-opportunities',
      $value
    );
    $value = str_replace(
      'https://nam11.safelinks.protection.outlook.com/?url=http%3A%2F%2Fgo.codepath.org%2FAndroidS21&amp;data=04%7C01%7Cdlevy1%40brynmawr.edu%7C334ba7293b3d49b5d64908d8a5c5ec6c%7Cc94b117b616347fd93f8b8001804ae6f%7C1%7C0%7C637441616794532028%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C1000&amp;sdata=4zXl2MXEmn1dNvY%2Fk3uIh4CTj7RYvtL6dRjGFuEEQoI%3D&amp;reserved=0',
      'https://www.brynmawr.edu/inside/cleanup',
      $value
    );
    $value = str_replace(
      'https://generalassemb.ly/education?dateType=any&amp;free=true&amp;where=online#catalog-results',
      'https://generalassemb.ly/education?dateType=any&free=true&where=online#catalog-results',
      $value
    );
    $value = str_replace(
      'https://www.volunteermatch.org/?lor=42&amp;utm_source=mass_mailer&amp;utm_medium=email&amp;utm_content=559355&amp;utm_campaign=uni_targeted_emails',
      'https://www.volunteermatch.org/?lor=42',
      $value
    );
    return (new TransformInternalLinks)->transformLinks($value);
  }

}
