<?php

namespace Drupal\bmc_articles_archive_migrate\Plugin\migrate\process;

use Drupal\Core\Database\Database;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Gets Term Names from the D7 db for a TID given.
 *
 * @MigrateProcessPlugin(
 *   id = "news_archive_get_term_name"
 * )
 *
 * @code
 *   field_name:
 *     source: title
 *     plugin: news_archive_get_term_name
 * @endcode
 */
class GetTermName extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $tid = $value['tid'];
    $db = Database::getConnection('default', 'ladyhawke');
    $query = "SELECT name FROM taxonomy_term_data WHERE tid ='" . $tid . "'";
    $result = $db
      ->query($query)
      ->fetchAll();
    return $result[0]->name ?? NULL;
  }

}
