<?php

namespace Drupal\bmc_articles_archive_migrate;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Provides functions for a Featured Content Block Paragraph Entity.
 */
class CreateCalloutPara {

  /**
   * Creates a Callout parent paragraph entity.
   *
   * @param array $featblk
   *   The array of D7 Featured Content Block instance values.
   *
   * @return array|null
   *   The callout paragraph entity reference array.
   */
  public function createFromFeatBlk(array $featblk): ?array {
    try {
      if (isset($featblk['field_landing_page_image']["uri"])) {
        $image_src = str_replace('public://', 'https://www.brynmawr.edu/sites/default/files/', $featblk['field_landing_page_image']["uri"]);
        $file_vars = (new CreateMediaItems)->createFileEntityfromSrc($image_src);
        $media_array = (new CreateMediaItems)->generateMediaArray($featblk['field_landing_page_image'], $file_vars);
      }
      else {
        $media_array = NULL;
      }
      $ctas = [];
      foreach ($featblk["ctas"] as $d7_cta) {
        $cta = [];
        $cta_title = '';
        $cta_uri = '';
        if (isset($d7_cta["url"])) {
          $cta_uri = (new TransformInternalLinks)->checkCtaLinkUri($d7_cta["url"]);
        }
        if (isset($d7_cta["title"])) {
          $cta_title = $d7_cta["title"];
        }
        if (!empty($cta_title) && !empty($cta_uri)) {
          $cta = [
            'uri'  => $cta_uri,
            'title' => $cta_title,
          ];
        }
        if (!empty($cta)) {
          $ctas[] = $cta;
        }
      }

      if (!empty($ctas)) {
        $paragraph = Paragraph::create([
          'type' => 'callout',
          'field_p_callout_heading' => [
            'value'  => $featblk['field_landing_featured_intro'],
          ],
          'field_p_callout_cta' => $ctas,
          'field_p_callout_body' => [
            'value'  => $featblk['field_teaser_text'],
          ],
          'field_p_callout_bgcolor' => [
            'value'  => 'grey',
          ],
          'field_p_callout_h_level' => [
            'value'  => 'h3',
          ],
          'field_p_callout_image' => $media_array,
        ]);
      }
      else {
        $paragraph = Paragraph::create([
          'type' => 'callout',
          'field_p_callout_heading' => [
            'value'  => $featblk['field_landing_featured_intro'],
          ],
          'field_p_callout_body' => [
            'value'  => $featblk['field_teaser_text'],
          ],
          'field_p_callout_bgcolor' => [
            'value'  => 'grey',
          ],
          'field_p_callout_h_level' => [
            'value'  => 'h3',
          ],
          'field_p_callout_image' => $media_array,
        ]);
      }

      $paragraph->save();

      return [
        'target_id' => $paragraph->id(),
        'target_revision_id' => $paragraph->getRevisionId(),
      ];
    }
    catch (InvalidPluginDefinitionException | EntityStorageException | PluginNotFoundException $e) {
      return NULL;
    }
  }

}
