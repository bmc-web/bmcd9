<?php

namespace Drupal\bmc_migrate;

use Drupal\Core\Database\Database;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;

/**
 * Provides functions for creating Sidebar Paragraph Entities.
 */
class CreateSidebarParas {

  /**
   * Loads/downloads & creates Drupal file entity for linked file or image.
   *
   * @param string $fid
   *   The Drupal 7 fid of the file being referenced.
   *
   * @return array
   *   Variables relating to migrated Drupal file entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function createFileEntityFromFid(string $fid): array {
    $file_vars = [];
    $db = Database::getConnection('default', 'bmcd7');
    $query = "SELECT filename,uri FROM file_managed WHERE fid = '"
      . $fid
      . "'";
    $result = $db
      ->query($query)
      ->fetchAll();
    if (isset($result[0]->uri)) {
      $src = str_replace(
        'public://',
        '/sites/default/files/',
        $result[0]->uri);
      $file_vars = $this->createFileEntityFromSrc($src);
    }
    return $file_vars;
  }

  /**
   * Loads/downloads & creates Drupal file entity for linked file or image.
   *
   * @param string $src
   *   URL of image or file being migrated.
   *
   * @return array
   *   Variables relating to migrated Drupal file entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function createFileEntityFromSrc(string $src): array {
    $file_vars = [];

    if (
      $this->strposa($src, [
        'https://www.brynmawr.edu',
        'http://www.brynmawr.edu',
      ]) !== FALSE
    ) {
      $absolute_file_url = $src;
    }
    else {
      $absolute_file_url = 'https://www.brynmawr.edu' . $src;
    }
    // Remove url-encoded ampersands in file names as they cause errors.
    if (strpos($absolute_file_url, '&amp;') !== FALSE) {
      $absolute_file_url =
        str_replace('&amp;', '&', $absolute_file_url);
    }

    $new_file_dir_name =
      str_replace(
        ['https://www.brynmawr.edu', 'http://www.brynmawr.edu'],
        '',
        $src
      );

    $new_file_dir_name =
      str_replace(
        '/sites/default/files/',
        '',
    str_replace(strrchr($new_file_dir_name, '/'), '', $new_file_dir_name)
      );

    $new_drupal_file_dir_name = 'public://migrated-files/' . $new_file_dir_name;
    if ($new_file_dir_name === '/sites/default/files') {
      $new_drupal_file_dir_name = 'public://migrated-files';
    }

    // Clean up edge cases where the absolute URL is getting appended.
    $new_drupal_file_dir_name =
        str_replace(
          'public://migrated-files/https://www.brynmawr.edu/sites/default/files/',
          'public://migrated-files/',
          $new_drupal_file_dir_name
        );

    $new_drupal_file_dir_name =
      str_replace(
        'public://migrated-files/http://www.brynmawr.edu/sites/default/files/',
        'public://migrated-files/',
        $new_drupal_file_dir_name
      );

    // Check if this file already exists in Drupal filesystem as file entity.
    $file_uri_to_check =
      $new_drupal_file_dir_name
      . strrchr($absolute_file_url, '/');

    // Clean up edge cases where the absolute URL is getting appended.
    $file_uri_to_check =
      str_replace(
        'public://migrated-files/https://www.brynmawr.edu/sites/default/files/',
        'public://migrated-files/',
        $file_uri_to_check
      );
    $file_uri_to_check =
      str_replace(
        'public://migrated-files/http://www.brynmawr.edu/sites/default/files/',
        'public://migrated-files/',
        $file_uri_to_check
      );

    $is_existing_file =
      \Drupal::entityTypeManager()
        ->getStorage('file')
        ->loadByProperties(['uri' => $file_uri_to_check]);

    // Use existing file variables.
    if (count($is_existing_file) > 0) {
      $file = reset($is_existing_file);
      $file_vars['fid'] = $file->id();
      $file_vars['filename'] = $file->getFilename();
      $file_vars['url'] = str_replace(
        'public://',
        '/sites/default/files/',
        $file_uri_to_check
      );
      $file_vars['valid_link'] = TRUE;
    }

    // Download file and create new file entity.
    else {
      \Drupal::service('file_system')->prepareDirectory($new_drupal_file_dir_name, FileSystemInterface::CREATE_DIRECTORY);
      $file_headers = @get_headers($absolute_file_url);
      // Checking if file exists.
      if (!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
        $file_vars['valid_link'] = FALSE;
      }
      else {
        $file = system_retrieve_file($absolute_file_url, $new_drupal_file_dir_name, TRUE, FileSystemInterface::EXISTS_REPLACE);
        // Double-checking if file exists.
        if (!empty($file)) {
          $file_vars['valid_link'] = TRUE;
          $file_vars['fid'] = $file->id();
          $file_vars['filename'] = $file->getFilename();
          $file_vars['url'] = str_replace(
            'public://',
            '/sites/default/files/',
            $file->getFileUri()
          );
          $file->save();
        }
        else {
          $file_vars['valid_link'] = FALSE;
        }
      }
    }
    return $file_vars;
  }

  /**
   * Downloads image, creates media entity and returns a media entity object.
   *
   * @param array $value
   *   The value of the field being processed.
   *
   * @return array
   *   An array with the Media ID set as target ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException|\Drupal\Core\Entity\EntityStorageException
   */
  public function generateMediaArray(array $value): ?array {
    $db = Database::getConnection('default', 'bmcd7');
    $query = "SELECT filename,uri FROM file_managed WHERE fid = '"
      . $value['fid']
      . "'";
    $result = $db
      ->query($query)
      ->fetchAll();
    if (isset($result[0]->uri)) {
      $src = str_replace(
        'public://',
        '/sites/default/files/',
        $result[0]->uri);
      $new_file = $this->createFileEntityFromSrc($src);
      if (isset($new_file['fid'])) {
        $file = File::load($new_file['fid']);
        $file_references = \Drupal::service('file.usage')->listUsage($file);

        // Load existing Media entity using this file if it exists.
        if (isset($file_references["file"]["media"])) {
          $media = Media::load(array_key_first($file_references["file"]["media"]));
        }

        // Create a new media entity if it doesn't.
        else {

          // The Alt and Title fields can be empty but not null.
          if (!isset($value['alt'])) {
            $value['alt'] = '';
          }
          if (!isset($value['title'])) {
            $value['title'] = '';
          }

          $media = Media::create([
            'bundle' => 'image',
            'uid' => '1',
            'status' => 1,
            "thumbnail" => [
              "target_id" => $new_file['fid'],
              "alt" => $value['alt'],
            ],
            'field_media_image' => [
              'target_id' => $new_file['fid'],
              'alt' => $value['alt'],
              'title' => $value['title'],
            ],
          ]);
          $imported_file_name = preg_replace(
            '/\\.[^.\\s]{3,4}$/',
            '',
            $result[0]->filename
          );
          $media->setName($imported_file_name)->setPublished(TRUE)->save();
        }
      }
    }
    if (isset($media) && is_object($media)) {
      return [
        'target_id' => $media->mid->value,
      ];
    }
    return NULL;
  }

  /**
   * Copied from 7.x media module media.filter.inc.
   *
   * Parses the contents of a CSS declaration block and returns a keyed array of
   * property names and values.
   *
   * @param string $declarations
   *   One or more CSS declarations delimited by a semicolon. The same as a CSS
   *   declaration block
   *   (see http://www.w3.org/TR/CSS21/syndata.html#rule-sets)
   *   but without the opening and closing curly braces. Also the same as the
   *   value of an inline HTML style attribute.
   *
   * @return array
   *   A keyed array. The keys are CSS property names, and the values are CSS
   *   property values.
   */
  public function mediaParseCssDeclarations(string $declarations): array {
    $properties = [];
    foreach (array_map('trim', explode(";", $declarations)) as $declaration) {
      if ($declaration != '') {
        [$name, $value] = array_map('trim', explode(':', $declaration, 2));
        $properties[strtolower($name)] = $value;
      }
    }
    return $properties;
  }

  /**
   * Runs strpos for a given array.
   */
  public function strposa(string $haystack, array $needles, int $offset = 0): bool {
    foreach ($needles as $needle) {
      if (strpos($haystack, $needle, $offset) !== FALSE) {
        // Stop on first true result.
        return TRUE;
      }
    }
    return FALSE;
  }

}
