<?php

namespace Drupal\bmc_migrate;

/**
 * Provides utility functions for manipulating content.
 */
class ProcessUtilities {

  /**
   * Truncates a string to given characters and appends an ellipsis.
   *
   * @param string $string
   *   String being processed.
   * @param int $length
   *   Length string should be truncated to.
   * @param string $append
   *   HTML entity to be appended end of string. Defaults to ellipsis.
   *
   * @return string
   *   Truncated/processed string/
   */
  public function truncate(string $string, int $length = 100, string $append = "&hellip;"): string {
    $string = trim($string);
    if (strlen($string) > $length) {
      $string = wordwrap($string, $length);
      $string = explode("\n", $string, 2);
      $string = $string[0] . $append;
    }
    return $string;
  }

}
