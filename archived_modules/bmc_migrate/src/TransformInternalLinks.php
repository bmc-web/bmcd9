<?php

namespace Drupal\bmc_migrate;

use Drupal\Core\Database\Database;

/**
 * Provides functions for Transforming Internal Links.
 */
class TransformInternalLinks {

  /**
   * {@inheritdoc}
   */
  public function transformLinks($value) {
    // Replace internal page links.
    $post_links = $this->getLinks($value, TRUE);
    if (!empty($post_links)) {
      foreach ($post_links as $link) {
        $link = str_replace('&amp;', '&', $link);
        // Check if this is an internal D7 link.
        if (strpos($link, 'node') === 0 || strpos($link, '/node') === 0) {
          $new_link = $this->generateNewLinkFromNodePath($link);
        }
        if ($this->startsWith($link, 'https://') == FALSE
          && $this->startsWith($link, 'http://') == FALSE
          && $this->startsWith($link, '/') == FALSE &&
          $this->startsWith($link, 'node') == FALSE) {

          if (strpos($link, '.com') !== FALSE) {
            $new_link = 'https://' . $link;
          }

          elseif (strpos($link, '@') !== FALSE) {
            if (strpos($link, 'mailto:') !== FALSE) {
              $new_link = 'mailto' . $link;
            }
          }

          elseif (strpos($link, 'tel:') !== FALSE) {
            $new_link = $link;
          }

          else {
            $new_link = $this->generateNewLinkFromD7Alias($link);
          }

        }
        // Only replace the link if we have a new link.
        if (isset($new_link)) {
          $value = str_replace($link, $new_link, $value);
        }
      }
    }
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getLinks($html_as_string, $get_attrs = FALSE): array {
    preg_match_all('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $html_as_string, $matches);
    return $matches['href'];
  }

  /**
   * Generates a new link from a D7 internal link starting with '/node/'.
   *
   * @param string $link
   *   The '/node/*' level link to be parsed.
   */
  public function generateNewLinkFromNodePath(string $link): string {
    $matching_nid = '';
    $alias_source = str_replace('/node', 'node', $link);
    $link_nid = str_replace('node/', '', $alias_source);

    // If link node has already been migrated, use the new NID.
    if (is_numeric($link_nid)) {
      $existing_node_query = \Drupal::entityQuery('node')
        ->accessCheck()->condition('field_d7_legacy_nid', $link_nid);
      $matching_nids = $existing_node_query->execute();
      $matching_nid = reset($matching_nids);
    }

    if (!empty($matching_nid)) {
      $new_link = '/node/' . $matching_nid;
    }
    else {
      // If not, query the Drupal 7 db for an alias to use.
      $db = Database::getConnection('default', 'bmcd7');
      $query = "SELECT alias FROM url_alias WHERE source LIKE '"
        . $alias_source
        . "'";
      $result = $db
        ->query($query)
        ->fetchAll();

      // If we get an alias from the D7 url_alias table, let's use it.
      if ($result[0] && (strpos($result[0]->alias, 'rootlevel') === FALSE)) {
        $new_link = 'https://www.brynmawr.edu/' . $result[0]->alias;
      }
      // If there's no entry in the D7 alias table, direct to a holding page.
      else {
        $new_link = 'https://www.brynmawr.edu/inside/cleanup';
      }
    }
    return $new_link;
  }

  /**
   * Generates a new link from a D7 aliased link.
   *
   * @param string $link
   *   The '/node/*' level link to be parsed.
   */
  public function generateNewLinkFromD7Alias(string $link): string {
    $new_link = $link;
    // If not, query the Drupal 7 db for an alias to use.
    $db = Database::getConnection('default', 'bmcd7');
    $alias_query = "SELECT alias, source FROM url_alias WHERE alias LIKE '"
      . $link
      . "'";
    $alias_result = $db
      ->query($alias_query)
      ->fetchAll();

    if (isset($alias_result[0]->source)) {
      $d7_nid = str_replace('node/', '', $alias_result[0]->source);
    }
    else {
      $redirect_query = "SELECT source, redirect FROM redirect WHERE source LIKE '"
        . $link
        . "'";

      $redirect_result = $db
        ->query($redirect_query)
        ->fetchAll();

      if (
        $alias_result[0]->redirect
        && $this->startsWith($alias_result[0]->redirect, 'node') !== FALSE
      ) {
        $d7_nid = str_replace('node/', '', $alias_result[0]->redirect);
      }
    }

    if (isset($d7_nid)) {
      $existing_node_query = \Drupal::entityQuery('node')
        ->accessCheck()->condition('field_d7_legacy_nid', $d7_nid);
      $matching_nids = $existing_node_query->execute();
      $matching_nid = reset($matching_nids);
      if (!empty($matching_nid)) {
        $new_link = '/node/' . $matching_nid;
      }
      else {
        $new_link = 'https://www.brynmawr.edu/' . $link;
      }
    }
    else {
      $new_link = 'https://www.brynmawr.edu/' . $link;
    }
    return $new_link;
  }

  /**
   * Function to check string starting ith given substring.
   */
  public function startsWith($string, $startString): bool {
    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
  }

  /**
   * Checks a CTA style link URI.
   */
  public function checkCtaLinkUri($link): string {
    $new_link = $link;
    // Check if this is an internal D7 link.
    if (strpos($link, 'node') === 0
      || strpos($link, '/node') === 0
    ) {
      $new_link = $this->generateNewLinkFromNodePath($link);
    }
    if ($this->startsWith($link, 'https://') == FALSE
      && $this->startsWith($link, 'http://') == FALSE
      && $this->startsWith($link, '/') == FALSE
      && $this->startsWith($link, 'node') == FALSE
      && strpos($link, '@') == FALSE
      && strpos($link, 'mailto:') == FALSE
      && strpos($link, 'tel:') == FALSE
      && strpos($link, '.com/') == FALSE
      && !empty($link)
    ) {
      $new_link = $this->generateNewLinkFromD7Alias($link);
    }
    return $new_link;
  }

}
