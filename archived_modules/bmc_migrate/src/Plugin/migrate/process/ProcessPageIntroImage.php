<?php

namespace Drupal\bmc_migrate\Plugin\migrate\process;

use Drupal\bmc_migrate\CreateMediaItems;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Migrates images from the Page intro image field.
 *
 * @MigrateProcessPlugin(
 *   id = "process_page_intro_image"
 * )
 *
 * @code
 *   field_name:
 *     source: title
 *     plugin: process_page_intro_image
 * @endcode
 */
class ProcessPageIntroImage extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!empty($value)) {
      try {
        $mid_array = (new CreateMediaItems)->generateMediaArray($value);
      }
      catch (InvalidPluginDefinitionException | PluginNotFoundException | EntityStorageException $e) {
      }
    }
    return $mid_array ?? NULL;
  }

}
