<?php

namespace Drupal\bmc_migrate\Plugin\migrate\process;

use Drupal\bmc_migrate\ProcessUtilities;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Migrates images from an image field.
 *
 * @MigrateProcessPlugin(
 *   id = "process_location_details"
 * )
 *
 * @code
 *   field_name:
 *     source: title
 *     plugin: process_location_details
 * @endcode
 */
class ProcessLocationDetails extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!empty($value)) {
      $value = (new ProcessUtilities)->truncate($value, 507, '...');
    }
    return $value;
  }

}
