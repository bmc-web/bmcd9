<?php

namespace Drupal\bmc_migrate\Plugin\migrate\process;

use Drupal\bmc_migrate\CreateMediaItems;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Replaces references to image/file embeds with file/media entity references.
 *
 * Finds image embed tags within a WYSIWYG section, prepares a full URL path for
 * each instance, downloads it as a managed file, creates a media entity from
 * that file and re-writes the embed tag to use the <drupal entity> format.
 *
 * @MigrateProcessPlugin(
 *   id = "process_wysiwyg"
 * )
 *
 * @code
 *   field_name:
 *     source: title
 *     plugin: process_wysiwyg
 * @endcode
 */
class ProcessWysiwyg extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!empty($value)) {

      // Replace HTML embeds of Images in WYSIWYG source with D9 Media embeds.
      $embedded_images = $this->getImages($value, TRUE);
      foreach ($embedded_images as $embedded_image) {
        try {
          $file_vars = (new CreateMediaItems)->createFileEntityFromSrc($embedded_image['attr']['src']);
        }
        catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
        }
        if ($file_vars['valid_link'] === FALSE) {
          continue;
        }
        // Update the markup to use Media entity embeds.
        $value = $this->replaceWithMediaEmbed($value, $embedded_image, $file_vars);
      }

      // Replacing D7 Media embeds of Images with D9 Media embeds.
      $embedded_media_images = $this->getMediaImages($value);
      foreach ($embedded_media_images as $embedded_media_image) {
        try {
          $media_file_vars = (new CreateMediaItems)->createFileEntityFromFid($embedded_media_image['d7_fid']);
        }
        catch (InvalidPluginDefinitionException | PluginNotFoundException | EntityStorageException $e) {
          $media_file_vars = [];
        }
        if ($media_file_vars['valid_link'] !== TRUE) {
          continue;
        }
        // Update the markup to use Media entity embeds.
        $value = $this->replaceWithMediaEmbed($value, $embedded_media_image, $media_file_vars);
      }

      // Uses the getLinks function in TransformInternalLinks to return
      // an indexed array of all links in the WYSIWYG field.
      $links = $this->getLinks($value, TRUE);
      foreach ($links as $link) {
        try {
          $value = $this->migrateInternalFiles($value, $link);
        }
        catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
        }
      }
    }
    return $value;
  }

  /**
   * Return an array of all image embeds in the string.
   *
   * @param string $html_string
   *   The WYSIWYG HTML string being parsed.
   * @param bool $get_attrs
   *   Return the image attributes as well as the source.
   *
   * @return array
   *   Array of images embeds found in string with attributes separated out.
   */
  public function getImages(string $html_string, $get_attrs = TRUE): array {
    $embedded_images = [];
    $image_matches = [];

    // Get all images.
    preg_match_all('/<img (.+)\/>/', $html_string, $image_matches, PREG_SET_ORDER);

    // Loop the images and add the raw img html tag to $embedded_images.
    foreach ($image_matches as $image_match) {
      $embedded_image = [];
      $embedded_image['html'] = $image_match[0];

      // If attributes requested get them and add them to array.
      if ($get_attrs === TRUE) {
        preg_match_all('/\s+?(.+)="([^"]*)"/U', $image_match[0], $image_attr_matches, PREG_SET_ORDER);
        foreach ($image_attr_matches as $image_attr) {
          $embedded_image['attr'][$image_attr[1]] = $image_attr[2];
        }
      }
      $embedded_images[] = $embedded_image;
    }
    return $embedded_images;
  }

  /**
   * Return an array of all media image embeds in the string.
   *
   * @param string $html_string
   *   The WYSIWYG HTML string being parsed.
   *
   * @return array
   *   Array of images embeds found in string with attributes separated out.
   */
  public function getMediaImages(string $html_string): array {
    $embedded_images = [];
    $image_matches = [];

    preg_match_all('/\[\[.*?\]\]/s', $html_string, $image_matches, PREG_SET_ORDER);

    // Loop the images and add the raw img html tag to $embedded_images.
    foreach ($image_matches as $image_match) {
      $embedded_image = [];
      $embedded_image['html'] = $image_match[0];
      $image_match = str_replace("[[", "", $image_match);
      $image_match = str_replace("]]", "", $image_match);
      $tag = $image_match[0];

      if (is_string($tag)) {
        // Make it into a fancy array.
        $tag_info = Json::decode($tag);
        if (!isset($tag_info['fid'])) {
          continue;
        }
        $embedded_image['d7_fid'] = $tag_info['fid'];

        // The class attributes is a string, but drupal requires it to be an
        // array, so we fix it here.
        if (!empty($tag_info['attributes']['class'])) {
          $tag_info['attributes']['class'] = explode(" ", $tag_info['attributes']['class']);
        }

        $settings['attributes'] = is_array($tag_info['attributes']) ? $tag_info['attributes'] : [];

        // Parse Inline styles in case they are different.
        if (isset($settings['attributes']['style'])) {
          $css_properties = (new CreateMediaItems)->mediaParseCssDeclarations($settings['attributes']['style']);
          foreach (['width', 'height'] as $dimension) {
            if (isset($css_properties[$dimension]) && substr($css_properties[$dimension], -2) == 'px') {
              $settings[$dimension] = substr($css_properties[$dimension], 0, -2);
            }
            elseif (isset($settings['attributes'][$dimension])) {
              $settings[$dimension] = $settings['attributes'][$dimension];
            }
          }
        }
        $embedded_image['attr'] = $settings['attributes'];

      }

      $embedded_images[] = $embedded_image;
    }

    return $embedded_images;
  }

  /**
   * Migrates a linked file if it's stored in Drupal 7 filesystem.
   *
   * @param string $value
   *   WYSIWYG/HTML string being processed by this plugin.
   * @param array $link
   *   Link within string currently being processed.
   *
   * @return string
   *   Entire WYSIWYG/HTML string with link re-written.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function migrateInternalFiles(string $value, array $link): string {
    $is_internal_file_link = FALSE;
    $internal_link_types = [
      '/sites/default/files/',
      'https://www.brynmawr.edu/sites/default/files/',
      'http://www.brynmawr.edu/sites/default/files/',
    ];

    foreach ($internal_link_types as $type) {
      if (strpos($link['attr']['href'], $type) === 0) {
        $is_internal_file_link = TRUE;
      }
    }

    if ($is_internal_file_link === TRUE) {
      $file_ext = trim(
        substr(
          $link['attr']['href'],
          strrpos($link['attr']['href'], '.')
          + 1
        )
      );

      $allowed_file_types = [
        'txt', 'rtf', 'doc', 'docx', 'ppt', 'pptx', 'xls',
        'xlsx', 'pdf', 'odf', 'odg', 'odp', 'ods', 'odt', 'fodt', 'fods', 'fodp',
        'fodg', 'key', 'numbers', 'pages',
      ];

      if (in_array($file_ext, $allowed_file_types)) {
        $file_vars = (new CreateMediaItems)->createFileEntityFromSrc($link['attr']['href']);

        if ($file_vars['valid_link'] === FALSE) {
          return $value;
        }

        $file_link_markup =
          str_replace(
            $link['attr']['href'],
            $file_vars['url'],
            $link['html']
          );
        $value = str_replace($link['html'], $file_link_markup, $value);

      }
    }
    return $value;
  }

  /**
   * Loads/creates media entity for migrated image and rewrites it's embed tag.
   *
   * @param string $value
   *   WYSIWYG/HTML string being processed by this plugin.
   * @param array $embedded_image
   *   Variables from img tag currently being processed.
   * @param array $file_vars
   *   Variables from the Drupal file entity created for the migrated image.
   *
   * @return string
   *   WYSIWYG/HTML string with image embed rewritten to use Media embed tags.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function replaceWithMediaEmbed(string $value, array $embedded_image, array $file_vars): string {
    $data_align = 'none';
    $this->buildMediaAttributes($embedded_image, $data_align);

    // If we've been passed a media ID use it.
    if (isset($file_vars['mid'])) {
      $media = Media::load($file_vars['mid']);
    }
    // Otherwise search by file ID for an existing media entity, or create one.
    else {
      // Load the referenced file to see if a Media entity already uses it.
      $file = File::load($file_vars['fid']);
      $file_references = \Drupal::service('file.usage')->listUsage($file);

      // Rewrite img tag to use Media entity markup.
      // Load existing Media entity using this file if it exists.
      if (isset($file_references["file"]["media"])) {
        $media = Media::load(array_key_first($file_references["file"]["media"]));
      }

      // Create a new media entity if it doesn't.
      else {
        // The Alt and Title fields can be empty but not null.
        if (!isset($embedded_image['attr']['alt'])) {
          $embedded_image['attr']['alt'] = '';
        }
        if (!isset($embedded_image['attr']['title'])) {
          $embedded_image['attr']['title'] = '';
        }
        $media = Media::create([
          'bundle' => 'image',
          'uid' => '1',
          'status' => 1,
          "thumbnail" => [
            "target_id" => $file_vars['fid'],
            "alt" => $file_vars['filename'],
          ],
          'field_media_image' => [
            'target_id' => $file_vars['fid'],
            'alt' => $embedded_image['attr']['alt'],
            'title' => $embedded_image['attr']['title'],
          ],
        ]);
        $imported_file_name = preg_replace(
          '/\\.[^.\\s]{3,4}$/',
          '',
          $file_vars['filename']
        );
        $media->setName($imported_file_name)->setPublished(TRUE)->save();
      }
    }

    $data_entity_uuid = $media->uuid();
    $new_media_embed_string =
      '<drupal-entity alt="' . $embedded_image['attr']['alt'] . '" data-align="' . $data_align . '" data-embed-button="image" data-entity-embed-display="view_mode:media.wysiwyg_full" data-entity-embed-display-settings="" data-entity-type="media" data-entity-uuid="' . $data_entity_uuid . '"></drupal-entity>';
    $value = str_replace($embedded_image['html'], $new_media_embed_string, $value);

    return $value;
  }

  /**
   * Parses an inline styling string and returns an array of CSS properties.
   *
   * @param string $style_string
   *   Inline styling to be parsed.
   *
   * @return array
   *   Array of CSS properties.
   */
  protected function parseCss(string $style_string): array {
    $results = [];
    preg_match_all("/([\w-]+)\s*:\s*([^;]+)\s*;?/", $style_string, $matches, PREG_SET_ORDER);
    foreach ($matches as $match) {
      $results[$match[1]] = $match[2];
    }
    return $results;
  }

  /**
   * Truncates a string to given characters and appends an ellipsis.
   *
   * @param string $string
   *   String being processed.
   * @param int $length
   *   Length string should be truncated to.
   * @param string $append
   *   HTML entity to be appended end of string. Defaults to ellipsis.
   *
   * @return string
   *   Truncated/processed string/
   */
  protected function truncate(string $string, $length = 100, $append = "&hellip;"): string {
    $string = trim($string);
    if (strlen($string) > $length) {
      $string = wordwrap($string, $length);
      $string = explode("\n", $string, 2);
      $string = $string[0] . $append;
    }

    return $string;
  }

  /**
   * {@inheritdoc}
   */
  public function getLinks($html_string, $get_attrs = FALSE): array {
    $post_links = [];
    $link_matches = [];

    // Get all links.
    preg_match_all(
      '<a href=\x22(.+?)\x22>',
      $html_string,
      $link_matches,
      PREG_SET_ORDER
    );

    // Loop the links and add the raw img html tag to $post_links.
    foreach ($link_matches as $link_match) {
      $post_link['html'] = $link_match[0];
      // If attributes requested get them and add them to array.
      if ($get_attrs === TRUE) {
        preg_match_all(
          '/\s+?(.+)="([^"]*)"/U', $link_match[0],
          $link_attr_matches,
          PREG_SET_ORDER
        );
        foreach ($link_attr_matches as $link_attr) {
          $post_link['attr'][$link_attr[1]] = $link_attr[2];
        }
      }
      $post_links[] = $post_link;
    }
    return $post_links;
  }

  /**
   * Parses the img tag array to add attributes usable by Media embed.
   *
   * @param array $embedded_image
   *   The img tag array.
   * @param string $data_align
   *   The data-align property to be used in the Media embed.
   */
  protected function buildMediaAttributes(array &$embedded_image, string &$data_align): void {
    if (isset($embedded_image['attr']['alt'])) {
      $embedded_image['attr']['alt'] =
        $this->truncate($embedded_image['attr']['alt'], 100);
    }
    if (isset($embedded_image['attr']['title'])) {
      $embedded_image['attr']['title'] =
        $this->truncate($embedded_image['attr']['title'], 100);
    }
    if (isset($embedded_image['attr']['style'])) {
      $properties = $this->parseCss($embedded_image['attr']['style']);
      if (isset($properties['float'])) {
        $data_align = $properties['float'];
      }
    }
    if (isset($embedded_image['attr']['view_mode'])
      && strpos($embedded_image['attr']['view_mode'], 'left') !== FALSE
    ) {
      $data_align = 'left';
    }
    if (
      isset($embedded_image['attr']['view_mode'])
      && strpos($embedded_image['attr']['view_mode'], 'right') !== FALSE
    ) {
      $data_align = 'right';
    }
  }

}
