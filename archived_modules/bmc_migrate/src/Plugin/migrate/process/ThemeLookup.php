<?php

namespace Drupal\bmc_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Sets the destination Theme.
 *
 * @MigrateProcessPlugin(
 *   id = "theme_lookup"
 * )
 *
 * @code
 *   field_name:
 *     source: title
 *     plugin: theme_lookup
 * @endcode
 */
class ThemeLookup extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $d7_dept_tid = $value['tid'];
    $d7_themes = [
      // Bulletin.
      '695',
      // GSAS.
      '461',
      // GSSWSR.
      '520',
      // Postbac.
      '454',
    ];
    $theme_translation = [
      '695' => '1',
      '461' => '2',
      '520' => '3',
      '454' => '6',
    ];

    if (in_array($d7_dept_tid, $d7_themes)) {
      $value['target_id'] = $theme_translation[$d7_dept_tid];
    }
    else {
      $value['target_id'] = '4';
    }
    return $value;
  }

}
