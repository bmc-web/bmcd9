<?php

namespace Drupal\bmc_migrate\Plugin\migrate\process;

use Drupal\Core\Database\Database;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Sets the author user ID.
 *
 * @MigrateProcessPlugin(
 *   id = "author_lookup"
 * )
 *
 * @code
 *   field_name:
 *     source: title
 *     plugin: author_lookup
 * @endcode
 */
class AuthorLookup extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $source_author = ($row->getSource())['php_author'];
    if (
      !isset($source_author)
      || $source_author == 'admin'
      || $source_author == 'czavisca'
    ) {
      return 166;
    }
    else {
      $db = Database::getConnection('default', 'default');
      $query = "SELECT uid,name FROM users_field_data WHERE name = '"
        . $source_author
        . "'";
      $result = $db
        ->query($query)
        ->fetchAll();
      if (empty($result)) {
        return 166;
      }
      else {
        return $result[0]->uid;
      }
    }
  }

}
