<?php

namespace Drupal\bmc_migrate\Plugin\migrate\process;

use Drupal\bmc_migrate\CreateMediaItems;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Migrates images from an image field.
 *
 * @MigrateProcessPlugin(
 *   id = "create_bulletin_gallery"
 * )
 *
 * @code
 *   field_name:
 *     source: title
 *     plugin: create_bulletin_gallery
 * @endcode
 */
class CreateBulletinGallery extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $gallery_array = $row->getSourceProperty('tfield_image_gallery');
    if (!empty($gallery_array)) {
      $parent_mid_array = [];
      foreach ($gallery_array as $gal_item) {
        try {
          $mid_array = (new CreateMediaItems)->generateMediaArray($gal_item);
          $parent_mid_array[] = $mid_array;
        }
        catch (InvalidPluginDefinitionException | PluginNotFoundException | EntityStorageException $e) {
        }
      }
      return $parent_mid_array;
    }
    return [];
  }

}
