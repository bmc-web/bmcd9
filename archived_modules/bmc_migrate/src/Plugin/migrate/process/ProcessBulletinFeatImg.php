<?php

namespace Drupal\bmc_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Migrates images from an image field.
 *
 * @MigrateProcessPlugin(
 *   id = "process_bulletin_featimg"
 * )
 *
 * @code
 *   field_name:
 *     source: title
 *     plugin: process_bulletin_featimg
 * @endcode
 */
class ProcessBulletinFeatImg extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $type = ($row->getSourceProperty("field_primary_image_type"))[0]["value"];
    if (!empty($type) && $type == 'double_wide') {
      return '1';
    }
    return '0';
  }

}
