<?php

namespace Drupal\bmc_migrate\Plugin\migrate\process;

use Drupal\bmc_migrate\CreateMediaItems;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\media\Entity\Media;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Prepends the main WYSIWYG field with an Intro image.
 *
 * If there is a value in the field_intro_image field, and value in the
 *  field_intro_image_type is "wide", we should embed that image in the
 * <drupal entity> format as the first thing in the main page body.
 *
 * @MigrateProcessPlugin(
 *   id = "prepend_page_intro_image"
 * )
 *
 * @code
 *   field_name:
 *     source: body
 *     plugin: prepend_page_intro_image
 * @endcode
 */
class PrependPageIntroImage extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!empty($value)) {

      if (!empty(($row->getSource())['field_intro_image'])
        && ($row->getSource())["field_intro_image_type"][0]["value"] === 'wide') {
        $d7_intro_image = ($row->getSourceProperty('field_intro_image'))[0];
        try {
          $intro_image_mid =
            ((new CreateMediaItems)->generateMediaArray($d7_intro_image))['target_id'];
          $intro_image_media = Media::load($intro_image_mid);
          $data_entity_uuid = $intro_image_media->uuid();
          $new_media_embed_string =
            '<drupal-entity alt="' . $d7_intro_image['alt'] . '" data-embed-button="image" data-entity-embed-display="view_mode:media.wysiwyg_full" data-entity-embed-display-settings="" data-entity-type="media" data-entity-uuid="' . $data_entity_uuid . '"></drupal-entity>';
          $value = $new_media_embed_string . $value;
        }
        catch (InvalidPluginDefinitionException | PluginNotFoundException | EntityStorageException $e) {
        }

      }
    }
    return $value;
  }

}
