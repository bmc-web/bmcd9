<?php

namespace Drupal\bmc_migrate\Plugin\migrate\process;

use Drupal\bmc_migrate\CreateMediaItems;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Migrates image field to image field (no media).
 *
 * @MigrateProcessPlugin(
 *   id = "process_standard_image"
 * )
 *
 * @code
 *   field_name:
 *     source: title
 *     plugin: process_standard_image
 * @endcode
 */
class ProcessStandardImage extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    if (!empty($value)) {
      try {
        $file_array = (new CreateMediaItems)->createFileEntityFromFid($value);
      }
      catch (InvalidPluginDefinitionException | PluginNotFoundException | EntityStorageException $e) {
      }
    }
    return $file_array['fid'] ?? NULL;
  }

}
