<?php

namespace Drupal\bmc_migrate\Plugin\migrate\process;

use Drupal\bmc_migrate\CreateCarouselPara;
use Drupal\bmc_migrate\CreateImageGalleryPara;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Creates WYSIWYG para bundle as the node is being migrated.
 *
 * @MigrateProcessPlugin(
 *   id = "create_news_paras",
 *   handle_multiples = TRUE
 * )
 */
class CreateNewsParas extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $paragraphs = [];

    if (!empty($row->getSourceProperty('tfield_carousel'))) {
      $carousel = $row->getSourceProperty('tfield_carousel');
      $paragraphs[] = (new CreateCarouselPara)->createParentPara($carousel);
    }

    if (!empty($row->getSourceProperty('tfield_image_gallery'))) {
      $gallery = $row->getSourceProperty('tfield_image_gallery');
      $gallery_caption = $row->getSourceProperty('tfield_image_gallery_caption');
      if (!empty($gallery_caption)) {
        $paragraphs[] = (new CreateImageGalleryPara)->createGalleryPara($gallery, $gallery_caption);
      }
      else {
        $paragraphs[] = (new CreateImageGalleryPara)->createGalleryPara($gallery);
      }
    }

    if (!empty($row->getSourceProperty('tfield_custom_sidebar_text'))) {
      $sidebar = $row->getSourceProperty('tfield_custom_sidebar_text');
      $paragraphs[] = $this->createWysiwygParagraphItem($sidebar['value']);
    }
    return $paragraphs;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() : bool {
    return TRUE;
  }

  /**
   * Generates a paragraph item entity.
   *
   * @param string $i
   *   Values from the row being migrated.
   *
   * @return array
   *   Target/revision IDs to pass back to the entity reference field.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createWysiwygParagraphItem($i) : array {

    if (strpos($i, '<iframe') !== FALSE
      || strpos($i, '<script') !== FALSE) {
      $paragraph = Paragraph::create([
        'type' => 'embed',
        'field_p_embed_body' => [
          'value' => $i,
          'format' => 'embed',
        ],
      ]);
    }
    else {
      $paragraph = Paragraph::create([
        'type' => 'wysiwyg',
        'field_wysiwyg_body' => [
          'value'  => $i,
          'format' => 'advanced',
        ],
      ]);
    }

    $paragraph->save();

    return [
      'target_id' => $paragraph->id(),
      'target_revision_id' => $paragraph->getRevisionId(),
    ];
  }

}
