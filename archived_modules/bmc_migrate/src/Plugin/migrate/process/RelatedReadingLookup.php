<?php

namespace Drupal\bmc_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Sets the author user ID.
 *
 * @MigrateProcessPlugin(
 *   id = "related_reading_lookup"
 * )
 *
 * @code
 *   field_name:
 *     source: title
 *     plugin: related_reading_lookup
 * @endcode
 */
class RelatedReadingLookup extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (is_numeric($value['target_id'])) {
      $nid = $value['target_id'];
      $existing_node_query = \Drupal::entityQuery('node')
        ->accessCheck()->condition('field_d7_legacy_nid', $nid);
      $matching_nids = $existing_node_query->execute();
      $matching_nid = reset($matching_nids);
      if (!empty($matching_nid)) {
        return ['target_id' => $matching_nid];
      }
    }
    return ['target_id' => '471'];
  }

}
