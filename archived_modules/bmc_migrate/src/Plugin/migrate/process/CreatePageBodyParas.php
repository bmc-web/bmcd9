<?php

namespace Drupal\bmc_migrate\Plugin\migrate\process;

use Drupal\bmc_migrate\CreateMediaItems;
use Drupal\bmc_migrate\ProcessLinksets;
use Drupal\bmc_migrate\CreateCalloutPara;
use Drupal\bmc_migrate\CreateCarouselPara;
use Drupal\bmc_migrate\CreateImageGalleryPara;
use Drupal\bmc_migrate\TransformInternalLinks;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\media\Entity\Media;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Creates WYSIWYG para bundle as the node is being migrated.
 *
 * @MigrateProcessPlugin(
 *   id = "create_page_body_paras",
 *   handle_multiples = TRUE
 * )
 */
class CreatePageBodyParas extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $paragraphs = [];

    // Generate the main Body area paragraph entity.
    $this->getMainBodyPara($value, $paragraphs);

    if (!empty($row->getSourceProperty('tfield_carousel'))) {
      $carousel = $row->getSourceProperty('tfield_carousel');
      $paragraphs[] = (new CreateCarouselPara)->createParentPara($carousel);
    }
    if (!empty($row->getSourceProperty('tfield_featured_content_block'))) {
      $featblks = $row->getSourceProperty('tfield_featured_content_block');
      foreach ($featblks as $featblk) {
        $paragraphs[] = (new CreateCalloutPara)->createFromFeatBlk($featblk);
      }
    }
    if (!empty($row->getSourceProperty('tfield_image_gallery'))) {
      $gallery = $row->getSourceProperty('tfield_image_gallery');
      $gallery_caption = $row->getSourceProperty('tfield_image_gallery_caption');
      if (!empty($gallery_caption)) {
        $paragraphs[] = (new CreateImageGalleryPara)->createGalleryPara($gallery, $gallery_caption);
      }
      else {
        $paragraphs[] = (new CreateImageGalleryPara)->createGalleryPara($gallery);
      }
    }

    if (!empty($row->getSourceProperty('tfield_sidebar'))) {
      $sidebar = $row->getSourceProperty('tfield_sidebar');
      if (isset($sidebar["field_cta"][0])) {
        $ctas = '';
        foreach ($sidebar["field_cta"] as $cta) {
          if (!empty($cta['url'])) {
            $cta_url = (new TransformInternalLinks)->checkCtaLinkUri($cta['url']);
            $cta_link_text = '<a class="cta--button--primary" href="' . $cta_url . '">' . $cta['title'] . '</a>';
            $ctas .= $cta_link_text;
          }
        }
        $paragraphs[] = $this->createWysiwygParagraphItem($ctas);
      }
      if (!empty($sidebar["field_custom_sidebar_text"])) {
        $paragraphs[] = $this->createWysiwygParagraphItem($sidebar["field_custom_sidebar_text"]);
      }
      if (!empty($sidebar["field_contact_info"])) {
        $this->createContactWysiwygPara($sidebar["field_contact_info"], $paragraphs);
      }

      if (!empty($sidebar["field_image"])) {
        $this->createImageWysiwygPara($sidebar["field_image"], $paragraphs);
      }

      if (!empty($row->getSourceProperty('tfield_info_link'))) {
        $info_links = $row->getSourceProperty('tfield_info_link');
        $paragraphs[] = (new ProcessLinksets)->createInfoLinkset($info_links);
      }

      if (!empty($row->getSourceProperty('tfield_image_link'))) {
        $linkset = $row->getSourceProperty('tfield_image_link');
        $paragraphs[] = (new ProcessLinksets)->createImageLinkset($linkset);
      }
    }
    return $paragraphs;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple(): bool {
    return TRUE;
  }

  /**
   * Generates a paragraph item entity.
   *
   * @param string $i
   *   Values from the row being migrated.
   *
   * @return array
   *   Target/revision IDs to pass back to the entity reference field.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createWysiwygParagraphItem($i): array {
    if (strpos($i, '<iframe') !== FALSE
      || strpos($i, '<script') !== FALSE) {
      $paragraph = Paragraph::create([
        'type' => 'embed',
        'field_p_embed_body' => [
          'value' => $i,
          'format' => 'embed',
        ],
      ]);
    }
    else {
      $paragraph = Paragraph::create([
        'type' => 'wysiwyg',
        'field_wysiwyg_body' => [
          'value' => $i,
          'format' => 'advanced',
        ],
      ]);
    }

    $paragraph->save();

    return [
      'target_id' => $paragraph->id(),
      'target_revision_id' => $paragraph->getRevisionId(),
    ];
  }

  /**
   * Prepares the Main Body paragraph entity.
   *
   * @param string $value
   *   Main Body value string.
   * @param array $paragraphs
   *   Paragraphs array to pass to migrate process.
   */
  public function getMainBodyPara($value, array &$paragraphs): void {
    if (is_array($value)) {
      foreach ($value as $i) {
        try {
          $paragraphs[] = $this->createWysiwygParagraphItem($i);
        }
        catch (EntityStorageException $e) {
        }
      }
    }
    else {
      try {
        $paragraphs[] = $this->createWysiwygParagraphItem($value);
      }
      catch (EntityStorageException $e) {
      }
    }
  }

  /**
   * Creates a WYSIWYG para with embedded image from field_image value.
   *
   * @param array $field_image
   *   The field_image variables.
   * @param array $paragraphs
   *   Paragraphs array with WYSIWYG para added.
   */
  public function createImageWysiwygPara(array $field_image, array &$paragraphs): void {
    foreach ($field_image as $image) {
      $image_src = str_replace('public://', 'https://www.brynmawr.edu/sites/default/files/', $image['uri']);
      try {
        $file_vars = (new CreateMediaItems)->createFileEntityfromSrc($image_src);
        $media_array = (new CreateMediaItems)->generateMediaArray($image, $file_vars);
        $image_media = Media::load($media_array['target_id']);
        $data_entity_uuid = $image_media->uuid();
        $image_media_embed_string =
          '<drupal-entity alt="' . $image['alt'] . '" data-embed-button="image" data-entity-embed-display="view_mode:media.wysiwyg_full" data-entity-embed-display-settings="" data-entity-type="media" data-entity-uuid="' . $data_entity_uuid . '"></drupal-entity>';
        $paragraphs[] = $this->createWysiwygParagraphItem($image_media_embed_string);
      }
      catch (InvalidPluginDefinitionException | PluginNotFoundException | EntityStorageException $e) {
      }
    }
  }

  /**
   * Creates a WYSIWYG para from field_contact_info.
   *
   * @param array $field_contact_info
   *   The field_contact_info variables array.
   * @param array $paragraphs
   *   Paragraphs array with WYSIWYG para added.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createContactWysiwygPara(array $field_contact_info, array &$paragraphs): void {
    foreach ($field_contact_info as $contact) {
      $contact_para = '<p>';
      if (isset($contact["field_contact_info_name"])) {
        $contact_para .= $contact["field_contact_info_name"] . '<br />';
      }
      if (isset($contact["field_street_address_1"])) {
        $contact_para .= $contact["field_street_address_1"] . '<br />';
      }
      if (isset($contact["field_street_address_2"])) {
        $contact_para .= $contact["field_street_address_2"] . '<br />';
      }
      if (isset($contact["field_city"])) {
        $contact_para .= $contact["field_city"] . '<br />';
      }
      if (isset($contact["field_state"])) {
        $contact_para .= $contact["field_state"];
      }
      if (isset($contact["field_zipcode"])) {
        $contact_para .= ', ' . $contact["field_zipcode"] . '<br />';
      }
      else {
        $contact_para .= '<br />';
      }
      if (isset($contact["field_phone"])) {
        $contact_para .=
          '<a href="tel:' . $contact["field_phone"] . '">'
          . $contact["field_phone"] . '</a><br />';
      }
      if (isset($contact["field_fax"])) {
        $contact_para .= '<strong>Fax:</strong> ' . $contact["field_fax"] . '<br />';
      }
      if (isset($contact["field_email_link"])) {
        $contact_para .= '<a href="mailto:' . $contact["field_email_link"] . '">' . $contact["field_email_link"] . '</a></p>';
      }
      if (isset($contact["field_freeform_contact_info"])) {
        $contact_para .= '<p>' . $contact["field_freeform_contact_info"] . '</p>';
      }
      if ($contact_para !== '<p>Bryn Mawr College<br />101 North Merion Avenue<br />Bryn Mawr<br />Pennsylvania, 19010<br /><a href="tel:610-526-5000">610-526-5000</a><br /><a href="mailto:info@brynmawr.edu">info@brynmawr.edu</a></p>') {
        $paragraphs[] = $this->createWysiwygParagraphItem($contact_para);
      }
    }
  }

}
