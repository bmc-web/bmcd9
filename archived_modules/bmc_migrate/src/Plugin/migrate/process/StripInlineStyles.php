<?php

namespace Drupal\bmc_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Removes all inline styling in a WYSIWYG block of text.
 *
 * @MigrateProcessPlugin(
 *   id = "strip_inline_styles"
 * )
 *
 * @code
 *   field_name:
 *     source: title
 *     plugin: strip_inline_styles
 * @endcode
 */
class StripInlineStyles extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (isset($value['value'])) {
      $value = $value['value'];
    }
    $value = str_replace(
    [
      '<div class="clearfix">&nbsp;</div>',
      '<div class="gtx-trans-icon">&nbsp;</div>',
    ],
    '', $value);
    $value = preg_replace('/(<[^>]*) style=("[^"]+"|\'[^\']+\')([^>]*>)/i', '$1$3', $value);
    $value =  str_replace([
      '<p></p>',
      '<p>&nbsp;</p>',
      '<div>&nbsp;</div>',
      '<h4>&nbsp;</h4>',
      '<div></div>',
      "<div>\n</div>",
    ],
      '',
      $value);
    return $value;
  }

}
