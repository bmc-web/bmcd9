<?php

namespace Drupal\bmc_migrate\Plugin\migrate\process;

use Drupal\bmc_migrate\TransformInternalLinks;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Migrates images from an image field.
 *
 * @MigrateProcessPlugin(
 *   id = "process_event_addl_links"
 * )
 *
 * @code
 *   field_name:
 *     source: title
 *     plugin: process_event_addl_links
 * @endcode
 */
class ProcessEventAddlLinks extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!empty($value)) {

      // Add Register Link to Body.
      if (!empty($row->getSourceProperty('tfield_register_link'))) {
        $reg_link = $row->getSourceProperty('tfield_register_link');
        if (isset($reg_link['url'])) {
          $reg_url = (new TransformInternalLinks)->checkCtaLinkUri($reg_link['url']);
          $reg_link_text = '<p><a class="cta--button--primary" href="' . $reg_url . '">' . $reg_link['title'] . '</a></p>';
          $value .= $reg_link_text;
        }

      }

      // Add Related Links to Body.
      if (!empty($row->getSourceProperty('tfield_related_links'))) {
        $links = $row->getSourceProperty('tfield_related_links');
        $links_text = '<h3>Related Links</h3><ul>';
        foreach ($links as $link) {
          if (isset($link['url'])) {
            $url = (new TransformInternalLinks)->checkCtaLinkUri($link['url']);
            $links_text .= '<li><a href="' . $url . '">' . $link['title'] . '</a></li>';
          }
        }
        $links_text .= '</ul>';
        $value .= $links_text;
      }
    }
    return $value;
  }

}
