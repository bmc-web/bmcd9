<?php

namespace Drupal\bmc_migrate\Plugin\migrate\process;

use Drupal\bmc_migrate\CreateCarouselPara;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Creates Office Hours para bundle as the node is being migrated.
 *
 * @MigrateProcessPlugin(
 *   id = "create_office_hours",
 *   handle_multiples = TRUE
 * )
 */
class CreateOfficeHoursPara extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $paragraphs = [];

    if (is_array($value)) {
      foreach ($value as $i) {
        try {
          $paragraphs[] = $this->createOfficeHoursParagraphItem($i);
        }
        catch (EntityStorageException $e) {
        }
      }
    }
    else {
      try {
        $paragraphs[] = $this->createOfficeHoursParagraphItem($value);
      }
      catch (EntityStorageException $e) {
      }
    }

    return $paragraphs;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() : bool {
    return TRUE;
  }

  /**
   * Generates a paragraph item entity.
   *
   * @param string $i
   *   Values from the row being migrated.
   *
   * @return array
   *   Target/revision IDs to pass back to the entity reference field.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createOfficeHoursParagraphItem($i) : array {

    $paragraph = Paragraph::create([
      'type' => 'office_hours',
      'field_p_officehours_text' => [
        'value' => $i['message'],
      ],
      'field_p_officehours_hours' => [
        'value' => $i['time_string'],
      ],
    ]);

    $paragraph->save();

    return [
      'target_id' => $paragraph->id(),
      'target_revision_id' => $paragraph->getRevisionId(),
    ];
  }

}
