<?php

namespace Drupal\bmc_migrate\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node as d7_node;
use Drupal\redirect\Entity\Redirect;

/**
 * Published nodes from the d7 database.
 *
 * @MigrateSource(
 *   id = "bmc_biography_node",
 *   source_module = "node"
 * )
 */
class BmcBiographyNode extends d7_node {

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    $query = parent::query();

    // Only migrate published records.
    $query->condition('n.status', 1);

    // Debug only grab a few records. 2728
    // $query->condition('n.nid', [2346, 2311, 2534, 2350, 2399], 'IN');

    // Get the URL alias.
    $query->leftJoin('url_alias', 'ua', "ua.source = CONCAT('node/', n.nid)");
    $query->addExpression('GROUP_CONCAT(DISTINCT ua.alias)', 'php_alias');

    $query->leftJoin('users', 'u', "u.uid = n.uid");
    $query->addExpression('GROUP_CONCAT(DISTINCT u.name)', 'php_author');

    // Only return one row per node.
    // This is one way of solving "isn't in GROUP BY" issues with < MySQL 5.7.
    // We cannot set ONLY_FULL_GROUP_BY in Webfaction, unfortunately.
    $query->groupBy('n.nid');
    $query->groupBy('n.uid');
    $query->groupBy('n.type');
    $query->groupBy('n.language');
    $query->groupBy('n.status');
    $query->groupBy('n.created');
    $query->groupBy('n.changed');
    $query->groupBy('n.comment');
    $query->groupBy('n.promote');
    $query->groupBy('n.sticky');
    $query->groupBy('n.tnid');
    $query->groupBy('n.translate');
    $query->groupBy('nr.vid');
    $query->groupBy('nr.title');
    $query->groupBy('nr.log');
    $query->groupBy('nr.timestamp');
    $query->groupBy('nr.uid');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = $this->baseFields();
    $fields['body/format'] = $this->t('Format of body');
    $fields['body/value'] = $this->t('Full text of body');
    $fields['body/summary'] = $this->t('Summary of body');
    return $fields;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function prepareRow(Row $row): bool {
    $nid = $row->getSourceProperty('nid');
    // Get the Other fields of Node.
    $this->setNodeOtherFields($row);

    // Get Sidebar Field Collection Data.
    // $this->setSidebarFieldCollection($row);

    // Get Office Hours Field Collection data.
    $this->setOfficeHourFieldCollection($row);

    $this->splitAliases($row);
    $row->setSourceProperty('php_nid', [$row->getSourceProperty('nid')]);
    return parent::prepareRow($row);
  }

  /**
   * Splits out multiple aliases into a primary alias and redirects.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function splitAliases(Row $row): void {
    $aliases = explode(',', $row->getSourceProperty('php_alias'));
    if (count($aliases) > 1) {
      $row->setSourceProperty('php_alias', $aliases[0]);
      /** @var \Drupal\redirect\RedirectRepository $repository */
      $source_url = '/' . $aliases[1];
      $redirect_url = '/' . $aliases[0];
      $repository = \Drupal::service('redirect.repository');
      $redirect_exists = $repository->findBySourcePath($source_url);
      if (!isset($redirect_exists)) {
        Redirect::create([
          'redirect_source' => $source_url,
          'redirect_redirect' => $redirect_url,
          'status_code' => 301,
        ])->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function setNodeOtherFields(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        fld.body_value,
        fld.body_summary,
        fld.body_format,
        ftn.field_title_note_value,
        fde.field_degrees_education_value,
        fde.field_degrees_education_summary,
        fde.field_degrees_education_format,
        fri.field_research_interests_value,
        fri.field_research_interests_summary,
        fri.field_research_interests_format,
        fm.uid,
        fm.filename,
        fm.uri,
        fm.filemime,
        fm.filesize,
        fii.field_bio_image_fid,
        fii.field_bio_image_alt,
        fii.field_bio_image_title,
        fii.field_bio_image_width,
        fii.field_bio_image_height
      FROM
        node n
      LEFT JOIN
        {field_data_body} fld
      ON n.nid = fld.entity_id
      LEFT JOIN
        {field_data_field_title_note} ftn
      ON
        n.nid = ftn.entity_id
      LEFT JOIN
        {field_data_field_degrees_education} fde
      ON
        n.nid = fde.entity_id
      LEFT JOIN
        {field_data_field_research_interests} fri
      ON
        n.nid = fri.entity_id
      LEFT JOIN
        {field_data_field_bio_image} fii
      ON
        n.nid = fii.entity_id
      LEFT JOIN
        {file_managed} fm
      ON
        fii.field_bio_image_fid = fm.fid
      WHERE
        n.nid = :nid', [':nid' => $nid]);

    foreach ($result as $record) {
      $row->setSourceProperty('tfield_degrees_education',
        [
          'value' => $record->field_degrees_education_value,
          'summary' => $record->field_degrees_education_summary,
          'format' => $record->field_degrees_education_format
        ]
      );
      $row->setSourceProperty('tfield_research_interests',
        [
          'value' => $record->field_research_interests_value,
          'summary' => $record->field_research_interests_summary,
          'format' => $record->field_research_interests_format
        ]
      );
      $bio_image = [
        'uid' => $record->uid,
        'filename' => $record->filename,
        'uri' => $record->uri,
        'filemime' => $record->filemime,
        'filesize' => $record->filesize,
        'target_id' => $record->field_bio_image_fid,
        'alt' => $record->field_bio_image_alt,
        'title' => $record->field_bio_image_title,
        'width' => $record->field_bio_image_width,
        'height' => $record->field_bio_image_height,
      ];
      $row->setSourceProperty('tfield_bio_image', $bio_image);
      $row->setSourceProperty('tfield_title_note', $record->field_title_note_value);
      $row->setSourceProperty('tbody_value', $record->body_value);
      $row->setSourceProperty('tbody_summary', $record->body_summary);
      $row->setSourceProperty('tbody_format', $record->body_format);
    }
    $website = $this->getDatabase()->query('
      SELECT
        field_website_url,
        field_website_title,
        field_website_attributes
      FROM
        {field_data_field_website}
      WHERE
        entity_id = :nid
      ORDER BY delta', [':nid' => $nid]);
    $ctas = [];
    foreach ($website as $cta) {
      $ctas[] = [
        'url' => $cta->field_website_url,
        'title' => $cta->field_website_title,
        'attributes' => $cta->field_website_attributes
      ];
    }

    $row->setSourceProperty('tfield_website', $ctas);
  }

  /**
   * {@inheritdoc}
   */
  protected function setSidebarFieldCollection(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        fs.field_sidebar_value,
        fsml.field_social_media_links_value,
        fcst.field_custom_sidebar_text_value,
        fcst.field_custom_sidebar_text_format
      FROM
        {field_data_field_sidebar} fs
      LEFT JOIN
        {field_data_field_social_media_links} fsml
      ON
        fs.field_sidebar_value = fsml.entity_id
      LEFT JOIN
        {field_data_field_custom_sidebar_text} fcst
      ON
        fs.field_sidebar_value = fcst.entity_id
      WHERE
        fs.entity_id = :nid', [':nid' => $nid]);
    foreach ($result as $record) {
      $field_sidebar['field_custom_sidebar_text'] = $record->field_custom_sidebar_text_value;
      $field_sidebar['field_custom_sidebar_text_format'] = $record->field_custom_sidebar_text_format;
      $field_sidebar['field_social_media_links_value'] = $record->field_social_media_links_value;
      // Fetch Sidebar Cta Data.
      $sidebar_cta = $this->getDatabase()->query('
        SELECT
          field_cta_url,
          field_cta_title,
          field_cta_attributes
        FROM
          {field_data_field_cta}
        WHERE
          entity_id = :entity_id
        ORDER BY delta', [':entity_id' => $record->field_sidebar_value]);
      $ctas = [];
      foreach ($sidebar_cta as $cta) {
        $ctas[] = [
          'url' => $cta->field_cta_url,
          'title' => $cta->field_cta_title,
          'attributes' => $cta->field_cta_attributes
        ];
      }
      $field_sidebar['field_cta'] = $ctas;
      // Fetch Sidebar Images.
      $sidebar_images = $this->getDatabase()->query('
        SELECT
          fm.uid,
          fm.filename,
          fm.uri,
          fm.filemime,
          fm.filesize,
          fi.field_image_fid,
          fi.field_image_alt,
          fi.field_image_title,
          fi.field_image_width,
          fi.field_image_height
        FROM
          {field_data_field_image} fi
        LEFT JOIN
          {file_managed} fm
        ON
          fi.field_image_fid = fm.fid
        WHERE
          fi.entity_id = :nid', [':nid' => $record->field_sidebar_value]);
      $images = [];
      foreach ($sidebar_images as $img) {
        $images[] = [
          'uid' => $img->uid,
          'filename' => $img->filename,
          'uri' => $img->uri,
          'filemime' => $img->filemime,
          'filesize' => $img->filesize,
          'target_id' => $img->field_files_fid,
          'alt' => $img->field_image_alt,
          'title' => $img->field_image_title,
          'width' => $img->field_image_width,
          'height' => $img->field_image_height,
        ];
      }
      $field_sidebar['field_image'] = $images;
      // Get Social Media Links from Field Collection.
      $social_media_links = $this->getSocialMediaLinksFieldCollection($row, $record->field_social_media_links_value);
      $field_sidebar['field_social_media_links'] = $social_media_links;
      // Get Contact Info from Field collection.
      $contact_info = $this->getContactInfoFieldCollection($row, $record->field_sidebar_value);
      $field_sidebar['field_contact_info'] = $contact_info;
      $row->setSourceProperty('tfield_sidebar', $field_sidebar);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getSocialMediaLinksFieldCollection(Row $row, $entity_id): array {
    $social_links = [];
    $result = $this->getDatabase()->query('
      SELECT
        fft.field_follow_text_value,
        ffu.field_facebook_url_url,
        ffu.field_facebook_url_title,
        ffu.field_facebook_url_attributes,
        ftu.field_twitter_url_url,
        ftu.field_twitter_url_title,
        ftu.field_twitter_url_attributes,
        flu.field_linkedin_url_url,
        flu.field_linkedin_url_title,
        flu.field_linkedin_url_attributes,
        fpu.field_pinterest_url_url,
        fpu.field_pinterest_url_title,
        fpu.field_pinterest_url_attributes,
        fiu.field_instagram_url_url,
        fiu.field_instagram_url_title,
        fiu.field_instagram_url_attributes,
        ftum.field_tumblr_url_url,
        ftum.field_tumblr_url_title,
        ftum.field_tumblr_url_attributes,
        fyu.field_youtube_url_url,
        fyu.field_youtube_url_title,
        fyu.field_youtube_url_attributes
      FROM
        {field_data_field_social_media_links} fsml
      LEFT JOIN
        {field_data_field_follow_text} fft
      ON
        fsml.field_social_media_links_value = fft.entity_id
      LEFT JOIN
        {field_data_field_facebook_url} ffu
      ON
        fsml.field_social_media_links_value = ffu.entity_id
      LEFT JOIN
        {field_data_field_twitter_url} ftu
      ON
        fsml.field_social_media_links_value = ftu.entity_id
      LEFT JOIN
        {field_data_field_linkedin_url} flu
      ON
        fsml.field_social_media_links_value = flu.entity_id
      LEFT JOIN
        {field_data_field_pinterest_url} fpu
      ON
        fsml.field_social_media_links_value = fpu.entity_id
      LEFT JOIN
        {field_data_field_instagram_url} fiu
      ON
        fsml.field_social_media_links_value = fiu.entity_id
      LEFT JOIN
        {field_data_field_tumblr_url} ftum
      ON
        fsml.field_social_media_links_value = ftum.entity_id
      LEFT JOIN
        {field_data_field_youtube_url} fyu
      ON
        fsml.field_social_media_links_value = fyu.entity_id
      WHERE
        fsml.field_social_media_links_value = :entity_id', [':entity_id' => $entity_id]);
    foreach ($result as $record) {
      $social_links['field_follow_text_value'] = $record->field_follow_text_value;
      $social_links['field_facebook_url_url'] = $record->field_facebook_url_url;
      $social_links['field_facebook_url_title'] = $record->field_facebook_url_title;
      $social_links['field_facebook_url_attributes'] = $record->field_facebook_url_attributes;
      $social_links['field_twitter_url_url'] = $record->field_twitter_url_url;
      $social_links['field_twitter_url_title'] = $record->field_twitter_url_title;
      $social_links['field_twitter_url_attributes'] = $record->field_twitter_url_attributes;
      $social_links['field_linkedin_url_url'] = $record->field_linkedin_url_url;
      $social_links['field_linkedin_url_title'] = $record->field_linkedin_url_title;
      $social_links['field_linkedin_url_attributes'] = $record->field_linkedin_url_attributes;
      $social_links['field_pinterest_url_url'] = $record->field_pinterest_url_url;
      $social_links['field_pinterest_url_title'] = $record->field_pinterest_url_title;
      $social_links['field_pinterest_url_attributes'] = $record->field_pinterest_url_attributes;
      $social_links['field_instagram_url_url'] = $record->field_instagram_url_url;
      $social_links['field_instagram_url_title'] = $record->field_instagram_url_title;
      $social_links['field_instagram_url_attributes'] = $record->field_instagram_url_attributes;
      $social_links['field_tumblr_url_url'] = $record->field_tumblr_url_url;
      $social_links['field_tumblr_url_title'] = $record->field_tumblr_url_title;
      $social_links['field_tumblr_url_attributes'] = $record->field_tumblr_url_attributes;
      $social_links['field_youtube_url_url'] = $record->field_youtube_url_url;
      $social_links['field_youtube_url_title'] = $record->field_youtube_url_title;
      $social_links['field_youtube_url_attributes'] = $record->field_youtube_url_attributes;
    }

    $result = $this->getDatabase()->query('
      SELECT
        fasl.field_active_social_links_value
      FROM
        {field_data_field_active_social_links} fasl
      WHERE
        fasl.entity_id = :entity_id', [':entity_id' => $entity_id]);
    foreach ($result as $record) {
      $social_links['field_active_social_links'][] = $record->field_active_social_links_value;
    }
    return $social_links;
  }

  /**
   * {@inheritdoc}
   */
  protected function getContactInfoFieldCollection(Row $row, $entity_id): array {
    $contact_info_list = [];
    $result = $this->getDatabase()->query('
      SELECT
        fci.field_contact_info_value,
        fcin.field_contact_info_name_value,
        fsa1.field_street_address_1_value,
        fsa2.field_street_address_2_value,
        fc.field_city_value,
        td.name as state,
        fz.field_zipcode_value,
        fp.field_phone_value,
        ff.field_fax_value,
        fel.field_email_link_email,
        fcinfo.field_freeform_contact_info_value
      FROM
        {field_data_field_contact_info} fci
      LEFT JOIN
        {field_data_field_contact_info_name} fcin
      ON
        fci.field_contact_info_value = fcin.entity_id
      LEFT JOIN
        {field_data_field_street_address_1} fsa1
      ON
        fci.field_contact_info_value = fsa1.entity_id
      LEFT JOIN
        {field_data_field_street_address_2} fsa2
      ON
        fci.field_contact_info_value = fsa2.entity_id
      LEFT JOIN
        {field_data_field_city} fc
      ON
        fci.field_contact_info_value = fc.entity_id
      LEFT JOIN
        {field_data_field_contact_state} fcs
      ON
        fci.field_contact_info_value = fcs.entity_id
      LEFT JOIN
        {taxonomy_term_data} td
      ON
        fcs.field_contact_state_tid = td.tid
      LEFT JOIN
        {field_data_field_zipcode} fz
      ON
        fci.field_contact_info_value = fz.entity_id
      LEFT JOIN
        {field_data_field_phone} fp
      ON
        fci.field_contact_info_value = fp.entity_id
      LEFT JOIN
        {field_data_field_fax} ff
      ON
        fci.field_contact_info_value = ff.entity_id
      LEFT JOIN
        {field_data_field_email_link} fel
      ON
        fci.field_contact_info_value = fel.entity_id
      LEFT JOIN
        {field_data_field_freeform_contact_info} fcinfo
      ON
        fci.field_contact_info_value = fcinfo.entity_id
      WHERE
        fci.entity_id = :entity_id', [':entity_id' => $entity_id]);
    foreach ($result as $record) {
      $contact_info['field_contact_info_name'] = $record->field_contact_info_name_value;
      $contact_info['field_street_address_1'] = $record->field_street_address_1_value;
      $contact_info['field_street_address_2'] = $record->field_street_address_2_value;
      $contact_info['field_city'] = $record->field_city_value;
      $contact_info['field_state'] = $record->state;
      $contact_info['field_zipcode'] = $record->field_zipcode_value;
      $contact_info['field_phone'] = $record->field_phone_value;
      $contact_info['field_fax'] = $record->field_fax_value;
      $contact_info['field_email_link'] = $record->field_email_link_email;
      $contact_info['field_freeform_contact_info'] = $record->field_freeform_contact_info_value;
      $confact_info_list[] = $contact_info;
    }

    return $confact_info_list;
  }

  /**
   * {@inheritdoc}
   */
  protected function setOfficeHourFieldCollection(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        fc.field_office_hours_collection_value,
        fcm.field_custom_message_value
      FROM
        {field_data_field_office_hours_collection} fc
      LEFT JOIN
        {field_data_field_custom_message} fcm
      ON
        fc.field_office_hours_collection_value = fcm.entity_id
      WHERE
        fc.entity_id = :nid', [':nid' => $nid]);

    $field_office_hour_list = [];
    foreach ($result as $record) {
      $message = $record->field_custom_message_value;

      $timelist = $this->getDatabase()->query('
        SELECT
          *
        FROM
          {field_data_field_start_time_office}
        WHERE
          entity_id = :entity_id
        ORDER BY delta', [':entity_id' => $record->field_office_hours_collection_value]);

      $office_hours = [];
      foreach ($timelist as $t) {
        // We only want one custom message per bundle instance, so set to empty first.
        $office_hours[] = [
          'time_string' => $this->getTimeAsText($t),
          'message' => '',
        ];
      }
      // Add the custom message to last instance.
      $last_key = array_key_last($office_hours);
      $office_hours[$last_key]['message'] = $message;
    }
    $row->setSourceProperty('tfield_office_hours_collection', $office_hours);
  }

  /**
   * {@inheritdoc}
   */
  protected function getTimeAsText($time) {

    $text_string = '';
    $start_time = '';
    $end_time = '';
    $days = [];

    if ($time->field_start_time_office_mon) {
      $days[] = 'Monday';
    }
    if ($time->field_start_time_office_tue) {
      $days[] = 'Tuesday';
    }
    if ($time->field_start_time_office_wed) {
      $days[] = 'Wednesday';
    }
    if ($time->field_start_time_office_thu) {
      $days[] = 'Thursday';
    }
    if ($time->field_start_time_office_fri) {
      $days[] = 'Friday';
    }
    if ($time->field_start_time_office_sat) {
      $days[] = 'Saturday';
    }
    if ($time->field_start_time_office_sun) {
      $days[] = 'Sunday';
    }

    if ($time->field_start_time_office_value) {
      $start_time = gmdate('g:ia', $time->field_start_time_office_value);
    }

    if ($time->field_start_time_office_value2) {
      $end_time = gmdate('g:ia', $time->field_start_time_office_value2);
    }

    if (!empty($end_time)) {
      $start_time .= '-';
    }

    $text_string = implode(', ', $days) . ' BmcBiographyNode.php' . $start_time . $end_time;

    return $text_string;

  }

}
