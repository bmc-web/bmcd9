<?php

namespace Drupal\bmc_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node as d7_node;

/**
 * Published nodes from the d7 database.
 *
 * @MigrateSource(
 *   id = "bmc_page_node",
 *   source_module = "node"
 * )
 */
class BmcPageNode extends d7_node {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    // Only migrate published records.
    $query->condition('n.status', 1);

    // Get the URL alias.
    $query->leftJoin('url_alias', 'ua', "ua.source = CONCAT('node/', n.nid)");
    $query->addExpression('GROUP_CONCAT(DISTINCT ua.alias)', 'php_alias');

    $query->leftJoin('users', 'u', "u.uid = n.uid");
    $query->addExpression('GROUP_CONCAT(DISTINCT u.name)', 'php_author');

    // Only return one row per node.
    // This is one way of solving "isn't in GROUP BY" issues with < MySQL 5.7.
    // We cannot set ONLY_FULL_GROUP_BY in Webfaction, unfortunately.
    $query->groupBy('n.nid');
    $query->groupBy('n.uid');
    $query->groupBy('n.type');
    $query->groupBy('n.language');
    $query->groupBy('n.status');
    $query->groupBy('n.created');
    $query->groupBy('n.changed');
    $query->groupBy('n.comment');
    $query->groupBy('n.promote');
    $query->groupBy('n.sticky');
    $query->groupBy('n.tnid');
    $query->groupBy('n.translate');
    $query->groupBy('nr.vid');
    $query->groupBy('nr.title');
    $query->groupBy('nr.log');
    $query->groupBy('nr.timestamp');
    $query->groupBy('nr.uid');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = $this->baseFields();
    $fields['body/format'] = $this->t('Format of body');
    $fields['body/value'] = $this->t('Full text of body');
    $fields['body/summary'] = $this->t('Summary of body');
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $nid = $row->getSourceProperty('nid');

    // Get the Gallery Images.
    $this->setGalleryImages($row);
    // Get the Other fields of Node.
    $this->setNodeOtherFields($row);
    // Get Carousel Field Collection data.
    $use_syndicated_slides = $row->getSourceProperty('field_use_syndicated_slides');
    if ($use_syndicated_slides) {
      $this->setSyndicatedCarousel($row);
    }
    else {
      $this->setCarouselFieldCollection($row);
    }

    // Get Featured Box Field collection data.
    $this->setFeaturedBoxFieldCollection($row);
    // Get Text Linkset Field Collection Data.
    $this->setTextLinkFieldCollection($row);

    // Get Image Link Field Collection Data.
    $this->setImageLinkFieldCollection($row);
    // Get Sidebar Field Collection Data.
    $this->setSidebarFieldCollection($row);

    $this->splitAliases($row);
    $row->setSourceProperty('php_nid', [$row->getSourceProperty('nid')]);
    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  protected function splitAliases(Row $row): void {
    $aliases = explode(',', $row->getSourceProperty('php_alias'));
    if (count($aliases) > 1) {
      $row->setSourceProperty('php_alias', $aliases[0]);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function setNodeOtherFields(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        ttd.name,
        fiit.field_intro_image_type_value,
        ft.field_tagline_value,
        ft.field_tagline_format,
        fi.field_intro_value,
        fi.field_intro_summary,
        fi.field_intro_format,
        fss.field_use_syndicated_slides_value,
        fct.field_carousel_type_value,
        fld.body_value,
        fld.body_summary,
        fld.body_format,
        fs.field_spotlight_target_id,
        fab.field_article_block_target_id,
        ff.field_fact_target_id,
        fe.field_event_target_id,
        fgc.field_image_gallery_caption_value,
        fm.uid,
        fm.filename,
        fm.uri,
        fm.filemime,
        fm.filesize,
        fii.field_intro_image_fid,
        fii.field_intro_image_alt,
        fii.field_intro_image_title,
        fii.field_intro_image_width,
        fii.field_intro_image_height
      FROM
        node n
      LEFT JOIN
        {field_data_field_intro_image_type} fiit
      ON
        n.nid = fiit.entity_id
      LEFT JOIN
        {field_data_field_tagline} ft
      ON
        n.nid = ft.entity_id
      LEFT JOIN
        {field_data_field_intro} fi
      ON
        n.nid = fi.entity_id
      LEFT JOIN
        {field_data_field_use_syndicated_slides} fss
      ON n.nid = fss.entity_id
      LEFT JOIN
        {field_data_field_carousel_type} fct
      ON n.nid = fct.entity_id
      LEFT JOIN
        {field_data_body} fld
      ON n.nid = fld.entity_id
      LEFT JOIN
        {field_data_field_spotlight} fs
      ON n.nid = fs.entity_id
      LEFT JOIN
        {field_data_field_article_block} fab
      ON n.nid = fab.entity_id
      LEFT JOIN
        {field_data_field_fact} ff
      ON n.nid = ff.entity_id
      LEFT JOIN
        {field_data_field_event} fe
      ON n.nid = fe.entity_id
      LEFT JOIN
        {field_data_field_image_gallery_caption} fgc
      ON n.nid = fgc.entity_id
      LEFT JOIN
        {field_data_field_intro_image} fii
      ON
        n.nid = fii.entity_id
      LEFT JOIN
        {file_managed} fm
      ON
        fii.field_intro_image_fid = fm.fid
      LEFT JOIN
        {field_data_field_department} fd
      ON
        n.nid = fd.entity_id
      LEFT JOIN
        {taxonomy_term_data} ttd
      ON
        fd.field_department_tid = ttd.tid
      WHERE
        n.nid = :nid', [':nid' => $nid]);

    foreach ($result as $record) {
      $row->setSourceProperty('tfield_department', $record->name);
      $row->setSourceProperty('tfield_intro_image_type', $record->field_intro_image_type_value);
      $row->setSourceProperty('tfield_tagline',
        [
          'value' => $record->field_tagline_value,
          'format' => $record->field_tagline_format,
        ]
      );
      $row->setSourceProperty('tfield_intro',
        [
          'value' => $record->field_intro_value,
          'summary' => $record->field_intro_summary,
          'format' => $record->field_intro_format,
        ]
      );
      $intro_image = [
        'uid' => $record->uid,
        'filename' => $record->filename,
        'uri' => $record->uri,
        'filemime' => $record->filemime,
        'filesize' => $record->filesize,
        'target_id' => $record->field_intro_image_fid,
        'alt' => $record->field_intro_image_alt,
        'title' => $record->field_intro_image_title,
        'width' => $record->field_intro_image_width,
        'height' => $record->field_intro_image_height,
      ];
      $row->setSourceProperty('tfield_intro_image', $intro_image);
      $row->setSourceProperty('tfield_spotlight', $record->field_spotlight_target_id);
      $row->setSourceProperty('tfield_article_block', $record->field_article_block_target_id);
      $row->setSourceProperty('tfield_fact', $record->field_fact_target_id);
      $row->setSourceProperty('tfield_event', $record->field_event_target_id);
      $row->setSourceProperty('tfield_image_gallery_caption', $record->field_image_gallery_caption_value);
      $row->setSourceProperty('tfield_use_syndicated_slides', $record->field_use_syndicated_slides_value);
      $row->setSourceProperty('body_value', $record->body_value);
      $row->setSourceProperty('body_summary', $record->body_summary);
      $row->setSourceProperty('body_format', $record->body_format);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function setGalleryImages(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        fm.uid,
        fm.filename,
        fm.uri,
        fm.filemime,
        fm.filesize,
        fig.field_image_gallery_fid,
        fig.field_image_gallery_alt,
        fig.field_image_gallery_title,
        fig.field_image_gallery_width,
        fig.field_image_gallery_height
      FROM
        {field_data_field_image_gallery} fig
      LEFT JOIN
        {file_managed} fm
      ON
        fig.field_image_gallery_fid = fm.fid
      WHERE
        fig.entity_id = :nid', [':nid' => $nid]);
    $images = [];
    foreach ($result as $record) {
      $images[] = [
        'uid' => $record->uid,
        'filename' => $record->filename,
        'uri' => $record->uri,
        'filemime' => $record->filemime,
        'filesize' => $record->filesize,
        'target_id' => $record->field_files_fid,
        'alt' => $record->field_image_gallery_alt,
        'title' => $record->field_image_gallery_title,
        'width' => $record->field_image_gallery_width,
        'height' => $record->field_image_gallery_height,
      ];
    }
    $row->setSourceProperty('tfield_image_gallery', $images);
  }

  /**
   * {@inheritdoc}
   */
  protected function setSyndicatedCarousel(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        fsc.field_syndicated_carousel_value,
        fp.field_premium_value
      FROM
        {field_data_field_syndicated_carousel} fsc
      LEFT JOIN
        {field_data_field_premium} fp
      ON
        fc.field_syndicated_carousel_value = fp.entity_id
      WHERE
        fsc.entity_id = :nid', [':nid' => $nid]);
    $field_syndicated_carousel = [];
    foreach ($result as $record) {
      $field_syndicated_carousel['field_premium'] = $record->field_premium_value;
      // Get the Deparment Term names.
      $department = $this->getDatabase()->query('
        SELECT
          GROUP_CONCAT(td.name) as tnames
        FROM
          {field_data_field_department_multi} fld
        LEFT JOIN
          {taxonomy_term_data} td
        ON
          fld.field_department_multi_tid = td.tid
        WHERE
          fld.entity_id = :nid', [':nid' => $nid]);
      foreach ($department as $dep) {
        if (!is_null($dep->tnames)) {
          $field_syndicated_carousel['field_department_multi'] = explode(',', $dep->tnames);
        }
      }
      // Get topic term names.
      $topics = $this->getDatabase()->query('
        SELECT
          GROUP_CONCAT(td.name) as tnames
        FROM
          {field_data_field_article_terms} fld
        LEFT JOIN
          {taxonomy_term_data} td
        ON
          fld.field_article_terms_tid = td.tid
        WHERE
          fld.entity_id = :nid', [':nid' => $nid]);
      foreach ($topics as $topic) {
        if (!is_null($topic->tnames)) {
          $field_syndicated_carousel['field_article_terms'] = explode(',', $topic->tnames);
        }
      }
    }
    $row->setSourceProperty('tfield_syndicated_carousel', $field_syndicated_carousel);
  }

  /**
   * {@inheritdoc}
   */
  protected function setCarouselFieldCollection(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        fc.field_carousel_value,
        fst.field_slide_title_value,
        fsh.field_slide_header_value,
        fsb.field_slide_body_value,
        fsl.field_slide_share_link_url,
        fsl.field_slide_share_link_title,
        fsl.field_slide_share_link_attributes,
        fsi.field_slide_image_fid,
        fsi.field_slide_image_alt,
        fsi.field_slide_image_title,
        fsi.field_slide_image_width,
        fsi.field_slide_image_height,
        fm.uid,
        fm.filename,
        fm.uri,
        fm.filemime,
        fm.filesize
      FROM
        {field_data_field_carousel} fc
      LEFT JOIN
        {field_data_field_slide_image} fsi
      ON
        fc.field_carousel_value = fsi.entity_id
      LEFT JOIN
        {field_data_field_slide_title} fst
      ON
        fc.field_carousel_value = fst.entity_id
      LEFT JOIN
        {field_data_field_slide_header} fsh
      ON
        fc.field_carousel_value = fsh.entity_id
      LEFT JOIN
        {field_data_field_slide_body} fsb
      ON
        fc.field_carousel_value = fsb.entity_id
      LEFT JOIN
        {field_data_field_slide_share_link} fsl
      ON
        fc.field_carousel_value = fsl.entity_id
      LEFT JOIN
        {file_managed} fm
      ON
        fsi.field_slide_image_fid = fm.fid
      WHERE
        fc.entity_id = :nid', [':nid' => $nid]);
    $field_carousel_list = [];
    foreach ($result as $record) {
      $field_carousel['field_slide_title'] = $record->field_slide_title_value;
      $field_carousel['field_slide_header'] = $record->field_slide_header_value;
      $field_carousel['field_slide_body'] = $record->field_slide_body_value;
      $field_carousel['field_slide_body'] = $record->field_slide_body_value;
      $field_carousel['field_slide_share_link'] = [
        'url' => $record->field_slide_share_link_url,
        'title' => $record->field_slide_share_link_title,
        'attributes' => $record->field_slide_share_link_attributes,
      ];
      $field_carousel['field_slide_image'] = [
        'uid' => $record->uid,
        'filename' => $record->filename,
        'uri' => $record->uri,
        'filemime' => $record->filemime,
        'filesize' => $record->filesize,
        'target_id' => $record->field_files_fid,
        'alt' => $record->field_intro_image_alt,
        'title' => $record->field_intro_image_title,
        'width' => $record->field_intro_image_width,
        'height' => $record->field_intro_image_height,
      ];
      $carusel_cta = $this->getDatabase()->query('
        SELECT
          field_cta_url,
          field_cta_title,
          field_cta_attributes
        FROM
          {field_data_field_cta}
        WHERE
          entity_id = :entity_id
        ORDER BY delta', [':entity_id' => $record->field_carousel_value]);
      $ctas = [];
      foreach ($carusel_cta as $cta) {
        $ctas[] = [
          'url' => $cta->field_cta_url,
          'title' => $cta->field_cta_title,
          'attributes' => $cta->field_cta_attributes,
        ];
      }
      $field_carousel['ctas'] = $ctas;
      $field_carousel_list[] = $field_carousel;
    }
    $row->setSourceProperty('tfield_carousel', $field_carousel_list);
  }

  /**
   * {@inheritdoc}
   */
  protected function setFeaturedBoxFieldCollection(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        fc.field_featured_content_block_value,
        flfi.field_landing_featured_intro_value,
        ftt.field_teaser_text_value,
        fct.field_column_type_value,
        flpi.field_landing_page_image_fid,
        flpi.field_landing_page_image_alt,
        flpi.field_landing_page_image_title,
        flpi.field_landing_page_image_width,
        flpi.field_landing_page_image_height,
        fm.uid,
        fm.filename,
        fm.uri,
        fm.filemime,
        fm.filesize
      FROM
        {field_data_field_featured_content_block} fc
      LEFT JOIN
        {field_data_field_landing_page_image} flpi
      ON
        fc.field_featured_content_block_value = flpi.entity_id
      LEFT JOIN
        {field_data_field_landing_featured_intro} flfi
      ON
        fc.field_featured_content_block_value = flfi.entity_id
      LEFT JOIN
        {field_data_field_teaser_text} ftt
      ON
        fc.field_featured_content_block_value = ftt.entity_id
      LEFT JOIN
        {field_data_field_column_type} fct
      ON
        fc.field_featured_content_block_value = fct.entity_id
      LEFT JOIN
        {file_managed} fm
      ON
        flpi.field_landing_page_image_fid = fm.fid
      WHERE
        fc.entity_id = :nid', [':nid' => $nid]);
    $field_featured_content_block_list = [];
    foreach ($result as $record) {
      $field_featured_content_block['field_landing_featured_intro'] = $record->field_landing_featured_intro_value;
      $field_featured_content_block['field_teaser_text'] = $record->field_teaser_text_value;
      $field_featured_content_block['field_column_type'] = $record->field_column_type_value;

      $field_featured_content_block['field_landing_page_image'] = [
        'uid' => $record->uid,
        'filename' => $record->filename,
        'uri' => $record->uri,
        'filemime' => $record->filemime,
        'filesize' => $record->filesize,
        'target_id' => $record->field_landing_page_image_fid,
        'alt' => $record->field_landing_page_image_alt,
        'title' => $record->field_landing_page_image_title,
        'width' => $record->field_landing_page_image_width,
        'height' => $record->field_landing_page_image_height,
      ];
      $featurebox_cta = $this->getDatabase()->query('
        SELECT
          field_cta_url,
          field_cta_title,
          field_cta_attributes
        FROM
          {field_data_field_cta}
        WHERE
          entity_id = :entity_id
        ORDER BY delta', [':entity_id' => $record->field_featured_content_block_value]);
      $ctas = [];
      foreach ($featurebox_cta as $cta) {
        $ctas[] = [
          'url' => $cta->field_cta_url,
          'title' => $cta->field_cta_title,
          'attributes' => $cta->field_cta_attributes,
        ];
      }
      $field_featured_content_block['ctas'] = $ctas;
      $field_featured_content_block_list[] = $field_featured_content_block;
    }
    $row->setSourceProperty('tfield_featured_content_block', $field_featured_content_block_list);
  }

  /**
   * {@inheritdoc}
   */
  protected function setTextLinkFieldCollection(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        fil.field_info_links_value,
        fit.field_info_text_value,
        flt.field_link_text_value
      FROM
        {field_data_field_info_links} fil
      LEFT JOIN
        {field_data_field_info_text} fit
      ON
        fil.field_info_links_value = fit.entity_id
      LEFT JOIN
        {field_data_field_link_text} flt
      ON
        fil.field_info_links_value = flt.entity_id
      WHERE
        fil.entity_id = :nid', [':nid' => $nid]);
    $field_info_link_list = [];
    foreach ($result as $record) {
      $field_info_link['field_info_text'] = $record->field_info_text_value;
      $field_info_link['field_link_text'] = $record->field_link_text_value;

      $info_link_cta = $this->getDatabase()->query('
        SELECT
          field_cta_url,
          field_cta_title,
          field_cta_attributes
        FROM
          {field_data_field_cta}
        WHERE
          entity_id = :entity_id
        ORDER BY delta', [':entity_id' => $record->field_info_links_value]);
      $ctas = [];
      foreach ($info_link_cta as $cta) {
        $ctas[] = [
          'url' => $cta->field_cta_url,
          'title' => $cta->field_cta_title,
          'attributes' => $cta->field_cta_attributes,
        ];
      }
      $field_info_link['ctas'] = $ctas;
      $field_info_link_list[] = $field_info_link;
    }
    $row->setSourceProperty('tfield_info_link', $field_info_link_list);
  }

  /**
   * {@inheritdoc}
   */
  protected function setImageLinkFieldCollection(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        fil.field_image_links_value,
        fit.field_info_text_value,
        flt.field_link_text_value,
        flpi.field_landing_page_image_fid,
        flpi.field_landing_page_image_alt,
        flpi.field_landing_page_image_title,
        flpi.field_landing_page_image_width,
        flpi.field_landing_page_image_height,
        fm.uid,
        fm.filename,
        fm.uri,
        fm.filemime,
        fm.filesize
      FROM
        {field_data_field_image_links} fil
      LEFT JOIN
        {field_data_field_landing_page_image} flpi
      ON
        fil.field_image_links_value = flpi.entity_id
      LEFT JOIN
        {field_data_field_info_text} fit
      ON
        fil.field_image_links_value = fit.entity_id
      LEFT JOIN
        {field_data_field_link_text} flt
      ON
        fil.field_image_links_value = flt.entity_id
      LEFT JOIN
        {file_managed} fm
      ON
        flpi.field_landing_page_image_fid = fm.fid
      WHERE
        fil.entity_id = :nid', [':nid' => $nid]);
    $field_image_link_list = [];
    foreach ($result as $record) {
      $field_image_link['field_info_text'] = $record->field_info_text_value;
      $field_image_link['field_link_text'] = $record->field_link_text_value;
      $field_image_link['field_landing_page_image'] = [
        'uid' => $record->uid,
        'filename' => $record->filename,
        'uri' => $record->uri,
        'filemime' => $record->filemime,
        'filesize' => $record->filesize,
        'target_id' => $record->field_landing_page_image_fid,
        'alt' => $record->field_landing_page_image_alt,
        'title' => $record->field_landing_page_image_title,
        'width' => $record->field_landing_page_image_width,
        'height' => $record->field_landing_page_image_height,
      ];
      $info_link_cta = $this->getDatabase()->query('
        SELECT
          field_cta_url,
          field_cta_title,
          field_cta_attributes
        FROM
          {field_data_field_cta}
        WHERE
          entity_id = :entity_id
        ORDER BY delta', [':entity_id' => $record->field_image_links_value]);
      $ctas = [];
      foreach ($info_link_cta as $cta) {
        $ctas[] = [
          'url' => $cta->field_cta_url,
          'title' => $cta->field_cta_title,
          'attributes' => $cta->field_cta_attributes,
        ];
      }
      $field_image_link['ctas'] = $ctas;
      $field_image_link_list[] = $field_image_link;
    }
    $row->setSourceProperty('tfield_image_link', $field_image_link_list);
  }

  /**
   * {@inheritdoc}
   */
  protected function setSidebarFieldCollection(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        fs.field_sidebar_value,
        fsml.field_social_media_links_value,
        fcst.field_custom_sidebar_text_value,
        fcst.field_custom_sidebar_text_format
      FROM
        {field_data_field_sidebar} fs
      LEFT JOIN
        {field_data_field_social_media_links} fsml
      ON
        fs.field_sidebar_value = fsml.entity_id
      LEFT JOIN
        {field_data_field_custom_sidebar_text} fcst
      ON
        fs.field_sidebar_value = fcst.entity_id
      WHERE
        fs.entity_id = :nid', [':nid' => $nid]);
    foreach ($result as $record) {
      $field_sidebar['field_custom_sidebar_text'] = $record->field_custom_sidebar_text_value;
      $field_sidebar['field_custom_sidebar_text_format'] = $record->field_custom_sidebar_text_format;
      $field_sidebar['field_social_media_links_value'] = $record->field_social_media_links_value;
      // Fetch Sidebar Cta Data.
      $sidebar_cta = $this->getDatabase()->query('
        SELECT
          field_cta_url,
          field_cta_title,
          field_cta_attributes
        FROM
          {field_data_field_cta}
        WHERE
          entity_id = :entity_id
        ORDER BY delta', [':entity_id' => $record->field_sidebar_value]);
      $ctas = [];
      foreach ($sidebar_cta as $cta) {
        $ctas[] = [
          'url' => $cta->field_cta_url,
          'title' => $cta->field_cta_title,
          'attributes' => $cta->field_cta_attributes,
        ];
      }
      $field_sidebar['field_cta'] = $ctas;
      // Fetch Sidebar Images.
      $sidebar_images = $this->getDatabase()->query('
        SELECT
          fm.uid,
          fm.filename,
          fm.uri,
          fm.filemime,
          fm.filesize,
          fi.field_image_fid,
          fi.field_image_alt,
          fi.field_image_title,
          fi.field_image_width,
          fi.field_image_height
        FROM
          {field_data_field_image} fi
        LEFT JOIN
          {file_managed} fm
        ON
          fi.field_image_fid = fm.fid
        WHERE
          fi.entity_id = :nid', [':nid' => $record->field_sidebar_value]);
      $images = [];
      foreach ($sidebar_images as $img) {
        $images[] = [
          'uid' => $img->uid,
          'filename' => $img->filename,
          'uri' => $img->uri,
          'filemime' => $img->filemime,
          'filesize' => $img->filesize,
          'target_id' => $img->field_files_fid,
          'alt' => $img->field_image_alt,
          'title' => $img->field_image_title,
          'width' => $img->field_image_width,
          'height' => $img->field_image_height,
        ];
      }
      $field_sidebar['field_image'] = $images;
      // Get Social Media Links from Field Collection.
      $social_media_links = $this->getSocialMediaLinksFieldCollection($row, $record->field_social_media_links_value);
      $field_sidebar['field_social_media_links'] = $social_media_links;
      // Get Contact Info from Field collection.
      $contact_info = $this->getContactInfoFieldCollection($row, $record->field_sidebar_value);
      $field_sidebar['field_contact_info'] = $contact_info;
      $row->setSourceProperty('tfield_sidebar', $field_sidebar);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getSocialMediaLinksFieldCollection(Row $row, $entity_id): array {
    $social_links = [];
    $result = $this->getDatabase()->query('
      SELECT
        fft.field_follow_text_value,
        ffu.field_facebook_url_url,
        ffu.field_facebook_url_title,
        ffu.field_facebook_url_attributes,
        ftu.field_twitter_url_url,
        ftu.field_twitter_url_title,
        ftu.field_twitter_url_attributes,
        flu.field_linkedin_url_url,
        flu.field_linkedin_url_title,
        flu.field_linkedin_url_attributes,
        fpu.field_pinterest_url_url,
        fpu.field_pinterest_url_title,
        fpu.field_pinterest_url_attributes,
        fiu.field_instagram_url_url,
        fiu.field_instagram_url_title,
        fiu.field_instagram_url_attributes,
        ftum.field_tumblr_url_url,
        ftum.field_tumblr_url_title,
        ftum.field_tumblr_url_attributes,
        fyu.field_youtube_url_url,
        fyu.field_youtube_url_title,
        fyu.field_youtube_url_attributes
      FROM
        {field_data_field_social_media_links} fsml
      LEFT JOIN
        {field_data_field_follow_text} fft
      ON
        fsml.field_social_media_links_value = fft.entity_id
      LEFT JOIN
        {field_data_field_facebook_url} ffu
      ON
        fsml.field_social_media_links_value = ffu.entity_id
      LEFT JOIN
        {field_data_field_twitter_url} ftu
      ON
        fsml.field_social_media_links_value = ftu.entity_id
      LEFT JOIN
        {field_data_field_linkedin_url} flu
      ON
        fsml.field_social_media_links_value = flu.entity_id
      LEFT JOIN
        {field_data_field_pinterest_url} fpu
      ON
        fsml.field_social_media_links_value = fpu.entity_id
      LEFT JOIN
        {field_data_field_instagram_url} fiu
      ON
        fsml.field_social_media_links_value = fiu.entity_id
      LEFT JOIN
        {field_data_field_tumblr_url} ftum
      ON
        fsml.field_social_media_links_value = ftum.entity_id
      LEFT JOIN
        {field_data_field_youtube_url} fyu
      ON
        fsml.field_social_media_links_value = fyu.entity_id
      WHERE
        fsml.field_social_media_links_value = :entity_id', [':entity_id' => $entity_id]);
    foreach ($result as $record) {
      $social_links['field_follow_text_value'] = $record->field_follow_text_value;
      $social_links['field_facebook_url_url'] = $record->field_facebook_url_url;
      $social_links['field_facebook_url_title'] = $record->field_facebook_url_title;
      $social_links['field_facebook_url_attributes'] = $record->field_facebook_url_attributes;
      $social_links['field_twitter_url_url'] = $record->field_twitter_url_url;
      $social_links['field_twitter_url_title'] = $record->field_twitter_url_title;
      $social_links['field_twitter_url_attributes'] = $record->field_twitter_url_attributes;
      $social_links['field_linkedin_url_url'] = $record->field_linkedin_url_url;
      $social_links['field_linkedin_url_title'] = $record->field_linkedin_url_title;
      $social_links['field_linkedin_url_attributes'] = $record->field_linkedin_url_attributes;
      $social_links['field_pinterest_url_url'] = $record->field_pinterest_url_url;
      $social_links['field_pinterest_url_title'] = $record->field_pinterest_url_title;
      $social_links['field_pinterest_url_attributes'] = $record->field_pinterest_url_attributes;
      $social_links['field_instagram_url_url'] = $record->field_instagram_url_url;
      $social_links['field_instagram_url_title'] = $record->field_instagram_url_title;
      $social_links['field_instagram_url_attributes'] = $record->field_instagram_url_attributes;
      $social_links['field_tumblr_url_url'] = $record->field_tumblr_url_url;
      $social_links['field_tumblr_url_title'] = $record->field_tumblr_url_title;
      $social_links['field_tumblr_url_attributes'] = $record->field_tumblr_url_attributes;
      $social_links['field_youtube_url_url'] = $record->field_youtube_url_url;
      $social_links['field_youtube_url_title'] = $record->field_youtube_url_title;
      $social_links['field_youtube_url_attributes'] = $record->field_youtube_url_attributes;
    }

    $result = $this->getDatabase()->query('
      SELECT
        fasl.field_active_social_links_value
      FROM
        {field_data_field_active_social_links} fasl
      WHERE
        fasl.entity_id = :entity_id', [':entity_id' => $entity_id]);
    foreach ($result as $record) {
      $social_links['field_active_social_links'][] = $record->field_active_social_links_value;
    }
    return $social_links;
  }

  /**
   * {@inheritdoc}
   */
  protected function getContactInfoFieldCollection(Row $row, $entity_id): array {
    $confact_info_list = [];
    $result = $this->getDatabase()->query('
      SELECT
        fci.field_contact_info_value,
        fcin.field_contact_info_name_value,
        fsa1.field_street_address_1_value,
        fsa2.field_street_address_2_value,
        fc.field_city_value,
        td.name as state,
        fz.field_zipcode_value,
        fp.field_phone_value,
        ff.field_fax_value,
        fel.field_email_link_email,
        fcinfo.field_freeform_contact_info_value
      FROM
        {field_data_field_contact_info} fci
      LEFT JOIN
        {field_data_field_contact_info_name} fcin
      ON
        fci.field_contact_info_value = fcin.entity_id
      LEFT JOIN
        {field_data_field_street_address_1} fsa1
      ON
        fci.field_contact_info_value = fsa1.entity_id
      LEFT JOIN
        {field_data_field_street_address_2} fsa2
      ON
        fci.field_contact_info_value = fsa2.entity_id
      LEFT JOIN
        {field_data_field_city} fc
      ON
        fci.field_contact_info_value = fc.entity_id
      LEFT JOIN
        {field_data_field_contact_state} fcs
      ON
        fci.field_contact_info_value = fcs.entity_id
      LEFT JOIN
        {taxonomy_term_data} td
      ON
        fcs.field_contact_state_tid = td.tid
      LEFT JOIN
        {field_data_field_zipcode} fz
      ON
        fci.field_contact_info_value = fz.entity_id
      LEFT JOIN
        {field_data_field_phone} fp
      ON
        fci.field_contact_info_value = fp.entity_id
      LEFT JOIN
        {field_data_field_fax} ff
      ON
        fci.field_contact_info_value = ff.entity_id
      LEFT JOIN
        {field_data_field_email_link} fel
      ON
        fci.field_contact_info_value = fel.entity_id
      LEFT JOIN
        {field_data_field_freeform_contact_info} fcinfo
      ON
        fci.field_contact_info_value = fcinfo.entity_id
      WHERE
        fci.entity_id = :entity_id', [':entity_id' => $entity_id]);
    foreach ($result as $record) {
      $contact_info['field_contact_info_name'] = $record->field_contact_info_name_value;
      $contact_info['field_street_address_1'] = $record->field_street_address_1_value;
      $contact_info['field_street_address_2'] = $record->field_street_address_2_value;
      $contact_info['field_city'] = $record->field_city_value;
      $contact_info['field_state'] = $record->state;
      $contact_info['field_zipcode'] = $record->field_zipcode_value;
      $contact_info['field_phone'] = $record->field_phone_value;
      $contact_info['field_fax'] = $record->field_fax_value;
      $contact_info['field_email_link'] = $record->field_email_link_email;
      $contact_info['field_freeform_contact_info'] = $record->field_freeform_contact_info_value;
      $confact_info_list[] = $contact_info;
    }
    return $confact_info_list;
  }

}
