<?php

namespace Drupal\bmc_migrate\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node as d7_node;
use Drupal\redirect\Entity\Redirect;

/**
 * Published nodes from the d7 database.
 *
 * @MigrateSource(
 *   id = "bmc_bulletin_article_node",
 *   source_module = "node"
 * )
 */
class BmcBulletinArticleNode extends d7_node {

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    $query = parent::query();

    // Only migrate published records, of Bulletin type.
    $included_nodes = ['9702', '9703', '9704', '9705', '9707', '9708', '9709', '9711', '9712', '9713', '9714', '9719', '9721',
      '9774', '10081', '10089', '10090', '10117', '10118', '10119', '10122', '10140', '10141', '10158', '10159', '10162', '10163', '10165', '10166', '10183', '10185', '10186', '10187', '10188', '10189', '10190', '10191', '10203', '10205', '10206', '10207', '10208', '10209', '10210', '10211', '10212', '10214', '10215', '10216', '10217', '10220', '10222', '10223', '10224', '10225', '10226', '10227', '10228', '10229', '10251', '10260', '10270', '10271', '10273', '10274', '10275', '10279', '10341', '10614', '10676', '10996', '11002', '11003', '11093', '11094', '11096', '11099', '11100', '11101', '11102', '11112', '11115', '11116', '11117', '11119', '11121', '11123', '11124', '11125', '11130', '11132', '11133', '11263', '11266', '11480', '11483', '11499', '11575', '11576', '11577', '11578', '11579', '11580', '11581', '11582', '11583', '11584', '11587', '11588', '11593', '11595', '11598', '11599', '11600', '11602', '11603', '11605', '11607', '11631', '11710', '11738', '11934', '11948', '11950', '11951', '11952', '11953', '11954', '11958', '11959', '11960', '11962', '11963', '11964', '11965', '11966', '11967', '11973', '11975', '11976', '11978', '12038', '12041', '12114', '12200', '12223', '12224', '12227', '12228', '12238', '12397', '12462', '12464', '12467', '12493', '12520', '12521', '12523', '12527', '12529', '12530', '12531', '12532', '12533', '12534', '12556', '12583', '12604', '12605', '12607', '12608', '12645', '12646', '12665', '12675', '12676', '12677', '12679', '12680', '12718', '12719', '12720', '12721', '12722', '12723', '12762', '12763', '12763', '12772', '12799', '12837', '12929', '13187', '13253', '13470', '13471', '13472', '13473', '13474', '13476', '13478', '13498', '13499', '13500', '13501', '13504', '13505', '13506', '13507', '13509', '13521', '13523', '13524', '13525', '13597', '13598', '14015', '14129', '14147', '14148', '14149', '14152', '14153', '14155', '14163', '14213', '14214', '14215', '14419', '14420', '14421', '14422', '14428', '14432', '14434', '14435', '14436', '14441', '14455', '14456', '14457', '14458', '14459', '14460', '14461', '14462', '14464', '14465', '14466', '14467', '14468', '14469', '14470', '14837', '14846', '14847', '14848', '15055', '15238', '15239', '15240', '15241', '15242', '15245', '15246', '15251', '15304', '15305', '15307', '15309', '15312', '15313', '15314', '15315', '15317', '15329', '15331', '15332', '15335', '15341', '15342', '15456', '15457', '15458', '15690', '15811', '15837', '15875', '15876', '15898', '15900', '15901', '15911', '15921', '15959', '15984', '15985', '16099', '16100', '16102', '16103', '16104', '16106', '16113', '16126', '16134', '16135', '16137', '16146', '16147', '16179', '16180', '16198', '16199', '16200', '16201', '16202', '16203', '16204', '16205', '16206', '16235', '16236', '16237', '16440', '16518', '16532', '16534', '16572', '16575', '16607', '16608', '16613', '16614', '16637', '16641', '16665', '16669', '16670', '16671', '16676', '16677', '16678', '16681', '16682', '16683', '16774', '16775', '16797', '16798', '16799', '16800', '16801', '16802', '16803', '16804', '16811', '16812', '16814', '16817', '16819', '16820', '16821', '16822', '16825', '16825', '16827', '16827', '16829', '16831', '16833', '16837', '16839', '16841', '16843', '16850', '17382', '17386', '17388', '17389', '17392', '17393', '17394', '17397', '17398', '17399', '17400', '17401', '17420', '17422', '17423', '17426', '17427', '17428', '17542', '18002', '18012', '18048', '18078', '18243', '18244', '18310', '18312', '18313', '18314', '18316', '18316', '18317', '18317', '18318', '18318', '18319', '18319', '18321', '18322', '18322', '18323', '18324', '18325', '18326', '18326', '18327', '18328', '18329', '18330', '18331', '18331', '18332', '18332', '18334', '18334', '18335', '18335', '18336', '18336', '18349', '18396', '18736', '19451', '19561', '19736', '19871', '19896', '19901', '19911', '20296', '20421', '21056', '21106', '21111', '21456', '21621', '21631', '21636', '21651', '21781', '21791', '21796', '21806', '21811', '21821', '21826', '21831', '21836', '21841', '21846', '21851', '21856', '21861', '21866', '22131', '22216', '22431', '22766', '23011', '23266', '23661', '24106', '24111', '24126', '24161', '24166', '24171', '24176', '24191', '24216', '24221', '24226', '24231', '24261', '26056', '27471', '27476', '27481', '27496', '27496', '27501', '27501', '27516', '27516', '27521', '27521', '27541', '27546', '27556', '27561', '27596', '27606', '27651', '27656', '27666', '27671', '27746', '27751', '27756', '27761', '27771', '27776', '27781', '27786', '27791', '27796', '27801', '27806', '27821', '27826', '27836', '28771', '30051', '30166', '30186', '30191', '30196', '30201', '30876', '31176', '31341', '31351', '31356', '31361', '31366', '31371', '31376', '31381', '31386', '31391', '31396', '31401', '31406', '31416', '31421', '31426', '31451', '31456', '31461', '31466', '31471', '31476', '33701', '33706', '33711', '33921', '33926', '33936', '33946', '33956', '33966', '33971', '33976', '34011', '34016', '34021', '34031', '34036', '34041', '34046', '34056', '34076', '34081', '34091',
    ];
    $query->condition('n.status', 1)->condition('n.nid', $included_nodes, 'IN');

    // Get the URL alias.
    $query->leftJoin('url_alias', 'ua', "ua.source = CONCAT('node/', n.nid)");
    $query->addExpression('GROUP_CONCAT(DISTINCT ua.alias)', 'php_alias');

    $query->leftJoin('users', 'u', "u.uid = n.uid");
    $query->addExpression('GROUP_CONCAT(DISTINCT u.name)', 'php_author');

    // Only return one row per node.
    // This is one way of solving "isn't in GROUP BY" issues with < MySQL 5.7.
    // We cannot set ONLY_FULL_GROUP_BY in Webfaction, unfortunately.
    $query->groupBy('n.nid');
    $query->groupBy('n.uid');
    $query->groupBy('n.type');
    $query->groupBy('n.language');
    $query->groupBy('n.status');
    $query->groupBy('n.created');
    $query->groupBy('n.changed');
    $query->groupBy('n.comment');
    $query->groupBy('n.promote');
    $query->groupBy('n.sticky');
    $query->groupBy('n.tnid');
    $query->groupBy('n.translate');
    $query->groupBy('nr.vid');
    $query->groupBy('nr.title');
    $query->groupBy('nr.log');
    $query->groupBy('nr.timestamp');
    $query->groupBy('nr.uid');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = $this->baseFields();
    $fields['body/format'] = $this->t('Format of body');
    $fields['body/value'] = $this->t('Full text of body');
    $fields['body/summary'] = $this->t('Summary of body');
    return $fields;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function prepareRow(Row $row): bool {
    $nid = $row->getSourceProperty('nid');
    // Get the Gallery Images.
    $this->setGalleryImages($row);
    // Get the Other fields of Node.
    $this->setNodeOtherFields($row);
    $this->setCarouselFieldCollection($row);

    $this->splitAliases($row);
    $row->setSourceProperty('php_nid', [$row->getSourceProperty('nid')]);
    return parent::prepareRow($row);
  }

  /**
   * Splits out multiple aliases into a primary alias and redirects.
   *
   * @throws \Exception
   */
  protected function splitAliases(Row $row): void {
    $aliases = explode(',', $row->getSourceProperty('php_alias'));
    if (count($aliases) > 1) {
      $row->setSourceProperty('php_alias', $aliases[0]);
      /** @var \Drupal\redirect\RedirectRepository $repository */
      $source_url = '/' . $aliases[1];
      $redirect_url = '/' . $aliases[0];
      $repository = \Drupal::service('redirect.repository');
      $redirect_exists = $repository->findBySourcePath($source_url);
      if (!isset($redirect_exists)) {
        Redirect::create([
          'redirect_source' => $source_url,
          'redirect_redirect' => $redirect_url,
          'status_code' => 301,
        ])->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function setNodeOtherFields(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        ttd.name,
        fiit.field_primary_image_type_value,
        ft.field_byline_value,
        ft.field_byline_format,
        fi.field_blurb_value,
        fi.field_blurb_format,
        fp.field_premium_value,
        fpb.field_premium_blub_value,
        fct.field_carousel_type_value,
        figc.field_image_gallery_caption_value,
        fcst.field_custom_sidebar_text_value,
        fcst.field_custom_sidebar_text_format,
        feb.field_expert_blurb_value,
        fbdi.field_bulletin_drop_in_value,
        fbdi.field_bulletin_drop_in_format,
        fld.body_value,
        fld.body_summary,
        fld.body_format,
        fm.uid,
        fm.filename,
        fm.uri,
        fm.filemime,
        fm.filesize,
        fii.field_primary_image_fid,
        fii.field_primary_image_alt,
        fii.field_primary_image_title,
        fii.field_primary_image_width,
        fii.field_primary_image_height
      FROM
        node n
      LEFT JOIN
        {field_data_field_primary_image_type} fiit
      ON
        n.nid = fiit.entity_id
      LEFT JOIN
        {field_data_field_byline} ft
      ON
        n.nid = ft.entity_id
      LEFT JOIN
        {field_data_field_blurb} fi
      ON
        n.nid = fi.entity_id
      LEFT JOIN
        {field_data_field_premium} fp
      ON
        n.nid = fp.entity_id
      LEFT JOIN
        {field_data_field_premium_blub} fpb
      ON
        n.nid = fpb.entity_id
      LEFT JOIN
        {field_data_field_carousel_type} fct
      ON
        n.nid = fct.entity_id
      LEFT JOIN
        {field_data_field_image_gallery_caption} figc
      ON
        n.nid = figc.entity_id
      LEFT JOIN
        {field_data_field_custom_sidebar_text} fcst
      ON
        n.nid = fcst.entity_id
      LEFT JOIN
        {field_data_field_bulletin_drop_in} fbdi
      ON
        n.nid = fbdi.entity_id
      LEFT JOIN
        {field_data_body} fld
      ON n.nid = fld.entity_id
      LEFT JOIN
        {field_data_field_expert_blurb} feb
      ON n.nid = feb.entity_id
      LEFT JOIN
        {field_data_field_primary_image} fii
      ON
        n.nid = fii.entity_id
      LEFT JOIN
        {file_managed} fm
      ON
        fii.field_primary_image_fid = fm.fid
      LEFT JOIN
        {field_data_field_department} fd
      ON
        n.nid = fd.entity_id
      LEFT JOIN
        {taxonomy_term_data} ttd
      ON
        fd.field_department_tid = ttd.tid
      WHERE
        n.nid = :nid', [':nid' => $nid]);

    foreach ($result as $record) {
      $row->setSourceProperty('tfield_department', $record->name);
      $row->setSourceProperty('tfield_premium', $record->field_premium_value);
      $row->setSourceProperty('tfield_premium_blub', $record->field_premium_blub_value);
      $row->setSourceProperty('tfield_carousel_type', $record->field_carousel_type_value);
      $row->setSourceProperty('tfield_image_gallery_caption', $record->field_image_gallery_caption_value);
      $row->setSourceProperty('tfield_primary_image_type', $record->field_primary_image_type_value);
      $row->setSourceProperty('tfield_expert_blurb', $record->field_expert_blurb_value);
      $row->setSourceProperty('tfield_bulletin_drop_in',
        [
          'value' => $record->field_bulletin_drop_in_value,
          'format' => $record->field_bulletin_drop_in_format,
        ]
      );
      $row->setSourceProperty('tfield_custom_sidebar_text',
        [
          'value' => $record->field_custom_sidebar_text_value,
          'format' => $record->field_custom_sidebar_text_format,
        ]
      );
      $row->setSourceProperty('tfield_blurb',
        [
          'value' => $record->field_blurb_value,
          'format' => $record->field_blurb_format,
        ]
      );
      $row->setSourceProperty('tfield_byline',
        [
          'value' => $record->field_byline_value,
          'format' => $record->field_byline_format,
        ]
      );
      $primary_image = [
        'uid' => $record->uid,
        'filename' => $record->filename,
        'uri' => $record->uri,
        'filemime' => $record->filemime,
        'filesize' => $record->filesize,
        'target_id' => $record->field_primary_image_fid,
        'alt' => $record->field_primary_image_alt,
        'title' => $record->field_primary_image_title,
        'width' => $record->field_primary_image_width,
        'height' => $record->field_primary_image_height,
      ];
      $row->setSourceProperty('tfield_primary_image', $primary_image);
      $row->setSourceProperty('body_value', $record->body_value);
      $row->setSourceProperty('body_summary', $record->body_summary);
      $row->setSourceProperty('body_format', $record->body_format);
    }

    // Get Multi Department.
    $department_multi = $this->getDatabase()->query('
      SELECT
        GROUP_CONCAT(td.name) as tnames
      FROM
        {field_data_field_department_multi} fld
      LEFT JOIN
        {taxonomy_term_data} td
      ON
        fld.field_department_multi_tid = td.tid
      WHERE
        fld.entity_id = :nid', [':nid' => $nid]);
    foreach ($department_multi as $dep) {
      if (!is_null($dep->tnames)) {
        $row->setSourceProperty('tfield_department_multi', explode(',', $dep->tnames));
      }
    }

    // Get topic term names.
    $topics = $this->getDatabase()->query('
      SELECT
        GROUP_CONCAT(td.name) as tnames
      FROM
        {field_data_field_article_terms} fld
      LEFT JOIN
        {taxonomy_term_data} td
      ON
        fld.field_article_terms_tid = td.tid
      WHERE
        fld.entity_id = :nid', [':nid' => $nid]);
    foreach ($topics as $topic) {
      if (!is_null($topic->tnames)) {
        $row->setSourceProperty('tfield_article_terms', explode(',', $topic->tnames));
      }
    }

    // Get CTA Links.
    $cta = $this->getDatabase()->query('
      SELECT
        field_cta_url,
        field_cta_title,
        field_cta_attributes
      FROM
        {field_data_field_cta}
      WHERE
        entity_id = :nid
      ORDER BY delta', [':nid' => $nid]);
    $ctas = [];
    foreach ($cta as $cta) {
      $ctas[] = [
        'url' => $cta->field_cta_url,
        'title' => $cta->field_cta_title,
        'attributes' => $cta->field_cta_attributes,
      ];
    }
    $row->setSourceProperty('tfield_cta', $ctas);

    // Get Related Links.
    $related_links = $this->getDatabase()->query('
      SELECT
        field_related_links_url,
        field_related_links_title,
        field_related_links_attributes
      FROM
        {field_data_field_related_links}
      WHERE
        entity_id = :nid
      ORDER BY delta', [':nid' => $nid]);
    $links = [];
    foreach ($related_links as $cta) {
      $links[] = [
        'url' => $cta->field_related_links_url,
        'title' => $cta->field_related_links_title,
        'attributes' => $cta->field_related_links_attributes,
      ];
    }
    $row->setSourceProperty('tfield_related_links', $links);

    // Get Bulletin Related nodes.
    $bulletin_related_links = $this->getDatabase()->query('
      SELECT
        field_bulletin_related_target_id
      FROM
        {field_data_field_bulletin_related}
      WHERE
        entity_id = :nid
      ORDER BY delta', [':nid' => $nid]);
    $links = [];
    foreach ($bulletin_related_links as $cta) {
      $links[] = $cta->field_bulletin_related_target_id;
    }
    $row->setSourceProperty('tfield_bulletin_related', $links);

    // Bulletin Issue & Bulletin Section Term Names.
    $bullentin = $this->getDatabase()->query('
      SELECT
        td1.name as issue,
        td2.name as section
      FROM
        {node} n
      LEFT JOIN
        {field_data_field_bulletin_issue} fbi
      ON
        n.nid = fbi.entity_id
      LEFT JOIN
        {taxonomy_term_data} td1
      ON
        fbi.field_bulletin_issue_tid = td1.tid
      LEFT JOIN
        {field_data_field_bulletin_section} fbs
      ON
        n.nid = fbs.entity_id
      LEFT JOIN
        {taxonomy_term_data} td2
      ON
        fbs.field_bulletin_section_tid = td2.tid
      WHERE
        n.nid = :nid', [':nid' => $nid]);
    foreach ($bullentin as $result) {
      if (!is_null($result->issue)) {
        $row->setSourceProperty('tfield_bulletin_issue', $result->issue);
      }
      if (!is_null($result->section)) {
        $row->setSourceProperty('tfield_bulletin_section', $result->section);
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function setGalleryImages(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        fm.uid,
        fm.filename,
        fm.uri,
        fm.filemime,
        fm.filesize,
        fig.field_image_gallery_fid,
        fig.field_image_gallery_alt,
        fig.field_image_gallery_title,
        fig.field_image_gallery_width,
        fig.field_image_gallery_height
      FROM
        {field_data_field_image_gallery} fig
      LEFT JOIN
        {file_managed} fm
      ON
        fig.field_image_gallery_fid = fm.fid
      WHERE
        fig.entity_id = :nid', [':nid' => $nid]);
    $images = [];
    foreach ($result as $record) {
      $images[] = [
        'uid' => $record->uid,
        'filename' => $record->filename,
        'uri' => $record->uri,
        'filemime' => $record->filemime,
        'filesize' => $record->filesize,
        'target_id' => $record->field_files_fid,
        'alt' => $record->field_image_gallery_alt,
        'title' => $record->field_image_gallery_title,
        'width' => $record->field_image_gallery_width,
        'height' => $record->field_image_gallery_height,
      ];
    }
    $row->setSourceProperty('tfield_image_gallery', $images);
  }

  /**
   * {@inheritdoc}
   */
  protected function setCarouselFieldCollection(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        fc.field_carousel_value,
        fst.field_slide_title_value,
        fsh.field_slide_header_value,
        fsb.field_slide_body_value,
        fsl.field_slide_share_link_url,
        fsl.field_slide_share_link_title,
        fsl.field_slide_share_link_attributes,
        fsi.field_slide_image_fid,
        fsi.field_slide_image_alt,
        fsi.field_slide_image_title,
        fsi.field_slide_image_width,
        fsi.field_slide_image_height,
        fm.uid,
        fm.filename,
        fm.uri,
        fm.filemime,
        fm.filesize
      FROM
        {field_data_field_carousel} fc
      LEFT JOIN
        {field_data_field_slide_image} fsi
      ON
        fc.field_carousel_value = fsi.entity_id
      LEFT JOIN
        {field_data_field_slide_title} fst
      ON
        fc.field_carousel_value = fst.entity_id
      LEFT JOIN
        {field_data_field_slide_header} fsh
      ON
        fc.field_carousel_value = fsh.entity_id
      LEFT JOIN
        {field_data_field_slide_body} fsb
      ON
        fc.field_carousel_value = fsb.entity_id
      LEFT JOIN
        {field_data_field_slide_share_link} fsl
      ON
        fc.field_carousel_value = fsl.entity_id
      LEFT JOIN
        {file_managed} fm
      ON
        fsi.field_slide_image_fid = fm.fid
      WHERE
        fc.entity_id = :nid', [':nid' => $nid]);
    $field_carousel_list = [];
    foreach ($result as $record) {
      $field_carousel['field_slide_title'] = $record->field_slide_title_value;
      $field_carousel['field_slide_header'] = $record->field_slide_header_value;
      $field_carousel['field_slide_body'] = $record->field_slide_body_value;
      $field_carousel['field_slide_body'] = $record->field_slide_body_value;
      $field_carousel['field_slide_share_link'] = [
        'url' => $record->field_slide_share_link_url,
        'title' => $record->field_slide_share_link_title,
        'attributes' => $record->field_slide_share_link_attributes,
      ];
      $field_carousel['field_slide_image'] = [
        'uid' => $record->uid,
        'filename' => $record->filename,
        'uri' => $record->uri,
        'filemime' => $record->filemime,
        'filesize' => $record->filesize,
        'target_id' => $record->field_files_fid,
        'alt' => $record->field_intro_image_alt,
        'title' => $record->field_intro_image_title,
        'width' => $record->field_intro_image_width,
        'height' => $record->field_intro_image_height,
      ];
      $carusel_cta = $this->getDatabase()->query('
        SELECT
          field_cta_url,
          field_cta_title,
          field_cta_attributes
        FROM
          {field_data_field_cta}
        WHERE
          entity_id = :entity_id
        ORDER BY delta', [':entity_id' => $record->field_carousel_value]);
      $ctas = [];
      foreach ($carusel_cta as $cta) {
        $ctas[] = [
          'url' => $cta->field_cta_url,
          'title' => $cta->field_cta_title,
          'attributes' => $cta->field_cta_attributes,
        ];
      }
      $field_carousel['ctas'] = $ctas;
      $field_carousel_list[] = $field_carousel;
    }
    $row->setSourceProperty('tfield_carousel', $field_carousel_list);
  }

}
