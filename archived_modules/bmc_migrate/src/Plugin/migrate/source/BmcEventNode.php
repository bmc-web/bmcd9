<?php

namespace Drupal\bmc_migrate\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node as d7_node;

/**
 * Published nodes from the d7 database.
 *
 * @MigrateSource(
 *   id = "bmc_event_node",
 *   source_module = "node"
 * )
 */
class BmcEventNode extends d7_node {

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    $query = parent::query();

    // Only migrate published records.
    $query->condition('n.status', 1)->condition('n.created', '1535760000', '>=');

    // Get the URL alias.
    $query->leftJoin('url_alias', 'ua', "ua.source = CONCAT('node/', n.nid)");
    $query->addExpression('GROUP_CONCAT(DISTINCT ua.alias)', 'php_alias');

    $query->leftJoin('users', 'u', "u.uid = n.uid");
    $query->addExpression('GROUP_CONCAT(DISTINCT u.name)', 'php_author');

    // Only return one row per node.
    // This is one way of solving "isn't in GROUP BY" issues with < MySQL 5.7.
    // We cannot set ONLY_FULL_GROUP_BY in Webfaction, unfortunately.
    $query->groupBy('n.nid');
    $query->groupBy('n.uid');
    $query->groupBy('n.type');
    $query->groupBy('n.language');
    $query->groupBy('n.status');
    $query->groupBy('n.created');
    $query->groupBy('n.changed');
    $query->groupBy('n.comment');
    $query->groupBy('n.promote');
    $query->groupBy('n.sticky');
    $query->groupBy('n.tnid');
    $query->groupBy('n.translate');
    $query->groupBy('nr.vid');
    $query->groupBy('nr.title');
    $query->groupBy('nr.log');
    $query->groupBy('nr.timestamp');
    $query->groupBy('nr.uid');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = $this->baseFields();
    $fields['body/format'] = $this->t('Format of body');
    $fields['body/value'] = $this->t('Full text of body');
    $fields['body/summary'] = $this->t('Summary of body');
    return $fields;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function prepareRow(Row $row): bool {
    $nid = $row->getSourceProperty('nid');
    // Get the Gallery Images.
    $this->setGalleryImages($row);
    // Get the Other fields of Node.
    $this->setNodeOtherFields($row);
    $this->setCarouselFieldCollection($row);
    $this->splitAliases($row);
    $row->setSourceProperty('php_nid', [$row->getSourceProperty('nid')]);
    return parent::prepareRow($row);
  }

  /**
   * Splits out multiple aliases into a primary alias and redirects.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function splitAliases(Row $row): void {
    $aliases = explode(',', $row->getSourceProperty('php_alias'));
    if (count($aliases) > 1) {
      $row->setSourceProperty('php_alias', $aliases[0]);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function setNodeOtherFields(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        ttd.name,
        fiit.field_primary_image_type_value,
        fi.field_blurb_value,
        fi.field_blurb_format,
        fct.field_carousel_type_value,
        figc.field_image_gallery_caption_value,
        fcst.field_custom_sidebar_text_value,
        fcst.field_custom_sidebar_text_format,
        fld.field_location_detail_value,
        frl.field_register_link_url,
        frl.field_register_link_title,
        frl.field_register_link_attributes,
        fdt.field_date_time_value,
        fdt.field_date_time_value2,
        fb.body_value,
        fb.body_summary,
        fb.body_format,
        fm.uid,
        fm.filename,
        fm.uri,
        fm.filemime,
        fm.filesize,
        fii.field_primary_image_fid,
        fii.field_primary_image_alt,
        fii.field_primary_image_title,
        fii.field_primary_image_width,
        fii.field_primary_image_height
      FROM
        node n
      LEFT JOIN
        {field_data_field_primary_image_type} fiit
      ON
        n.nid = fiit.entity_id
      LEFT JOIN
        {field_data_field_blurb} fi
      ON
        n.nid = fi.entity_id
      LEFT JOIN
        {field_data_field_carousel_type} fct
      ON
        n.nid = fct.entity_id
      LEFT JOIN
        {field_data_field_image_gallery_caption} figc
      ON
        n.nid = figc.entity_id
      LEFT JOIN
        {field_data_field_custom_sidebar_text} fcst
      ON
        n.nid = fcst.entity_id
      LEFT JOIN
        {field_data_field_date_time} fdt
      ON
        n.nid = fdt.entity_id
      LEFT JOIN
        {field_data_field_register_link} frl
      ON
        n.nid = frl.entity_id
      LEFT JOIN
        {field_data_field_location_detail} fld
      ON
        n.nid = fld.entity_id
      LEFT JOIN
        {field_data_body} fb
      ON n.nid = fb.entity_id
      LEFT JOIN
        {field_data_field_primary_image} fii
      ON
        n.nid = fii.entity_id
      LEFT JOIN
        {file_managed} fm
      ON
        fii.field_primary_image_fid = fm.fid
      LEFT JOIN
        {field_data_field_department} fd
      ON
        n.nid = fd.entity_id
      LEFT JOIN
        {taxonomy_term_data} ttd
      ON
        fd.field_department_tid = ttd.tid
      WHERE
        n.nid = :nid', [':nid' => $nid]);

    foreach ($result as $record) {
      $row->setSourceProperty('tfield_department', $record->name);
      $row->setSourceProperty('tfield_carousel_type', $record->field_carousel_type_value);
      $row->setSourceProperty('tfield_image_gallery_caption', $record->field_image_gallery_caption_value);
      $row->setSourceProperty('tfield_primary_image_type', $record->field_primary_image_type_value);
      $row->setSourceProperty('tfield_location_detail', $record->field_location_detail_value);
      $row->setSourceProperty('tfield_date_time',
        [
          'date1' => $record->field_date_time_value,
          'date2' => $record->field_date_time_value2,
        ]
      );
      $row->setSourceProperty('tfield_custom_sidebar_text',
        [
          'value' => $record->field_blurb_value,
          'format' => $record->field_blurb_format,
        ]
      );
      $row->setSourceProperty('tfield_register_link',
        [
          'url' => $record->field_register_link_url,
          'title' => $record->field_register_link_title,
          'attributes' => $record->field_register_link_attributes,
        ]
      );
      $row->setSourceProperty('tfield_blurb',
        [
          'value' => $record->field_blurb_value,
          'format' => $record->field_blurb_format,
        ]
      );

      $primary_image = [
        'uid' => $record->uid,
        'filename' => $record->filename,
        'uri' => $record->uri,
        'filemime' => $record->filemime,
        'filesize' => $record->filesize,
        'target_id' => $record->field_primary_image_fid,
        'alt' => $record->field_primary_image_alt,
        'title' => $record->field_primary_image_title,
        'width' => $record->field_primary_image_width,
        'height' => $record->field_primary_image_height,
      ];
      $row->setSourceProperty('tfield_primary_image', $primary_image);
      $row->setSourceProperty('body_value', $record->body_value);
      $row->setSourceProperty('body_summary', $record->body_summary);
      $row->setSourceProperty('body_format', $record->body_format);
    }

    // Get Multi Department.
    $department_multi = $this->getDatabase()->query('
      SELECT
        GROUP_CONCAT(td.name) as tnames
      FROM
        {field_data_field_department_multi} fld
      LEFT JOIN
        {taxonomy_term_data} td
      ON
        fld.field_department_multi_tid = td.tid
      WHERE
        fld.entity_id = :nid', [':nid' => $nid]);
    foreach ($department_multi as $dep) {
      if (!is_null($dep->tnames)) {
        $row->setSourceProperty('tfield_department_multi', explode(',', $dep->tnames));
      }
    }

    // Get topic term names.
    $topics = $this->getDatabase()->query('
      SELECT
        GROUP_CONCAT(td.name) as tnames
      FROM
        {field_data_field_article_terms} fld
      LEFT JOIN
        {taxonomy_term_data} td
      ON
        fld.field_article_terms_tid = td.tid
      WHERE
        fld.entity_id = :nid', [':nid' => $nid]);
    foreach ($topics as $topic) {
      if (!is_null($topic->tnames)) {
        $row->setSourceProperty('tfield_article_terms', explode(',', $topic->tnames));
      }
    }

    // Get Event type term names.
    $event_types = $this->getDatabase()->query('
      SELECT
        GROUP_CONCAT(td.name) as tnames
      FROM
        {field_data_field_event_type} fld
      LEFT JOIN
        {taxonomy_term_data} td
      ON
        fld.field_event_type_tid = td.tid
      WHERE
        fld.entity_id = :nid', [':nid' => $nid]);
    foreach ($event_types as $event) {
      if (!is_null($event->tnames)) {
        $row->setSourceProperty('tfield_event_type', explode(',', $event->tnames));
      }
    }

    // Get Related Links.
    $related_links = $this->getDatabase()->query('
      SELECT
        field_related_links_url,
        field_related_links_title,
        field_related_links_attributes
      FROM
        {field_data_field_related_links}
      WHERE
        entity_id = :nid
      ORDER BY delta', [':nid' => $nid]);
    $links = [];
    foreach ($related_links as $cta) {
      $links[] = [
        'url' => $cta->field_related_links_url,
        'title' => $cta->field_related_links_title,
        'attributes' => $cta->field_related_links_attributes,
      ];
    }
    $row->setSourceProperty('tfield_related_links', $links);

    // Get Event Audience and Event Location term name.
    $record = $this->getDatabase()->query('
      SELECT
        td1.name as audience,
        td2.name as location
      FROM
        {node} n
      LEFT JOIN
        {field_data_field_events_audience} fbi
      ON
        n.nid = fbi.entity_id
      LEFT JOIN
        {taxonomy_term_data} td1
      ON
        fbi.field_events_audience_tid = td1.tid
      LEFT JOIN
        {field_data_field_events_location} fbs
      ON
        n.nid = fbs.entity_id
      LEFT JOIN
        {taxonomy_term_data} td2
      ON
        fbs.field_events_location_tid = td2.tid
      WHERE
        n.nid = :nid', [':nid' => $nid]);
    foreach ($record as $result) {
      if (!is_null($result->issue)) {
        $row->setSourceProperty('tfield_events_audience', $result->audience);
      }
      if (!is_null($result->section)) {
        $row->setSourceProperty('tfield_events_location', $result->location);
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function setGalleryImages(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        fm.uid,
        fm.filename,
        fm.uri,
        fm.filemime,
        fm.filesize,
        fig.field_image_gallery_fid,
        fig.field_image_gallery_alt,
        fig.field_image_gallery_title,
        fig.field_image_gallery_width,
        fig.field_image_gallery_height
      FROM
        {field_data_field_image_gallery} fig
      LEFT JOIN
        {file_managed} fm
      ON
        fig.field_image_gallery_fid = fm.fid
      WHERE
        fig.entity_id = :nid', [':nid' => $nid]);
    $images = [];
    foreach ($result as $record) {
      $images[] = [
        'uid' => $record->uid,
        'filename' => $record->filename,
        'uri' => $record->uri,
        'filemime' => $record->filemime,
        'filesize' => $record->filesize,
        'target_id' => $record->field_files_fid,
        'alt' => $record->field_image_gallery_alt,
        'title' => $record->field_image_gallery_title,
        'width' => $record->field_image_gallery_width,
        'height' => $record->field_image_gallery_height,
      ];
    }
    $row->setSourceProperty('tfield_image_gallery', $images);
  }

  /**
   * {@inheritdoc}
   */
  protected function setCarouselFieldCollection(Row $row): void {
    $nid = $row->getSourceProperty('nid');
    $result = $this->getDatabase()->query('
      SELECT
        fc.field_carousel_value,
        fst.field_slide_title_value,
        fsh.field_slide_header_value,
        fsb.field_slide_body_value,
        fsl.field_slide_share_link_url,
        fsl.field_slide_share_link_title,
        fsl.field_slide_share_link_attributes,
        fsi.field_slide_image_fid,
        fsi.field_slide_image_alt,
        fsi.field_slide_image_title,
        fsi.field_slide_image_width,
        fsi.field_slide_image_height,
        fm.uid,
        fm.filename,
        fm.uri,
        fm.filemime,
        fm.filesize
      FROM
        {field_data_field_carousel} fc
      LEFT JOIN
        {field_data_field_slide_image} fsi
      ON
        fc.field_carousel_value = fsi.entity_id
      LEFT JOIN
        {field_data_field_slide_title} fst
      ON
        fc.field_carousel_value = fst.entity_id
      LEFT JOIN
        {field_data_field_slide_header} fsh
      ON
        fc.field_carousel_value = fsh.entity_id
      LEFT JOIN
        {field_data_field_slide_body} fsb
      ON
        fc.field_carousel_value = fsb.entity_id
      LEFT JOIN
        {field_data_field_slide_share_link} fsl
      ON
        fc.field_carousel_value = fsl.entity_id
      LEFT JOIN
        {file_managed} fm
      ON
        fsi.field_slide_image_fid = fm.fid
      WHERE
        fc.entity_id = :nid', [':nid' => $nid]);
    $field_carousel_list = [];
    foreach ($result as $record) {
      $field_carousel['field_slide_title'] = $record->field_slide_title_value;
      $field_carousel['field_slide_header'] = $record->field_slide_header_value;
      $field_carousel['field_slide_body'] = $record->field_slide_body_value;
      $field_carousel['field_slide_body'] = $record->field_slide_body_value;
      $field_carousel['field_slide_share_link'] = [
        'url' => $record->field_slide_share_link_url,
        'title' => $record->field_slide_share_link_title,
        'attributes' => $record->field_slide_share_link_attributes,
      ];
      $field_carousel['field_slide_image'] = [
        'uid' => $record->uid,
        'filename' => $record->filename,
        'uri' => $record->uri,
        'filemime' => $record->filemime,
        'filesize' => $record->filesize,
        'target_id' => $record->field_files_fid,
        'alt' => $record->field_intro_image_alt,
        'title' => $record->field_intro_image_title,
        'width' => $record->field_intro_image_width,
        'height' => $record->field_intro_image_height,
      ];
      $carousel_cta = $this->getDatabase()->query('
        SELECT
          field_cta_url,
          field_cta_title,
          field_cta_attributes
        FROM
          {field_data_field_cta}
        WHERE
          entity_id = :entity_id
        ORDER BY delta', [':entity_id' => $record->field_carousel_value]);
      $ctas = [];
      foreach ($carousel_cta as $cta) {
        $ctas[] = [
          'url' => $cta->field_cta_url,
          'title' => $cta->field_cta_title,
          'attributes' => $cta->field_cta_attributes,
        ];
      }
      $field_carousel['ctas'] = $ctas;
      $field_carousel_list[] = $field_carousel;
    }
    $row->setSourceProperty('tfield_carousel', $field_carousel_list);
  }

}
