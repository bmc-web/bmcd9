# Backstop.js
## The example command below would run a backstop test for only PB scenarios
## make bs-test filter='pb_*'
bs-approve:
	./node_modules/backstopjs/cli/index.js approve --configPath=backstoprc.js --pathfile=bs-paths
bs-reference:
	./node_modules/backstopjs/cli/index.js reference --configPath=backstoprc.js --pathfile=bs-paths --filter=$(filter)
bs-test:
	./node_modules/backstopjs/cli/index.js test --debug --configPath=backstoprc.js --pathfile=bs-paths --filter=$(filter)

# Psfeeds
get-psfeeds:
	# Get psfeeds from prod.
	scp -r bmcd9.prod@bmcd9.ssh.prod.acquia-sites.com:/shared/sites/default/files/psfeeds docroot/sites/default/files

# Lando/Drush
sync-prod-to-local:
	# Sync db to local
	lando drush sql-sync @bmcd9_acquia.prod @self
sync-dev-to-local:
	# Sync db to local
	lando drush sql-sync @bmcd9_acquia.dev @self
sync-test-to-local:
	# Sync db to local
	lando drush sql-sync @bmcd9_acquia.test @self
sync-old-dev-to-local:
	# Sync db to local
	lando drush sql-sync @ifactory_acquia.dev @self
sync-old-test-to-local:
	# Sync db to local
	lando drush sql-sync @ifactory_acquia.test @self

# Cypress
cy-start:
	#Start Cypress
	./node_modules/.bin/cypress open

-include Makefile.local
