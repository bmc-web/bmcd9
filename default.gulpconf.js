/**
 * Copy this file to gulpfile.js and
 * update for local environment
 */

// Local environment for browsersync proxy
const localenv = 'http://YOURLOCAL.env'

// Comment line to set preferred option
const openPref = false;
// const openPref = true;

module.exports = {
  env: localenv,
  open: openPref
}
