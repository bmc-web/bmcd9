const gulp = require('gulp');
const autoprefixer = require('autoprefixer');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const imagemin = require('gulp-imagemin');
const browsersync = require('browser-sync').create();
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');

const config = require('./gulpconf');

// BrowserSync
function browserSync(done) {
  browsersync.init({
    open: config.open,
    port: 3000,
    proxy: config.env,
  });
  done();
}

// Title used for system notifications
const notifyInfo = {
  title: 'Gulp',
};

// Error notification settings for plumber
const plumberErrorHandler = {
  errorHandler: notify.onError({
    title: notifyInfo.title,
    icon: notifyInfo.icon,
    message: 'Error: <%= error.message %>',
  }),
};

// Optimize Images
function images() {
  return gulp
    .src(['docroot/themes/bmc_d9/assets/**/*.{gif,png,jpg,jpeg,svg}'])
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.mozjpeg({ progressive: true,  quality: 90  }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo({
          plugins: [
            {
              removeViewBox: false,
              collapseGroups: true,
              removeComments: true,
            },
          ],
        }),
      ]),
    )
    .pipe(
      gulp.dest(function setFile(file) {
        return file.base;
      }),
    );
}

// CSS development task
function css() {
  return gulp
    .src('docroot/themes/bmc_d9/scss/*.scss', { sourcemaps: true })
    .pipe(plumber(plumberErrorHandler))
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'compressed' }))
    .pipe(
      postcss([
        autoprefixer({
          grid: 'autoplace',
        }),
      ]),
    )
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('docroot/themes/bmc_d9/css'))
    .pipe(browsersync.stream());
}

function js() {
  return gulp
    .src(['docroot/themes/bmc_d9/js/**/*.js', '!docroot/themes/bmc_d9/js/_compiled/**/*.js'])
    .pipe(plumber(plumberErrorHandler))
    .pipe(babel())
    //.pipe(uglify())
    .pipe(gulp.dest('docroot/themes/bmc_d9/js/_compiled/'))
    .pipe(browsersync.stream());
}

function programJs() {
  return gulp
    .src(['docroot/modules/custom/bmc_program_finder/js/*.js', '!docroot/modules/custom/bmc_program_finder/js/_compiled/**/*.js'])
    .pipe(plumber(plumberErrorHandler))
    .pipe(babel())
    //.pipe(uglify())
    .pipe(gulp.dest('docroot/modules/custom/bmc_program_finder/js/_compiled/'))
    .pipe(browsersync.stream());
}

// Watch files
function watchFiles() {
  gulp.watch('docroot/themes/bmc_d9/scss/**/*', css);
  gulp.watch(['docroot/themes/bmc_d9/js/**/*.js', '!docroot/themes/bmc_d9/js/_compiled/**/*.js'], js);
  gulp.watch(['docroot/modules/custom/bmc_program_finder/js/*.js', '!docroot/modules/custom/bmc_program_finder/js/_compiled/**/*.js'], programJs);
}

// Watch files during development
const watch = gulp.parallel(watchFiles, browserSync);

// Export tasks
exports.images = images;
exports.css = css;
exports.js = js;
exports.watch = watch;
exports.default = watch;
