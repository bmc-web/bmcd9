const config = require('./gulpconf');
const refURL = 'https://brynmawr.edu/';
const pathConfig = {};

/**
 * Backstop label prefix conventions
 * el_ - Element
 * pb_ - Paragraph bundle
 * ct_ - Content type
 * lp_ - Landing/Listing Page
 * mp_ - Misc page
*/

pathConfig.array = [
  {
    // Hero: Marketing theme NEW
    label: 'el_hero',
    url: `${config.env}/node/1091`,
    referenceUrl:`${refURL}/node/1091`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  {
    // Hero: Inside BMC theme NEW
    label: 'el_inside_hero',
    url: `${config.env}/node/5811`,
    referenceUrl:`${refURL}/node/5811`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  // {
  //   // PB: Accordion
  //   label: 'pb_accordion',
  //   url: `${config.env}/node/91`,
  //   referenceUrl:`${refURL}/node/91`,
  //   delay: 3000,
  //   selectorExpansion: true,
  //   expect: 0,
  //   misMatchThreshold: 0.1,
  //   requireSameDimensions: true,
  // },
  // {
  //   // PB: WYSIWYG
  //   label: 'pb_wysiwyg',
  //   url: `${config.env}/node/126`,
  //   referenceUrl:`${refURL}/node/126`,
  //   delay: 3000,
  //   selectorExpansion: true,
  //   expect: 0,
  //   misMatchThreshold: 0.1,
  //   requireSameDimensions: true,
  // },
  // {
  //   // PB: Social Connect
  //   label: 'pb_social_media_connect',
  //   url: `${config.env}/node/191`,
  //   referenceUrl:`${refURL}/node/191`,
  //   delay: 3000,
  //   selectorExpansion: true,
  //   expect: 0,
  //   misMatchThreshold: 0.1,
  //   requireSameDimensions: true,
  // },
  // {
  //   // Element: Lightbox
  //   label: 'el_lightbox',
  //   url: `${config.env}/node/126`,
  //   referenceUrl:`${refURL}/node/126`,
  //   delay: 3000,
  //   clickSelector: '.play-video',
  //   postInteractionWait: 10000,
  //   selectorExpansion: true,
  //   expect: 0,
  //   misMatchThreshold: 0.1,
  //   requireSameDimensions: true,
  // },
  // {
  //   // PB: Image Gallery - Open
  //   label: 'pb_image_gallery_open',
  //   url: `${config.env}/node/61`,
  //   referenceUrl:`${refURL}/node/61`,
  //   delay: 3000,
  //   clickSelector: '.image-gallery__open',
  //   postInteractionWait: 10000,
  //   selectorExpansion: true,
  //   expect: 0,
  //   misMatchThreshold: 0.1,
  //   requireSameDimensions: true,
  // },
  // {
  //   // PB: Image Gallery
  //   label: 'pb_image_gallery',
  //   url: `${config.env}/node/61`,
  //   referenceUrl:`${refURL}/node/61`,
  //   delay: 3000,
  //   selectorExpansion: true,
  //   expect: 0,
  //   misMatchThreshold: 0.1,
  //   requireSameDimensions: true,
  // },
  // {
  //   // PB: Feature - Quote
  //   label: 'pb_quote',
  //   url: `${config.env}/node/266`,
  //   referenceUrl:`${refURL}/node/266`,
  //   delay: 3000,
  //   selectorExpansion: true,
  //   expect: 0,
  //   misMatchThreshold: 0.1,
  //   requireSameDimensions: true,
  // },
  // {
  //   // PB: Feature - Grid
  //   label: 'pb_feat_grid',
  //   url: `${config.env}/node/221`,
  //   referenceUrl:`${refURL}/node/221`,
  //   delay: 3000,
  //   selectorExpansion: true,
  //   expect: 0,
  //   misMatchThreshold: 0.1,
  //   requireSameDimensions: true,
  // },
  // {
  //   // PB: Social Media Callout
  //   label: 'pb_smcallout',
  //   url: `${config.env}/node/86`,
  //   referenceUrl:`${refURL}/node/86`,
  //   delay: 3000,
  //   selectorExpansion: true,
  //   expect: 0,
  //   misMatchThreshold: 0.1,
  //   requireSameDimensions: true,
  // },
  // {
  //   // PB: Image Detail Large
  //   label: 'pb_imgdetlg',
  //   url: `${config.env}/node/176`,
  //   referenceUrl:`${refURL}/node/176`,
  //   delay: 3000,
  //   selectorExpansion: true,
  //   expect: 0,
  //   misMatchThreshold: 0.1,
  //   requireSameDimensions: true,
  // },
  {
    // PB: Callout NEW
    label: 'pb_callout',
    url: `${config.env}/node/1091`,
    referenceUrl:`${refURL}/node/1091`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  // {
  //   // PB: Carousel
  //   label: 'pb_carousel',
  //   url: `${config.env}/node/66`,
  //   referenceUrl:`${refURL}/node/66`,
  //   delay: 3000,
  //   selectorExpansion: true,
  //   expect: 0,
  //   misMatchThreshold: 0.1,
  //   requireSameDimensions: true,
  // },
  {
    // PB: Feature - Medium NEW
    label: 'pb_featmd',
    url: `${config.env}/node/1091`,
    referenceUrl:`${refURL}/node/1091`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  {
    // Element: Pagination NEW
    label: 'el_pagination',
    url: `${config.env}/node/196`,
    referenceUrl:`${refURL}/node/196`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  {
    // PB: Special List Small NEW
    label: 'pb_slistsm',
    url: `${config.env}/node/11`,
    referenceUrl:`${refURL}/node/11`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  {
    // PB: Special List Large NEW
    label: 'pb_slistlg',
    url: `${config.env}/node/5811`,
    referenceUrl:`${refURL}/node/5811`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  // {
  //   // PB: Columns
  //   label: 'pb_columns',
  //   url: `${config.env}/node/31`,
  //   referenceUrl:`${refURL}/node/31`,
  //   delay: 3000,
  //   selectorExpansion: true,
  //   expect: 0,
  //   misMatchThreshold: 0.1,
  //   requireSameDimensions: true,
  // },
  // {
  //   // PB: Story 1 Up
  //   // PB: Story 3 up
  //   label: 'pb_story_1_up',
  //   url: `${config.env}/node/326`,
  //   referenceUrl:`${refURL}/node/326`,
  //   delay: 3000,
  //   selectorExpansion: true,
  //   expect: 0,
  //   misMatchThreshold: 0.1,
  //   requireSameDimensions: true,
  // },
  {
    // Stories listing page
    label: 'story_listing',
    url: `${config.env}/node/196`,
    referenceUrl:`${refURL}/node/196`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  // {
  //   // Node: Admissions Officer
  //   label: 'node_admino',
  //   url: `${config.env}/node/136`,
  //   referenceUrl:`${refURL}/node/136`,
  //   delay: 3000,
  //   selectorExpansion: true,
  //   expect: 0,
  //   misMatchThreshold: 0.1,
  //   requireSameDimensions: true,
  // },
  {
  // Page: News Listing
  label: 'news_listing',
  url: `${config.env}/news`,
  referenceUrl:`${refURL}/news`,
  delay: 3000,
  selectorExpansion: true,
  expect: 0,
  misMatchThreshold: 0.1,
  requireSameDimensions: true,
  },
  {
  // Page: News Listing Tagged
  label: 'news_listing',
    url: `${config.env}/news?tagged=13`,
    referenceUrl:`${refURL}/news?tagged=13`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  // {
  //   // BP: News Contact (Manual)
  //   label: 'pb_news_contact_manual',
  //   url: `${config.env}/node/456`,
  //   referenceUrl:`${refURL}/node/456`,
  //   delay: 3000,
  //   selectorExpansion: true,
  //   expect: 0,
  //   misMatchThreshold: 0.1,
  //   requireSameDimensions: true,
  // },
  // {
  //   // PB: News Contact (Tag)
  //   label: 'pb_news_contact_tag',
  //   url: `${config.env}/node/451`,
  //   referenceUrl:`${refURL}/node/451`,
  //   delay: 3000,
  //   selectorExpansion: true,
  //   expect: 0,
  //   misMatchThreshold: 0.1,
  //   requireSameDimensions: true,
  // },
  {
    // Element: Marketing Header
    label: 'el_marketing_header',
    url: `${config.env}`,
    referenceUrl:`${refURL}`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  {
    // Element: Marketing Header Menu
    label: 'el_marketing_header_menu',
    url: `${config.env}`,
    referenceUrl:`${refURL}`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
    clickSelector: '.header-container__nav-button',
    postInteractionWait: 10000,
  },
  {
    // Element: Inside Header
    label: 'el_inside_header',
    url: `${config.env}/node/11`,
    referenceUrl:`${refURL}/node/11`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
    postInteractionWait: 10000,
  },
  {
    // Element: Inside Header Dropdown
    label: 'el_inside_header_menu',
    url: `${config.env}/node/11`,
    referenceUrl:`${refURL}/node/11`,
    delay: 4000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
    hoverSelector: '.block-bmc-custom-header-container-inside .menu--inside>ul>li',
    postInteractionWait: 10000,
  },
  {
    // Element: FAB Mobile open
    label: 'el_fab_open',
    url: `${config.env}`,
    referenceUrl:`${refURL}`,
    delay: 4000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
    clickSelector: '.fab__toggle',
    postInteractionWait: 10000,
    viewports: [
      {
        label: 'phone',
        width: 320,
        height: 480,
      },
      {
        label: 'tablet-portrait',
        width: 700,
        height: 1024,
      },
      {
        label: 'tablet-landscape',
        width: 1024,
        height: 768,
      },
    ]
  },
  {
    // Element: postbac footer
    label: 'el_postbac_footer',
    url: `${config.env}/node/776`,
    referenceUrl:`${refURL}/node/776`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
    postInteractionWait: 10000,
  },
  {
    // Element: socialw footer
    label: 'el_socialw_footer',
    url: `${config.env}/node/766`,
    referenceUrl:`${refURL}/node/776`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
    postInteractionWait: 10000,
  },
  {
    // Element: gsas footer
    label: 'el_gsas_footer',
    url: `${config.env}/node/771`,
    referenceUrl:`${refURL}/node/771`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
    postInteractionWait: 10000,
  },
  {
    // Element: gsas footer
    label: 'el_gsas_footer',
    url: `${config.env}/node/771`,
    referenceUrl:`${refURL}/node/771`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
    postInteractionWait: 10000,
  },
  {
    // Element: section nav
    label: 'el_section_nav',
    url: `${config.env}/node/7426`,
    referenceUrl:`${refURL}/node/7426`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
    postInteractionWait: 10000,
  },
  {
    // Element: sec nav open
    label: 'el_section_nav_open',
    url: `${config.env}/node/7426`,
    referenceUrl:`${refURL}/node/7426`,
    delay: 4000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
    clickSelector: '.section-nav__toggle',
    postInteractionWait: 10000,
  },
  {
    // Office List
    label: 'ct_office_list',
    url: `${config.env}/node/216`,
    referenceUrl:`${refURL}/node/216`,
    delay: 4000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
    postInteractionWait: 10000,
  },
  {
    // Search: GSAS
    label: 'el_search_gsas',
    url: `${config.env}/node/801?cx=92fe312c256e380b0&cof=&keys=academics&op=Search&form_build_id=form-XBEPzbHUxeif8tnqerfi5Sqbgyjy5A29rh36KDx2Das&form_id=google_cse_search_box_form`,
    referenceUrl:`${refURL}/node/801?cx=92fe312c256e380b0&cof=&keys=academics&op=Search&form_build_id=form-XBEPzbHUxeif8tnqerfi5Sqbgyjy5A29rh36KDx2Das&form_id=google_cse_search_box_form`,
    delay: 4000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  {
    // Search: Main
    label: 'el_search_main',
    url: `${config.env}/node/791?cx=92fe312c256e380b0&cof=&keys=academics&op=Search&form_build_id=form-XBEPzbHUxeif8tnqerfi5Sqbgyjy5A29rh36KDx2Das&form_id=google_cse_search_box_form`,
    referenceUrl:`${refURL}/node/791?cx=92fe312c256e380b0&cof=&keys=academics&op=Search&form_build_id=form-XBEPzbHUxeif8tnqerfi5Sqbgyjy5A29rh36KDx2Das&form_id=google_cse_search_box_form`,
    delay: 4000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  {
    // Search: PostBac
    label: 'el_search_postbac',
    url: `${config.env}/node/811?cx=92fe312c256e380b0&cof=&keys=academics&op=Search&form_build_id=form-XBEPzbHUxeif8tnqerfi5Sqbgyjy5A29rh36KDx2Das&form_id=google_cse_search_box_form`,
    referenceUrl:`${refURL}/node/811?cx=92fe312c256e380b0&cof=&keys=academics&op=Search&form_build_id=form-XBEPzbHUxeif8tnqerfi5Sqbgyjy5A29rh36KDx2Das&form_id=google_cse_search_box_form`,
    delay: 4000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  {
    // Search: socialwork
    label: 'el_search_socialwork',
    url: `${config.env}/node/806?cx=92fe312c256e380b0&cof=&keys=academics&op=Search&form_build_id=form-XBEPzbHUxeif8tnqerfi5Sqbgyjy5A29rh36KDx2Das&form_id=google_cse_search_box_form`,
    referenceUrl:`${refURL}/node/806?cx=92fe312c256e380b0&cof=&keys=academics&op=Search&form_build_id=form-XBEPzbHUxeif8tnqerfi5Sqbgyjy5A29rh36KDx2Das&form_id=google_cse_search_box_form`,
    delay: 4000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  {
    // Search: Inside BMC
    label: 'el_search_inside',
    url: `${config.env}/node/796?cx=92fe312c256e380b0&cof=&keys=academics&op=Search&form_build_id=form-XBEPzbHUxeif8tnqerfi5Sqbgyjy5A29rh36KDx2Das&form_id=google_cse_search_box_form`,
    referenceUrl:`${refURL}/node/796?cx=92fe312c256e380b0&cof=&keys=academics&op=Search&form_build_id=form-XBEPzbHUxeif8tnqerfi5Sqbgyjy5A29rh36KDx2Das&form_id=google_cse_search_box_form`,
    delay: 4000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  {
    // Bulletin Past Issues
    label: 'lp_bulletin_archive',
    url: `${config.env}/bulletin/archive`,
    referenceUrl:`${refURL}/bulletin/archive`,
    delay: 300,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  {
    // Program Finder
    label: 'lp_program_finder',
    url: `${config.env}/node/4981`,
    referenceUrl:`${refURL}/node/4981`,
    delay: 4000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  {
    // Homepage
    label: 'ct_home',
    url: `${config.env}`,
    referenceUrl:`${refURL}`,
    delay: 4000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },

  {
    // Story: Standard
    label: 'ct_story_standard',
    url: `${config.env}/node/53241`,
    referenceUrl:`${refURL}/node/53241`,
    delay: 4000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  {
    // Story: Featured
    label: 'ct_story_feat',
    url: `${config.env}/node/53906`,
    referenceUrl:`${refURL}/node/53906`,
    delay: 4000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  {
    // Page: Current Students
    label: 'mp_current_students',
    url: `${config.env}/node/5206`,
    referenceUrl:`${refURL}/node/5206`,
    delay: 4000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  {
  // Event listing page
  label: 'events_listing',
  url: `${config.env}/node/831`,
  referenceUrl:`${refURL}/node/831`,
  delay: 3000,
  selectorExpansion: true,
  expect: 0,
  misMatchThreshold: 0.1,
  requireSameDimensions: true,
},
  {
    // Tagged Event listing page
    label: 'events_listing_tagged',
    url: `${config.env}/node/846?tags=4046`,
    referenceUrl:`${refURL}/node/846?tags=4046`,
    delay: 3000,
    selectorExpansion: true,
    expect: 0,
    misMatchThreshold: 0.1,
    requireSameDimensions: true,
  },
  // {
  //   // Course Page
  //   label: 'ct_course',
  //   url: `${config.env}/node/5196`,
  //   referenceUrl:`${refURL}/node/5196`,
  //   delay: 4000,
  //   selectorExpansion: true,
  //   expect: 0,
  //   misMatchThreshold: 0.1,
  //   requireSameDimensions: true,
  // },
];

module.exports = pathConfig;
