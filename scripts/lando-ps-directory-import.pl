#!/usr/bin/perl

# Online Directory MySQL Loader
# Andrew Lacey 7/16/2009
# This script loads the contents of four text files (person.txt, student.txt, department.txt, subdepartment.txt) into the "directory" MySQL database

# NOTE: Some calls have been commented out, because these feeds are not currently used in Drupal.
# -AL 4/2/2015

use strict;
use DBI;

# Define the field lists for each table:
my @personfields = ("dcode", "scode", "lastname", "firstname", "middlename", "jobtitle", "building", "phone", "room", "building2", "phone2", "room2", "email", "status", "onleave", "labnumber", "id", "rank", "jobcodetitle", "deptphone", "empl_active", "term_active", "homepageurl", "person_type");
my @deptfields = ("code", "name", "phone", "fax", "email", "building", "crossref", "comments", "isprimary", "homepageurl", "chair", "cochair", "intchair1", "intchair2", "actchair1", "actchair2");
my @subdeptfields = ("dcode", "code", "name", "phone", "fax", "email", "comments");

my $dbconn;

$dbconn = DBI->connect("dbi:mysql:main_db:database", "main_user", "main_pass");

clear_table("ps_person", $dbconn);
load_table("/app/psfeeds/person_new.txt", "ps_person", \@personfields, $dbconn);

clear_table("ps_department", $dbconn);
load_table("/app/psfeeds/department.txt", "ps_department", \@deptfields, $dbconn);

clear_table("ps_subdepartment", $dbconn);
load_table("/app/psfeeds/subdepartment.txt", "ps_subdepartment", \@subdeptfields, $dbconn);

sub clear_table {

    my $tablename = shift;
    my $mysqlconn = shift;
    my $query;
    my $statement;

    $query = "TRUNCATE TABLE $tablename";

    $statement = $mysqlconn->prepare($query);
    $statement->execute();

}

sub load_table {

    my $filename = shift;
    my $tablename = shift;
    my $fieldlist = shift;
    my $mysqlconn = shift;
    my @fieldarr = @$fieldlist; # access array from array reference
    my $linein;
    my @lineparts;
    my $x;
    my $fieldstr;
    my $valuestr;
    my $query;
    my $statement;

    system("dos2unix $filename"); # switch newlines from Windows to UNIX (otherwise you get a stray CR at the end of the last field)

    open(INFILE, $filename);

    while(<INFILE>) {

	$fieldstr = "";
	$valuestr = "";

	$linein = $_;
	chomp($linein);

	# IMPORTANT: The third argument (-1) preserves empty trailing fields and is necessary!
	# If it's omitted, NULLs will be inserted into the database for such fields, which breaks queries in the PHP modules.
	@lineparts = split('\t', $linein, -1);

	# Build lists of fields and values for query:
	for($x = 0; $x < @fieldarr; $x ++) {

	    $fieldstr = $fieldstr . $fieldarr[$x];

	    # "Blank" fields should be empty. The old code (Mysql module rather than DBI module) evidently did this for us.
	    if ($lineparts[$x] eq " ") {

		$lineparts[$x] = "";

	    }

	    $valuestr = $valuestr . $mysqlconn->quote($lineparts[$x]);

	    # Insert comma delimiters where necessary:
	    if ($x < (@fieldarr - 1)) {

		$fieldstr = $fieldstr . ", ";
		$valuestr = $valuestr . ", ";

	    }

	}

	$query = "INSERT INTO $tablename ($fieldstr) VALUES ($valuestr)";

	$statement = $mysqlconn->prepare($query);
	$statement->execute();

    }

    close(INFILE);

}

print "PeopleSoft Directory data (ps-directory-import.pl) successfully imported to the Drupal 9 database.\n";

#********************************************************************************************#
