#!/usr/bin/perl

# Academic Plan MySQL Loader
# Andrew Lacey 1/25/2011
# This script loads the contents of academicplan.txt into the "courses" MySQL database

use strict;
use DBI;

# Define the field lists for each table:
my @planfields = ("dcode", "dname", "description");

my $dbconn;

$dbconn = DBI->connect("dbi:mysql:1c1ec7ec0aae439a9801a70440661daf:db-1c1ec7ec0aae439a9801a70440661daf.cdb.database.services.acquia.io", "uYSRhqtxbf1j7xu4", "Ns8rMSiuSbvXNiIpU69QWuUROQq0AOCRMgCbEXneb");
print("Database connection successful\n") if defined $dbconn;

clear_table("ps_academicplan", $dbconn);
load_table("/home/clouduser/psfeeds/academicplan.txt", "ps_academicplan", \@planfields, $dbconn);

#********************************************************************************************#

# clear_table($tablename, $mysqlconn);

sub clear_table {

    my $tablename = shift;
    my $mysqlconn = shift;
    my $query;
    my $statement;

    $query = "TRUNCATE TABLE $tablename";

    $statement = $mysqlconn->prepare($query);
    $statement->execute();

}

#********************************************************************************************#

# load_table($filename, $tablename, \@fieldlist, $mysqlconn)

sub load_table {

    my $filename = shift;
    my $tablename = shift;
    my $fieldlist = shift;
    my $mysqlconn = shift;
    my @fieldarr = @$fieldlist; # access array from array reference
    my $linein;
    my @lineparts;
    my $x;
    my $fieldstr;
    my $valuestr;
    my $query;
    my $statement;

    # substitute ASCII characters for WINDOWS-1252 characters
    # DEC 145 146 147 148 are Windows smart-quotes
    # DEC 150 is Windows endash
    # DEC 151 is Windows emdash
    # These do not convert properly to UTF-8, so they must be substituted out first
    system("sed -i -e \'s/\\d145/\'\\\'\'/\g' $filename");
    system("sed -i -e \'s/\\d146/\'\\\'\'/\g' $filename");
    system("sed -i -e \'s/\\d147/\'\\\"\'/\g' $filename");
    system("sed -i -e \'s/\\d148/\'\\\"\'/\g' $filename");
    system("sed -i -e \'s/\\d150/-/\g' $filename");
    system("sed -i -e \'s/\\d151/--/\g' $filename");

    system("dos2unix $filename"); # switch newlines from Windows to UNIX (otherwise you get stray ^M characters)

    $/ = "~"; # record separator

    open(INFILE, $filename);

    while(<INFILE>) {

	$fieldstr = "";
	$valuestr = "";

	$linein = $_;
	chomp($linein);

	@lineparts = split('%%%%', $linein); # Field separator changed from TAB to '%%%%' due to tabs in data. 6/9/2017 AL

	$lineparts[0] =~ s/\n//g; # remove stray newlines that exist because we're using ~ as the record separator
	$lineparts[1] =~ s/\n//g;

	# Build lists of fields and values for query:
	for($x = 0; $x < @fieldarr; $x ++) {

	    $fieldstr = $fieldstr . $fieldarr[$x];

	    # "Blank" fields should be empty. The old code (Mysql module rather than DBI module) evidently did this for us.
	    if ($lineparts[$x] eq " ") {

		$lineparts[$x] = "";

	    }

	    $valuestr = $valuestr . $mysqlconn->quote($lineparts[$x]);

	    # Insert comma delimiters where necessary:
	    if ($x < (@fieldarr - 1)) {

		$fieldstr = $fieldstr . ", ";
		$valuestr = $valuestr . ", ";

	    }

	}

	$query = "INSERT INTO $tablename ($fieldstr) VALUES ($valuestr)";

	$statement = $mysqlconn->prepare($query);
	$statement->execute();

    }

    close(INFILE);

}

print "PeopleSoft academic plan data (ps-academicplan-import.pl) successfully imported to the Drupal 9 database.\n";

#********************************************************************************************#
