#!/usr/bin/perl

# Course Listings MySQL Loader
# Andrew Lacey 8/21/2009
# This script loads the contents of two text files (course2.txt, classcomponent.txt) into the "ps_course" MySQL database

use strict;
use DBI;

# Define the field lists for each table:
my @course2fields = ("ID", "subjectareacode", "subjectareadesc", "catalognumber", "longtitle", "description", "divrequirement", "approach1", "approach2", "approach3", "majorattr", "crosslist1", "crosslist2", "crosslist3", "crosslist4", "conccode1", "concdesc1", "conccode2", "concdesc2", "conccode3", "concdesc3", "conccode4", "concdesc4", "units", "url", "homedepartment");

my @classcomponentfields = ("courseID", "term", "classnumber", "topic", "section", "comp_start1", "comp_end1", "comp_days1", "comp_facility1", "comp_desc1", "comp_start2", "comp_end2", "comp_days2", "comp_facility2", "comp_desc2", "comp_start3", "comp_end3", "comp_days3", "comp_facility3", "comp_desc3", "comp_start4", "comp_end4", "comp_days4", "comp_facility4", "comp_desc4", "comp_start5", "comp_end5", "comp_days5", "comp_facility5", "comp_desc5", "comp_start6", "comp_end6", "comp_days6", "comp_facility6", "comp_desc6", "instructor", "quarters", "mode_of_instruction");

my $dbconn;

$dbconn = DBI->connect("dbi:mysql:1c1ec7ec0aae439a9801a70440661daf:db-1c1ec7ec0aae439a9801a70440661daf.cdb.database.services.acquia.io", "uYSRhqtxbf1j7xu4", "Ns8rMSiuSbvXNiIpU69QWuUROQq0AOCRMgCbEXneb");
print("Database connection successful\n") if defined $dbconn;
clear_table("ps_course", $dbconn);
load_table("/home/clouduser/psfeeds/course2.txt", "ps_course", \@course2fields, $dbconn);

clear_table("ps_classcomponent", $dbconn);
load_table("/home/clouduser/psfeeds/classcomponent.txt", "ps_classcomponent", \@classcomponentfields, $dbconn);

#********************************************************************************************#

# clear_table($tablename, $mysqlconn);

sub clear_table {

    my $tablename = shift;
    my $mysqlconn = shift;
    my $query;
    my $statement;

    $query = "TRUNCATE TABLE $tablename";

    $statement = $mysqlconn->prepare($query);
    $statement->execute();

}

#********************************************************************************************#

# load_table($filename, $tablename, \@fieldlist, $mysqlconn)

sub load_table {

    my $filename = shift;
    my $tablename = shift;
    my $fieldlist = shift;
    my $mysqlconn = shift;
    my @fieldarr = @$fieldlist; # access array from array reference
    my $linein;
    my @lineparts;
    my $x;
    my $fieldstr;
    my $valuestr;
    my $query;
    my $statement;

    # substitute ASCII characters for WINDOWS-1252 characters
    # DEC 145 146 147 148 are Windows smart-quotes
    # DEC 150 is Windows endash
    # DEC 151 is Windows emdash
    # These do not convert properly to UTF-8, so they must be substituted out first
    system("sed -i -e \'s/\\d145/\'\\\'\'/\g' $filename");
    system("sed -i -e \'s/\\d146/\'\\\'\'/\g' $filename");
    system("sed -i -e \'s/\\d147/\'\\\"\'/\g' $filename");
    system("sed -i -e \'s/\\d148/\'\\\"\'/\g' $filename");
    system("sed -i -e \'s/\\d150/-/\g' $filename");
    system("sed -i -e \'s/\\d151/--/\g' $filename");

    system("dos2unix $filename"); # switch newlines from Windows to UNIX (otherwise you get a stray CR at the end of the last field)

    open(INFILE, $filename);

    while(<INFILE>) {

	$fieldstr = "";
	$valuestr = "";

	$linein = $_;
	chomp($linein);

	@lineparts = split('\t', $linein);

	# Build lists of fields and values for query:
	for($x = 0; $x < @fieldarr; $x ++) {

	    $fieldstr = $fieldstr . $fieldarr[$x];

	    # "Blank" fields should be empty. The old code (Mysql module rather than DBI module) evidently did this for us.
	    if ($lineparts[$x] eq " ") {

		$lineparts[$x] = "";

	    }

	    $valuestr = $valuestr . $mysqlconn->quote($lineparts[$x]);

	    # Insert comma delimiters where necessary:
	    if ($x < (@fieldarr - 1)) {

		$fieldstr = $fieldstr . ", ";
		$valuestr = $valuestr . ", ";

	    }

	}

	$query = "INSERT INTO $tablename ($fieldstr) VALUES ($valuestr)";

	$statement = $mysqlconn->prepare($query);
	$statement->execute();

    }

    close(INFILE);

}

print "PeopleSoft course data (ps-course-import.pl) successfully imported to the Drupal 9 database.\n";

#********************************************************************************************#
