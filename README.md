CONTENTS OF THIS FILE
---------------------
* Environments
* Setting up a Local Development Environment
* Version Control
* Development & Deployment Strategy
* Migrations

## Drush Aliases
You will find aliases for all known (and published) environments if
you use `drush sa` within the codebase.

# Setting up a Local Development Environment
Install Lando from https://github.com/lando/lando/releases.
Use the `lando start` command.
Put your database credentials in settings.local.php. Use stage_file_proxy to
automatically use files from the production server without having to sync them.
You should be able to `drush en stage_file_proxy` or enable through the admin UI.
Just make sure you don't commit `config/sync/default/stage_file_proxy.settings.yml`
or the corresponding line in `config/sync/default/core.extension.yml`.
We aren't using config split or similar on this site.

# Version Control
The repo for this website lives in Bitbucket (origin) and Acquia.
Here's a sample from the .git/config file:
```
[remote "origin"]
	url = git@bitbucket.org:bmc-web/bmcd9.git
        fetch = +refs/heads/*:refs/remotes/origin/*
[remote "acquia"]
	url = bmcd9@svn-21939.prod.hosting.acquia.com:bmcd9.git
        fetch = +refs/heads/*:refs/remotes/acquia/*
[remote "all"]
 	url = bmcd9@svn-21939.prod.hosting.acquia.com:bmcd9.git
 	url = git@bitbucket.org:bmc-web/bmcd9.git
```

# Development & Deployment Strategy
## Overview
Developers must work locally in feature branches that are based off the
`stage` branch.

After development of the feature is complete, developer must merge the feature
branch into the `dev` branch for integration testing on the dev site.
The developer must run the appropriate `drush` commands on the dev server.
For example, `drush cim`, `drush updb`, `drush cr`, `etc.`.

If testing goes well, the developer must merge the code into `stage` and again
run appropriate `drush` commands on the staging server.

When the lead developer is ready for production deployment they will merge
`stage` into `master` (via `git pull origin stage`), create a tag, and deploy
the tag. The lead developer should then run the appropriate `drush` commands on
the production server.

## Feature Branches
All of your work on a particular issue/task should happen in a single branch.
As stated above You should create this branch from the most recent code in stage.
The feature should be prefixed with feature/XXXX, where XXXX is the Teamwork
task number. For example:
```
git checkout stage
git pull origin stage
git checkout -b feature/30041277-create-image-media-type
```

## Staging Feature Branch Work for Next Deployment

1. Double check the work on the branches you want to merge in
2. `git checkout stage; git pull origin stage`
3. For each FB you want to deploy:
  1. `git checkout feature/FOOBAR`
  2. `git pull origin stage` (resolve any merge conflicts; if any, consider retesting in dev / stage)
  3. `git checkout stage`
  4. `git merge --no-ff feature/FOOBAR`
  5. `git push all stage`
4. Optionally test in stage
  1. Log into acquia.com
  2. Take stage/test db backup
  3. Drag prod db down to stage
  4. `drush cim` and `drush cr` and `drush updb` as needed
5. Move to _Staged_ column in ticket system

## Deploying Staged Work to Production

1. `git checkout master; git pull origin master`
2. `git pull origin stage`
3. `git tag -a "2019-06-11" -m "2021-06-11"`
4. `git push all master --tags`
5. Log into acquia.com
6. Take prod database backup
7. Switch code on prod environment; pick the new tag
8. `drush cim` and `drush cr` and `drush updb` as needed

## Rules and Best Practices
- You should only pull from `origin`
- You should push to Bitbucket and Acquia (note the `all` alias above)
- You should never overwrite the production database.
- You should always create your feature branches from the `stage` branch.
- You should merge your feature branch into the `dev` branch for testing.
- You should only merge ready-to-deploy code into the `stage` branch.
- With the exception of entering content, you should make all Drupal admin
  UI-based changes via code deployment (as configuration and/or update hooks)
  - If you manually update settings, configuration, etc. in any Acquia
    environment, your changes may be overwritten.
